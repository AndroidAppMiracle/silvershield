package com.silvershield.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.AccRejPenAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.responseModels.GetAlertLogResponseModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AcceptRejectPendingListingActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.alert_user_rv)
    RecyclerView alert_user_rv;
    @BindView(R.id.title_tv)
    Custome_Regular_TextView title_tv;
    String statusSt = "", dispatch_id = "";
    @BindView(R.id.home_tv)
    TextView home_tv;
    private GetAlertLogResponseModel model;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_reject_pending_listing);
        ButterKnife.bind(this);
        if (showAlertIcon())
            home_tv.setVisibility(View.VISIBLE);
        else
            home_tv.setVisibility(View.GONE);
        home_tv.setVisibility(View.GONE);
        title_tv.setText("Alert Logs");
        bundle = getIntent().getExtras();
        if (bundle != null) {
            model = bundle.getParcelable("model");
            statusSt = bundle.getString("status");
        }
        if (statusSt.equalsIgnoreCase("ACCEPT")) {
            title_tv.setText("Accepeted Notifications");
        } else if (statusSt.equalsIgnoreCase("REJECT")) {
            title_tv.setText("Rejected Notifications");
        } else if (statusSt.equalsIgnoreCase("PENDING")) {
            title_tv.setText("Pending Notifications");
        }
        if (model != null) {
            dispatch_id = model.getDispatch_id();
            if (!dispatch_id.isEmpty()) {
                getAllList();
            }
        }
    }

    @OnClick(R.id.cross)
    public void closePage() {
        AcceptRejectPendingListingActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AcceptRejectPendingListingActivity.this.finish();
    }

    public void getAllList() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllListARP(APIServerResponse.GET_ACC_REJ_PEND, getAuthToken(), dispatch_id, statusSt, AcceptRejectPendingListingActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case GET_ACC_REJ_PEND:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("Ok")) {
                            AccRejPenAdapter accRejPenAdapter = new AccRejPenAdapter(getApplicationContext(), jsonObject);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                            alert_user_rv.setLayoutManager(linearLayoutManager);
                            alert_user_rv.setAdapter(accRejPenAdapter);
                            accRejPenAdapter.notifyDataSetChanged();
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
