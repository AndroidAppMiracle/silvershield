package com.silvershield.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonObject;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.silvershield.R;
import com.silvershield.adapter.CountryNamesAdapter;
import com.silvershield.adapter.DepartmentsAdapter;
import com.silvershield.adapter.EntryFromAdapter;
import com.silvershield.adapter.PurposeAdapter;
import com.silvershield.adapter.SuffixAdapter;
import com.silvershield.adapter.VisitIndividualAdapter;
import com.silvershield.adapter.VisitorDocTypeAdapter;
import com.silvershield.custome_controls.Custome_Bold_TextView;
import com.silvershield.dialog_fragment.SuggestionDialogFragment;
import com.silvershield.interfaces.SuggestionDialogClick;
import com.silvershield.modal.Countries;
import com.silvershield.modal.Departments;
import com.silvershield.modal.Docs_type;
import com.silvershield.modal.Entry_from;
import com.silvershield.modal.GetSuffixDoctypeResponse;
import com.silvershield.modal.Individuals;
import com.silvershield.modal.Matches;
import com.silvershield.modal.Offenders;
import com.silvershield.modal.Purpose;
import com.silvershield.modal.SOResponse;
import com.silvershield.modal.Suffix;
import com.silvershield.modal.SuggesionResponse;
import com.silvershield.modal.WatchListCheck;
import com.silvershield.modal.WatchListMatchModal;
import com.silvershield.modal.requestModels.CheckWatchListRequestModel;
import com.silvershield.modal.requestModels.CreateVisitorRequestModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.io.File;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class AddNewVisitorActivity extends BaseActivity implements APIServerResponse, ImagePickerCallback, EasyPermissions.PermissionCallbacks, SuggestionDialogClick {


    private static final int PLACE_PICKER_REQUEST = 1;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int RC_CAMERA_PERM = 342;
    public static String suffixId, docTypeId, visitor_id_number, visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id, vms_visitor_purpose_id, vms_visitor_other_purpose,
            country_idSt, matchId = "0", checkout = "";
    static boolean watchFlag = false, soFlag = false, cbFlag = false;
    private final int requestCode = 20;
    public String profileUrl = "";
    public ImageView profile_img;
    public int responseCode = 101;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.etDOB)
    EditText etDOB;
    SimpleDateFormat format;
    Dialog dialog;
    @BindView(R.id.alert_img)
    ImageView alert_img;
    @BindView(R.id.suffix_choose_et)
    EditText suffix_choose_et;
    @BindView(R.id.doct_type)
    EditText visitor_dept_id_et;
    @BindView(R.id.visitor_id_number)
    EditText visitor_id_number_et;
    @BindView(R.id.country_origin)
    EditText country_origin_et;
    @BindView(R.id.timing_et)
    EditText timing_et;
    @BindView(R.id.visiting_dept_et)
    EditText visiting_dept_et;
    @BindView(R.id.entry_form_et)
    EditText entery_form_et;
    @BindView(R.id.purpose_of_visit)
    EditText purpose_of_visit_et;
    @BindView(R.id.email_et)
    EditText email_et;
    @BindView(R.id.ph_number_et)
    EditText ph_number_et;
    @BindView(R.id.first_name_et)
    EditText first_name_et;
    //last_name_et
    @BindView(R.id.last_name_et)
    EditText last_name_et;
    @BindView(R.id.mid_name_et)
    EditText mid_name_et;
    @BindView(R.id.local_code_number)
    TextView local_code_number;
    @BindView(R.id.submit_btn)
    Button submit_btn;
    @BindView(R.id.proceed_btn)
    Button proceed_btn;
    @BindView(R.id.three_btn_ll)
    LinearLayout three_btn_ll;
    //three_btn_ll
    @BindView(R.id.outer_proceed)
    Button outer_proceed;
    @BindView(R.id.welcome_txt)
    Custome_Bold_TextView welcome_txt;
    String finalDate, welcomeUserName = "";
    AlertDialog.Builder builder;
    SuffixAdapter suffixAdapter;
    List<Suffix> suffixList;
    List<Docs_type> docs_types;
    List<Entry_from> entry_from;
    List<Departments> departmentsList;
    List<Countries> countriesList;
    @BindView(R.id.txt_search)
    AutoCompleteTextView txtSearch;
    List<Individuals> individualsList;
    VisitIndividualAdapter adapter;
    List<Purpose> purposesList;
    RecyclerView suggestion_user_rv;
    List<Matches> matchesList;
    Bitmap profileBitMap;
    String checkin = "";
    RequestOptions requestOptions = new RequestOptions();
    boolean dialogShown;
    Calendar calendar = Calendar.getInstance();
    CheckWatchListRequestModel requestModel;
    private String pickerPath = "";
    private CreateVisitorRequestModel visitorRequestModel;
    private CameraImagePicker cameraPicker;
    private boolean showProccedBtn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_visitor);
        ButterKnife.bind(this);
        if (showAlertIcon())
            alert_img.setVisibility(View.VISIBLE);
        else
            alert_img.setVisibility(View.GONE);
        logout_btn.setVisibility(View.GONE);
        getAllDetail();
        initVariable();
        showLoading();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                settingIntent();
                hideLoading();
            }
        }, 3000);

        visitor_dept_id_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // if (visitor_dept_id_et.getText().toString().equalsIgnoreCase("No ID Available")) {
                if (visitor_dept_id_et.getText().toString().equalsIgnoreCase("No ID Available")) {
                    visitor_id_number_et.setText("N/A");
                    visitor_id_number_et.setEnabled(false);

                } else {
                    visitor_id_number_et.setText("");
                    visitor_id_number_et.setEnabled(true);
                }
                // Handler handler = new HandleSimpler();
                try {
                    if (docs_types.size() != 0) {
                        for (int i = 0; i < docs_types.size(); i++) {
                            if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                                docTypeId = docs_types.get(i).getId();
                            } else if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                                docTypeId = docs_types.get(i).getId();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    showProceedBtn(true);
                }
            }
        });
        initComponents();
    }

    private void initComponents() {
        String countryCide = getCountryCode();
        Log.d("countryCode", countryCide);
        local_code_number.setText(getCountryCode());
        visitorRequestModel = new CreateVisitorRequestModel();
        first_name_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etDOB.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        email_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        last_name_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mid_name_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        country_origin_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        entery_form_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ph_number_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        purpose_of_visit_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        suffix_choose_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        timing_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        visiting_dept_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        visitor_id_number_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showProceedBtn(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void showProceedBtn(boolean flag) {
        if (flag) {
            proceed_btn.setVisibility(View.GONE);
            submit_btn.setVisibility(View.VISIBLE);
        } else {
            proceed_btn.setVisibility(View.VISIBLE);
            submit_btn.setVisibility(View.GONE);
        }
    }

    public void initVariable() {
        watchFlag = false;
        soFlag = false;
        cbFlag = false;
        suffixId = "";
        docTypeId = "";
        visitor_id_number = "";
        visiting_individual_id = "";
        visiting_department_id = "";
        vms_visitor_entering_from_id = "";
        vms_visitor_purpose_id = "";
        vms_visitor_other_purpose = "";
        country_idSt = "";
        profile_img = (ImageView) findViewById(R.id.profile_img);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        title_tv.setText("Add New Visitor");
        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
            }
        });

        requestOptions.placeholder(R.drawable.dummy_users);
        requestOptions.error(R.drawable.dummy_users);
        format = new SimpleDateFormat("HH:mm a", Locale.getDefault());
        String myDate = format.format(new Date());
        timing_et.setText(myDate);

    }

    public void settingIntent() {
        if (getIntent().hasExtra("firstName")) {
            first_name_et.setText(getIntent().getStringExtra("firstName"));
        }
        if (getIntent().hasExtra("middleName")) {
            mid_name_et.setText(getIntent().getStringExtra("middleName"));
        }
        if (getIntent().hasExtra("lastName")) {
            last_name_et.setText(getIntent().getStringExtra("lastName"));
        }
        if (getIntent().hasExtra("email")) {
            email_et.setText(getIntent().getStringExtra("email"));
        }
        if (getIntent().hasExtra("dob")) {
            etDOB.setText(getIntent().getStringExtra("dob"));
        }
        if (getIntent().hasExtra("driving_lisence_yes_no")) {
            visitor_dept_id_et.setText("Driver's License number");
        }
        if (getIntent().hasExtra("visitor_id_number")) {
            visitor_id_number_et.setText(getIntent().getStringExtra("visitor_id_number"));
            downloadDLImage(getIntent().getStringExtra("visitor_id_number"));
        }
        if (getIntent().hasExtra("checkin")) {
            /*   visitor_dept_id_et.setText("Driver's License number");*/
            checkin = getIntent().getStringExtra("checkin");
        }
        if (getIntent().hasExtra("badge_number")) {
            matchId = getIntent().getStringExtra("badge_number");
        }
        if (getIntent().hasExtra("result")) {
            String fullName = "";
            if (getIntent().hasExtra("firstName")) {
                fullName = getIntent().getStringExtra("firstName");
            }
            if (getIntent().hasExtra("middleName")) {
                fullName += " " + getIntent().getStringExtra("middleName");
            }
            if (getIntent().hasExtra("lastName")) {
                fullName += " " + getIntent().getStringExtra("lastName");
            }
            welcome_txt.setText("Welcome Back:  " + fullName);
        }

        String profile_pic = getIntent().getStringExtra("profile_pic");
        if (profile_pic != null && !profile_pic.isEmpty())
            Glide.with(this).setDefaultRequestOptions(requestOptions).load(getIntent().getStringExtra("profile_pic")).into(profile_img);
        if (getIntent().hasExtra("visitor_doc_name")) {
            visitor_dept_id_et.setText(getIntent().getStringExtra("visitor_doc_name"));
            if (visitor_dept_id_et.getText().toString().equalsIgnoreCase("No ID Available")) {
                visitor_id_number_et.setText("N/A");
                visitor_dept_id_et.setHintTextColor(getResources().getColor(R.color.hint));
                visitor_id_number_et.setHintTextColor(getResources().getColor(R.color.hint));
                visitor_id_number_et.setEnabled(false);
            } else {
                visitor_id_number_et.setText("");
                visitor_id_number_et.setHintTextColor(getResources().getColor(R.color.Red));
                visitor_id_number_et.setHintTextColor(getResources().getColor(R.color.Red));
                visitor_id_number_et.setEnabled(true);
            }
            if (getIntent().hasExtra("result")) {
                if (getIntent().hasExtra("badge_number")) {
                    visitor_id_number_et.setEnabled(true);
                    visitor_dept_id_et.setText("Quick Badge");
                    visitor_id_number_et.setText("" + getIntent().getStringExtra("badge_number"));
                }
            }
        }

        try {
            if (docs_types.size() != 0) {
                for (int i = 0; i < docs_types.size(); i++) {
                    if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                        docTypeId = docs_types.get(i).getId();
                    } else if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                        docTypeId = docs_types.get(i).getId();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @OnClick(R.id.submit_btn)
    public void submitClick() {
        if (first_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill First name", Toast.LENGTH_SHORT).show();
        } else if (last_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill Last name", Toast.LENGTH_SHORT).show();
        } else if (etDOB.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill dob name", Toast.LENGTH_SHORT).show();
        } else if (visitor_dept_id_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select ID type ", Toast.LENGTH_SHORT).show();
        } else if (visitor_id_number_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please enter valid ID Number ", Toast.LENGTH_SHORT).show();
        } else if (entery_form_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select entry ", Toast.LENGTH_SHORT).show();
        } else if (ph_number_et.getText().toString().length() > 10) {
            Toast.makeText(getApplicationContext(), "Invalid phone number", Toast.LENGTH_SHORT).show();
        } else {
            downloadImage("WL");
        }
    }

    public void downloadImage(final String checkType) {
        showLoading();
        if (checkType.equalsIgnoreCase("WL")) {
            checkWatchList();
        }
        hideLoading();
    }

    public void downloadDLImage(String dlVisitorId) {
        showLoading();
        ServerAPI.getInstance().getDlDetailImage(APIServerResponse.DOWNLOAD_FILE, getAuthToken(), dlVisitorId, getClientId(), getParentIdValue(), new APIServerResponse() {
            @Override
            public void onSuccess(int tag, Response response) {
                hideLoading();
                if (response.isSuccessful()) {
                    JsonObject jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                        profileUrl = jsonObject.get("profile_pic_web").getAsString();
                        if (!profileUrl.equalsIgnoreCase("")) {
                            Glide.with(AddNewVisitorActivity.this).setDefaultRequestOptions(requestOptions).load(profileUrl).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                    mProgress.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                    mProgress.setVisibility(View.GONE);
                                    return false;
                                }
                            }).thumbnail(0.1f).into(profile_img);
                        }
                        downloadImage("");
                    }
                }
            }

            @Override
            public void onError(int tag, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    public void checkWatchList() {
        if (isConnectedToInternet()) {
            showLoading();
            requestModel = new CheckWatchListRequestModel();
            requestModel.setFirst_name(first_name_et.getText().toString().trim());
            requestModel.setLast_name(last_name_et.getText().toString().trim());
            requestModel.setDob(etDOB.getText().toString().trim());
            requestModel.setClients_id(getClientId());
            ServerAPI.getInstance().checkWatchListExistence(APIServerResponse.CHECK_IN_WATCH_LIST, getAuthToken(), requestModel, AddNewVisitorActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.proceed_btn)
    public void proceedContinue() {
        if (vms_visitor_entering_from_id != null) {
            //   if (checkin.equalsIgnoreCase("")) {
            if (isConnectedToInternet()) {
                showLoading();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String myDate = format.format(new Date());

                String dobString = etDOB.getText().toString();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                Date myDateNew = null;
                try {
                    myDateNew = dateFormat.parse(dobString);
                    SimpleDateFormat timeFormat = new SimpleDateFormat("MMM-dd-yyyy");
                    finalDate = dateFormat.format(myDateNew);
                    System.out.println(finalDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                ServerAPI.getInstance().createVisitor(APIServerResponse.CREATE_VISITOR,
                        getAuthToken(), first_name_et.getText().toString(), last_name_et.getText().toString(),
                        mid_name_et.getText().toString(),
                        suffix_choose_et.getText().toString(), email_et.getText().toString(),
                        local_code_number.getText().toString().trim() + ph_number_et.getText().toString(),
                        finalDate,
                        getUserID(), getParentIdValue(), docTypeId, visitor_id_number_et.getText().toString().trim(), localToGMT(myDate)
                        , getUserID(), visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id,
                        vms_visitor_purpose_id, "", pickerPath, getClientId(), "", "",
                        "", country_idSt, "0", getUserID(), matchId, AddNewVisitorActivity.this);

            } else {
                showSnack(Constants.INTERNET_CONNECTION);
            }
        } else {
            showSnack("Please select entry from..");
        }
    }

    @OnClick(R.id.outer_proceed)
    public void outProceedContinue() {
        if (checkin.equalsIgnoreCase("")) {
            if (isConnectedToInternet()) {
                showLoading();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String myDate = format.format(new Date());

                String dobString = etDOB.getText().toString();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                Date myDateNew = null;
                try {
                    myDateNew = dateFormat.parse(dobString);
                    SimpleDateFormat timeFormat = new SimpleDateFormat("MMM-dd-yyyy");
                    finalDate = timeFormat.format(myDateNew);
                    System.out.println(finalDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                ServerAPI.getInstance().createVisitor(APIServerResponse.CREATE_VISITOR,
                        getAuthToken(), first_name_et.getText().toString(), last_name_et.getText().toString(), mid_name_et.getText().toString(),
                        suffix_choose_et.getText().toString(), email_et.getText().toString(), local_code_number.getText().toString() + ph_number_et.getText().toString(),
                        finalDate,
                        getUserID(), getParentIdValue(), docTypeId, visitor_id_number_et.getText().toString(), localToGMT(myDate)
                        , getUserID(), visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id,
                        vms_visitor_purpose_id, "", pickerPath, getClientId(), "", "",
                        "", country_idSt, "0", getUserID(), matchId, AddNewVisitorActivity.this);

            } else {
                showSnack(Constants.INTERNET_CONNECTION);

            }
        } else {
            Toast.makeText(getApplicationContext(), "User already checkedin", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.criminal_btn)
    public void criminalBackgroundCheckBtn() {
        String dobString = etDOB.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(dobString);
            SimpleDateFormat timeFormat = new SimpleDateFormat("MMM-dd-yyyy");
            finalDate = timeFormat.format(myDate);
            System.out.println(finalDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (first_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill First name", Toast.LENGTH_SHORT).show();
        } else if (last_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill Last name", Toast.LENGTH_SHORT).show();
        } else if (etDOB.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill dob name", Toast.LENGTH_SHORT).show();
        } else if (visitor_dept_id_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select ID type ", Toast.LENGTH_SHORT).show();
        } else if (visitor_id_number_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please enter valid ID Number ", Toast.LENGTH_SHORT).show();
        } else if (entery_form_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select entry ", Toast.LENGTH_SHORT).show();
        } else if (ph_number_et.getText().toString().length() > 10) {
            Toast.makeText(getApplicationContext(), "Invalid phone number", Toast.LENGTH_SHORT).show();
        } else {

            builder = new AlertDialog.Builder(this);
            builder.setTitle("SilverShield");
            builder.setMessage(getResources().getString(R.string.add_new_user_alert_popup_string))
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().checkCB(APIServerResponse.CHECK_CB, getAuthToken(), first_name_et.getText().toString(), last_name_et.getText().toString(), etDOB.getText().toString(), email_et.getText().toString(), local_code_number.getText() + "" + ph_number_et.getText().toString(), getUserID(), profileUrl, getClientId(), getParentIdValue(), getUserID(), AddNewVisitorActivity.this);
                            } else {
                                showSnack(Constants.INTERNET_CONNECTION);
                            }
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @OnClick(R.id.entry_form_et)
    public void entryFromEt() {
        final Dialog dialog = new Dialog(AddNewVisitorActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
        EditText entery_form_et = (EditText) findViewById(R.id.entry_form_et);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        if (entry_from.size() != 0) {
            EntryFromAdapter suffixAdapter = new EntryFromAdapter(getApplicationContext(), dialog, entry_from, entery_form_et, vms_visitor_entering_from_id);
            cat_rv.setAdapter(suffixAdapter);
            suffixAdapter.notifyDataSetChanged();
        }

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.purpose_of_visit)
    public void pusposeOfVisit() {
        final Dialog dialog = new Dialog(AddNewVisitorActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        EditText editText = (EditText) findViewById(R.id.purpose_of_visit);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        if (departmentsList.size() != 0) {
            PurposeAdapter visitingDeptAdapter = new PurposeAdapter(getApplicationContext(), dialog, purposesList, editText, vms_visitor_purpose_id);
            cat_rv.setAdapter(visitingDeptAdapter);
            visitingDeptAdapter.notifyDataSetChanged();
        }

        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.suffix_choose_et)
    public void suffixChooseDialog() {
        final Dialog dialog = new Dialog(AddNewVisitorActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
        EditText suffix_choose_et = (EditText) findViewById(R.id.suffix_choose_et);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        try {
            if (suffixList != null) {
                if (suffixList.size() != 0) {
                    suffixAdapter = new SuffixAdapter(getApplicationContext(), dialog, suffixList, suffix_choose_et, suffixId);
                    cat_rv.setAdapter(suffixAdapter);
                    suffixAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
    }

    @OnClick(R.id.doct_type)
    public void visitoeDeptChoose() {
        final Dialog dialog = new Dialog(AddNewVisitorActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        EditText editText = (EditText) findViewById(R.id.doct_type);

        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        if (docs_types != null) {
            if (docs_types.size() != 0) {
                VisitorDocTypeAdapter visitorDocTypeAdapter = new VisitorDocTypeAdapter(getApplicationContext(), dialog, docs_types, editText, docTypeId);
                cat_rv.setAdapter(visitorDocTypeAdapter);
                visitorDocTypeAdapter.notifyDataSetChanged();
            }
        }
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.country_origin)
    public void countryGetting() {
        final Dialog dialog = new Dialog(AddNewVisitorActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        EditText editText = (EditText) findViewById(R.id.country_origin);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        if (docs_types.size() != 0) {
            CountryNamesAdapter countryNamesAdapter = new CountryNamesAdapter(getApplicationContext(), dialog, countriesList, editText);
            cat_rv.setAdapter(countryNamesAdapter);
            countryNamesAdapter.notifyDataSetChanged();
        }

        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public void checkSuggestionList(String firstNameSt, String lastNameSt, String dobSt) {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().checkSuggetion(APIServerResponse.GET_SUGESTION, getAuthToken(), firstNameSt, lastNameSt, dobSt, getParentIdValue(), getClientId(), getUserID(), AddNewVisitorActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.etDOB)
    public void getDoB() {
        if (first_name_et.getText().toString().equalsIgnoreCase("") || first_name_et.getText().toString().isEmpty()) {
            /*first_name_et.setError("Please fill first name");*/
            first_name_et.setError("");
            showToast("Please fill first name", Toast.LENGTH_SHORT);
        } else if (last_name_et.getText().toString().equalsIgnoreCase("") || last_name_et.getText().toString().isEmpty()) {
            /*  last_name_et.setError("Please fill last name");*/
            last_name_et.setError("");
            showToast("Please fill last name", Toast.LENGTH_SHORT);
        } else {
            openDateDia();
        }
    }

    public void openDateDia() {
        final Calendar cal = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        Log.e("Date is", "Date is" + format);
                        etDOB.setText(format);
                        finalDate = format;
                        if (first_name_et.getText().toString().equalsIgnoreCase("") || first_name_et.getText().toString().isEmpty()) {
                            /*first_name_et.setError("Please fill first name");*/
                            first_name_et.setError("");
                            showToast("Please fill first name", Toast.LENGTH_SHORT);
                        } else if (last_name_et.getText().toString().equalsIgnoreCase("") || last_name_et.getText().toString().isEmpty()) {
                            /*  last_name_et.setError("Please fill last name");*/
                            last_name_et.setError("");
                            showToast("Please fill last name", Toast.LENGTH_SHORT);
                        } else {
                            checkSuggestionList(first_name_et.getText().toString(), last_name_et.getText().toString(), format);
                        }
                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);

        datePicker.setCancelable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    @OnClick(R.id.visiting_dept_et)
    public void vistingDeptDialog() {
        final Dialog dialog = new Dialog(AddNewVisitorActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        EditText editText = (EditText) findViewById(R.id.visiting_dept_et);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        if (docs_types != null) {
            if (docs_types.size() != 0) {
                DepartmentsAdapter departmentsAdapter = new DepartmentsAdapter(getApplicationContext(), dialog, departmentsList, editText, visiting_department_id);
                cat_rv.setAdapter(departmentsAdapter);
                departmentsAdapter.notifyDataSetChanged();
            }
        }
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AddNewVisitorActivity.this.finish();
    }

    @OnClick(R.id.home_tv)
    public void onBackCall() {
        AddNewVisitorActivity.this.finish();
    }

    @OnClick(R.id.alert_img)
    public void notificationPageCall() {
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        startActivity(intent);
    }

    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        hasPermissionInManifest(AddNewVisitorActivity.this, Manifest.permission.CAMERA);
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();

        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK && requestCode == responseCode) {
                if (this.requestCode == requestCode && resultCode == RESULT_OK) {
                    profileUrl = data.getExtras().get("data").toString();
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    profileBitMap = bitmap;
                    profile_img.setImageBitmap(bitmap);
                }
                if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                    profileUrl = "";
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.dummy_users);
                    requestOptions.error(R.drawable.dummy_users);
                    Glide.with(this).setDefaultRequestOptions(requestOptions).load(pickerPath).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).thumbnail(0.1f).into(profile_img);
                }
            }
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
                if (data == null) {
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.dummy_users);
                    requestOptions.error(R.drawable.dummy_users);
                    Glide.with(this).setDefaultRequestOptions(requestOptions).load(getResources().getDrawable(R.drawable.dummy_users)).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).thumbnail(0.1f).into(profile_img);
                } else {
                    Glide.with(this).load(pickerPath).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).thumbnail(0.1f).into(profile_img);
                }
            }
            profileUrl = "";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void getAllDetail() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllNewVisitorPageDetail(APIServerResponse.GET_NEW_VISITOR_DETAIL, getAuthToken(), getParentIdValue(), getClientId(), getUserID(), AddNewVisitorActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case GET_NEW_VISITOR_DETAIL:
                        GetSuffixDoctypeResponse getSuffixDoctypeResponse = (GetSuffixDoctypeResponse) response.body();
                        hideLoading();
                        if (getSuffixDoctypeResponse.getStatus().equalsIgnoreCase("OK")) {
                            suffixList = getSuffixDoctypeResponse.getSuffix();
                            docs_types = getSuffixDoctypeResponse.getDocs_type();
                            entry_from = getSuffixDoctypeResponse.getEntry_from();
                            departmentsList = getSuffixDoctypeResponse.getDepartments();
                            countriesList = getSuffixDoctypeResponse.getCountries();
                            individualsList = getSuffixDoctypeResponse.getIndividuals();
                            purposesList = getSuffixDoctypeResponse.getPurpose();

                            txtSearch.setThreshold(1);
                            adapter = new VisitIndividualAdapter(this, R.layout.activity_main, R.id.category_name_tv, individualsList);

                            txtSearch.setAdapter(adapter);
                            txtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Object item = adapterView.getItemAtPosition(i);
                                    if (item instanceof Individuals) {
                                        Individuals individuals = (Individuals) item;
                                        visiting_individual_id = individuals.getId();
                                    }
                                }
                            });

                            if (getIntent().hasExtra("visitor_doc_name")) {
                                visitor_dept_id_et.setText(getIntent().getStringExtra("visitor_doc_name"));

                                try {
                                    for (int i = 0; i <= docs_types.size(); i++) {
                                        if (docs_types.get(i).getDocument_name().equalsIgnoreCase("No ID Available")) {
                                            docTypeId = docs_types.get(i).getId();
                                        } else if (docs_types.get(i).getDocument_name().equalsIgnoreCase("Driver's License number")) {
                                            docTypeId = docs_types.get(i).getId();
                                        }
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                if (visitor_dept_id_et.getText().toString().equalsIgnoreCase("No ID Available")) {
                                    visitor_id_number_et.setText("N/A");
                                    visitor_id_number_et.setEnabled(false);
                                } else {
                                    visitor_id_number_et.setText("");
                                    visitor_id_number_et.setEnabled(true);
                                }
                            }
                        }
                        break;
                    case GET_SUGESTION:
                        SuggesionResponse suggesionResponse = (SuggesionResponse) response.body();
                        hideLoading();
                        if (suggesionResponse.getStatus().equalsIgnoreCase("OK")) {
                            SuggestionDialogFragment dialogFragment = new SuggestionDialogFragment();
                            dialogFragment.setData(suggesionResponse.getMatches(), this);
                            dialogFragment.show(getSupportFragmentManager(), "suggestion_list");
                        } else {
                            showToast(suggesionResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                    case CHECK_IN_WATCH_LIST:
                        WatchListCheck watchListCheck = (WatchListCheck) response.body();
                        hideLoading();
                        if (watchListCheck.getStatus().equalsIgnoreCase("OK")) {
                            if (watchListCheck.getMatches().size() > 0) {
                                Intent intent = new Intent(getApplicationContext(), WatchListDetailActivity.class);
                                List<WatchListMatchModal> offendersList = watchListCheck.getMatches();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("match_list", (Serializable) offendersList);
                                bundle.putString("first_name", first_name_et.getText().toString());
                                bundle.putString("last_name", last_name_et.getText().toString());
                                bundle.putString("age", etDOB.getText().toString());
                                bundle.putString("phNumber", ph_number_et.getText().toString());
                                bundle.putString("email", email_et.getText().toString());
                                bundle.putBoolean("cbFlag", cbFlag);
                                bundle.putBoolean("soFlag", soFlag);
                                bundle.putString("purpose_id", vms_visitor_purpose_id);
                                bundle.putString("suffixChoose", suffix_choose_et.getText().toString() + "");
                                bundle.putString("docTypeId", docTypeId);
                                bundle.putString("finalDate", finalDate);
                                bundle.putString("visitorIdNumber", visitor_id_number_et.getText().toString());
                                bundle.putString("visiting_individual_id", visiting_individual_id);
                                bundle.putString("visiting_department_id", visiting_department_id);
                                bundle.putString("vms_visitor_entering_from_id", vms_visitor_entering_from_id);
                                bundle.putString("vms_visitor_purpose_id", vms_visitor_purpose_id);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String myDate = format.format(new Date());
                                bundle.putString("timing", localToGMT(myDate));
                                bundle.putString("country_idSt", country_idSt);
                                bundle.putString("log_id", matchId);
                                bundle.putString("profileUrl", pickerPath);
                                intent.putExtra("profileUrl", pickerPath);

                                intent.putExtras(bundle);
                                startActivity(intent);
                            } else {
                                successPage("WatchList clear ", "watchlist");
                            }
                        } else {
                            successPage("WatchList clear ", "watchlist");
                        }
                        break;

                    case CHECK_SEX_OFFENDER:
                        SOResponse sexOffender = (SOResponse) response.body();
                        hideLoading();
                        if (sexOffender.getStatus().equalsIgnoreCase("OK")) {
                            if (sexOffender.getSOData().getOffenders().size() != 0) {
                                Intent intent = new Intent(getApplicationContext(), SoOffenderActivity.class);
                                List<Offenders> offendersList = sexOffender.getSOData().getOffenders();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("offender_array", (Serializable) offendersList);
                                bundle.putString("first_name", first_name_et.getText().toString());
                                bundle.putString("last_name", last_name_et.getText().toString());
                                bundle.putString("age", etDOB.getText().toString());
                                bundle.putString("phNumber", local_code_number.getText() + ph_number_et.getText().toString());
                                bundle.putString("email", email_et.getText().toString());
                                bundle.putBoolean("cbFlag", cbFlag);
                                bundle.putBoolean("soFlag", soFlag);
                                bundle.putString("purpose_id", vms_visitor_purpose_id);
                                bundle.putString("suffixChoose", suffix_choose_et.getText().toString() + "");
                                bundle.putString("docTypeId", docTypeId);
                                bundle.putString("finalDate", finalDate);
                                bundle.putString("visitorIdNumber", visitor_id_number_et.getText().toString());
                                bundle.putString("visiting_individual_id", visiting_individual_id);
                                bundle.putString("visiting_department_id", visiting_department_id);
                                intent.putExtra("visitor_individual", txtSearch.getText().toString());
                                bundle.putString("vms_visitor_entering_from_id", vms_visitor_entering_from_id);
                                bundle.putString("profileBitMap", String.valueOf(profileBitMap));
                                bundle.putString("vms_visitor_purpose_id", vms_visitor_purpose_id);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String myDate = format.format(new Date());
                                bundle.putString("timing", localToGMT(myDate));
                                bundle.putString("country_idSt", country_idSt);
                                bundle.putString("log_id", matchId);
                                if (profileUrl != null) {
                                    if (profileUrl.equalsIgnoreCase("")) {
                                        intent.putExtra("profileUrl", pickerPath);
                                    } else {
                                        intent.putExtra("profileUrl", "");
                                    }
                                }
                                intent.putExtras(bundle);
                                startActivity(intent);
                            } else {
                                successPage("Sex Offender check Cleared!", "SO");
                            }
                        } else {
                            successPage("WatchList clear ", "watchlist");
                        }
                        break;
                    case CHECK_CB:
                        hideLoading();
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            if (jsonObject.get("data") != null) {
                                if (jsonObject.get("data").getAsJsonObject().get("Results").getAsJsonObject().getAsJsonArray("Records").size() != 0) {
                                    Intent intent = new Intent(getApplicationContext(), CriminalRecordActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("resultObject", jsonObject.get("data").getAsJsonObject().get("Results").getAsJsonObject().toString());
                                    bundle.putString("first_name", first_name_et.getText().toString());
                                    bundle.putString("last_name", last_name_et.getText().toString());
                                    bundle.putString("age", etDOB.getText().toString());
                                    bundle.putString("entery_from", entery_form_et.getText().toString());
                                    bundle.putBoolean("cbFlag", cbFlag);
                                    bundle.putString("phNumber", local_code_number.getText() + ph_number_et.getText().toString());
                                    bundle.putString("dob", etDOB.getText().toString());
                                    bundle.putString("email", email_et.getText().toString());
                                    bundle.putString("enteryFrom", entery_form_et.getText().toString());
                                    bundle.putString("profile_img", pickerPath);
                                    bundle.putString("purpose_id", vms_visitor_purpose_id);
                                    bundle.putString("suffixChoose", suffix_choose_et.getText().toString() + "");
                                    bundle.putString("docTypeId", docTypeId);
                                    bundle.putString("finalDate", finalDate);
                                    bundle.putString("visitorIdNumber", visitor_id_number_et.getText().toString());
                                    bundle.putString("visiting_individual_id", visiting_individual_id);
                                    bundle.putString("visiting_department_id", visiting_department_id);
                                    bundle.putString("vms_visitor_entering_from_id", vms_visitor_entering_from_id);
                                    bundle.putString("vms_visitor_purpose_id", vms_visitor_purpose_id);
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                    String myDate = format.format(new Date());
                                    bundle.putString("timing_et",/* timing_et.getText().toString()*/ localToGMT(myDate));
                                    if (profileUrl != null) {
                                        if (profileUrl.equalsIgnoreCase("")) {
                                            intent.putExtra("profileUrl", pickerPath);
                                        } else {
                                            intent.putExtra("profileUrl", "");
                                        }
                                    }
                                    bundle.putString("country_idSt", country_idSt);
                                    bundle.putString("log_id", matchId);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } else {
                                    successPage("Criminal Background Check is clear", "criminal");
                                }
                            } else {
                                successPage("Criminal Background Check is clear", "criminal");
                            }
                        }
                        break;
                    case CREATE_VISITOR:
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        hideLoading();
                        if (jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                            Intent intent = new Intent(getApplicationContext(), VisitorInfoActivity.class);
                            intent.putExtra("entered_by", jsonObject1.get("entered_by").getAsString());
                            intent.putExtra("purpose", jsonObject1.get("visit_purpose").getAsString());
                            intent.putExtra("visitinDept", jsonObject1.get("department_name").getAsString());
                            intent.putExtra("timing", jsonObject1.get("check_in").getAsString());
                            intent.putExtra("email", jsonObject1.get("email").getAsString());
                            intent.putExtra("phone", jsonObject1.get("phone").getAsString());
                            intent.putExtra("firstName", jsonObject1.get("first_name").getAsString());
                            intent.putExtra("lastName", jsonObject1.get("last_name").getAsString());
                            intent.putExtra("middleName", jsonObject1.get("middle_name").getAsString());
                            intent.putExtra("visitor_individual", txtSearch.getText().toString());
                            intent.putExtra("logId", jsonObject1.get("log_id").getAsString());
                            intent.putExtra("webImage", jsonObject1.get("profile_pic_web").getAsString());
                            if (profileUrl != null) {

                                if (profileUrl.equalsIgnoreCase("")) {
                                    intent.putExtra("profileUrl", pickerPath);
                                } else {
                                    intent.putExtra("profileUrl", "");
                                }
                            }
                            Bundle bundle = new Bundle();
                            intent.putExtra("object", String.valueOf(jsonObject1));
                            // intent.putExtra("log_id",);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                            break;
                        } else {
                            alertWithSingleTitle("Alert", jsonObject1.get("message").getAsString(), "error");

                        }
                }
            } catch (Exception ex) {
                hideLoading();
                ex.printStackTrace();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }

    public void successPage(String textSt, String checkType) {
        if (dialogShown) {
            return;
        } else {
            dialogShown = true;
            dialog = new Dialog(AddNewVisitorActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_green_successfull);
            dialog.setTitle(null);
            TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
            textView.setText(textSt + "");
            Window window = dialog.getWindow();
            window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(true);
            dialog.setTitle(null);
            dialog.show();
            Handler handler = new Handler();
            if (checkType.equalsIgnoreCase("watchlist")) {
                watchFlag = true;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialogShown = false;

                        dialog.dismiss();
                        if (isConnectedToInternet()) {
                            showLoading();
                            ServerAPI.getInstance().checkSexOffenderExistence(APIServerResponse.CHECK_SEX_OFFENDER, getAuthToken(), first_name_et.getText().toString(), last_name_et.getText().toString(), etDOB.getText().toString(), getClientId(), AddNewVisitorActivity.this);
                        } else {
                            showSnack(Constants.INTERNET_CONNECTION);
                        }
                    }
                }, 1000);
            } else if (checkType.equalsIgnoreCase("criminal")) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cbFlag = true;
                        dialogShown = false;
                        /* if (watchFlag) {*/
                        outer_proceed.setVisibility(View.VISIBLE);
                        three_btn_ll.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                }, 1000);
            } else {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        soFlag = true;
                        dialogShown = false;
                        if (cbFlag) {
                            dialog.dismiss();
                            outer_proceed.setVisibility(View.VISIBLE);
                            three_btn_ll.setVisibility(View.GONE);

                        } else {
                            outer_proceed.setVisibility(View.GONE);
                            three_btn_ll.setVisibility(View.VISIBLE);
                            dialog.dismiss();
                        }
                        dialog.dismiss();
                        showProceedBtn(false);
                    }
                }, 1000);
            }
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ChosenImage image = list.get(0);
            if (image != null) {
                if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                    pickerPath = image.getThumbnailPath();
                else
                    pickerPath = image.getOriginalPath();
                profile_img.setVisibility(View.VISIBLE);
                profile_img.setImageURI(Uri.fromFile(new File(pickerPath)));
            } else
                showSnack("Invalid Image");
        }
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    private void setData(Matches matches) {
        email_et.setText(matches.getEmail() + "");
        ph_number_et.setText(matches.getPhone() + "");
        profileUrl = matches.getProfile_pic_web();
        pickerPath = matches.getProfile_pic_web();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.dummy_users);
        requestOptions.error(R.drawable.dummy_users);
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(matches.getProfile_pic_web()).into(profile_img);
    }

    @Override
    public void onClick(Matches matches) {
        setData(matches);
    }
}
