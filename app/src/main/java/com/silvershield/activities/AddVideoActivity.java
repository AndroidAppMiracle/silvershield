package com.silvershield.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.CategoriesListingAdapter;
import com.silvershield.modal.CategoriesDetail;
import com.silvershield.modal.GetCategoryResponse;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AddVideoActivity extends BaseActivity implements APIServerResponse {

    static String mycate = "";
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.categories_et)
    EditText categories_et;
    @BindView(R.id.number_attemps_ll)
    LinearLayout number_attemps_ll;
    GetCategoryResponse getCategoryResponse;
    List<CategoriesDetail> categoriesDetailList;

    List<String> list = new ArrayList<>();

    List<String> noAttemptsArr = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vedio);
        ButterKnife.bind(this);
        title_tv.setText("ADD VIDEOS");
        getAllAssignToDetail();

       /* if (list.size() != 0)
        {
            list.clear();
        }

        list.add("All");
        list.add("Department");
        list.add("Indi");
        list.add("All");
        list.add("All");*/
        if (getIntent().hasExtra("bundle")) {
            Bundle bundle = getIntent().getBundleExtra("bundle");
            getCategoryResponse = bundle.getParcelable("categories");
            categoriesDetailList = (List<CategoriesDetail>) getIntent().getSerializableExtra("categoriesLis");
        }

        logout_btn.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
    }


    @OnClick(R.id.select_category)
    public void showCategoriesPopup() {
        final Dialog dialog = new Dialog(AddVideoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        CategoriesListingAdapter categoriesListingAdapter = new CategoriesListingAdapter(getApplicationContext(), dialog, categoriesDetailList, mycate, categories_et);
        cat_rv.setAdapter(categoriesListingAdapter);
        categoriesListingAdapter.notifyDataSetChanged();
        // categories_et.setText("" + mycate);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.categories_et)
    public void showCatPopup() {
        final Dialog dialog = new Dialog(AddVideoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        CategoriesListingAdapter categoriesListingAdapter = new CategoriesListingAdapter(getApplicationContext(), dialog, categoriesDetailList, mycate, categories_et);
        cat_rv.setAdapter(categoriesListingAdapter);
        categoriesListingAdapter.notifyDataSetChanged();
        // categories_et.setText("" + mycate);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AddVideoActivity.this.finish();
    }

    @OnClick(R.id.home_tv)
    public void backActivity() {
        AddVideoActivity.this.finish();
    }

    public void getAllDetail() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllNewVisitorPageDetail(APIServerResponse.GET_NEW_VISITOR_DETAIL,getAuthToken(), getParentIdValue(), getClientId(), getUserID(), AddVideoActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }


    public void getAllAssignToDetail() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAsignToByDetail(APIServerResponse.GET_ASIGN_TO_BT_DETAIL,getAuthToken(), getUserID(), getParentIdValue(), getClientId(), AddVideoActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();

        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GET_ASIGN_TO_BT_DETAIL:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {

                        }
                        break;
                }
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
