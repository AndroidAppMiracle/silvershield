package com.silvershield.activities;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AlertLocationActivity extends BaseActivity implements APIServerResponse {

    static String enetredCode, alarmType, callTypePerson;
    static String fnialAddress;
    static int selectedLocation = -1;
    @BindView(R.id.close_img)
    ImageView close_img;
    @BindView(R.id.location_rad_group)
    RadioGroup location_rad_group;
    @BindView(R.id.enter_custome_location)
    EditText enter_custome_location;
    @BindView(R.id.additional_info)
    EditText additional_info;
    double latitude, longitude;
    JsonArray locationArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_location);
        ButterKnife.bind(this);
        if (getIntent().hasExtra("verifiedCode")) {
            enetredCode = getIntent().getStringExtra("verifiedCode");
            alarmType = getIntent().getStringExtra("alarmType");
            callTypePerson = getIntent().getStringExtra("subType");
        }
        getAllLocation();
        enter_custome_location.setEnabled(false);
        location_rad_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                View radioButton = radioGroup.findViewById(i);
                int index = radioGroup.indexOfChild(radioButton);
                //   Toast.makeText(getApplicationContext(), "" + index, Toast.LENGTH_SHORT).show();
                selectedLocation = index;
                if (index == 0) {
                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    startActivity(intent);
                    enter_custome_location.setEnabled(false);
                    additional_info.setEnabled(false);
                } else if (index == 1) {
                    latitude = 0.0;
                    longitude = 0.0;
                    enter_custome_location.setEnabled(true);
                    additional_info.setEnabled(true);
                } else {
                    latitude = 0.0;
                    longitude = 0.0;
                    enter_custome_location.setEnabled(false);
                    additional_info.setEnabled(true);
                }
            }
        });
        //Intent values coming from googlemap location
        if (getIntent().hasExtra("address")) {
            fnialAddress = getIntent().getStringExtra("address");
            latitude = getIntent().getDoubleExtra("lat", 0);
            longitude = getIntent().getDoubleExtra("long", 0);

            if (fnialAddress.isEmpty() || fnialAddress.equalsIgnoreCase("") || fnialAddress == null) {

            } else {
                additional_info.setText(fnialAddress + "");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().hasExtra("address")) {
            fnialAddress = getIntent().getStringExtra("address");
            latitude = getIntent().getDoubleExtra("lat", 0);
            longitude = getIntent().getDoubleExtra("long", 0);

            if (fnialAddress.isEmpty() || fnialAddress.equalsIgnoreCase("") || fnialAddress == null) {

            } else {
                additional_info.setText(fnialAddress + "");
            }
        }
    }

    @OnClick(R.id.submit_btn)
    public void callAlarmWithLocation() {
        if (selectedLocation == 1) {
            fnialAddress = enter_custome_location.getText().toString();
        } else {
            fnialAddress = locationArray.get(2).getAsString();
        }
        if (!fnialAddress.equalsIgnoreCase("")) {
            if (selectedLocation != -1) {
                if (alarmType.equalsIgnoreCase("internal")) {
                    if (isConnectedToInternet()) {
                        showLoading();
                        ServerAPI.getInstance().callAlarm(APIServerResponse.CALL_ALARM, getAuthToken(),/* getClientId()*/ getUserID(),
                                getClientId(), getParentIdValue(),
                                callTypePerson, "SMS",
                                getFirstName() + "" + getLastName(),
                                "", enetredCode, String.valueOf(latitude), String.valueOf(longitude), fnialAddress, additional_info.getText().toString(), AlertLocationActivity.this);

                        //call alarm
                    } else {
                        showSnack(Constants.INTERNET_CONNECTION);
                    }

                } else {
                    if (callTypePerson.equalsIgnoreCase("EMERGENCY")) {
                        if (isConnectedToInternet()) {
                            showLoading();
                            ServerAPI.getInstance();
                            ServerAPI.getInstance().callLockDownAlarm(APIServerResponse.CALL_LOCK_EMERGENCY, getAuthToken(), getUserID(), getParentIdValue(), getClientId(), "SMS", "", enetredCode, AlertLocationActivity.this);
                        } else {
                            showSnack(Constants.INTERNET_CONNECTION);
                        }
                    } else {
                        if (isConnectedToInternet()) {
                            showLoading();
                            ServerAPI.getInstance();
                            ServerAPI.getInstance().callNonEmergenceyAlarm(APIServerResponse.CALL_LOCK_NON_EMERGENCY, getAuthToken(), getUserID(), getParentIdValue(), getClientId(), "SMS", "", enetredCode, AlertLocationActivity.this);
                        } else {
                            showSnack(Constants.INTERNET_CONNECTION);
                        }
                    }
                }
            } else {
                Toast.makeText(getApplicationContext(), "Please select your location", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getAllLocation() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllLocations(APIServerResponse.GET_ALL_LOCATION, getAuthToken(), getClientId(), AlertLocationActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.close_img)
    public void closeImage() {
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case GET_ALL_LOCATION:
                        JsonObject locationMainJsonObj = (JsonObject) response.body();
                        /*{"status":"OK","locations":["Current Location","Other Location","Building A","Building B","Building C"]}*/
                        if (locationMainJsonObj.get("status").getAsString().equalsIgnoreCase("ok")) {
                            locationArray = locationMainJsonObj.getAsJsonArray("locations");
                            RadioGroup.LayoutParams childParam1 = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                            for (int i = 0; i < locationArray.size(); i++) {
                                RadioButton radioButton = new RadioButton(getApplicationContext());
                                radioButton.setId(i);
                                radioButton.setText(locationArray.get(i).getAsString());
                                radioButton.setTextColor(getResources().getColor(R.color.profile_primary_address_color));
                                int textColor = Color.parseColor("#4f4f4f");
                                radioButton.setButtonTintList(ColorStateList.valueOf(textColor));
                                location_rad_group.addView(radioButton, childParam1);
                            }
                        }
                        break;
                    case CALL_LOCK_EMERGENCY:
                        //{"status":"OK","message":"Alarm has been sent to Police"}
                        JsonObject emergencyObject = (JsonObject) response.body();
                        if (emergencyObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            Toast.makeText(getApplicationContext(), emergencyObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            Intent intent1 = new Intent(getApplicationContext(), NotificationActivity.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), emergencyObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case CALL_LOCK_NON_EMERGENCY:
                        JsonObject nonEmergObject = (JsonObject) response.body();
                        if (nonEmergObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            Intent intent1 = new Intent(getApplicationContext(), NotificationActivity.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);
                            finish();
                            Toast.makeText(getApplicationContext(), nonEmergObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), nonEmergObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case CALL_ALARM:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                            Intent intent1 = new Intent(getApplicationContext(), NotificationActivity.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);
                            finish();

                            Toast.makeText(getApplicationContext(), jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LOGOUT:
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        if (jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                            Toast.makeText(getApplicationContext(), jsonObject1.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            setUserLoggedIn(false);
                            clearPreferences();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        break;
                }
            } catch (Exception ex) {
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


}
