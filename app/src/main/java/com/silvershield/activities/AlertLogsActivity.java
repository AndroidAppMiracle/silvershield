package com.silvershield.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.AlertLogAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.interfaces.AlertLongClick;
import com.silvershield.java.CautionarySettings;
import com.silvershield.modal.responseModels.GetAlertLogResponseModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AlertLogsActivity extends BaseActivity implements APIServerResponse, AlertLongClick {

    @BindView(R.id.logs_rv)
    RecyclerView logs_rv;
    @BindView(R.id.home_tv)
    TextView home_tv;
    @BindView(R.id.title_tv)
    Custome_Regular_TextView title_tv;
    @BindView(R.id.cross)
    ImageView cross;
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swip_refresh;
    private List<GetAlertLogResponseModel> modelList;
    private AlertLogAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_logs);
        ButterKnife.bind(this);

        initComonents();
        swip_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllLogsAlarm();
            }
        });
    }

    public void initComonents() {
        title_tv.setText("Alarms Logs");
        home_tv.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        logs_rv.setLayoutManager(linearLayoutManager);
        modelList = new ArrayList<>();
        adapter = new AlertLogAdapter(AlertLogsActivity.this, modelList, this);
        logs_rv.setAdapter(adapter);
        getAllLogsAlarm();
    }

    @OnClick(R.id.cross)
    public void closePage() {
        AlertLogsActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AlertLogsActivity.this.finish();
    }

    public void getAllLogsAlarm() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllLogsAlarm(APIServerResponse.GET_ALL_LOGS, getAuthToken(), getClientId(), AlertLogsActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case GET_ALL_LOGS:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            JsonArray jsonArray = jsonObject.getAsJsonArray("alarms");
                            if (jsonArray != null) {
                                Gson gson = new Gson();
                                for (int i = 0; i < jsonArray.size(); i++) {
                                    JsonElement jsonElement = jsonArray.get(i);
                                    modelList.add(gson.fromJson(jsonElement,GetAlertLogResponseModel.class));
                                }
                                adapter.refreshData(modelList);
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "No Alarm found", Toast.LENGTH_SHORT).show();
                            }
                            if (swip_refresh.isRefreshing()) {
                                swip_refresh.setRefreshing(false);
                            }

                        } else {
                            if (swip_refresh.isRefreshing()) {
                                swip_refresh.setRefreshing(false);
                            }
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onLongClick(String dispatch_id) {
        CautionarySettings cautionarySettings = new CautionarySettings(this, dispatch_id, this);
        cautionarySettings.clearAlert("Alert", getResources().getString(R.string.conf_delete_alarm));
    }

    @Override
    public void onReloadPage() {
        getAllLogsAlarm();
    }
}
