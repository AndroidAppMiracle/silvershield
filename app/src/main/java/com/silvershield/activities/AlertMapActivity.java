package com.silvershield.activities;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.responseModels.GetAlertLogResponseModel;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlertMapActivity extends FragmentActivity implements OnMapReadyCallback {

    @BindView(R.id.title_tv)
    Custome_Regular_TextView title_tv;
    @BindView(R.id.cancle_tv)
    TextView cancle_tv;
    double latitude, longitude;
    String fnialAddress;
    private GoogleMap mMap;
    private Bundle bundle;
    private GetAlertLogResponseModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_map);
        ButterKnife.bind(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.getView().setClickable(false);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            model = bundle.getParcelable("model");
            if (model != null) {
                latitude = Double.parseDouble(model.getLocation_lat());
                longitude = Double.parseDouble(model.getLocation_lng());
            }
        }

        if (latitude != 0.0) {
            Geocoder geoCoder = new Geocoder(AlertMapActivity.this, Locale.getDefault());
            StringBuilder builder = new StringBuilder();
            try {
                List<Address> address = geoCoder.getFromLocation(latitude, longitude, 1);
                int maxLines = address.get(0).getMaxAddressLineIndex();
                try {
                    for (int i = 0; i <= maxLines; i++) {
                        String addressStr = address.get(0).getAddressLine(i);
                        builder.append(addressStr);
                        builder.append(" ");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                fnialAddress = builder.toString(); //This is the complete address.
                title_tv.setText(fnialAddress + "");
                Toast.makeText(getApplicationContext(), fnialAddress, Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                // Handle IOException
                e.printStackTrace();
            } catch (NullPointerException e) {
                // Handle NullPointerException
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Location Found", Toast.LENGTH_SHORT).show();
            AlertMapActivity.this.finish();
        }
    }


    @OnClick(R.id.cancle_tv)
    public void cancleClick() {
        AlertMapActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AlertMapActivity.this.finish();
    }
    
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title("location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }
}
