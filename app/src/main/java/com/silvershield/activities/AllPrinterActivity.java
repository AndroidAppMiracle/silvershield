package com.silvershield.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.silvershield.R;
import com.silvershield.adapter.AllPrinterAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.PrinterId;
import com.silvershield.modal.PrinterSettingResponse;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.observers.Observable;
import com.silvershield.observers.Observer;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;
import com.silvershield.utils.ObservableSingleton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AllPrinterActivity extends BaseActivity implements APIServerResponse, Observer {

    List<PrinterId> printerIdList;
    private Observable mObservable;
    @BindView(R.id.printer_rv)
    RecyclerView printer_rv;
    @BindView(R.id.cancle_tv)
    Custome_Regular_TextView cancle_tv;

    String eventId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_printer);
        ButterKnife.bind(this);
      /*  mObservable = ObservableSingleton.getInstance();
        mObservable.attach(AllPrinterActivity.this);*/
        if (getIntent().hasExtra("eventId")) {
            eventId = getIntent().getStringExtra("eventId");
        }
        getAllPrinter();
    }

    public void getAllPrinter() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getPrinterSetting(APIServerResponse.GET_ALL_PRINTER, getAuthToken(), getClientId(), AllPrinterActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.cancle_tv)
    public void cancleBtnClick() {
        AllPrinterActivity.this.finish();
    }


    @Override
    public void onSuccess(int tag, Response response) {
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case GET_ALL_PRINTER:
                        hideLoading();
                        PrinterSettingResponse printerSettingResponse = (PrinterSettingResponse) response.body();
                        if (printerSettingResponse.getStatus().equalsIgnoreCase("Ok")) {
                            printerIdList = printerSettingResponse.getPrinterId();
                            AllPrinterAdapter allPrinterAdapter = new AllPrinterAdapter(AllPrinterActivity.this, printerIdList, eventId);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                            printer_rv.setLayoutManager(linearLayoutManager);
                            printer_rv.setAdapter(allPrinterAdapter);
                            allPrinterAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getApplicationContext(), "No printer found ", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                hideLoading();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
        hideLoading();
    }

    @Override
    public void update() {
        mObservable.detach(this);
    }

    @Override
    public void updateObserver(boolean bool) {
        try {
            mObservable.detach(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObserverProgress(int percentage) {

    }
}
