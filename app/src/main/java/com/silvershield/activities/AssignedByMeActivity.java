package com.silvershield.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.adapter.CategoriesListingAdapter;
import com.silvershield.modal.CategoriesDetail;
import com.silvershield.modal.GetCategoryResponse;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AssignedByMeActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.select_category)
    Button select_category;
    static String myCat = "";
    List<CategoriesDetail> arrayList = arrayList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    GetCategoryResponse getCategoryResponse;
    public static String categoriesName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_by_me);
        ButterKnife.bind(this);
        getAllCategories();

        title_tv.setText("Assigned by Me");
        logout_btn.setImageDrawable(getResources().getDrawable(R.drawable.add));
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));


        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("categories", getCategoryResponse);
                Intent intent = new Intent(getApplicationContext(), AddVideoActivity.class);
                intent.putExtra("bundle", bundle);
                intent.putExtra("categoriesLis", (Serializable) getCategoryResponse.getCategories());
                startActivity(intent);
            }
        });
    }


    @OnClick(R.id.home_tv)
    public void backCall() {
        AssignedByMeActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        AssignedByMeActivity.this.finish();
    }

    public void getAllCategories() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllCategories(APIServerResponse.GETALLCATEGORIES,getAuthToken(), getClientId(), AssignedByMeActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();

        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GETALLCATEGORIES:
                        getCategoryResponse = (GetCategoryResponse) response.body();
                        arrayList.clear();
                        if (getCategoryResponse.getStatus().equalsIgnoreCase("OK")) {
                            arrayList = getCategoryResponse.getCategories();
                        } else {

                        }
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


    @OnClick(R.id.select_category)
    public void showCategoriesPopup() {
        final Dialog dialog = new Dialog(AssignedByMeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        CategoriesListingAdapter categoriesListingAdapter = new CategoriesListingAdapter(getApplicationContext(), dialog, arrayList, myCat,null);
        cat_rv.setAdapter(categoriesListingAdapter);
        categoriesListingAdapter.notifyDataSetChanged();
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}


