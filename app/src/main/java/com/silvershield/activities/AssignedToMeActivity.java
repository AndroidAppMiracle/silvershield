package com.silvershield.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.silvershield.R;
import com.silvershield.adapter.AssignedToMeCasesAdapter;
import com.silvershield.modal.AssignToMeReponse;
import com.silvershield.modal.Tests;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AssignedToMeActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.allVedioRv)
    RecyclerView allVedioRv;
    LinearLayoutManager linearLayoutManager;

    List<Tests> tests;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;

    AssignedToMeCasesAdapter assignedToMeCasesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_to_me);
        ButterKnife.bind(this);
        if (showAlertIcon())
            home_img.setVisibility(View.VISIBLE);
        else
            home_img.setVisibility(View.GONE);
        logout_btn.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        linearLayoutManager = new LinearLayoutManager(this);
        allVedioRv.setLayoutManager(linearLayoutManager);
        getAllCasesAssignedToMe();

    }

    @OnClick(R.id.home_tv)
    public void backCall() {
        AssignedToMeActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AssignedToMeActivity.this.finish();
    }

    public void getAllCasesAssignedToMe() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllAssignedCasesToME(APIServerResponse.GET_CASES_ASIGNED_TO_ME,getAuthToken(),  getUserID(), getParentIdValue(), getClientId(), AssignedToMeActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        tests = new ArrayList<>();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GET_CASES_ASIGNED_TO_ME:
                        AssignToMeReponse assignToMeReponse = (AssignToMeReponse) response.body();
                        tests.clear();
                        if (assignToMeReponse.getStatus().equalsIgnoreCase("OK")) {
                            tests = assignToMeReponse.getTests();
                        }
                        assignedToMeCasesAdapter = new AssignedToMeCasesAdapter(getApplicationContext(), tests);
                        allVedioRv.setAdapter(assignedToMeCasesAdapter);
                        assignedToMeCasesAdapter.notifyDataSetChanged();
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }




}
