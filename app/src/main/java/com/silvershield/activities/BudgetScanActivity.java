package com.silvershield.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.manateeworks.BarcodeScanner;
import com.manateeworks.CameraManager;
import com.manateeworks.MWOverlay;
import com.manateeworks.MWParser;
import com.silvershield.R;
import com.silvershield.modal.Offenders;
import com.silvershield.modal.SOResponse;
import com.silvershield.modal.WatchListCheck;
import com.silvershield.modal.WatchListMatchModal;
import com.silvershield.modal.requestModels.CheckWatchListRequestModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

public class BudgetScanActivity extends BaseActivity implements SurfaceHolder.Callback, ActivityCompat.OnRequestPermissionsResultCallback, APIServerResponse {

    public static final boolean USE_MWANALYTICS = false;
    public static final boolean PDF_OPTIMIZED = false;
    /* Parser */
    /*
     * MWPARSER_MASK - Set the desired parser type Available options:
     * MWParser.MWP_PARSER_MASK_ISBT MWParser.MWP_PARSER_MASK_AAMVA
     * MWParser.MWP_PARSER_MASK_IUID MWParser.MWP_PARSER_MASK_HIBC
     * MWParser.MWP_PARSER_MASK_SCM MWParser.MWP_PARSER_MASK_NONE
     */    public static final int MWPARSER_MASK = MWParser.MWP_PARSER_MASK_AAMVA;
    public static final int USE_RESULT_TYPE = BarcodeScanner.MWB_RESULT_TYPE_MW;
    public static final BudgetScanActivity.OverlayMode OVERLAY_MODE = BudgetScanActivity.OverlayMode.OM_MWOVERLAY;
    // !!! Rects are in format: x, y, width, height !!!
    public static final Rect RECT_LANDSCAPE_1D = new Rect(3, 20, 94, 60);
    public static final Rect RECT_LANDSCAPE_2D = new Rect(20, 5, 60, 90);
    public static final Rect RECT_PORTRAIT_1D = new Rect(20, 3, 60, 94);
    public static final Rect RECT_PORTRAIT_2D = new Rect(20, 5, 60, 90);
    public static final Rect RECT_FULL_1D = new Rect(3, 3, 94, 94);
    public static final Rect RECT_FULL_2D = new Rect(20, 5, 60, 90);
    // public static final int MWPARSER_MASK = MWParser.MWP_PARSER_MASK_NONE;
    public static final Rect RECT_DOTCODE = new Rect(30, 20, 40, 60);
    public static final int ID_AUTO_FOCUS = 0x01;
    public static final int ID_DECODE = 0x02;
    public static final int ID_RESTART_PREVIEW = 0x04;
    public static final int ID_DECODE_SUCCEED = 0x08;
    public static final int ID_DECODE_FAILED = 0x10;
    private static final String MSG_CAMERA_FRAMEWORK_BUG = "Sorry, the Android camera encountered a problem: ";
    public static int MAX_THREADS = Runtime.getRuntime().availableProcessors();
    static boolean watchFlag = false, soFlag = false, cbFlag = false;
    String firstName, lastName, dob, resultSt;
    JsonObject eventObject;
    Dialog dialog;
    String type_click;
    List<String> keyScanList;
    List<String> valueScanList;
    HashMap<String, String> stringMap = new HashMap<>();
    boolean flashOn = false;
    Intent intent;
    Bundle bundle;
    boolean dialogShown;
    BudgetScanActivity.State state = BudgetScanActivity.State.STOPPED;
    private Handler decodeHandler;
    private boolean hasSurface;
    private String package_name;
    private int activeThreads = 0;
    private ImageButton zoomButton;
    private ImageButton buttonFlash;
    private ImageButton closeButton;
    private ImageView imageOverlay;
    private int zoomLevel = 0;
    private int firstZoom = 150;
    private int secondZoom = 300;
    private SurfaceHolder surfaceHolder;


    /* Analytics */
    /*
     * private String encResult; private String tName; private String
     * analyticsTag = "TestTag";
     */
    private boolean surfaceChanged = false;

    public Handler getHandler() {
        return decodeHandler;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        surfaceChanged = false;
        package_name = getPackageName();

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(getResources().getIdentifier("activity_capture", "layout", package_name));
        imageOverlay = (ImageView) findViewById(R.id.imageOverlay);
        // register your copy of library with given key

        if (getIntent() != null) {
            type_click = getIntent().getStringExtra("calling_type");
        }

        int registerResult = BarcodeScanner.MWBregisterSDK("P8fAk1nM+Z9AhnFipv1sAD8NSAa/UzDcwborbDghWZc=", this);

        switch (registerResult) {
            case BarcodeScanner.MWB_RTREG_OK:
                Log.i("MWBregisterSDK", "Registration OK");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_KEY:
                Log.e("MWBregisterSDK", "Registration Invalid Key");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_CHECKSUM:
                Log.e("MWBregisterSDK", "Registration Invalid Checksum");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_APPLICATION:
                Log.e("MWBregisterSDK", "Registration Invalid Application");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_SDK_VERSION:
                Log.e("MWBregisterSDK", "Registration Invalid SDK Version");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_KEY_VERSION:
                Log.e("MWBregisterSDK", "Registration Invalid Key Version");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_PLATFORM:
                Log.e("MWBregisterSDK", "Registration Invalid Platform");
                break;
            case BarcodeScanner.MWB_RTREG_KEY_EXPIRED:
                Log.e("MWBregisterSDK", "Registration Key Expired");
                break;

            default:
                Log.e("MWBregisterSDK", "Registration Unknown Error");
                break;
        }

        // choose code type or types you want to search for
        if (PDF_OPTIMIZED) {
            BarcodeScanner.MWBsetLevel(3);
            BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL);
            BarcodeScanner.MWBsetActiveCodes(BarcodeScanner.MWB_CODE_MASK_PDF);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF, RECT_LANDSCAPE_1D);
        } else {
            // Our sample app is configured by default to search both directions...
            BarcodeScanner.MWBsetDirection(
                    BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL | BarcodeScanner.MWB_SCANDIRECTION_VERTICAL);

            // Our sample app is configured by default to search all supported barcodes...
            // But for better performance, only activate the symbologies your application requires...


            if (type_click.equalsIgnoreCase("budge")) {
                BarcodeScanner.MWBsetActiveCodes(
                    /*BarcodeScanner.MWB_CODE_MASK_25 | BarcodeScanner.MWB_CODE_MASK_39 | BarcodeScanner.MWB_CODE_MASK_93
                            | BarcodeScanner.MWB_CODE_MASK_128 | BarcodeScanner.MWB_CODE_MASK_AZTEC
                            | BarcodeScanner.MWB_CODE_MASK_DM | BarcodeScanner.MWB_CODE_MASK_EANUPC*/
                        /*|*/ BarcodeScanner.MWB_CODE_MASK_QR
                          /*  | BarcodeScanner.MWB_CODE_MASK_CODABAR | BarcodeScanner.MWB_CODE_MASK_11
                            | BarcodeScanner.MWB_CODE_MASK_MAXICODE | BarcodeScanner.MWB_CODE_MASK_POSTAL
                            | BarcodeScanner.MWB_CODE_MASK_MSI | BarcodeScanner.MWB_CODE_MASK_RSS*/);


            } else {

                BarcodeScanner.MWBsetActiveCodes(
                        BarcodeScanner.MWB_CODE_MASK_PDF);


            }

            // set the scanning rectangle based on scan direction(format in pct:
            // x, y, width, height)
          /*  BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_93, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC, RECT_FULL_2D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM, RECT_FULL_2D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC, RECT_FULL_1D);*/

            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR, RECT_FULL_2D);
           /* BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_CODABAR, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DOTCODE, RECT_DOTCODE);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_11, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MSI, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MAXICODE, RECT_FULL_1D);
            BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_POSTAL, RECT_FULL_1D);*/
            BarcodeScanner.MWBsetLevel(2);
        }

        if (OVERLAY_MODE == BudgetScanActivity.OverlayMode.OM_IMAGE) {
            imageOverlay.setVisibility(View.VISIBLE);
        }

        /* Analytics */
        /*
         * Register with given apiUser and apiKey
         */

      /*   if (USE_MWANALYTICS) {
         MWBAnalytics.init(getApplicationContext(), "apiUser", "apiKey");
         }*/

        /* For better performance, set like this for PORTRAIT scanning...*/
        // BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_VERTICAL);
  /*       set the scanning rectangle based on scan direction(format in pct: x,
         y, width, height)*/
/*         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25,
         RECT_PORTRAIT_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39,
         RECT_PORTRAIT_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_93,
         RECT_PORTRAIT_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128,
         RECT_PORTRAIT_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC,
         RECT_PORTRAIT_2D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM,
         RECT_PORTRAIT_2D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC,
         RECT_PORTRAIT_1D);*/
        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF,
                RECT_PORTRAIT_1D);
        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR,
                RECT_PORTRAIT_2D);
      /*   BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS,
         RECT_PORTRAIT_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_CODABAR,RECT_PORTRAIT_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DOTCODE,RECT_DOTCODE);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_11,
         RECT_PORTRAIT_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MSI,
         RECT_PORTRAIT_1D);*/

         /*or like this for LANDSCAPE scanning - Preferred for dense or wide
         codes...*/
        // BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL);
        /* set the scanning rectangle based on scan direction(format in pct: x,
         y, width, height)*/
        /* BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25,
         RECT_LANDSCAPE_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39,
         RECT_LANDSCAPE_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_93,
         RECT_LANDSCAPE_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128,
         RECT_LANDSCAPE_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC,
         RECT_LANDSCAPE_2D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM,
         RECT_LANDSCAPE_2D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC,
         RECT_LANDSCAPE_1D);*/
     /*   BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF,
                RECT_LANDSCAPE_1D);
        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR,
                RECT_LANDSCAPE_2D);*/
        /* BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS,
         RECT_LANDSCAPE_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_CODABAR,RECT_LANDSCAPE_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DOTCODE,RECT_DOTCODE);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_11,
         RECT_LANDSCAPE_1D);
         BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MSI,
         RECT_LANDSCAPE_1D);*/

        // set decoder effort level (1 - 5)
        // for live scanning scenarios, a setting between 1 to 3 will suffice
        // levels 4 and 5 are typically reserved for batch scanning
        BarcodeScanner.MWBsetResultType(USE_RESULT_TYPE);

        // Set minimum result length for low-protected barcode types
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_25, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_MSI, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_39, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_CODABAR, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_11, 5);

        // Set adittional options for GS1 and ECI
        // BarcodeScanner.MWBsetParam(BarcodeScanner.MWB_CODE_MASK_DM,
        // BarcodeScanner.MWB_PAR_ID_ECI_MODE,
        // BarcodeScanner.MWB_PAR_VALUE_ECI_ENABLED);
        // BarcodeScanner.MWBsetParam(BarcodeScanner.MWB_CODE_MASK_DM,
        // BarcodeScanner.MWB_PAR_ID_RESULT_PREFIX,
        // BarcodeScanner.MWB_PAR_VALUE_RESULT_PREFIX_ALWAYS);
        // BarcodeScanner.MWBsetParam(BarcodeScanner.MWB_CODE_MASK_128,
        // BarcodeScanner.MWB_PAR_ID_RESULT_PREFIX,
        // BarcodeScanner.MWB_PAR_VALUE_RESULT_PREFIX_ALWAYS);
        // BarcodeScanner.MWBsetParam(BarcodeScanner.MWB_CODE_MASK_QR,
        // BarcodeScanner.MWB_PAR_ID_RESULT_PREFIX,
        // BarcodeScanner.MWB_PAR_VALUE_RESULT_PREFIX_ALWAYS);

        // BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_EANUPC,
        // BarcodeScanner.MWB_CFG_EANUPC_DISABLE_ADDON);

        CameraManager.init(getApplication());

        hasSurface = false;
        state = State.STOPPED;
        decodeHandler = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case ID_DECODE:
                        decode((byte[]) msg.obj, msg.arg1, msg.arg2);
                        break;
                    case ID_AUTO_FOCUS:
                        if (state == BudgetScanActivity.State.PREVIEW || state == BudgetScanActivity.State.DECODING) {
                            CameraManager.get().requestAutoFocus(decodeHandler, ID_AUTO_FOCUS);
                        }
                        break;
                    case ID_RESTART_PREVIEW:
                        restartPreviewAndDecode();
                        break;
                    case ID_DECODE_SUCCEED:
                        state = BudgetScanActivity.State.STOPPED;
                        handleDecode((BarcodeScanner.MWResult) msg.obj);
                        break;
                    case ID_DECODE_FAILED:
                        break;
                }
                return false;
            }
        });

        zoomButton = (ImageButton) findViewById(R.id.zoomButton);
        zoomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                zoomLevel++;
                if (zoomLevel > 2) {
                    zoomLevel = 0;
                }

                switch (zoomLevel) {
                    case 0:
                        CameraManager.get().setZoom(100);
                        break;
                    case 1:
                        CameraManager.get().setZoom(firstZoom);
                        break;
                    case 2:
                        CameraManager.get().setZoom(secondZoom);
                        break;

                    default:
                        break;
                }

            }
        });
        buttonFlash = (ImageButton) findViewById(R.id.flashButton);
        buttonFlash.setOnClickListener(new View.OnClickListener() {
            // @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            @Override
            public void onClick(View v) {
                toggleFlash();
            }
        });

        closeButton = (ImageButton) findViewById(R.id.close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BudgetScanActivity.this.finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        final SurfaceView surfaceView = (SurfaceView) findViewById(
                getResources().getIdentifier("preview_view", "id", package_name));
        surfaceHolder = surfaceView.getHolder();


        recycleOverlayImage();
        if (OVERLAY_MODE == BudgetScanActivity.OverlayMode.OM_MWOVERLAY) {
            MWOverlay.removeOverlay();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    MWOverlay.addOverlay(BudgetScanActivity.this, surfaceView);
                }
            }, 1);
        }

        if (hasSurface) {
            Log.i("Init Camera", "On resume");
            initCamera();
        } else {
            // Install the callback and wait for surfaceCreated() to init the
            // camera.
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
    }

    @SuppressLint("Override")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        /* Analytics */
        /*
         * else if (requestCode == 12333){ if (encResult != null && tName !=
         * null){ MWBAnalytics.MWB_sendReport(encResult, tName, analyticsTag); }
         * }
         */
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopScaner();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopScaner();
    }

    private void stopScaner() {
        /* Stops the scanner when the activity goes in background */
        if (OVERLAY_MODE == BudgetScanActivity.OverlayMode.OM_MWOVERLAY) {
            MWOverlay.removeOverlay();
        }

        imageOverlay.setImageDrawable(null);

        CameraManager.get().stopPreview();
        CameraManager.get().closeDriver();
        state = BudgetScanActivity.State.STOPPED;
        flashOn = false;
        updateFlash();
    }

    @Override
    public void onConfigurationChanged(Configuration config) {

        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();

        CameraManager.get().updateCameraOrientation(rotation);

        super.onConfigurationChanged(config);
    }

    private void toggleFlash() {
        flashOn = !flashOn;
        updateFlash();
    }

    private void updateFlash() {

        if (!CameraManager.get().isTorchAvailable()) {
            buttonFlash.setVisibility(View.GONE);
            return;

        } else {
            buttonFlash.setVisibility(View.VISIBLE);
        }

        if (flashOn) {
            buttonFlash.setImageResource(R.drawable.flashbuttonon);
        } else {
            buttonFlash.setImageResource(R.drawable.flashbuttonoff);
        }

        CameraManager.get().setTorch(flashOn);

        buttonFlash.postInvalidate();

    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i("Init Camera", "On Surface changed");
        initCamera();
        surfaceChanged = true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_FOCUS || keyCode == KeyEvent.KEYCODE_CAMERA) {
            // Handle these events so they don't launch the Camera app
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void decode(final byte[] data, final int width, final int height) {

        if (activeThreads >= MAX_THREADS || state == BudgetScanActivity.State.STOPPED) {
            return;
        }

        new Thread(new Runnable() {
            public void run() {
                activeThreads++;

                byte[] rawResult = BarcodeScanner.MWBscanGrayscaleImage(data, width, height);

                if (state == BudgetScanActivity.State.STOPPED) {
                    activeThreads--;
                    return;
                }

                BarcodeScanner.MWResult mwResult = null;

                if (rawResult != null && BarcodeScanner.MWBgetResultType() == BarcodeScanner.MWB_RESULT_TYPE_MW) {

                    BarcodeScanner.MWResults results = new BarcodeScanner.MWResults(rawResult);

                    if (results.count > 0) {
                        mwResult = results.getResult(0);
                        rawResult = mwResult.bytes;
                    }

                } else if (rawResult != null
                        && BarcodeScanner.MWBgetResultType() == BarcodeScanner.MWB_RESULT_TYPE_RAW) {
                    mwResult = new BarcodeScanner.MWResult();
                    mwResult.bytes = rawResult;
                    mwResult.text = rawResult.toString();
                    mwResult.type = BarcodeScanner.MWBgetLastType();
                    mwResult.bytesLength = rawResult.length;
                }

                if (mwResult != null) {
                    state = BudgetScanActivity.State.STOPPED;
                    Message message = Message.obtain(BudgetScanActivity.this.getHandler(), ID_DECODE_SUCCEED, mwResult);
                    message.arg1 = mwResult.type;
                    message.sendToTarget();
                } else {
                    Message message = Message.obtain(BudgetScanActivity.this.getHandler(), ID_DECODE_FAILED);
                    message.sendToTarget();
                }

                activeThreads--;
            }
        }).start();
    }

    private void restartPreviewAndDecode() {
        if (state == State.STOPPED) {
            state = State.PREVIEW;
            Log.i("preview", "requestPreviewFrame.");
            CameraManager.get().requestPreviewFrame(getHandler(), ID_DECODE);
            CameraManager.get().requestAutoFocus(getHandler(), ID_AUTO_FOCUS);
        }
    }

    public void handleDecode(BarcodeScanner.MWResult result) {

        String typeName = result.typeName;
        String barcode = result.text;
        String barCodeSt = barcode.replace("*", "");
        //   getScanBudgetDetail(barCodeSt);

        /* Parser */
        /*
         * Parser result handler. Edit this code for custom handling of the
         * parser result. Use MWParser.MWPgetJSON(MWPARSER_MASK,
         * result.encryptedResult.getBytes()); to get JSON formatted result
         */
        if (MWPARSER_MASK != MWParser.MWP_PARSER_MASK_NONE &&
                BarcodeScanner.MWBgetResultType() ==
                        BarcodeScanner.MWB_RESULT_TYPE_MW) {

            barcode = MWParser.MWPgetFormattedText(MWPARSER_MASK,
                    result.encryptedResult.getBytes());
            if (barcode == null) {
                String parserMask = "";

                switch (MWPARSER_MASK) {
                    case MWParser.MWP_PARSER_MASK_AAMVA:
                        parserMask = "AAMVA";
                        break;
                    case MWParser.MWP_PARSER_MASK_GS1:
                        parserMask = "GS1";
                        break;
                    case MWParser.MWP_PARSER_MASK_ISBT:
                        parserMask = "ISBT";
                        break;
                    case MWParser.MWP_PARSER_MASK_IUID:
                        parserMask = "IUID";
                        break;
                    case MWParser.MWP_PARSER_MASK_HIBC:
                        parserMask = "HIBC";
                        break;
                    case MWParser.MWP_PARSER_MASK_SCM:
                        parserMask = "SCM";
                        break;

                    default:
                        parserMask = "unknown";
                        break;
                }

                barcode = result.text + "\n*Not a valid " + parserMask +
                        " formatted barcode";
            }

        }
        /* Parser */


        if (result.locationPoints != null && CameraManager.get().

                getCurrentResolution()
                != null
                && OVERLAY_MODE == OverlayMode.OM_MWOVERLAY)

        {
            MWOverlay.showLocation(result.locationPoints.points, result.imageWidth, result.imageHeight);
        }

        if (result.isGS1) {
            typeName += " (GS1)";
        }

        String finalTypeName = barcode;


        final String userid = finalTypeName.replace("*", "");
        if (decodeHandler != null) {
            decodeHandler.sendEmptyMessage(ID_RESTART_PREVIEW);
            String arrUserId[] = userid.split("\n");
            getScanBudgetDetail(arrUserId[0]);
            Log.d("data found", arrUserId[0]);
        }
              /*      }
                })
                .setTitle(typeName)
                .setMessage(barcode)
                .setNegativeButton("Close", null)
                .show();*/

        /* Analytics */
        /*
         * Replace "TestTag" in order to send custom tag.
         */
        /*
         * if (USE_MWANALYTICS) { if
         * (ContextCompat.checkSelfPermission(BudgetScanActivity.this,
         * Manifest.permission.ACCESS_FINE_LOCATION) !=
         * PackageManager.PERMISSION_GRANTED) { encResult =
         * result.encryptedResult; tName = typeName;
         *
         * if
         * (ActivityCompat.shouldShowRequestPermissionRationale(BudgetScanActivity.
         * this, Manifest.permission.ACCESS_FINE_LOCATION)) {
         * MWBAnalytics.MWB_sendReport(result.encryptedResult, typeName,
         * analyticsTag); } else {
         * ActivityCompat.requestPermissions(BudgetScanActivity.this, new String[]
         * { Manifest.permission.ACCESS_FINE_LOCATION }, 12333); } } else { if
         * (encResult != null) { encResult = null; tName = null; }
         * MWBAnalytics.MWB_sendReport(result.encryptedResult, typeName,
         * analyticsTag); } }
         */
    }

    private void initCamera() {

        if (ContextCompat.checkSelfPermission(BudgetScanActivity.this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            /* WHEN TARGETING ANDROID 6 OR ABOVE, PERMISSION IS NEEDED */
            if (ActivityCompat.shouldShowRequestPermissionRationale(BudgetScanActivity.this,
                    Manifest.permission.CAMERA)) {

                new AlertDialog.Builder(BudgetScanActivity.this).setMessage("You need to allow access to the Camera")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                ActivityCompat.requestPermissions(BudgetScanActivity.this,
                                        new String[]{Manifest.permission.CAMERA}, 122346);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onBackPressed();
                    }
                }).create().show();
            } else {
                ActivityCompat.requestPermissions(BudgetScanActivity.this, new String[]{Manifest.permission.CAMERA},
                        12322);
            }
        } else {
            try {
                // Select desired camera resoloution. Not all devices
                // supports all
                // resolutions, closest available will be chosen
                // If not selected, closest match to screen resolution will
                // be
                // chosen
                // High resolutions will slow down scanning proccess on
                // slower
                // devices

                if (MAX_THREADS > 2 || PDF_OPTIMIZED) {
                    CameraManager.setDesiredPreviewSize(1280, 720);
                } else {
                    CameraManager.setDesiredPreviewSize(800, 480);
                }

                CameraManager.get().openDriver(surfaceHolder,
                        (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT));

                int maxZoom = CameraManager.get().getMaxZoom();
                if (maxZoom < 100) {
                    zoomButton.setVisibility(View.GONE);
                } else {
                    zoomButton.setVisibility(View.VISIBLE);
                    if (maxZoom < 300) {
                        secondZoom = maxZoom;
                        firstZoom = (maxZoom - 100) / 2 + 100;

                    }

                }
            } catch (IOException ioe) {
                displayFrameworkBugMessageAndExit(ioe.getMessage());
                return;
            } catch (RuntimeException e) {
                // Barcode Scanner has seen crashes in the wild of this
                // variety:
                // java.?lang.?RuntimeException: Fail to connect to camera
                // service
                displayFrameworkBugMessageAndExit(e.getMessage());
                return;
            }

            Log.i("preview", "start preview.");

            flashOn = false;

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    switch (zoomLevel) {
                        case 0:
                            CameraManager.get().setZoom(100);
                            break;
                        case 1:
                            CameraManager.get().setZoom(firstZoom);
                            break;
                        case 2:
                            CameraManager.get().setZoom(secondZoom);
                            break;

                        default:
                            break;
                    }

                }
            }, 300);
            CameraManager.get().startPreview();
            restartPreviewAndDecode();
            updateFlash();
        }
    }


  /*  public void handleDecode(BarcodeScanner.MWResult result) {

        String typeName = result.typeName;
        String barcode = result.text;
        String barCodeSt = barcode.replace("*", "");
        //   getScanBudgetDetail(barCodeSt);

        *//* Parser *//*
     *//*
     * Parser result handler. Edit this code for custom handling of the
     * parser result. Use MWParser.MWPgetJSON(MWPARSER_MASK,
     * result.encryptedResult.getBytes()); to get JSON formatted result
     *//*
        if (MWPARSER_MASK != MWParser.MWP_PARSER_MASK_NONE &&
                BarcodeScanner.MWBgetResultType() ==
                        BarcodeScanner.MWB_RESULT_TYPE_MW) {

            barcode = MWParser.MWPgetFormattedText(MWPARSER_MASK,
                    result.encryptedResult.getBytes());
            if (barcode == null) {
                String parserMask = "";

                switch (MWPARSER_MASK) {
                    case MWParser.MWP_PARSER_MASK_AAMVA:
                        parserMask = "AAMVA";
                        break;
                    case MWParser.MWP_PARSER_MASK_GS1:
                        parserMask = "GS1";
                        break;
                    case MWParser.MWP_PARSER_MASK_ISBT:
                        parserMask = "ISBT";
                        break;
                    case MWParser.MWP_PARSER_MASK_IUID:
                        parserMask = "IUID";
                        break;
                    case MWParser.MWP_PARSER_MASK_HIBC:
                        parserMask = "HIBC";
                        break;
                    case MWParser.MWP_PARSER_MASK_SCM:
                        parserMask = "SCM";
                        break;

                    default:
                        parserMask = "unknown";
                        break;
                }

                barcode = result.text + "\n*Not a valid " + parserMask +
                        " formatted barcode";
            }

        }
        *//* Parser *//*


        if (result.locationPoints != null && CameraManager.get().

                getCurrentResolution()
                != null
                && OVERLAY_MODE == BudgetScanActivity.OverlayMode.OM_MWOVERLAY)

        {
            MWOverlay.showLocation(result.locationPoints.points, result.imageWidth, result.imageHeight);
        }

        if (result.isGS1) {
            typeName += " (GS1)";
        }

        String finalTypeName = barcode;


        final String userid = finalTypeName.replace("*", "");
      *//*  new AlertDialog.Builder(this)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {*//*
        if (decodeHandler != null) {
            decodeHandler.sendEmptyMessage(ID_RESTART_PREVIEW);
           *//* if (type_click.equalsIgnoreCase("budge")) {*//*
                String arrUserId[] = userid.split("\n");
                *//*  if(arrUserId[0]) {*//*
                getScanBudgetDetail(arrUserId[0]);

           *//* }*//* *//*else {
                if (userid.contains("Not a valid AAMVA formatted barcode")) {
                    Toast.makeText(getApplicationContext(), "Not a valid format", Toast.LENGTH_SHORT).show();
                } else {

                    Log.d("data found", userid);

                    String responseArray[] = userid.split("\n");
                    keyScanList = new ArrayList<String>();
                    valueScanList = new ArrayList<String>();
                    for (int i = 0; i < 15; i++) {
                        String newSt[] = responseArray[i].split(":");
                        stringMap.put(newSt[0], newSt[1]);
                    }

                    String name = stringMap.get("Driver License Name ");
                    name = name.replace(" ", "");
                    String newarr[] = name.split(" ");

                    String arrayName[] = newarr[0].split(",");

                    Intent intent = new Intent(getApplicationContext(), *//**//*ScanUserDetailActivity*//**//*AddNewVisitorActivity.class);


                    intent.putExtra("firstName", arrayName[0]);
                    if (arrayName.length == 4) {
                        try {
                            intent.putExtra("middleName", arrayName[1]);
                        } catch (Exception ex) {
                            intent.putExtra("middleName", "");
                        }

                        try {
                            intent.putExtra("lastName", arrayName[2]);
                        } catch (Exception ex) {
                            intent.putExtra("lastName", "");
                        }
                    } else if (arrayName.length == 3) {
                        try {
                            intent.putExtra("middleName", arrayName[1]);
                        } catch (Exception ex) {
                            intent.putExtra("middleName", "");
                        }

                        try {
                            intent.putExtra("lastName", arrayName[2]);
                        } catch (Exception ex) {
                            intent.putExtra("lastName", "");
                        }
                    } else if (arrayName.length == 2) {
                        try {
                            intent.putExtra("lastName", arrayName[1]);
                            intent.putExtra("middleName", "");
                        } catch (Exception ex) {
                            intent.putExtra("lastName", "");
                        }
                    } else {
                        intent.putExtra("lastName", "");
                        intent.putExtra("middleName", "");
                    }

                    try {
                        if (newarr[0].contains(",")) {
                            if (arrayName.length == 2) {
                                intent.putExtra("firstName", arrayName[0]);
                                intent.putExtra("lastName", arrayName[1]);
                            } else if (arrayName.length == 3) {
                                intent.putExtra("firstName", arrayName[0]);
                                intent.putExtra("middleName", arrayName[1]);
                                intent.putExtra("lastName", arrayName[2]);
                            } else {
                                intent.putExtra("firstName", arrayName[0]);
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    String dobSt = stringMap.get("Date Of Birth ");
                    String DOB = "";

                   *//**//* char arr[] = dobSt.toCharArray();
                    String fullYear = arr[1] + "" + arr[2] + "" + arr[3] + "" + arr[4];
                    String fullMonth = arr[5] + "" + arr[6];
                    String fullDay = arr[7] + "" + arr[8];

                    DOB = fullYear + "-" + fullMonth + "-" + fullDay;*//**//*

                    if (!stringMap.containsKey("Jurisdiction-specific vehicleclass")) {

                        try {
                            char arr[] = dobSt.toCharArray();
                            String fullYear = arr[1] + "" + arr[2] + "" + arr[3] + "" + arr[4];
                            String fullMonth = arr[5] + "" + arr[6];
                            String fullDay = arr[7] + "" + arr[8];


                            DOB = fullYear + "-" + fullMonth + "-" + fullDay;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    } else {
                        try {
                            char arr[] = dobSt.toCharArray();
                            String fullMonth = arr[1] + "" + arr[2];
                            String fullDay = arr[3] + "" + arr[4];
                            String fullYear = arr[5] + "" + arr[6] + "" + arr[7] + "" + arr[8];
                  *//**//*          String fullYear = arr[1] + "" + arr[2] + "" + arr[3] + "" + arr[4];
                            String fullMonth = arr[5] + "" + arr[6];
                            String fullDay = arr[7] + "" + arr[8];*//**//*


                            DOB = fullYear + "-" + fullMonth + "-" + fullDay;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                    intent.putExtra("dob", DOB);

                    String city = stringMap.get("Address - City ");

                    if (stringMap.containsKey("Driver License Name ")) {
                        intent.putExtra("driving_lisence_yes_no", stringMap.get("Driver License Classification Code "));
                    }
                    if (stringMap.containsKey("Customer Id Number ")) {
                        intent.putExtra("visitor_id_number", stringMap.get("Customer Id Number "));
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }*//*
        }
              *//*      }
                })
                .setTitle(typeName)
                .setMessage(barcode)
                .setNegativeButton("Close", null)
                .show();*//*

     *//* Analytics *//*
     *//*
     * Replace "TestTag" in order to send custom tag.
     *//*
     *//*
     * if (USE_MWANALYTICS) { if
     * (ContextCompat.checkSelfPermission(BudgetScanActivity.this,
     * Manifest.permission.ACCESS_FINE_LOCATION) !=
     * PackageManager.PERMISSION_GRANTED) { encResult =
     * result.encryptedResult; tName = typeName;
     *
     * if
     * (ActivityCompat.shouldShowRequestPermissionRationale(BudgetScanActivity.
     * this, Manifest.permission.ACCESS_FINE_LOCATION)) {
     * MWBAnalytics.MWB_sendReport(result.encryptedResult, typeName,
     * analyticsTag); } else {
     * ActivityCompat.requestPermissions(BudgetScanActivity.this, new String[]
     * { Manifest.permission.ACCESS_FINE_LOCATION }, 12333); } } else { if
     * (encResult != null) { encResult = null; tName = null; }
     * MWBAnalytics.MWB_sendReport(result.encryptedResult, typeName,
     * analyticsTag); } }
     *//*
    }*/

    private void displayFrameworkBugMessageAndExit(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getIdentifier("app_name", "string", package_name));
        builder.setMessage(MSG_CAMERA_FRAMEWORK_BUG + message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }

    protected void recycleOverlayImage() {

        if (OVERLAY_MODE == BudgetScanActivity.OverlayMode.OM_IMAGE) {
            ((ImageView) imageOverlay.findViewById(R.id.imageOverlay)).setImageDrawable(getResources().getDrawable(R.drawable.overlay));
        } else {

            Drawable imageDrawable = imageOverlay.getDrawable();
            imageOverlay.setImageDrawable(null);

            if (imageDrawable != null && imageDrawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageDrawable);

                if (!bitmapDrawable.getBitmap().isRecycled()) {
                    bitmapDrawable.getBitmap().recycle();
                }
            }
        }
    }

    public void getScanBudgetDetail(String eventId) {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getScanEventBudget(APIServerResponse.EVENT_DETAIL, getAuthToken(), eventId, getUserID(), BudgetScanActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void checkWatchList(String firstName, String lastName, String dob) {
        if (isConnectedToInternet()) {
            showLoading();
            CheckWatchListRequestModel model = new CheckWatchListRequestModel();
            model.setFirst_name(firstName);
            model.setClients_id(getClientId());
            model.setDob(dob);
            model.setLast_name(lastName);
            ServerAPI.getInstance().checkWatchListExistence(APIServerResponse.CHECK_IN_WATCH_LIST, getAuthToken(), model, BudgetScanActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void successPage(String textSt, String checkType) {

        if (dialogShown) {
            return;
        } else {
            dialogShown = true;
            dialog = new Dialog(BudgetScanActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_green_successfull);
            dialog.setTitle(null);
            TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
            textView.setText(textSt + "");
            Window window = dialog.getWindow();
            window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(true);
            dialog.setTitle(null);
            dialog.show();
            Handler handler = new Handler();
            if (checkType.equalsIgnoreCase("watchlist")) {
                watchFlag = true;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialogShown = false;
                        dialog.dismiss();
                        if (isConnectedToInternet()) {
                            showLoading();
                            ServerAPI.getInstance().checkSexOffenderExistence(APIServerResponse.CHECK_SEX_OFFENDER, getAuthToken(), firstName, lastName, dob, getClientId(), BudgetScanActivity.this);
                        } else {
                            showSnack(Constants.INTERNET_CONNECTION);
                        }
                    }
                }, 1000);
            } else {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        soFlag = true;
                        dialogShown = false;
                        dialog.dismiss();
                        Intent intent = new Intent(getApplicationContext(), EventsActivity.class);
                        intent.putExtra("eventDetail", eventObject.toString());
                        startActivity(intent);
                    }
                }, 1000);
            }
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful() || String.valueOf(response.code()).contains("2")) {
            try {
                switch (tag) {
                    case APIServerResponse.EVENT_DETAIL:
                        JsonObject scanResult = (JsonObject) response.body();
                        /*{"status":"OK","event":{"id":498,"event_name":"test event","clients_id":31,
                        "user_id":1,"event_id":64,"first_name":"newguest","created_at":"2018-02-21 13:36:17",
                        "last_name":"thakur","company_name":"abc ltd","email":"guestthakur@yopmail.com",
                        "phone":"9896780139","attended":"PENDING","profile_pic_web":"","dob":"2010-02-16","eventGuestId":"SSD10134"}}*/
                        if (scanResult.get("status").toString().equalsIgnoreCase("\"" + "OK" + "\"")) {
                            firstName = scanResult.getAsJsonObject("event").get("first_name").getAsString();
                            lastName = scanResult.getAsJsonObject("event").get("last_name").getAsString();
                            dob = scanResult.getAsJsonObject("event").get("dob").getAsString();
                            eventObject = scanResult.getAsJsonObject("event");


                            checkWatchList(firstName, lastName, dob);


                          /*  Intent intent = new Intent(getApplicationContext(), EventsActivity.class);
                            intent.putExtra("eventDetail", scanResult.getAsJsonObject("event").toString());
                            startActivity(intent);*/
                        } else {
                            Toast.makeText(getApplicationContext(), "" + scanResult.get("message").toString(), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case CHECK_IN_WATCH_LIST:
                        WatchListCheck watchListCheck = (WatchListCheck) response.body();
                        hideLoading();
                        /*{"status":"OK","event":{"id":498,"event_name":"test event","clients_id":31,
                        "user_id":1,"event_id":64,"first_name":"newguest","created_at":"2018-02-21 13:36:17",
                        "last_name":"thakur","company_name":"abc ltd","email":"guestthakur@yopmail.com",
                        "phone":"9896780139","attended":"PENDING",
                        "profile_pic_web":"","dob":"2010-02-16","eventGuestId":"SSD10134"}}*/
                        if (watchListCheck.getStatus().equalsIgnoreCase("OK")) {
                            if (watchListCheck.getMatches().size() != 0) {
                                intent = new Intent(getApplicationContext(), WatchListDetailActivity.class);
                                List<WatchListMatchModal> offendersList = watchListCheck.getMatches();
                                bundle = new Bundle();
                                bundle.putSerializable("match_list", (Serializable) offendersList);
                                bundle.putString("first_name", firstName);
                                bundle.putString("last_name", lastName);
                                bundle.putString("age", dob);
                                bundle.putString("phNumber", eventObject.get("phone").getAsString());
                                bundle.putString("email", eventObject.get("email").getAsString());
                                bundle.putBoolean("cbFlag", cbFlag);
                                bundle.putBoolean("soFlag", soFlag);
                                bundle.putString("finalDate", dob);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String myDate = format.format(new Date());
                                bundle.putString("timing", localToGMT(myDate));

                                //kanchan ****    to check event page or visitor info call

                                bundle.putString("openPage", "eventCall");

                                if (!eventObject.get("profile_pic_web").getAsString().equalsIgnoreCase("")) {
                                    resultSt = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, eventObject.get("profile_pic_web").getAsString(), new APIServerResponse() {
                                        @Override
                                        public void onSuccess(int tag, Response response) {
                                            if (response.isSuccessful()) {
                                                intent.putExtra("profileUrl", resultSt);
                                                intent.putExtras(bundle);
                                                startActivity(intent);
                                            }
                                        }

                                        @Override
                                        public void onError(int tag, Throwable throwable) {
                                            throwable.printStackTrace();
                                        }
                                    });
                                } else {
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }

                            } else {
                                successPage("WatchList clear ", "watchlist");
                            }
                        } else {
                            successPage("WatchList clear ", "watchlist");
                        }
                        break;

                    case CHECK_SEX_OFFENDER:
                        SOResponse sexOffender = (SOResponse) response.body();
                        hideLoading();
                        if (sexOffender.getStatus().equalsIgnoreCase("OK")) {

                            if (sexOffender.getSOData().getOffenders().size() != 0) {
                                intent = new Intent(getApplicationContext(), SoOffenderActivity.class);
                                List<Offenders> offendersList = sexOffender.getSOData().getOffenders();
                                bundle = new Bundle();
                                bundle.putSerializable("offender_array", (Serializable) offendersList);
                                bundle.putString("first_name", firstName);
                                bundle.putString("last_name", lastName);
                                bundle.putString("age", dob);
/*{"status":"OK","event":{"id":498,"event_name":"test event","clients_id":31,
                        "user_id":1,"event_id":64,"first_name":"newguest","created_at":"2018-02-21 13:36:17",
                        "last_name":"thakur","company_name":"abc ltd","email":"guestthakur@yopmail.com",
                        "phone":"9896780139","attended":"PENDING",
                        "profile_pic_web":"","dob":"2010-02-16","eventGuestId":"SSD10134"}}*/
                                bundle.putString("phNumber", eventObject.get("phone").getAsString());
                                bundle.putString("email", eventObject.get("email").getAsString());
                                bundle.putBoolean("cbFlag", cbFlag);
                                bundle.putBoolean("soFlag", soFlag);
                                bundle.putString("finalDate", dob);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String myDate = format.format(new Date());
                                bundle.putString("timing", localToGMT(myDate));
                                if (!eventObject.get("profile_pic_web").getAsString().equalsIgnoreCase("")) {
                                    resultSt = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, eventObject.get("profile_pic_web").getAsString(), new APIServerResponse() {
                                        @Override
                                        public void onSuccess(int tag, Response response) {
                                            if (response.isSuccessful()) {
                                                intent.putExtra("profileUrl", resultSt);
                                                intent.putExtras(bundle);
                                                startActivity(intent);
                                            }
                                        }

                                        @Override
                                        public void onError(int tag, Throwable throwable) {
                                            throwable.printStackTrace();
                                        }
                                    });
                                } else {
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }

                            } else {
                                successPage("Sex Offender check Cleared!", "SO");
                            }

                        } else {
                            successPage("WatchList clear ", "watchlist");
                        }
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(getApplicationContext(), "Please check your Internet Connectivity", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }


    private enum State {
        STOPPED, PREVIEW, DECODING
    }

    private enum OverlayMode {
        OM_IMAGE, OM_MWOVERLAY, OM_NONE
    }


}
