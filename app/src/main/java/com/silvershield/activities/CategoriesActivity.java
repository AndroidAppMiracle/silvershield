package com.silvershield.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.silvershield.R;
import com.silvershield.adapter.CategoriesListingAdapter;
import com.silvershield.modal.CategoriesDetail;
import com.silvershield.modal.GetCategoryResponse;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class CategoriesActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;

    static String myCat = "";
    @BindView(R.id.all_categories_rv)
    RecyclerView all_categories_rv;
    List<CategoriesDetail> arrayList = arrayList = new ArrayList<>();
    CategoriesListingAdapter categoriesListingAdapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);
        if (showAlertIcon())
            home_img.setVisibility(View.VISIBLE);
        else
            home_img.setVisibility(View.GONE);
        title_tv.setText("Categories");
        logout_btn.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        linearLayoutManager = new LinearLayoutManager(this);
        all_categories_rv.setLayoutManager(linearLayoutManager);
        getAllCategories();
    }

    @OnClick(R.id.home_tv)
    public void backCall() {
        CategoriesActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        CategoriesActivity.this.finish();
    }

    public void getAllCategories() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllCategories(APIServerResponse.GETALLCATEGORIES, getAuthToken(), getClientId(), CategoriesActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();

        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GETALLCATEGORIES:
                        GetCategoryResponse getCategoryResponse = (GetCategoryResponse) response.body();
                        arrayList.clear();
                        if (getCategoryResponse.getStatus().equalsIgnoreCase("OK")) {
                            arrayList = getCategoryResponse.getCategories();
                            if (arrayList.size() != 0) {
                                /*ListAdapter listAdapter = new com.silvershield.adapter.ListAdapter(getApplicationContext(), R.id.list_title_tv, arrayList);
                                all_categories_rv.setAdapter(listAdapter);*/

                                categoriesListingAdapter = new CategoriesListingAdapter(getApplicationContext(), null, arrayList, myCat, null);
                                all_categories_rv.setAdapter(categoriesListingAdapter);
                                categoriesListingAdapter.notifyDataSetChanged();
                            } else {
                                Toast.makeText(getApplicationContext(), "No values found", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                        }
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }
}
