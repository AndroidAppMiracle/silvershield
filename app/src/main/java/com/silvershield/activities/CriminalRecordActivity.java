package com.silvershield.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.CriminalBackgroundListAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Offenders;
import com.silvershield.modal.SOResponse;
import com.silvershield.modal.StatusMessageModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;
import com.silvershield.utils.RecyclerViewClick;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class CriminalRecordActivity extends BaseActivity implements RecyclerViewClick, APIServerResponse {

    @BindView(R.id.offender_name_tv)
    Custome_Regular_TextView offender_name_tv;
    @BindView(R.id.main_profile_img)
    ImageView main_profile_img;
    @BindView(R.id.list_criminal_rv)
    RecyclerView list_criminal_rv;
    @BindView(R.id.stop_btn)
    Button stop_btn;

    //home_img
    @BindView(R.id.title_tv)
    TextView title;
    @BindView(R.id.home_tv)
    ImageView home_img;

    public static String selectedItem = "0";
    public static String matchUrl = "";
    public static String matchId = "0";
    public static String profileUrl = "";
    String recordsList;
    String firstName = "", lastName = "", phNumber = "", dobSt = "", emailSt = "",
            entryFrom = "", purpose_id = "", suffixChoose = "", docTypeId = "", finalDate = "",
            visitorIdNumber = "", visiting_individual_id = "", visiting_department_id = "",
            vms_visitor_entering_from_id = "", vms_visitor_purpose_id = "", timingST = "",
            country_idSt = "";


    JSONArray jsonArray;
    public static JSONObject jsonObject;
    JsonObject fullJsonObject;
    Boolean cbFlag = false, soFlag = false;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ctiminal_record);
        ButterKnife.bind(this);

        if (showAlertIcon())
            home_img.setVisibility(View.VISIBLE);
        else
            home_img.setVisibility(View.GONE);
        jsonObject = new JSONObject();
        Bundle bundle = getIntent().getExtras();
        selectedItem = "0";

        profileUrl = getIntent().getStringExtra("profileUrl");

        recordsList = bundle.getString("resultObject");
        firstName = bundle.getString("first_name");
        lastName = bundle.getString("last_name");

        cbFlag = bundle.getBoolean("cbFlag");
        soFlag = bundle.getBoolean("soFlag");

        phNumber = bundle.getString("phNumber");
        dobSt = bundle.getString("dob");
        emailSt = bundle.getString("email");
        entryFrom = bundle.getString("entery_from");
        title.setText("Criminal Background Check");
        purpose_id = bundle.getString("purpose_id");
        suffixChoose = bundle.getString("suffixChoose");
        docTypeId = bundle.getString("docTypeId");
        finalDate = bundle.getString("finalDate");
        visitorIdNumber = bundle.getString("visitorIdNumber");
        visiting_individual_id = bundle.getString("visiting_individual_id");
        visiting_department_id = bundle.getString("visiting_department_id");
        vms_visitor_entering_from_id = bundle.getString("vms_visitor_entering_from_id");
        vms_visitor_purpose_id = bundle.getString("vms_visitor_purpose_id");
        timingST = bundle.getString("timing_et");
        country_idSt = bundle.getString("country_idSt");
        matchId = bundle.getString("log_id");


        offender_name_tv.setText(lastName + ", " + firstName);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.back));
        try {
            JSONObject jsonObject = new JSONObject(recordsList);
            jsonArray = jsonObject.getJSONArray("Records");
            CriminalBackgroundListAdapter criminalBackgroundListAdapter = new CriminalBackgroundListAdapter(CriminalRecordActivity.this, jsonArray, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            list_criminal_rv.setLayoutManager(linearLayoutManager);
            list_criminal_rv.setAdapter(criminalBackgroundListAdapter);

            criminalBackgroundListAdapter.notifyDataSetChanged();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!getIntent().getStringExtra("profileUrl").equalsIgnoreCase("")) {
            downloadImage();
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.dummy_users);
            requestOptions.error(R.drawable.dummy_users);
            Glide.with(this).setDefaultRequestOptions(requestOptions).load(getIntent().getStringExtra("profileUrl")).into(main_profile_img);
        }
    }


    @OnClick(R.id.continue_btn)
    public void continue_btnClick() {
        if (soFlag) {
            createVisitor();
        } else {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().checkSexOffenderExistence(APIServerResponse.CHECK_SEX_OFFENDER, getAuthToken(), firstName, lastName, dobSt, /*getClientId()*/getClientId(), CriminalRecordActivity.this);
            } else {
                showSnack(Constants.INTERNET_CONNECTION);
            }
        }
    }


    public void createVisitor() {
        ServerAPI.getInstance().createVisitor(APIServerResponse.CREATE_VISITOR, getAuthToken(),
                firstName, lastName, "",
                suffixChoose, emailSt, phNumber, finalDate,
                getUserID(), getParentIdValue(), docTypeId, visitorIdNumber,
                timingST, getUserID(), visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id,
                vms_visitor_purpose_id, "", profileUrl, getClientId(), "", "",
                "", country_idSt, "0", getUserID(), matchId, CriminalRecordActivity.this);
    }

    @OnClick(R.id.home_tv)
    public void backButtonClick() {
        CriminalRecordActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CriminalRecordActivity.this.finish();
    }

    @OnClick(R.id.stop_btn)
    public void stopButtonClick() {
        try {
            if (selectedItem.equals("0")) {
                Toast.makeText(getApplicationContext(), "Please select one person from the st", Toast.LENGTH_SHORT).show();
            } else {
                // downloadImage();

                if (isConnectedToInternet()) {
                    showLoading();

                   /* if (jsonObject.has("Photo")) {
                        photoSt = jsonObject.getString("Photo").toString();
                    } else {
                        photoSt = "";
                    }*/

                    if (jsonObject.get("DOB").toString().toString() == null) {
                        dobSt = "";
                    } else {
                        dobSt = jsonObject.getString("DOB").toString();
                    }

                    ServerAPI.getInstance().stopVisitor(APIServerResponse.STOP_VISITOR, getAuthToken(), getUserID(), getParentIdValue(),
                            getClientId(), entryFrom, jsonObject.get("FirstName").toString() +
                                    jsonObject.get("LastName").toString(),
                            String.valueOf(jsonArray.length()), "CB", getFirstName() + "" + getLastName(),
                            firstName + "" + lastName, matchUrl
                            , jsonObject.get("FirstName").toString(),
                            jsonObject.get("LastName").toString(), emailSt, phNumber, dobSt, profileUrl, matchId, CriminalRecordActivity.this);

                } else {
                    showSnack(Constants.INTERNET_CONNECTION);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(getApplicationContext(), "Please select one person from the array", Toast.LENGTH_SHORT).show();
        }
    }


    public void downloadImage() {

        showLoading();
        if (profileUrl.equalsIgnoreCase("")) {

        } else {
            profileUrl = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, profileUrl, CriminalRecordActivity.this);
        }
        hideLoading();

    }

    @Override
    public void productClick(View v, int position) {
        int itemClickedPostion = list_criminal_rv.getChildPosition(v);
        try {
            jsonObject = jsonArray.getJSONObject(itemClickedPostion);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    AlertDialog.Builder builder;

    @OnClick(R.id.criminal_btn)
    public void checkSb() {

        if (!cbFlag) {
            builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.add_new_user_alert_popup_string))
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().checkCB(APIServerResponse.CHECK_CB, getAuthToken(), firstName, lastName, dobSt,
                                        emailSt, phNumber, getUserID(), profileUrl, getClientId(),
                                        getParentIdValue(), getUserID(), CriminalRecordActivity.this);
                            } else {
                                showSnack(Constants.INTERNET_CONNECTION);
                            }
                        }
                    })
                    .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            Toast.makeText(getApplicationContext(), "CBC already check", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case CHECK_CB:
                        hideLoading();
                        fullJsonObject = (JsonObject) response.body();
                        cbFlag = true;
                        if (fullJsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            cbFlag = true;
                            successPage("Criminal Background clear", "criminal");
                        } else {

                        }
                        break;
                    case STOP_VISITOR:
                        hideLoading();
                        StatusMessageModel jsonObject = (StatusMessageModel) response.body();
                        if (jsonObject.getStatus().equalsIgnoreCase("OK")) {
                            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "unable to stop ", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case CHECK_SEX_OFFENDER:
                        SOResponse sexOffender = (SOResponse) response.body();
                        hideLoading();
                        if (sexOffender.getStatus().equalsIgnoreCase("OK")) {
                            soFlag = true;
                            if (sexOffender.getSOData().getOffenders().size() != 0) {
                                Intent intent = new Intent(getApplicationContext(), SoOffenderActivity.class);
                                List<Offenders> offendersList = sexOffender.getSOData().getOffenders();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("offender_array", (Serializable) offendersList);
                                bundle.putString("first_name", firstName);
                                bundle.putString("last_name", lastName);
                                bundle.putString("age", dobSt);
                                bundle.putString("profile_img", profileUrl);
                                bundle.putString("phNumber", phNumber);
                                bundle.putString("email", emailSt);
                                bundle.putBoolean("cbFlag", cbFlag);
                                bundle.putBoolean("soFlag", soFlag);

                                bundle.putString("purpose_id", vms_visitor_purpose_id);
                                bundle.putString("suffixChoose", suffixChoose);
                                bundle.putString("docTypeId", docTypeId);
                                bundle.putString("finalDate", finalDate);
                                bundle.putString("visitorIdNumber", visitorIdNumber);
                                bundle.putString("visiting_individual_id", visiting_individual_id);
                                bundle.putString("visiting_department_id", visiting_department_id);
                                bundle.putString("vms_visitor_entering_from_id", vms_visitor_entering_from_id);
                                bundle.putString("vms_visitor_purpose_id", vms_visitor_purpose_id);
                                bundle.putString("timing_et", timingST);
                                /*       bundle.putString("profileUrl",profileUrl);*/
                                bundle.putString("country_idSt", country_idSt);
                                bundle.putString("log_id", matchId);

                                intent.putExtras(bundle);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                soFlag = true;
                                successPage("Sex Offender Clear ", "SO");
                            }
                        }
                        break;
                    case CREATE_VISITOR:
                        JsonObject jsonObject1 = (JsonObject) response.body();

                        if (jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                            Intent intent = new Intent(getApplicationContext(), VisitorInfoActivity.class);
                            intent.putExtra("entered_by", jsonObject1.get("entered_by").getAsString());
                            intent.putExtra("purpose", jsonObject1.get("visit_purpose").getAsString());
                            intent.putExtra("visitinDept", jsonObject1.get("department_name").getAsString());
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm a", Locale.getDefault());
                            String myDate = format.format(new Date());
                            intent.putExtra("timing", jsonObject1.get("check_in").getAsString());
                            intent.putExtra("email", jsonObject1.get("email").getAsString());
                            intent.putExtra("phone", jsonObject1.get("phone").getAsString());
                            intent.putExtra("firstName", jsonObject1.get("first_name").getAsString());
                            intent.putExtra("lastName", jsonObject1.get("last_name").getAsString());
                            intent.putExtra("middleName", jsonObject1.get("middle_name").getAsString());
                            intent.putExtra("logId", jsonObject1.get("log_id").getAsString());
                            intent.putExtra("profileUrl", profileUrl);
                            intent.putExtra("object", String.valueOf(jsonObject1));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            alertWithSingleTitle("Alert",  jsonObject1.get("message").getAsString(),"error");
                        }
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.","error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }


    public void successPage(String textSt, String checkType) {
        dialog = new Dialog(CriminalRecordActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_green_successfull);
        dialog.setTitle(null);
        TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
        textView.setText(textSt + "");
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();
            }
        }, 1000);

    }
}
