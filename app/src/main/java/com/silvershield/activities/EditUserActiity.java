package com.silvershield.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.silvershield.R;
import com.silvershield.adapter.CountryNamesAdapter;
import com.silvershield.adapter.DepartmentsAdapter;
import com.silvershield.adapter.EntryFromAdapter;
import com.silvershield.adapter.PurposeAdapter;
import com.silvershield.adapter.SuffixAdapter;
import com.silvershield.adapter.VisitIndividualAdapter;
import com.silvershield.adapter.VisitorDocTypeAdapter;
import com.silvershield.modal.Countries;
import com.silvershield.modal.Departments;
import com.silvershield.modal.Docs_type;
import com.silvershield.modal.Entry_from;
import com.silvershield.modal.GetSuffixDoctypeResponse;
import com.silvershield.modal.Individuals;
import com.silvershield.modal.Matches;
import com.silvershield.modal.Purpose;
import com.silvershield.modal.Suffix;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class EditUserActiity extends BaseActivity implements APIServerResponse, ImagePickerCallback, EasyPermissions.PermissionCallbacks {


    private static final int RC_CAMERA_PERM = 342;
    public static String pickerPath = "";
    public static String suffixId, suffixIdvalue, docTypeId,
            visitor_id_number, visiting_individual_id,
            visiting_department_id, vms_visitor_entering_from_id,
            vms_visitor_purpose_id, vms_visitor_other_purpose,
            country_idSt, enteredBy, checkedIn, visit_purpose_st, visitorId, log_id = "", visitor_id_document_type;
    static boolean watchFlag = false, soFlag = false, cbFlag = false;
    private final int requestCode = 20;
    public int responseCode = 101;
    public String vipSt = "";
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.profile_img)
    ImageView profile_img;
    @BindView(R.id.etDOB)
    EditText etDOB;
    SimpleDateFormat format;
    Dialog dialog;
    @BindView(R.id.suffix_choose_et)
    EditText suffix_choose_et;
    @BindView(R.id.doct_type)
    EditText visitor_dept_id_et;
    @BindView(R.id.visitor_id_number)
    EditText visitor_id_number_et;
    @BindView(R.id.country_origin)
    EditText country_origin_et;
    @BindView(R.id.timing_et)
    EditText timing_et;
    @BindView(R.id.visiting_dept_et)
    EditText visiting_dept_et;
    @BindView(R.id.entry_form_et)
    EditText entry_form_et;
    @BindView(R.id.purpose_of_visit)
    EditText purpose_of_visit_et;
    @BindView(R.id.email_et)
    EditText email_et;
    @BindView(R.id.ph_number_et)
    EditText ph_number_et;
    @BindView(R.id.first_name_et)
    EditText first_name_et;
    //last_name_et
    @BindView(R.id.last_name_et)
    EditText last_name_et;
    @BindView(R.id.mid_name_et)
    EditText mid_name_et;
    //submit_btn
    @BindView(R.id.submit_btn)
    Button submit_btn;
    @BindView(R.id.three_btn_ll)
    LinearLayout three_btn_ll;
    @BindView(R.id.local_code_number)
    TextView local_code_number;
    //three_btn_ll
    EditText purposeEt, editText;
    String finalDate;
    AlertDialog.Builder builder;
    SuffixAdapter suffixAdapter;
    List<Suffix> suffixList;
    List<Docs_type> docs_types;
    List<Entry_from> entry_from;
    List<Departments> departmentsList;
    List<Countries> countriesList;
    @BindView(R.id.txt_search)
    AutoCompleteTextView txtSearch;
    List<Individuals> individualsList;
    VisitIndividualAdapter adapter;
    List<Purpose> purposesList;
    RecyclerView suggestion_user_rv;
    List<Matches> matchesList;
    Matches matchesObj;
    String profileUrl = "";
    Bitmap profileBitMap;
    Uri imageUrl;
    String jsonObject = "";

    DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //HH for hour of the day (0 - 23)
    DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd h:mm a");
    Calendar calendar = Calendar.getInstance();
    private CameraImagePicker cameraPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_actiity);
        ButterKnife.bind(this);
        title_tv.setText("Edit Visitor");
        if (showAlertIcon())
            home_img.setVisibility(View.VISIBLE);
        else
            home_img.setVisibility(View.GONE);

        showLoading();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                initVariable();
                settingIntent();
                getAllDetail();
                hideLoading();
            }
        }, 3000);

        visitor_dept_id_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // if (visitor_dept_id_et.getText().toString().equalsIgnoreCase("No ID Available")) {
                if (visitor_dept_id_et.getText().toString().equalsIgnoreCase("No ID Available")) {
                    visitor_id_number_et.setText("N/A");
                    visitor_id_number_et.setEnabled(false);

                } else {
                    visitor_id_number_et.setText("");
                    visitor_id_number_et.setEnabled(true);
                }
                // Handler handler = new Handler();
                try {
                    if (docs_types.size() != 0) {
                        for (int i = 0; i < docs_types.size(); i++) {
                            if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                                docTypeId = docs_types.get(i).getId();
                            } else if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                                docTypeId = docs_types.get(i).getId();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void initVariable() {
        watchFlag = false;
        soFlag = false;
        cbFlag = false;

        suffixId = "";
        docTypeId = "";
        visitor_id_number = "";
        visiting_individual_id = "";
        visiting_department_id = "";
        vms_visitor_entering_from_id = "";
        vms_visitor_purpose_id = "";
        vms_visitor_other_purpose = "";
        country_idSt = "";
        visit_purpose_st = "";

        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        logout_btn.setImageDrawable(getResources().getDrawable(R.mipmap.red_notification));

        purposeEt = (EditText) findViewById(R.id.purpose_of_visit);

        editText = (EditText) findViewById(R.id.doct_type);
        format = new SimpleDateFormat("HH:mm a", Locale.getDefault());
        String myDate = format.format(new Date());
        timing_et.setText(myDate);
    }

    public void downloadImage() {
        showLoading();
        if (profileUrl.equalsIgnoreCase("")) {
        } else {
            pickerPath = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, getIntent().getStringExtra("profileUrl"), new APIServerResponse() {
                @Override
                public void onSuccess(int tag, Response response) {
                    if (response.isSuccessful()) {
                        hideLoading();
                        if (!profileUrl.equalsIgnoreCase("")) {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.drawable.dummy_users);
                            requestOptions.error(R.drawable.dummy_users);
                            Glide.with(getApplicationContext()).load(getIntent().getStringExtra("profileUrl")).apply(requestOptions).into(profile_img);
                        }
                    }
                }

                @Override
                public void onError(int tag, Throwable throwable) {
                    hideLoading();
                    throwable.printStackTrace();
                }
            });
        }
    }

    public void settingIntent() {

        if (getIntent().hasExtra("jsonObject")) {
            jsonObject = getIntent().getStringExtra("jsonObject");
            /*{"id":9417,"visitor_suffix":"","first_name":"york","middle_name":"",
            "last_name":"verma","ssn":"0","dob":"2000-02-08","phone":"","email":"",
            "profile_pic":"","profile_pic_web":"https:\/\/silverstaging.s3.amazonaws.com\/uploads\/visitors\/null",
            "bar_code_image":"","is_deleted":0,"parent_id":670,
            "log_id":14345,"is_vip":0,"is_qb":0,
            "is_watch":0,"watch_list_reason":"MANUAL",
            "check_in":"2018-01-08 12:47:42","check_out":"2018-01-08 07:51:11",
            "entered_by":"admin","entered_by_firstname":"Robin",
            "entered_by_lastname":"Baker","updated_by":"admin",
            "visiting_individual_id":"","visiting_individual":"",
            "visiting_individual_firstname":"","visiting_individual_lastname":"",
            "visiting_department_id":166,"department_name":"IT","visit_purpose":"",
            "purpose_other":"","visitor_id_document_types_id":0,
            "visitor_id_document_type":"No ID Available","visitor_id_number":"",
            "vms_visitor_entering_from_id":"6","entery_place":"DLF Gate 2","country_id":"","country_code":"","country_name":""}*/
            try {
                JSONObject detailObject = new JSONObject(jsonObject);

                visitorId = detailObject.getString("id");
                first_name_et.setText(detailObject.getString("first_name"));
                mid_name_et.setText(detailObject.getString("middle_name"));
                last_name_et.setText(detailObject.getString("last_name"));
                email_et.setText(detailObject.getString("email"));//(getIntent().getStringExtra("phone").contains("+"))&&(getIntent().getStringExtra("phone").length()>1)) {
                suffixIdvalue = detailObject.getString("visitor_suffix");
                if (!suffixIdvalue.equalsIgnoreCase("")) {
                    suffix_choose_et.setText(suffixIdvalue + "");
                }
                try {
                    if ((!detailObject.getString("phone").equalsIgnoreCase("")) && (detailObject.getString("phone").length() > 7)) {
                        String fullPhone = detailObject.getString("phone");
                        String code = fullPhone.substring(0, 3);
                        String phone = detailObject.getString("phone").substring(3, 13);
                        local_code_number.setText(code);
                        ph_number_et.setText(phone);
                    } else {
                        local_code_number.setText("+" + getCountryDialCode(this) + "");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                if (!detailObject.getString("dob").equalsIgnoreCase("")) {
                    etDOB.setText(detailObject.getString("dob"));
                    finalDate = detailObject.getString("dob");
                }
                checkedIn = detailObject.getString("check_in");
                timing_et.setText("" + convertTiming(checkedIn));
                log_id = detailObject.getString("log_id");
                enteredBy = detailObject.getString("entered_by_firstname") + " " + detailObject.getString("entered_by_lastname");
                docTypeId = detailObject.getString("visitor_id_document_types_id");
                visiting_individual_id = detailObject.getString("visiting_individual_id");
                visiting_department_id = detailObject.getString("visiting_department_id");
                visitor_id_number = detailObject.getString("visitor_id_number");
                vms_visitor_entering_from_id = detailObject.getString("vms_visitor_entering_from_id");
                country_idSt = detailObject.getString("country_id");
                visit_purpose_st = detailObject.getString("visit_purpose");
                visitor_id_document_type = detailObject.getString("visitor_id_document_type");
                if (!visitor_id_number.equalsIgnoreCase("")) {
                    visitor_id_number_et.setText(visitor_id_number + "");
                }

                if (!visit_purpose_st.equalsIgnoreCase("")) {
                    purpose_of_visit_et.setText(visit_purpose_st);
                }
                if (!visitor_id_document_type.equalsIgnoreCase("")) {
                    visitor_dept_id_et.setText(visitor_id_document_type);
                }
                if (!detailObject.getString("visiting_individual").equalsIgnoreCase("")) {
                    txtSearch.setText(detailObject.getString("visiting_individual") + "");
                }
                if (!detailObject.getString("department_name").equalsIgnoreCase("")) {
                    visiting_dept_et.setText(detailObject.getString("department_name") + "");
                }

                if (!detailObject.getString("vms_visitor_entering_from_id").equalsIgnoreCase("")) {
                    entry_form_et.setText(detailObject.getString("entery_place") + "");
                }
                if (!detailObject.getString("country_name").equalsIgnoreCase("")) {
                    country_origin_et.setText(detailObject.getString("country_name") + "");
                }

                profileUrl = getIntent().getStringExtra("profileUrl");
                if (!profileUrl.equalsIgnoreCase("")) {
                    downloadImage();
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.dummy_users);
                    requestOptions.error(R.drawable.dummy_users);
                    Glide.with(getApplicationContext()).load(getIntent().getStringExtra("profileUrl")).apply(requestOptions).into(profile_img);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (docs_types != null) {
            setDoctype();
        }
        timing_et.setEnabled(false);
    }

    @OnClick(R.id.home_tv)
    public void backPressed() {
        EditUserActiity.this.finish();
    }

    @OnClick(R.id.logout_btn)
    public void showNotification() {
        startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
    }

    public void setDoctype() {
        try {
            if (docs_types.size() != 0) {
                for (int i = 0; i < docs_types.size(); i++) {
                    if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                        docTypeId = docs_types.get(i).getId();
                    } else if (docs_types.get(i).getDocument_name().equalsIgnoreCase(visitor_dept_id_et.getText().toString())) {
                        docTypeId = docs_types.get(i).getId();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @OnClick(R.id.entry_form_et)
    public void entryFromEt() {
        final Dialog dialog = new Dialog(EditUserActiity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
        entry_form_et = (EditText) findViewById(R.id.entry_form_et);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        if (entry_from.size() != 0) {
            EntryFromAdapter suffixAdapter = new EntryFromAdapter(getApplicationContext(), dialog, entry_from, entry_form_et, vms_visitor_entering_from_id);
            cat_rv.setAdapter(suffixAdapter);
            suffixAdapter.notifyDataSetChanged();

        }

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.purpose_of_visit)
    public void pusposeOfVisit() {
        final Dialog dialog = new Dialog(EditUserActiity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        if (purposesList.size() != 0) {

            PurposeAdapter visitingDeptAdapter = new PurposeAdapter(getApplicationContext(), dialog, purposesList, purposeEt, vms_visitor_purpose_id);
            cat_rv.setAdapter(visitingDeptAdapter);
            visitingDeptAdapter.notifyDataSetChanged();
        }

        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.suffix_choose_et)
    public void suffixChooseDialog() {
        final Dialog dialog = new Dialog(EditUserActiity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        try {
            if (suffixList != null) {
                if (suffixList.size() != 0) {
                    suffixAdapter = new SuffixAdapter(getApplicationContext(), dialog, suffixList, suffix_choose_et, suffixId);
                    cat_rv.setAdapter(suffixAdapter);
                    suffixAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();


    }

    @OnClick(R.id.doct_type)
    public void visitoeDeptChoose() {
        final Dialog dialog = new Dialog(EditUserActiity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        if (docs_types != null) {
            if (docs_types.size() != 0) {
                VisitorDocTypeAdapter visitorDocTypeAdapter = new VisitorDocTypeAdapter(getApplicationContext(), dialog, docs_types, editText, docTypeId);
                cat_rv.setAdapter(visitorDocTypeAdapter);
                visitorDocTypeAdapter.notifyDataSetChanged();
            }
        }
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.country_origin)
    public void countryGetting() {
        final Dialog dialog = new Dialog(EditUserActiity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        EditText editText = (EditText) findViewById(R.id.country_origin);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        if (docs_types.size() != 0) {
            CountryNamesAdapter countryNamesAdapter = new CountryNamesAdapter(getApplicationContext(), dialog, countriesList, editText);
            cat_rv.setAdapter(countryNamesAdapter);
            countryNamesAdapter.notifyDataSetChanged();
        }
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.etDOB)
    public void getDoB() {
        if (first_name_et.getText().toString().equalsIgnoreCase("") || first_name_et.getText().toString().isEmpty()) {
            /*first_name_et.setError("Please fill first name");*/
            first_name_et.setError("");
            showToast("Please fill first name", Toast.LENGTH_SHORT);
        } else if (last_name_et.getText().toString().equalsIgnoreCase("") || last_name_et.getText().toString().isEmpty()) {
            /*  last_name_et.setError("Please fill last name");*/
            last_name_et.setError("");
            showToast("Please fill last name", Toast.LENGTH_SHORT);
        } else {
            openDateDia();
        }
    }

    public void openDateDia() {
        final Calendar cal = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        Log.e("Date is", "Date is" + format);
                        /*yyyy-MM-dd*/
                        etDOB.setText(format);

                        if (first_name_et.getText().toString().equalsIgnoreCase("") || first_name_et.getText().toString().isEmpty()) {
                            /*first_name_et.setError("Please fill first name");*/
                            first_name_et.setError("");
                            showToast("Please fill first name", Toast.LENGTH_SHORT);
                        } else if (last_name_et.getText().toString().equalsIgnoreCase("") || last_name_et.getText().toString().isEmpty()) {
                            /*  last_name_et.setError("Please fill last name");*/
                            last_name_et.setError("");
                            showToast("Please fill last name", Toast.LENGTH_SHORT);
                        } else {
                            //   checkSuggestionList(first_name_et.getText().toString(), last_name_et.getText().toString(), format);
                        }
                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.setCancelable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    @OnClick(R.id.visiting_dept_et)
    public void vistingDeptDialog() {
        final Dialog dialog = new Dialog(EditUserActiity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_listview);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);
        if (docs_types != null) {
            if (docs_types.size() != 0) {
                DepartmentsAdapter departmentsAdapter = new DepartmentsAdapter(getApplicationContext(), dialog, departmentsList, visiting_dept_et, visiting_department_id);
                cat_rv.setAdapter(departmentsAdapter);
                departmentsAdapter.notifyDataSetChanged();
            }
        }
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.submit_btn)
    public void submitClick() {
        if (first_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill First name", Toast.LENGTH_SHORT).show();
        } else if (last_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill Last name", Toast.LENGTH_SHORT).show();
        } else if (etDOB.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill dob name", Toast.LENGTH_SHORT).show();
        } else if (visitor_dept_id_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select ID type ", Toast.LENGTH_SHORT).show();
        } else if (visitor_id_number_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please enter valid ID Number ", Toast.LENGTH_SHORT).show();
        } else if (entry_form_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select entry ", Toast.LENGTH_SHORT).show();
        }/* else if (ph_number_et.getText().toString().length() > 10) {
            Toast.makeText(getApplicationContext(), "Invalid phone number", Toast.LENGTH_SHORT).show();
        }*/ else {
            updateInformation();
        }
    }

    @OnClick(R.id.criminal_btn)
    public void CBCCheck() {
        String dobString = etDOB.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-mm-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(dobString);
            SimpleDateFormat timeFormat = new SimpleDateFormat("MMM-dd-yyyy");
            finalDate = timeFormat.format(myDate);
            System.out.println(finalDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (first_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill First name", Toast.LENGTH_SHORT).show();
        } else if (last_name_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill Last name", Toast.LENGTH_SHORT).show();
        } else if (etDOB.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill dob name", Toast.LENGTH_SHORT).show();
        } else if (visitor_dept_id_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select ID type ", Toast.LENGTH_SHORT).show();
        } else if (visitor_id_number_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please enter valid ID Number ", Toast.LENGTH_SHORT).show();
        } else if (entry_form_et.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select entry ", Toast.LENGTH_SHORT).show();
        } else if (ph_number_et.getText().toString().replace("+", "").length() > 10) {
            Toast.makeText(getApplicationContext(), "Invalid phone number", Toast.LENGTH_SHORT).show();
        } else {

            builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.add_new_user_alert_popup_string))
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().checkCB(APIServerResponse.CHECK_CB, getAuthToken(), first_name_et.getText().toString(), last_name_et.getText().toString(), etDOB.getText().toString(), email_et.getText().toString(), ph_number_et.getText().toString(), getUserID(), profileUrl, getClientId(), getParentIdValue(), getUserID(), EditUserActiity.this);
                            } else {
                                showSnack(Constants.INTERNET_CONNECTION);
                            }
                        }
                    })
                    .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void updateInformation() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().updateUserInformation(APIServerResponse.UPDATE_INFORMATION, getAuthToken(),
                    first_name_et.getText().toString(), last_name_et.getText().toString(), mid_name_et.getText().toString(),
                    suffix_choose_et.getText().toString(), email_et.getText().toString(), "+" + getCountryDialCode(getApplicationContext()) + ph_number_et.getText().toString(),
                    finalDate,
                    getUserID(), docTypeId, visitor_id_number_et.getText().toString(),
                    localToGMT(checkedIn), visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id,
                    vms_visitor_purpose_id, pickerPath, "", country_idSt, visitorId, log_id, EditUserActiity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.profile_img)
    public void profileSelection() {
        takePicture();
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        hasPermissionInManifest(EditUserActiity.this, Manifest.permission.CAMERA);
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();

        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (this.requestCode == requestCode && resultCode == RESULT_OK) {
            profileUrl = data.getData().toString();
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            profile_img.setImageBitmap(bitmap);
        }*/

        try {
            if (resultCode == RESULT_OK && requestCode == responseCode) {
                if (this.requestCode == requestCode && resultCode == RESULT_OK) {
                    profileUrl = data.getExtras().get("data").toString();
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    profileBitMap = bitmap;
                    profile_img.setImageBitmap(bitmap);
                }


                if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                    profileUrl = "";
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.dummy_users);
                    requestOptions.error(R.drawable.dummy_users);
                    Glide.with(this).setDefaultRequestOptions(requestOptions).load(pickerPath).into(profile_img);
                }
            }

            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
                if (data == null) {
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.dummy_users);
                    requestOptions.error(R.drawable.dummy_users);
                    Glide.with(this).setDefaultRequestOptions(requestOptions).load(getResources().getDrawable(R.drawable.dummy_users)).into(profile_img);
                } else {
                    Glide.with(this).load(pickerPath).into(profile_img);
                }
            }
            profileUrl = "";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void getAllDetail() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllNewVisitorPageDetail(APIServerResponse.GET_NEW_VISITOR_DETAIL, getAuthToken(), getParentIdValue(), getClientId(), getUserID(), EditUserActiity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }


    @Override
    public void onSuccess(int tag, Response response) {
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case CHECK_CB:
                        hideLoading();
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            if (jsonObject.get("data") != null) {
                                if (jsonObject.get("data").getAsJsonObject().get("Results").getAsJsonObject().getAsJsonArray("Records").size() != 0) {
                                    Toast.makeText(getApplicationContext(), jsonObject.get("data").getAsJsonObject().get("Results").getAsJsonObject().getAsJsonArray("Records") + "", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), CriminalRecordActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("resultObject", jsonObject.get("data").getAsJsonObject().get("Results").getAsJsonObject().toString());
                                    bundle.putString("first_name", first_name_et.getText().toString());
                                    bundle.putString("last_name", last_name_et.getText().toString());
                                    bundle.putString("age", etDOB.getText().toString());
                                    bundle.putString("entery_from", entry_form_et.getText().toString());
                                    bundle.putBoolean("cbFlag", cbFlag);
                                    bundle.putString("phNumber", ph_number_et.getText().toString());
                                    bundle.putString("dob", etDOB.getText().toString());
                                    bundle.putString("email", email_et.getText().toString());
                                    bundle.putString("profile_img", profileUrl);
                                    bundle.putString("purpose_id", vms_visitor_purpose_id);
                                    bundle.putString("suffixChoose", suffix_choose_et.getText().toString() + "");
                                    bundle.putString("docTypeId", docTypeId);
                                    bundle.putString("finalDate", finalDate);
                                    bundle.putString("visitorIdNumber", visitor_id_number_et.getText().toString());
                                    bundle.putString("visiting_individual_id", visiting_individual_id);
                                    bundle.putString("visiting_department_id", visiting_department_id);
                                    bundle.putString("vms_visitor_entering_from_id", vms_visitor_entering_from_id);
                                    bundle.putString("vms_visitor_purpose_id", vms_visitor_purpose_id);
                                    bundle.putString("timing_et", timing_et.getText().toString());
                                    bundle.putString("profileUrl", profileUrl);
                                    bundle.putString("country_idSt", country_idSt);
                                    bundle.putString("log_id", "");
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } else {
                                    successPage("Criminal Background check is clear", "criminal");

                                }
                            } else {
                                successPage("Criminal Background check is clear", "criminal");
                            }
                        }
                        break;
                    case GET_NEW_VISITOR_DETAIL:

                        GetSuffixDoctypeResponse getSuffixDoctypeResponse = (GetSuffixDoctypeResponse) response.body();
                        hideLoading();

                        if (getSuffixDoctypeResponse.getStatus().equalsIgnoreCase("OK")) {
                            suffixList = getSuffixDoctypeResponse.getSuffix();
                            docs_types = getSuffixDoctypeResponse.getDocs_type();
                            entry_from = getSuffixDoctypeResponse.getEntry_from();
                            departmentsList = getSuffixDoctypeResponse.getDepartments();
                            countriesList = getSuffixDoctypeResponse.getCountries();
                            individualsList = getSuffixDoctypeResponse.getIndividuals();
                            purposesList = getSuffixDoctypeResponse.getPurpose();

                            txtSearch.setThreshold(1);
                            adapter = new VisitIndividualAdapter(this, R.layout.activity_main, R.id.category_name_tv, individualsList);
                            txtSearch.setAdapter(adapter);
                            txtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Object item = adapterView.getItemAtPosition(i);
                                    if (item instanceof Individuals) {
                                        Individuals individuals = (Individuals) item;
                                        visiting_individual_id = individuals.getId();
                                    }
                                }
                            });

                            if (getIntent().hasExtra("visitor_doc_name")) {
                                visitor_dept_id_et.setText(getIntent().getStringExtra("visitor_doc_name"));


                                for (int i = 0; i <= docs_types.size(); i++) {
                                    if (docs_types.get(i).getDocument_name().equalsIgnoreCase("No ID Available")) {
                                        docTypeId = docs_types.get(i).getId();
                                    } else if (docs_types.get(i).getDocument_name().equalsIgnoreCase("Driver's License number")) {
                                        docTypeId = docs_types.get(i).getId();
                                    }
                                }
                                if (visitor_dept_id_et.getText().toString().equalsIgnoreCase("No ID Available")) {
                                    visitor_id_number_et.setText("N/A");
                                    visitor_id_number_et.setEnabled(false);
                                } else {
                                    visitor_id_number_et.setText("");
                                    visitor_id_number_et.setEnabled(true);
                                }
                            }

                            if (docTypeId != null) {

                                for (Docs_type docs_type : docs_types) {
                                    if (docs_type.getDocument_name().equalsIgnoreCase("Select")) {

                                    } else {
                                        if (docs_type.getId().equalsIgnoreCase(docTypeId)) {
                                            if (docs_type.getDocument_name().equalsIgnoreCase("No ID Available")) {
                                                visitor_dept_id_et.setText(docs_type.getDocument_name() + "");
                                                visitor_id_number_et.setText("N/A");
                                                visitor_id_number_et.setEnabled(false);
                                            } else if (docs_type.getDocument_name().equalsIgnoreCase("Driver's License number")) {
                                                visitor_dept_id_et.setText(docs_type.getDocument_name() + "");
                                                visitor_id_number_et.setText(visitor_id_number);
                                            } else {
                                                visitor_id_number_et.setText("");
                                                visitor_id_number_et.setEnabled(true);
                                            }
                                        }
                                    }
                                }
                            }

                            if (suffixId != null) {
                                for (Suffix p : suffixList
                                        ) {
                                    if (p.getId().equalsIgnoreCase(suffixId)) {
                                        suffix_choose_et.setText(p.getSuffix_name() + "");
                                    }
                                }
                            }

                         /*   if (vms_visitor_entering_from_id != null) {
                                for (Entry_from p : entry_from
                                        ) {
                                    if (p.getId().equalsIgnoreCase(vms_visitor_entering_from_id)) {
                                        entry_form_et.setText(p.getEntery_place() + "");
                                    }
                                }

                            }*/

                            if (country_idSt != null) {
                                for (Countries p : countriesList
                                        ) {
                                    if (p.getId().equalsIgnoreCase(country_idSt)) {
                                        country_origin_et.setText(p.getCountry_name() + "");
                                    }
                                }
                            }
                            if (visiting_individual_id != null) {
                                for (Individuals individuals : individualsList) {
                                    if (individuals.getId().equalsIgnoreCase(visiting_individual_id)) {
                                        txtSearch.setText(individuals.getUsername());
                                        visiting_individual_id = individuals.getId();
                                    }
                                }
                            }

                            if (visit_purpose_st != null) {
                                for (Purpose p : purposesList
                                        ) {
                                    if (p.getVisit_purpose().equalsIgnoreCase(visit_purpose_st)) {
                                        purposeEt.setText(p.getVisit_purpose() + "");
                                    }
                                }
                            }
                        }
                        break;
                    case UPDATE_INFORMATION:
                        try {
                            hideLoading();
                            JsonObject jsonObject1 = (JsonObject) response.body();
                            if (jsonObject1.get("status").getAsString().equalsIgnoreCase("ok")) {
                                Toast.makeText(EditUserActiity.this, "" + jsonObject1.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(EditUserActiity.this, "" + jsonObject1.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    public void successPage(String textSt, String checkType) {
        dialog = new Dialog(EditUserActiity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_green_successfull);
        dialog.setTitle(null);
        TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
        textView.setText(textSt + "");
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1000);
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ChosenImage image = list.get(0);
            if (image != null) {
                if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                    pickerPath = image.getThumbnailPath();
                else
                    pickerPath = image.getOriginalPath();
                profile_img.setVisibility(View.VISIBLE);
                profile_img.setImageURI(Uri.fromFile(new File(pickerPath)));
            } else
                showSnack("Invalid Image");
        }
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
}
