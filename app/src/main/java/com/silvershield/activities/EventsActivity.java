package com.silvershield.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Bold_TextView;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.PrinterId;
import com.silvershield.modal.PrinterSettingResponse;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class EventsActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.print_btn)
    Button print_btn;
    @BindView(R.id.mark_attendence_btn)
    Button mark_attendence_btn;
    //cbc_check_btn
    @BindView(R.id.cbc_check_btn)
    Button cbcCheckBtn;

    @BindView(R.id.profile_img)
    ImageView profile_img;
    @BindView(R.id.event_name)
    Custome_Bold_TextView event_name;

    @BindView(R.id.name_tv)
    Custome_Regular_TextView name_tv;
    @BindView(R.id.email_tv)
    Custome_Regular_TextView email_tv;
    @BindView(R.id.phone_tv)
    Custome_Regular_TextView phone_tv;
    @BindView(R.id.company_name)
    Custome_Regular_TextView company_name;

    String jsonObjectSt, eventId = "", profileUrl = "", eventGuestID = "";
    JSONObject jsonObjectEvent;

    Dialog dialog;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;
    List<PrinterId> printerIdList;

    String firstName = "", lastName = "", dob = "";
    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        ButterKnife.bind(this);
        if (showAlertIcon())
            home_img.setVisibility(View.VISIBLE);
        else
            home_img.setVisibility(View.GONE);

        logout_btn.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        title_tv.setText("Event Detail");

        if (getIntent() != null) {
            jsonObjectSt = getIntent().getStringExtra("eventDetail");
            try {
                jsonObjectEvent = new JSONObject(jsonObjectSt);

                firstName = jsonObjectEvent.getString("first_name");
                lastName = jsonObjectEvent.getString("last_name");
                dob = jsonObjectEvent.getString("dob");
                if (!jsonObjectEvent.getString("profile_pic_web").equalsIgnoreCase("")) {
                    Glide.with(getApplicationContext()).load(jsonObjectEvent.getString("profile_pic_web")).into(profile_img);
                }
                event_name.setText(jsonObjectEvent.getString("event_name") + "");
                name_tv.setText("" + jsonObjectEvent.getString("last_name") + "" + jsonObjectEvent.getString("first_name"));
                company_name.setText("" + jsonObjectEvent.getString("company_name"));
                phone_tv.setText("" + jsonObjectEvent.getString("phone"));
                email_tv.setText("" + jsonObjectEvent.getString("email"));
                eventGuestID = jsonObjectEvent.getString("eventGuestId");
                eventId = jsonObjectEvent.getString("id");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }


        if(getEventCbcCheckValue().equalsIgnoreCase("1"))
        {
            cbcCheckBtn.setVisibility(View.VISIBLE);
        }else
        {
            cbcCheckBtn.setVisibility(View.GONE);
        }
    }

    private final int requestCode = 20;

    @OnClick(R.id.profile_img)
    public void clickPicture() {
        Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(photoCaptureIntent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.requestCode == requestCode && resultCode == RESULT_OK) {
            profileUrl = data.getExtras().get("data").toString();
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            profile_img.setImageBitmap(bitmap);
        }
    }

    @OnClick(R.id.print_btn)
    public void printBtnClicl() {
        createBadge();
    }

    public void createBadge() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().createBadge(APIServerResponse.CREATE_BADGE, getAuthToken(), getUserID(),
                    getParentIdValue(), "0", "0", "", "0", profileUrl,
                    email_tv.getText().toString(), phone_tv.getText().toString(), EventsActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.home_tv)
    public void homeBack() {
        EventsActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EventsActivity.this.finish();
    }

    @OnClick(R.id.print_btn)
    public void getAllPrinter() {
        createBadge();
    }


    public void getAllPrinterList() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getPrinterSetting(APIServerResponse.GET_ALL_PRINTER, getAuthToken(), getClientId(), EventsActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.mark_attendence_btn)
    public void markAttendence() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getEventBudgetDetail(APIServerResponse.MARK_ATTENDENCE, getAuthToken(), getUserID(), eventGuestID, "YES", EventsActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.cbc_check_btn)
    public void cbcCheck() {

        builder = new AlertDialog.Builder(this);
        builder.setTitle("SilverShield");
        builder.setMessage(getResources().getString(R.string.add_new_user_alert_popup_string))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (isConnectedToInternet()) {
                            showLoading();
                            ServerAPI.getInstance().eventCBCheck(APIServerResponse.EVENT_CBC_CHECK, getAuthToken(), firstName, lastName, dob, EventsActivity.this);
                        } else {
                            showSnack(Constants.INTERNET_CONNECTION);
                        }

                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();






    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case MARK_ATTENDENCE:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").toString().equalsIgnoreCase("\"" + "OK" + "\"")) {
                            Toast.makeText(getApplicationContext(), "" + jsonObject.get("message").toString().replace("\"", ""), Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case GET_ALL_PRINTER:
                        hideLoading();
                        PrinterSettingResponse printerSettingResponse = (PrinterSettingResponse) response.body();
                        if (printerSettingResponse.getStatus().equalsIgnoreCase("Ok")) {
                            printerIdList = printerSettingResponse.getPrinterId();
                            if (printerSettingResponse.getPrinterType().equalsIgnoreCase("IP")) {
                                Intent intent = new Intent(EventsActivity.this, AllPrinterActivity.class);
                                intent.putExtra("eventId", eventId);
                                intent.putExtra("printerMainObject", printerSettingResponse);
                                startActivity(intent);
                            } else {

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No printer found ", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case CREATE_BADGE:
                        hideLoading();
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        if (jsonObject1.get("status").toString().equalsIgnoreCase("\"" + "OK" + "\"")) {
                            Toast.makeText(getApplicationContext(), "" + jsonObject1.get("message").toString(), Toast.LENGTH_SHORT).show();
                            getAllPrinterList();
                        } else {
                            Toast.makeText(getApplicationContext(), "" + jsonObject1.get("message").toString(), Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case EVENT_CBC_CHECK:
                        hideLoading();
                        JsonObject cbcJsonObject = (JsonObject) response.body();
                        if (cbcJsonObject.get("status").toString().equalsIgnoreCase("\"" + "OK" + "\"")) {
                            Toast.makeText(getApplicationContext(), "" + cbcJsonObject.get("message").toString(), Toast.LENGTH_SHORT).show();
                        } else {
                            successPage("Criminal Background Check is clear", "criminal");

                        }
                        break;


                }
            } catch (Exception ex) {
                hideLoading();
                ex.printStackTrace();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }


    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    public void successPage(String textSt, String checkType) {
        dialog = new Dialog(EventsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_green_successfull);
        dialog.setTitle(null);
        TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
        textView.setText(textSt + "");
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1000);

    }
}
