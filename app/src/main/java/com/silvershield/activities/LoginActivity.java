package com.silvershield.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.silvershield.R;
import com.silvershield.modal.UserLoginResponse;
import com.silvershield.modal.responseModels.ResultModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.login_btn)
    Button login_btn;
    @BindView(R.id.email_txt_et)
    EditText etUserName;
    @BindView(R.id.password_txt_et)
    EditText etPassword;
    AlertDialog b;
    String FCMID = "";

//9988366200

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (getUserLoggedIn()) {
            getUserRoleString();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("UserRole", getUserRoleString());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    public String getUserRoleString() {
        String userType = "";
        if (getUserRole().equalsIgnoreCase("10")) {
            userType = "Super Admin";
        } else if (getUserRole().equalsIgnoreCase("20")) {
            userType = "Key System Admin";
        } else if (getUserRole().equalsIgnoreCase("60")) {
            userType = "System Admin";
        } else if (getUserRole().equalsIgnoreCase("30")) {
            userType = "STAFF";
        } else if (getUserRole().equalsIgnoreCase("40")) {
            userType = "GUARD";
        } else if (getUserRole().equalsIgnoreCase("70")) {
            userType = "STUDENT";
        } else if (getUserRole().equalsIgnoreCase("71")) {
            userType = "TEACHER";
        }
        return userType;
    }

    @OnClick(R.id.forget_password)
    public void forgetPasswordTv() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custome_layout, null);
        dialogBuilder.setView(dialogView);
        final EditText emailEt = (EditText) dialogView.findViewById(R.id.email_txt_et);

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        final Button cancleBtn = (Button) dialogView.findViewById(R.id.cancle_btn);
        Button okBtn = (Button) dialogView.findViewById(R.id.ok_btn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEt.getText().toString().trim();
                if (email.matches(emailPattern)) {
                    if (!emailEt.getText().toString().isEmpty()) {
                        if (isConnectedToInternet()) {
                            showLoading();
                            hideKeyboard();
                            ServerAPI.getInstance().forgetPassword(APIServerResponse.FORGET_PASSWORD, emailEt.getText().toString(), LoginActivity.this);
                        } else {
                            showSnack(Constants.INTERNET_CONNECTION);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Email id cannot be blank", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid email id", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.show();
    }

    @OnClick(R.id.login_btn)
    public void loginBtnClick() {
        try {
//            etUserName.setText("sanjeev.sharma");
            etPassword.setText("123456");
            if (etUserName.getText().toString().isEmpty()) {
                showSnack("Please enter a valid User-ID.");
                Toast.makeText(LoginActivity.this, "Please enter a valid User-ID.", Toast.LENGTH_SHORT).show();
            } else if (etPassword.getText().toString().isEmpty()) {
                showSnack("Please enter a valid Password.");
                Toast.makeText(LoginActivity.this, "Please enter password.", Toast.LENGTH_SHORT).show();
            } else {
               /* Intent intentSignUp = new Intent(LoginActivity.this, LoginActivity.class);
                startActivity(intentSignUp);*/
                FCMID = getFirebaseToken();
                loginApiCall();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loginApiCall() {
        if (isConnectedToInternet()) {
            showLoading();
            String currentTime = TimeZone.getDefault().getID();
            ServerAPI.getInstance().login(APIServerResponse.LOGIN, etUserName.getText().toString(), etPassword.getText().toString(), "ANDROID", FCMID, currentTime, LoginActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {

        }
//        return getResources().getConfiguration().locale.getCountry();
        return Locale.getDefault().getCountry();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.LOGIN:
                        UserLoginResponse userLoginResponse = (UserLoginResponse) response.body();
                        if (userLoginResponse.getStatus().equalsIgnoreCase("OK")) {
                            setUserLoggedIn(true);
                            Constants.USER_ID = userLoginResponse.getUserdata().getUser_id();
                            Constants.USER_NAME = userLoginResponse.getUserdata().getUsername();
                            setAuthToken(userLoginResponse.getAuthtoken());
                            setUserID(userLoginResponse.getUserdata().getUser_id());
                            String emailSt = userLoginResponse.getUserdata().getEmail();
                            setEmailID(emailSt);
                            String phoneNumber = userLoginResponse.getUserdata().getPhone_number();
                            setPhoneNumber(phoneNumber);
                            String roleSt = userLoginResponse.getUserdata().getRole();
                            setUserRole(roleSt);
                            String firstName = userLoginResponse.getUserdata().getFirst_name();
                            setFirstName(firstName);
                            String lastName = userLoginResponse.getUserdata().getLast_name();
                            setLastName(lastName);
                            setUserName(userLoginResponse.getUserdata().getUsername());
                            setProfileUrl(userLoginResponse.getUserdata().getProfile_pic());
                            setClientId(userLoginResponse.getUserdata().getClients_id());
                            setParentId(userLoginResponse.getUserdata().getParent_id());
                            setClientName(userLoginResponse.getUserdata().getClient_name());
                            setCountryCode(getCountryDialCode(this));
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.putExtra("UserRole", getUserRoleString());
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            showToast(userLoginResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                    case FORGET_PASSWORD:
                        ResultModel resultModel = (ResultModel) response.body();
                        b.dismiss();
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Alert")
//                                .setMessage(resultModel.getMessage())
                                .setMessage("An email has been sent with instructions for resetting your password.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        hideKeyboard();
                                    }
                                }).show();
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }
}
