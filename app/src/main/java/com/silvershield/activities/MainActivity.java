package com.silvershield.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.HomeCategoryAdapter;
import com.silvershield.modal.SettingMainResponse;
import com.silvershield.modal.SettingsModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.categories_rv)
    RecyclerView categories_rv;
    @BindView(R.id.home_tv)
    ImageView notification_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;
    HomeCategoryAdapter homeCategoryAdapter;
    List<String> deptNameArray = new ArrayList<>();
    List<Integer> images = new ArrayList();
    @BindView(R.id.circular_progress_bar)
    ProgressBar mProgress;
    MediaPlayer mp;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
        if (getIntent().getStringExtra("UserRole") != null) {
            title_tv.setText(getUserRoleString() + " Dashboard");
        }
        callSettingsApi();
        setOptionArray();
    }

    public void callSettingsApi() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getSetting(APIServerResponse.SETTINGS, getAuthToken(), getUserID(), MainActivity.this);
            ServerAPI.getInstance().getAllTimesZones(APIServerResponse.TIMEZONES, getAuthToken(), MainActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void setOptionArray() {
        homeCategoryAdapter = new HomeCategoryAdapter(getApplicationContext(), deptNameArray, images, getUserRole());
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        categories_rv.setItemAnimator(new DefaultItemAnimator());
        categories_rv.addItemDecoration(new DividerItemDecoration(MainActivity.this, LinearLayoutManager.VERTICAL));
        categories_rv.setLayoutManager(mLayoutManager);
        categories_rv.setAdapter(homeCategoryAdapter);
        homeCategoryAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        title_tv.setText(getUserRoleString() + " Dashboard");
        if (showAlertIcon())
            notification_img.setVisibility(View.VISIBLE);
        else
            notification_img.setVisibility(View.GONE);
    }

    public String getUserRoleString() {
        String userType = "";
        if (getUserRole().equalsIgnoreCase("10")) {
            userType = "Super Admin";
        } else if (getUserRole().equalsIgnoreCase("20")) {
            userType = "Key System Admin";
        } else if (getUserRole().equalsIgnoreCase("60")) {
            userType = "System Admin";
        } else if (getUserRole().equalsIgnoreCase("30")) {
            userType = "STAFF";
        } else if (getUserRole().equalsIgnoreCase("40")) {
            userType = "GUARD";
        } else if (getUserRole().equalsIgnoreCase("70")) {
            userType = "STUDENT";
        } else if (getUserRole().equalsIgnoreCase("71")) {
            userType = "TEACHER";
        }
        return userType;
    }

    public void addingArrayListAccToRole() {
        deptNameArray.clear();
        images.clear();
        if (getUserRole().equalsIgnoreCase("30") || getUserRole().equalsIgnoreCase("40")) {
            deptNameArray.add("Visitors");
            images.add(R.drawable.visitors);

            deptNameArray.add("Watch/VIP list");
            images.add(R.drawable.vip);

            deptNameArray.add("Virtual Accademy");
            images.add(R.drawable.virtualacademy);

            deptNameArray.add("Settings");
            images.add(R.drawable.settings);
            //   if (getManageEventValue().equalsIgnoreCase("1")) {
            deptNameArray.add("Events");
            images.add(R.drawable.group);
            //   }
        } else if (getUserRole().equalsIgnoreCase("10") || getUserRole().equalsIgnoreCase("20") || getUserRole().equalsIgnoreCase("60")) {
            deptNameArray.add("Guards");
            images.add(R.drawable.guard);

            deptNameArray.add("Staff");
            images.add(R.drawable.group);

            deptNameArray.add("Visitors");
            images.add(R.drawable.visitors);

            deptNameArray.add("Watch/VIP list");
            images.add(R.drawable.vip);

            deptNameArray.add("Virtual Accademy");
            images.add(R.drawable.virtualacademy);

            deptNameArray.add("Settings");
            images.add(R.drawable.settings);

            //if (getManageEventValue().equalsIgnoreCase("1")) {
            deptNameArray.add("Events");
            images.add(R.drawable.event);
            //  }

            deptNameArray.add("Departments");
            images.add(R.drawable.event);

        } else if (getUserRole().equalsIgnoreCase("71")) {
            deptNameArray.add("My Account");
            images.add(R.drawable.report);
        }
    }

    public void addingMainPageOptionApi() {
        deptNameArray.clear();
        images.clear();

        if (getManageVisitorsValue().equalsIgnoreCase("1") && getCheckInVisitorsValue().equalsIgnoreCase("1")) {
            deptNameArray.add("Visitors");
            images.add(R.drawable.visitors);
        }
        if (getManageStaffValue().equalsIgnoreCase("1")) {
            deptNameArray.add("Staff");
            images.add(R.drawable.group);
        }
        if ((getManageVisitorsValue().equalsIgnoreCase("1") && getCheckInVisitorsValue().equalsIgnoreCase("1")) && (getCreateVipList().equalsIgnoreCase("1") || getCreateWatchList().equalsIgnoreCase("1"))) {
            deptNameArray.add("Watch/VIP list");
            images.add(R.drawable.vip);
        }
        if (getManageEventValue().equalsIgnoreCase("1")) {
            deptNameArray.add("Events");
            images.add(R.drawable.event);
        }

        if (getUserRole().equalsIgnoreCase("71")) {

        } else {
            deptNameArray.add("Settings");
            images.add(R.drawable.settings);
        }
        mProgress.setVisibility(View.GONE);
        setOptionArray();
    }

    @OnClick(R.id.home_tv)
    public void notificationPageCall() {
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        startActivity(intent);
//        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @OnClick(R.id.logout_btn)
    public void logoutPageCall() {
        logoutCall();
    }

    public String checkState(int flag, int state) {
        if (flag == 1 && state == 1) {
            return "1";
        }
        return "0";
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            switch (tag) {
                case SETTINGS:
                    try {
                        deptNameArray.clear();
                        images.clear();
                        SettingMainResponse userLoginResponse = (SettingMainResponse) response.body();
                        if (userLoginResponse.getStatus().equalsIgnoreCase("OK")) {
                            List<SettingsModel> settings = userLoginResponse.getSettings();
                            for (int i = 0; i < settings.size(); i++) {
                                if (settings.get(i).getTitle().equalsIgnoreCase("alertAdminButton")) {
                                    setAlertAdmin(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertGuardAndAdminButton")) {
                                    setGuardAndAdmin(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertLockdownButton")) {
                                    setLockdown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertPoliceEmergencyButton")) {
                                    setalertPoliceEmergency(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertPoliceNonEmergencyButton")) {
                                    setAlertPoliceNonEmergencyButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("checkInVisitors")) {
                                    setCheckInVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("checkOutVisitors")) {
                                    setCheckOutVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("createQuickBadge")) {
                                    setCreateQuickBadge(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("createVipList")) {
                                    setCreateVipList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("createWatchList")) {
                                    setCreateWatchList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("sexOffenderCheck")) {
                                    setSexOffenderCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("criminalBackgroundCheck")) {
                                    setCriminalBackgroundCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("eventSexOffenderCheck")) {
                                    setEventSexOffenderCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("eventCriminalBackgroundCheck")) {
                                    setEventCbcCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("allowCallPolice")) {
                                    setAllowCallPolice(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("editPoliceContactNumbers")) {
                                    setEditPoliceContactNumbers(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageEvents")) {
                                    setManageEvent(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageBadges")) {
                                    setManageBadges(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageStaff")) {
                                    setManageStaff(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageGuards")) {
                                    setManageGuards(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageVisitors")) {
                                    setManageVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageTeachers")) {
                                    setManageTeachers(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageStudents")) {
                                    setManageStudents(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageParents")) {
                                    setManageParents(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageVideos")) {
                                    setManageVideos(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertGuardsButton")) {
                                    setAlertGuardsButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertStaffButton")) {
                                    setAlertStaffButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageSelfSignupRequests")) {
                                    setManageSelfSignupRequests(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertHardLockdownButton")) {
                                    setHardLockDown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertSoftLockdownButton")) {
                                    setSoftLockDown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertHardLockdownButtonDrill")) {
                                    setAlertHardLockdownButtonDrill(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertScreen")) {
                                    setAlertScreen(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("viewWatchList")) {
                                    setViewWatchList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("viewVipList")) {
                                    setViewVipList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                }
                            }
                            addingMainPageOptionApi();
                        } else {
                            mProgress.setVisibility(View.GONE);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    if (showAlertIcon())
                        notification_img.setVisibility(View.VISIBLE);
                    else
                        notification_img.setVisibility(View.GONE);
                    break;

                case TIMEZONES:
                    JsonObject jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                        setTimeZonesValue(jsonObject.get("timezones").getAsJsonObject().toString() + "");
                    } else {
                        Toast.makeText(getApplicationContext(), "No Time Zones found", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case LOGOUT:
                    JsonObject jsonObject1 = (JsonObject) response.body();
                    if (jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                        Toast.makeText(getApplicationContext(), jsonObject1.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        setUserLoggedIn(false);
                        clearPreferences();
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.this.finish();
    }

    //for all setting
    private class getAllSetting extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected String doInBackground(String... strings) {
            ServerAPI.getInstance().getSetting(APIServerResponse.SETTINGS, getAuthToken(), getUserID(), MainActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {
            hideLoading();
            new getTimeZoneArray().execute();
        }
    }

    //for getting all Time zones
    private class getTimeZoneArray extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            ServerAPI.getInstance().getAllTimesZones(APIServerResponse.TIMEZONES, getAuthToken(), MainActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {
        }
    }
}
