package com.silvershield.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.silvershield.R;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class MyProfileActivity extends BaseActivity implements APIServerResponse {


    @BindView(R.id.first_name_et)
    EditText first_name_et;
    //last_name_et
    @BindView(R.id.last_name_et)
    EditText last_name_et;
    @BindView(R.id.email_et)
    EditText email_et;
    @BindView(R.id.ph_number_et)
    EditText ph_number_et;

    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    //profile_img
    @BindView(R.id.profile_img)
    ImageView profile_img;

    @BindView(R.id.circular_progress_bar)
    ProgressBar mProgress;
    @BindView(R.id.alert_img)
    ImageView alert_img;
    @BindView(R.id.local_code_number)
    TextView local_code_number;

    @BindView(R.id.update_btn)
    Button update_btn;

    RequestOptions requestOptions = new RequestOptions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        title_tv.setText("My Profile");
        logout_btn.setVisibility(View.GONE);
        setValues();

    }

    public void setValues() {
        requestOptions.placeholder(R.drawable.dummy_users);
        requestOptions.error(R.drawable.dummy_users);

        if (getFirstName().equalsIgnoreCase("") || getFirstName().isEmpty()) {
        } else {
            first_name_et.setText(getFirstName() + "");
        }

        if (getLastName().equalsIgnoreCase("") || getLastName().isEmpty()) {
        } else {
            last_name_et.setText(getLastName() + "");
        }

        if (getEmailID().equalsIgnoreCase("") || getEmailID().isEmpty()) {
        } else {
            email_et.setText(getEmailID() + "");
        }

        if (getPhoneNumber().equalsIgnoreCase("") || getPhoneNumber().isEmpty()) {
        } else {
            if ((getPhoneNumber().contains("+")) && (getPhoneNumber().length() > 1)) {
                String code = getPhoneNumber().substring(0, 3);
                local_code_number.setText(code);
                String phone = getPhoneNumber().substring(3, 13);
                ph_number_et.setText(phone);
            } else {
                local_code_number.setText("+" + getCountryDialCode(this) + "");
                ph_number_et.setText(getPhoneNumber());
            }
        }

        if (!getProfileUrl().equalsIgnoreCase("")) {
            Glide.with(this).setDefaultRequestOptions(requestOptions).load(getProfileUrl()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    mProgress.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    mProgress.setVisibility(View.GONE);
                    return false;
                }
            }).thumbnail(0.1f).into(profile_img);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyProfileActivity.this.finish();
    }

    @OnClick(R.id.home_tv)
    public void backPage() {
        MyProfileActivity.this.finish();
    }


    @OnClick(R.id.update_btn)
    public void updateProfile() {

    }


    public void getAllTimeZones() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getSetting(APIServerResponse.GET_ALL_TIMEZONES, getAuthToken(), getUserID(), MyProfileActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }


    public void updateInfo() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getSetting(APIServerResponse.UPDATE_PROFILE, getAuthToken(), getUserID(), MyProfileActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
