package com.silvershield.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.NotificationAapter;
import com.silvershield.custome_controls.Custome_Bold_TextView;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.interfaces.SuccessCodeCheck;
import com.silvershield.modal.AlarmCheckModal;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity implements APIServerResponse, SuccessCodeCheck {

    String policeEmerg, policeNonEmerg;
    @BindView(R.id.internal_alarm_rv)
    RecyclerView internal_alarm_rv;
    @BindView(R.id.external_alarm_rv)
    RecyclerView external_alarm_rv;
    @BindView(R.id.title_tv)
    Custome_Regular_TextView title_tv;
    @BindView(R.id.title_ext)
    Custome_Bold_TextView title_ext;
    @BindView(R.id.cross)
    ImageView cross;
    NotificationAapter internalNotificationAdapter, externalNotificationAdapter;
    List<String> namesNotification = new ArrayList<String>();
    List<String> externalNotification = new ArrayList<String>();
    Boolean cautionary_confirm = false, cautionary_code = false;
    private RecyclerView.LayoutManager mLayoutManager, mLayoutManager1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        title_tv.setText("Alarm");
        namesNotification.clear();
        externalNotification.clear();

        if (getAlertGuardsButton().equalsIgnoreCase("1")) {
            namesNotification.add(Constants.ALERT_GUARD);
        }
        if (getAlertStaffButtonValue().equalsIgnoreCase("1")) {
            namesNotification.add(Constants.ALERT_STAFF);
        }
        if (getAlertAdminValue().equalsIgnoreCase("1")) {
            namesNotification.add(Constants.ALERT_ADMIN);
        }
        if (getGuardAndAdminValue().equalsIgnoreCase("1")) {
            namesNotification.add(Constants.ALERT_GUARD_ADMIN);
        }
        if (getHardLockDown().equalsIgnoreCase("1")) {
            namesNotification.add(Constants.HARD_LOCKDOWN_DRILL);
        }

        if (getAlertHardLockdownButtonDrill().equalsIgnoreCase("1")) {
            namesNotification.add(Constants.ALERT_HARD_LOCKDOWN);
        }

        if (getAlertSoftLockdownButton().equalsIgnoreCase("1")) {
            namesNotification.add(Constants.ALERT_SOFT_LOCKDOWN);
        }

        if (getAlertPoliceNonEmergencyButtonValue().equalsIgnoreCase("1")) {
            externalNotification.add(Constants.CONTACT_POLICE_NON_EMERGENCY);
        }

        if (getalertPoliceEmergencyValue().equalsIgnoreCase("1")) {
            externalNotification.add(Constants.CONTACT_POLICE_EMERGENCY);
        }

        internalNotificationAdapter = new NotificationAapter(NotificationActivity.this, namesNotification, null, "internal", this);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        internal_alarm_rv.setItemAnimator(new DefaultItemAnimator());
        internal_alarm_rv.addItemDecoration(new DividerItemDecoration(NotificationActivity.this, LinearLayoutManager.VERTICAL));
        internal_alarm_rv.setLayoutManager(mLayoutManager);
        internal_alarm_rv.setAdapter(internalNotificationAdapter);

        if (externalNotification.size() == 0) {
            title_ext.setVisibility(View.GONE);
        }

        externalNotificationAdapter = new NotificationAapter(NotificationActivity.this, null, externalNotification, "external", this);
        mLayoutManager1 = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        external_alarm_rv.setItemAnimator(new DefaultItemAnimator());
        external_alarm_rv.addItemDecoration(new DividerItemDecoration(NotificationActivity.this, LinearLayoutManager.VERTICAL));
        external_alarm_rv.setLayoutManager(mLayoutManager1);
        external_alarm_rv.setAdapter(externalNotificationAdapter);
    }


    @OnClick(R.id.home_tv)
    public void logsActivity() {
        Intent intent = new Intent(getApplicationContext(), AlertLogsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cross)
    public void closeNotification() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        NotificationActivity.this.finish();
    }

    public void getNotification() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getNotificationFlag(APIServerResponse.GETNOTIFICATIONFLAGS, getAuthToken(), getUserID(), getClientId(), getParentIdValue(), NotificationActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GETNOTIFICATIONFLAGS:
                        AlarmCheckModal alarmCheckModal = (AlarmCheckModal) response.body();
                        if (alarmCheckModal.getStatus().equalsIgnoreCase("OK")) {
                            policeEmerg = String.valueOf(alarmCheckModal.getPoliceEmerg());
                            policeNonEmerg = String.valueOf(alarmCheckModal.getPoliceNonEmerg());
                            externalNotification.clear();
                            if (policeEmerg.equalsIgnoreCase("false") || policeNonEmerg.equalsIgnoreCase("false")) {
                                externalNotification.clear();
                            } else if (policeEmerg.equalsIgnoreCase("true") || policeNonEmerg.equalsIgnoreCase("false")) {
                                externalNotification.add("Text police(Non Emergency)");
                            } else if (policeEmerg.equalsIgnoreCase("false") || policeNonEmerg.equalsIgnoreCase("true")) {
                                externalNotification.add("LockDown Active Killer(Emergency)");
                            } else {
                                externalNotification.add("Text police(Non Emergency)");
                                externalNotification.add("LockDown Active Killer(Emergency)");
                            }
                            if (externalNotification.size() == 0) {
                                title_ext.setVisibility(View.GONE);
                            }
                            externalNotificationAdapter = new NotificationAapter(NotificationActivity.this, null, externalNotification, "external", this);
                            mLayoutManager1 = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                            external_alarm_rv.setItemAnimator(new DefaultItemAnimator());
                            external_alarm_rv.addItemDecoration(new DividerItemDecoration(NotificationActivity.this, LinearLayoutManager.VERTICAL));
                            external_alarm_rv.setLayoutManager(mLayoutManager1);
                            external_alarm_rv.setAdapter(externalNotificationAdapter);
                        }
                        break;
                    case GETCAUTIONARY:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                            JsonObject settings = jsonObject.getAsJsonObject("settings");
                            if (settings.has("cautionary_confirm")) {
                                cautionary_confirm = settings.get("cautionary_confirm").getAsBoolean();
                            }
                            if (settings.has("cautionary_code")) {
                                cautionary_code = settings.get("cautionary_code").getAsBoolean();
                            }
                            if (externalNotification.size() == 0) {
                                title_ext.setVisibility(View.GONE);
                            }
                        }
                        break;
                    case LOGOUT:
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        if (jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                            Toast.makeText(getApplicationContext(), jsonObject1.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            setUserLoggedIn(false);
                            clearPreferences();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void onCodeVerify(String message, String enteredCode, String typeOfAlarms, String callTypePerson) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_green_successfull);
        dialog.setTitle(null);
        TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
        textView.setText(message);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(NotificationActivity.this, AlertLocationActivity.class);
                intent.putExtra("verifiedCode", enteredCode);
                intent.putExtra("alarmType", typeOfAlarms);
                intent.putExtra("subType", callTypePerson);
                startActivity(intent);
                dialog.dismiss();
            }
        }, 1000);
    }
}
