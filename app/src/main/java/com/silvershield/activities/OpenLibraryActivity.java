package com.silvershield.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.silvershield.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpenLibraryActivity extends AppCompatActivity {

    @BindView(R.id.open_ll_rv)
    RecyclerView open_ll_rv;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_library);
        ButterKnife.bind(this);
        layoutManager = new LinearLayoutManager(OpenLibraryActivity.this);
        open_ll_rv.setLayoutManager(layoutManager);
        getAllOpenLibrary();

    }

    public void getAllOpenLibrary() {

    }
}
