package com.silvershield.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.silvershield.R;
import com.silvershield.adapter.EntryFromAdapter;
import com.silvershield.adapter.PurposeAdapter;
import com.silvershield.adapter.VisitIndividualAdapter;
import com.silvershield.adapter.VisitorDocTypeAdapter;
import com.silvershield.custome_controls.Custome_Bold_TextView;
import com.silvershield.modal.Docs_type;
import com.silvershield.modal.Entry_from;
import com.silvershield.modal.GetSuffixDoctypeResponse;
import com.silvershield.modal.Individuals;
import com.silvershield.modal.Purpose;
import com.silvershield.modal.ScanUserDetail;
import com.silvershield.modal.Suffix;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class ScanUserDetailActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.txt_name_tv)
    Custome_Bold_TextView txt_name_tv;

    ScanUserDetail scanUserDetail;
    @BindView(R.id.visiting_dept)
    EditText visiting_dept;
    @BindView(R.id.visiting_individual)
    AutoCompleteTextView txtSearch;
    @BindView(R.id.entry_form_et)
    EditText entrng_frm;
    @BindView(R.id.purpose_of_visit)
    EditText purpose_of_visit;
    @BindView(R.id.profile_img)
    ImageView profile_img;
    String  suffixId;
    List<Suffix> suffixList;
    List<Docs_type> departmentsList;
    List<Entry_from> entry_from;
    List<Individuals> individualsList;
    List<Purpose> purposesList;
String vms_enteryFromId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_user_detail);
        ButterKnife.bind(this);

        getAllDetail();

        if (getIntent() != null) {
            scanUserDetail = getIntent().getParcelableExtra("user_detail");
            if (scanUserDetail != null) {

                if (scanUserDetail.getLast_name() == null) {
                    txt_name_tv.setText(scanUserDetail.getFirst_name() + "");
                } else {
                    txt_name_tv.setText(scanUserDetail.getFirst_name() + "" + scanUserDetail.getLast_name());
                }

                if (scanUserDetail.getProfile_pic().equalsIgnoreCase("")) {
                    Glide.with(getApplicationContext()).load(scanUserDetail.getProfile_pic()).into(profile_img);
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScanUserDetailActivity.this.finish();
        (new ActivityCapture()).finish();
    }

    public void getAllDetail() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllNewVisitorPageDetail(APIServerResponse.GET_NEW_VISITOR_DETAIL,getAuthToken(), getParentIdValue(), getClientId(), getUserID(), ScanUserDetailActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.entry_form_et)
    public void entryFromEt() {
        final Dialog dialog = new Dialog(ScanUserDetailActivity.this);
        dialog.setContentView(R.layout.custome_listview);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
        EditText entery_form_et = (EditText) findViewById(R.id.entry_form_et);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        if (entry_from.size() != 0) {
            EntryFromAdapter suffixAdapter = new EntryFromAdapter(getApplicationContext(), dialog, entry_from, entery_form_et,vms_enteryFromId);
            cat_rv.setAdapter(suffixAdapter);
            suffixAdapter.notifyDataSetChanged();
        }

        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @OnClick(R.id.visiting_dept)
    public void visitoeDeptChoose() {
        final Dialog dialog = new Dialog(ScanUserDetailActivity.this);
        dialog.setContentView(R.layout.custome_listview);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        EditText editText = (EditText) findViewById(R.id.visiting_dept);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        if (departmentsList.size() != 0) {
            VisitorDocTypeAdapter visitorDocTypeAdapter = new VisitorDocTypeAdapter(getApplicationContext(), dialog, departmentsList, editText,"");
            cat_rv.setAdapter(visitorDocTypeAdapter);
            visitorDocTypeAdapter.notifyDataSetChanged();
        }

        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    //purpose_of_visit
    @OnClick(R.id.purpose_of_visit)
    public void pusposeOfVisit() {
        final Dialog dialog = new Dialog(ScanUserDetailActivity.this);
        dialog.setContentView(R.layout.custome_listview);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        EditText editText = (EditText) findViewById(R.id.purpose_of_visit);
        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cat_rv.setLayoutManager(linearLayoutManager);

        if (departmentsList.size() != 0) {
            PurposeAdapter visitingDeptAdapter = new PurposeAdapter(getApplicationContext(), dialog, purposesList, editText,"");
            cat_rv.setAdapter(visitingDeptAdapter);
            visitingDeptAdapter.notifyDataSetChanged();
        }

        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case GET_NEW_VISITOR_DETAIL:
                        GetSuffixDoctypeResponse getSuffixDoctypeResponse = (GetSuffixDoctypeResponse) response.body();
                        if (getSuffixDoctypeResponse.getStatus().equalsIgnoreCase("OK")) {
                            suffixList = getSuffixDoctypeResponse.getSuffix();
                            departmentsList = getSuffixDoctypeResponse.getDocs_type();
                            entry_from = getSuffixDoctypeResponse.getEntry_from();
                            individualsList = getSuffixDoctypeResponse.getIndividuals();
                            purposesList=getSuffixDoctypeResponse.getPurpose();
                            txtSearch.setThreshold(1);
                            VisitIndividualAdapter adapter = new VisitIndividualAdapter(this, R.layout.activity_main, R.id.category_name_tv, individualsList);
                            txtSearch.setAdapter(adapter);
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

}
