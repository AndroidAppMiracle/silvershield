package com.silvershield.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.manateeworks.BarcodeScanner;
import com.silvershield.R;
import com.silvershield.utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScaningActivity extends BaseActivity {

    private final int SELECT_PHOTO = 1;
    private final int STORAGE_PERMISSION = 1351;
    private final Rect RECT_FULL = new Rect(0, 0, 100, 100);
    @BindView(R.id.scn_qk_badge_btn)
    Button scn_qk_badge_btn;
    @BindView(R.id.scn_any_id_btn)
    Button scn_any_id_btn;
    @BindView(R.id.add_mannualy_btn)
    Button add_mannualy_btn;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.logout_btn)
    ImageView alertLogs;
    @BindView(R.id.watch_list_img)
    ImageView watch_list_img;
    @BindView(R.id.alert_img)
    ImageView alert_img;
    private boolean decoderIsInit = false;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scaning);
        ButterKnife.bind(this);
        mContext = this;
        title_tv.setText("Visitors");
        alertLogs.setVisibility(View.GONE);
        watch_list_img.setVisibility(View.VISIBLE);
        alertLogs.setImageDrawable(getResources().getDrawable(R.mipmap.alert_logs));

        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        if (showAlertIcon())
            alert_img.setVisibility(View.VISIBLE);
        else
            alert_img.setVisibility(View.GONE);

        /* BARCODE SCANNER LIB  VERSION */
        int ver = BarcodeScanner.MWBgetLibVersion();
        int v1 = (ver >> 16);
        int v2 = (ver >> 8) & 0xff;
        int v3 = (ver & 0xff);

        add_mannualy_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCheckInVisitorsValue().equalsIgnoreCase("0")) {
                    alertWithSingleTitle("SilverShield", "Your are not allowed to checkin visitor.Please contact your Admin.", "error");
                } else {
                    addMannually();
                }
            }
        });
    }

    @OnClick(R.id.alert_img)
    public void alertImgClick() {
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.watch_list_img)
    public void watchListShow() {
        Intent intent = new Intent(getApplicationContext(), VisitorWithSearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.logout_btn)
    public void alertLogs() {
        Intent intent = new Intent(getApplicationContext(), AlertLogsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.home_tv)
    public void backCall() {
        ScaningActivity.this.finish();
    }

    @OnClick(R.id.scn_qk_badge_btn)
    public void scanQuickBadge() {
        Intent intent = new Intent(ScaningActivity.this, ActivityCapture.class);
        intent.putExtra("calling_type", "budge");
        startActivity(intent);
    }

    @OnClick(R.id.scn_any_id_btn)
    public void scanAnyId() {
        Intent intent = new Intent(ScaningActivity.this, ActivityCapture.class);
        intent.putExtra("calling_type", "other_id");
        startActivity(intent);
    }

    /*   @OnClick(R.id.add_mannualy_btn)*/
    public void addMannually() {
        Intent intent = new Intent(getApplicationContext(), AddNewVisitorActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScaningActivity.this.finish();
    }
}