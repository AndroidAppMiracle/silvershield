package com.silvershield.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.adapter.SettingAdapter;
import com.silvershield.modal.SettingMainResponse;
import com.silvershield.modal.SettingsModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class SettingsActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.setting_rv)
    RecyclerView setting_rv;
    @BindView(R.id.null_layout)
    LinearLayout null_layout;
    LinearLayoutManager linearLayoutManager;
    List<SettingsModel> settings;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.alert_img)
    ImageView alert_img;
    @BindView(R.id.watch_list_img)
    ImageView seeProfile;
    SettingAdapter settingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        getAllSetting();
        logout_btn.setVisibility(View.GONE);
        if (showAlertIcon())
            alert_img.setVisibility(View.VISIBLE);
        else
            alert_img.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        seeProfile.setVisibility(View.VISIBLE);
        seeProfile.setImageDrawable(getResources().getDrawable(R.drawable.profile));
        title_tv.setText("Settings");

        linearLayoutManager = new LinearLayoutManager(this);
        setting_rv.setLayoutManager(linearLayoutManager);
    }

    @OnClick(R.id.watch_list_img)
    public void seeProfile() {
        Intent intent = new Intent(getApplicationContext(), MyProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.alert_img)
    public void alertNotification() {
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.home_tv)
    public void backCall() {
        SettingsActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        SettingsActivity.this.finish();
    }

    public void getAllSetting() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getSetting(APIServerResponse.SETTINGS, getAuthToken(), getUserID(), SettingsActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public String checkState(int flag, int state) {
        if (flag == 1 && state == 1) {
            return "1";
        }
        return "0";
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case SETTINGS:
                        SettingMainResponse settingReponse = (SettingMainResponse) response.body();
                        if (settingReponse.getStatus().equalsIgnoreCase("OK")) {
                            settings = settingReponse.getSettings();
                            for (int i = 0; i < settings.size(); i++) {
                                if (settings.get(i).getTitle().equalsIgnoreCase("alertAdminButton")) {
                                    setAlertAdmin(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertGuardAndAdminButton")) {
                                    setGuardAndAdmin(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertLockdownButton")) {
                                    setLockdown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertPoliceEmergencyButton")) {
                                    setalertPoliceEmergency(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertPoliceNonEmergencyButton")) {
                                    setAlertPoliceNonEmergencyButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("checkInVisitors")) {
                                    setCheckInVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("checkOutVisitors")) {
                                    setCheckOutVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("createQuickBadge")) {
                                    setCreateQuickBadge(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("createVipList")) {
                                    setCreateVipList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("createWatchList")) {
                                    setCreateWatchList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("sexOffenderCheck")) {
                                    setSexOffenderCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("criminalBackgroundCheck")) {
                                    setCriminalBackgroundCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("eventSexOffenderCheck")) {
                                    setEventSexOffenderCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("eventCriminalBackgroundCheck")) {
                                    setEventCbcCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("allowCallPolice")) {
                                    setAllowCallPolice(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("editPoliceContactNumbers")) {
                                    setEditPoliceContactNumbers(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageEvents")) {
                                    setManageEvent(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageBadges")) {
                                    setManageBadges(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageStaff")) {
                                    setManageStaff(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageGuards")) {
                                    setManageGuards(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageVisitors")) {
                                    setManageVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageTeachers")) {
                                    setManageTeachers(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageStudents")) {
                                    setManageStudents(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageParents")) {
                                    setManageParents(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageVideos")) {
                                    setManageVideos(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertGuardsButton")) {
                                    setAlertGuardsButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertStaffButton")) {
                                    setAlertStaffButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("manageSelfSignupRequests")) {
                                    setManageSelfSignupRequests(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertHardLockdownButton")) {
                                    setHardLockDown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertSoftLockdownButton")) {
                                    setSoftLockDown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertHardLockdownButtonDrill")) {
                                    setAlertHardLockdownButtonDrill(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("alertScreen")) {
                                    setAlertScreen(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("viewWatchList")) {
                                    setViewWatchList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                } else if (settings.get(i).getTitle().equalsIgnoreCase("viewVipList")) {
                                    setViewVipList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
                                }
                            }
                            if (showAlertIcon())
                                alert_img.setVisibility(View.VISIBLE);
                            else
                                alert_img.setVisibility(View.GONE);
                            if (settings.size() > 0) {
                                settingAdapter = new SettingAdapter(getApplicationContext(), settings);
                                setting_rv.setAdapter(settingAdapter);
                                settingAdapter.notifyDataSetChanged();
                                settingAdapter.notifyDataSetChanged();
                            } else {
                                null_layout.setVisibility(View.VISIBLE);
                                setting_rv.setVisibility(View.GONE);
                            }
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
