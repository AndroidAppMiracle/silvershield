package com.silvershield.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.SoAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Offenders;
import com.silvershield.modal.SOResponse;
import com.silvershield.modal.StatusMessageModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;
import com.silvershield.utils.RecyclerViewClick;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class SoOffenderActivity extends BaseActivity implements RecyclerViewClick, APIServerResponse {

    @BindView(R.id.offender_name_tv)
    Custome_Regular_TextView offender_name_tv;

    @BindView(R.id.main_profile_img)
    ImageView main_profile_img;

    @BindView(R.id.alert_img)
    ImageView alert_img;
    @BindView(R.id.list_criminal_rv)
    RecyclerView list_criminal_rv;

    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swip_refresh;

    List<Offenders> offendersList;
    public static Offenders offenderDetail;
    public static String matchId = "";
    public static String selectedItem = "0", matchedPhoto = "";
    String firstName = "", lastName = "", profileUrl = "", phNumber = "", dobSt = "", emailSt = "",
            entryFrom = "", purpose_id = "", suffixChoose = "", docTypeId = "", finalDate = "",
            visitorIdNumber = "", visiting_individual_id = "", visiting_department_id = "",
            vms_visitor_entering_from_id = "", vms_visitor_purpose_id = "", timingST = "",
            country_idSt = "", ageSt = "";
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;

    Dialog dialog;
    JsonObject fullJsonObject;
    public static Boolean cbFlag = false, soFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_so_offender);
        ButterKnife.bind(this);
        alert_img.setVisibility(View.GONE);
        if (showAlertIcon())
            home_img.setVisibility(View.VISIBLE);
        else
            home_img.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        logout_btn.setImageDrawable(getResources().getDrawable(R.mipmap.red_notification));
        logout_btn.setVisibility(View.GONE);
        title_tv.setText("Sex Offender Check");


        Bundle bundle = getIntent().getExtras();
        offendersList = (List<Offenders>) bundle.getSerializable("offender_array");
        if (bundle.containsKey("offender_parciable")) {
            if (bundle.getParcelable("offender_parciable") != null) {
                offendersList = (List<Offenders>) bundle.getParcelable("offender_parciable");
            }
        }
        if (getIntent().hasExtra("profileUrl")) {
            profileUrl = getIntent().getStringExtra("profileUrl");
        }
        if (bundle.containsKey("first_name")) {
            firstName = bundle.getString("first_name");
        }
        if (bundle.containsKey("last_name")) {
            lastName = bundle.getString("last_name");
        }
        if (bundle.containsKey("email")) {
            emailSt = bundle.getString("email");
        }

        if (bundle.containsKey("phNumber")) {
            phNumber = bundle.getString("phNumber");
        }
        if (bundle.containsKey("age")) {
            ageSt = bundle.getString("age");
        }
        cbFlag = bundle.getBoolean("cbFlag");
        if (bundle.containsKey("purpose_id")) {
            purpose_id = bundle.getString("purpose_id");
        }
        if (bundle.containsKey("suffixChoose")) {
            suffixChoose = bundle.getString("suffixChoose");
        }

        if (bundle.containsKey("docTypeId")) {
            docTypeId = bundle.getString("docTypeId");
        }
        if (bundle.containsKey("finalDate")) {
            finalDate = bundle.getString("finalDate");
        }
        if (bundle.containsKey("visitorIdNumber")) {
            visitorIdNumber = bundle.getString("visitorIdNumber");
        }
        if (bundle.containsKey("visiting_individual_id")) {
            visiting_individual_id = bundle.getString("visiting_individual_id");
        }
        if (bundle.containsKey("visiting_department_id")) {
            visiting_department_id = bundle.getString("visiting_department_id");
        }

        if (bundle.containsKey("vms_visitor_entering_from_id")) {
            vms_visitor_entering_from_id = bundle.getString("vms_visitor_entering_from_id");
        }
        if (bundle.containsKey("vms_visitor_purpose_id")) {
            vms_visitor_purpose_id = bundle.getString("vms_visitor_purpose_id");
        }
        if (bundle.containsKey("timing")) {
            timingST = bundle.getString("timing");
        }

        if (bundle.containsKey("country_idSt")) {
            country_idSt = bundle.getString("country_idSt");
        }
        if (bundle.containsKey("log_id")) {
            matchId = bundle.getString("log_id");
        }

        offender_name_tv.setText(lastName + ", " + firstName);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.dummy_users);
        requestOptions.error(R.drawable.dummy_users);

        if (getIntent().hasExtra("profileUrl")) {
            profileUrl = getIntent().getStringExtra("profileUrl");
        }
     /*   downloadImage();*/
        if (getIntent().hasExtra("profileUrl")) {
            if (!getIntent().getStringExtra("profileUrl").equalsIgnoreCase("")) {
                Glide.with(this).setDefaultRequestOptions(requestOptions).load(getIntent().getStringExtra("profileUrl")).into(main_profile_img).onLoadFailed(getResources().getDrawable(R.drawable.dummy_users));
            }
        }
        SoAdapter adapter = new SoAdapter(SoOffenderActivity.this, offendersList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        list_criminal_rv.setLayoutManager(linearLayoutManager);
        list_criminal_rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @OnClick(R.id.continue_btn)
    public void continueBtn() {

       /* if (selectedItem == null || selectedItem.equals("0")) {
            Toast.makeText(getApplicationContext(), "Please select one person from the list.", Toast.LENGTH_SHORT).show();
        } else {*/
        /* if (soFlag) {*/
        createVisitor();
        /*   } else {
         *//* if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().checkSexOffenderExistence(APIServerResponse.CHECK_SEX_OFFENDER, getAuthToken(), firstName, lastName, ageSt, *//**//*getClientId()*//**//*getClientId(), SoOffenderActivity.this);
            } else {
                showSnack(Constants.INTERNET_CONNECTION);
            }*//*

            Intent intent = new Intent(getApplicationContext(), VisitorInfoActivity.class);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String myDate = format.format(new Date());
            intent.putExtra("timing", localToGMT(myDate));
            intent.putExtra("firstName",firstName);
            intent.putExtra("lastName", lastName);
            intent.putExtra("age", ageSt);
            if (!profileUrl.equalsIgnoreCase("")) {
                intent.putExtra("profileUrl", getIntent().getStringExtra("profileUrl"));
            } else {
                intent.putExtra("profileUrl", "");
            }

            // intent.putExtra("log_id",);
            startActivity(intent);
            finish();


            *//* }*//*
        }*/

    }

    @OnClick(R.id.home_tv)
    public void backButtonClick() {
        SoOffenderActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SoOffenderActivity.this.finish();
    }

    public void createVisitor() {
        ServerAPI.getInstance().createVisitor(APIServerResponse.CREATE_VISITOR, getAuthToken(),
                firstName, lastName, "",
                suffixChoose, emailSt, phNumber, finalDate,
                getUserID(), getParentIdValue(), docTypeId, visitorIdNumber,
                timingST, getUserID(), visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id,
                vms_visitor_purpose_id, "", profileUrl, getClientId(), "", "",
                "", country_idSt, "0", getUserID(), matchId, SoOffenderActivity.this);
    }

    @OnClick(R.id.stop_btn)
    public void stopButtonClick() {
        try {
            if (selectedItem.equals("0")) {
                Toast.makeText(getApplicationContext(), "Please select one person from the array", Toast.LENGTH_SHORT).show();
            } else {
                // downloadImage();
                if (isConnectedToInternet()) {
                    showLoading();
                    ServerAPI.getInstance().stopVisitor(APIServerResponse.STOP_VISITOR, getAuthToken(), getUserID(), getParentIdValue(),
                            getClientId(), entryFrom, offenderDetail.getFirstname(),
                            String.valueOf(offendersList.size()), "SO", getFirstName() + "" + getLastName(),
                            firstName + "" + lastName,
                            matchedPhoto, offenderDetail.getFirstname(),
                            offenderDetail.getLastname(), emailSt, phNumber, getDOB(), profileUrl, matchId, SoOffenderActivity.this);

                } else {
                    showSnack(Constants.INTERNET_CONNECTION);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(getApplicationContext(), "Please select one person from the array", Toast.LENGTH_SHORT).show();
        }
    }

    public void downloadImage() {

        showLoading();
        if (profileUrl.equalsIgnoreCase("")) {

        } else {
            profileUrl = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, profileUrl, SoOffenderActivity.this);
        }
        hideLoading();

    }

    @Override
    public void productClick(View v, int position) {
        int itemClickedPostion = list_criminal_rv.getChildPosition(v);
        try {
            //  jsonObject = jsonArray.getJSONObject(itemClickedPostion);
            offendersList.get(itemClickedPostion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    AlertDialog.Builder builder;

    @OnClick(R.id.criminal_btn)
    public void checkSb() {

        if (!cbFlag) {
            builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.add_new_user_alert_popup_string))
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().checkCB(APIServerResponse.CHECK_CB, getAuthToken(), firstName, lastName, ageSt,
                                        emailSt, phNumber, getUserID(), profileUrl, getClientId(),
                                        getParentIdValue(), getUserID(), SoOffenderActivity.this);
                            } else {
                                showSnack(Constants.INTERNET_CONNECTION);
                            }
                        }
                    })
                    .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            Toast.makeText(getApplicationContext(), "CBC already check", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case CHECK_CB:
                        hideLoading();
                        fullJsonObject = (JsonObject) response.body();
                        if (fullJsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            successPage("Criminal Background clear", "criminal");
                        } else {

                        }
                        break;
                    case STOP_VISITOR:
                        hideLoading();
                        StatusMessageModel jsonObject = (StatusMessageModel) response.body();
                        if (jsonObject.getStatus().equalsIgnoreCase("OK")) {
                            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "unable to stop ", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case CHECK_SEX_OFFENDER:
                        SOResponse sexOffender = (SOResponse) response.body();
                        hideLoading();
                        if (sexOffender.getStatus().equalsIgnoreCase("OK")) {

                            if (sexOffender.getSOData().getOffenders().size() != 0) {

                                Intent intent = new Intent(getApplicationContext(), SoOffenderActivity.class);
                                List<Offenders> offendersList = sexOffender.getSOData().getOffenders();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("offender_array", (Serializable) offendersList);
                                bundle.putString("first_name", firstName);
                                bundle.putString("last_name", lastName);
                                bundle.putString("age", ageSt);
                                if (!profileUrl.equalsIgnoreCase("")) {
                                    intent.putExtra("profileUrl", getIntent().getStringExtra("profileUrl"));
                                } else {
                                    intent.putExtra("profileUrl", "");
                                }
                                intent.putExtras(bundle);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            } else {
                                soFlag = true;
                                successPage("Sex Offender Clear ", "SO");
                            }

                        } else {
                            dialog = new Dialog(SoOffenderActivity.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_green_successfull);
                            dialog.setTitle(null);
                            TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);

                            Window window = dialog.getWindow();
                            window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
                            dialog.setCancelable(true);
                            dialog.setTitle(null);
                            dialog.show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    //open new activity
                                }
                            }, 2000);

                        }

                        break;

                    case CREATE_VISITOR:
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        if(jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                            Intent intent = new Intent(getApplicationContext(), VisitorInfoActivity.class);
                            intent.putExtra("purpose", jsonObject1.get("visit_purpose").getAsString());
                            intent.putExtra("visitinDept", jsonObject1.get("department_name").getAsString());
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                            String myDate = format.format(new Date());
                            intent.putExtra("entered_by", jsonObject1.get("entered_by").getAsString());
                            /*    intent.putExtra("timing", localToGMT(myDate));*/
                            intent.putExtra("timing", jsonObject1.get("check_in").getAsString());
                            intent.putExtra("finalDate", localToGMT(myDate));
                            intent.putExtra("email", jsonObject1.get("email").getAsString());
                            intent.putExtra("phone", jsonObject1.get("phone").getAsString());
                            intent.putExtra("firstName", jsonObject1.get("first_name").getAsString());
                            intent.putExtra("lastName", jsonObject1.get("last_name").getAsString());
                            intent.putExtra("middleName", jsonObject1.get("middle_name").getAsString());
                            intent.putExtra("logId", jsonObject1.get("log_id").getAsString());
                            if (profileUrl.equalsIgnoreCase("")) {
                                intent.putExtra("profileUrl", getIntent().getStringExtra(""));
                            } else {
                                intent.putExtra("profileUrl", profileUrl);
                            }
                            intent.putExtra("object", String.valueOf(jsonObject1));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }else {
                          /*  AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddNewVisitorActivity.this);
                            alertDialog.setTitle("Alert");
                            alertDialog.setMessage("User has already checked in.");
                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog.show();*/
                            alertWithSingleTitle("Alert","User has already been checked in","error");

                        }
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.","error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }


    public void successPage(String textSt, String checkType) {
        dialog = new Dialog(SoOffenderActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_green_successfull);
        dialog.setTitle(null);
        TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
        textView.setText(textSt + "");
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                cbFlag = true;
                dialog.dismiss();
            }
        }, 1000);

    }
}
