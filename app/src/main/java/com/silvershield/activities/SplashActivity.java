package com.silvershield.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.fcm.NotifyAlertDialog;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.List;

import io.fabric.sdk.android.Fabric;
import retrofit2.Response;

public class SplashActivity extends BaseActivity implements APIServerResponse {
    private final Handler handler = new Handler();
    boolean backpress = false;
    Intent intentActivity;
    private Runnable myRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // setContentView(R.layout.activity_splash);
        updateStatusBar();
        getmPrefs();

        if (getIntent().getExtras() != null) {
            if (isAppOnForeground(getApplicationContext(), "com.silvershield")) {
                if (getUserLoggedIn()) {
                    if (getIntent().getExtras().containsKey("category")) {
                        if (getIntent().getExtras().getString("category").equalsIgnoreCase("LOCATION_MAP")) {
                            intentActivity = new Intent(getApplicationContext(), AlertMapActivity.class);
                        } else {
                            intentActivity = new Intent(getApplicationContext(), NotifyAlertDialog.class);
                        }
                        intentActivity.putExtra("NOTIFICATION_MESSAGE", "" + getIntent().getExtras().get("message"));
                        intentActivity.putExtra("category", getIntent().getExtras().get("category").toString());
                        intentActivity.putExtra("alertId", getIntent().getExtras().get("status_key").toString());
                        if (getIntent().getExtras().containsKey("location_lat")) {
                            intentActivity.putExtra("location_lat", Double.parseDouble(getIntent().getExtras().getString("location_lat")));
                            intentActivity.putExtra("location_lng", Double.parseDouble(getIntent().getExtras().getString("location_lng")));
                        }
                        intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intentActivity);
                    } else {
                        callSplashActivity();
//                        getAccessTokenStatus();
                    }
                } else {
                    Intent gotoMain = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(gotoMain);
                }
            }
        } else {
            Intent gotoMain = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(gotoMain);
        }
    }

    public void callSplashActivity() {
        if (isConnectedToInternet()) {
            if (!backpress) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("UserRole", getUserRoleString());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        } else {
            Toast.makeText(this, "Internet is not working ", Toast.LENGTH_SHORT).show();
        }
    }

    public String getUserRoleString() {
        String userType = "";
        if (getUserRole().equalsIgnoreCase("10")) {
            userType = "Super Admin";
        } else if (getUserRole().equalsIgnoreCase("20")) {
            userType = "Key System Admin";
        } else if (getUserRole().equalsIgnoreCase("60")) {
            userType = "System Admin";
        } else if (getUserRole().equalsIgnoreCase("30")) {
            userType = "STAFF";
        } else if (getUserRole().equalsIgnoreCase("40")) {
            userType = "GUARD";
        } else if (getUserRole().equalsIgnoreCase("70")) {
            userType = "STUDENT";
        } else if (getUserRole().equalsIgnoreCase("71")) {
            userType = "TEACHER";
        }
        return userType;
    }

    public void updateStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(myRunnable);
        backpress = true;
    }

    private boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public void getAccessTokenStatus() {
        if (isConnectedToInternet()) {
            showLoading();
            ;
            ServerAPI.getInstance().refreshToken(APIServerResponse.REFRESH_TOKEN, getAuthToken(), SplashActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case REFRESH_TOKEN:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (response.code() == 401 || response.code() == 403) {
                            logoutCall();
                        } else {
                            if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                                setAuthToken(jsonObject.get("authToken").getAsString());
                            }
                            callSplashActivity();
                        }
                        break;
                    case LOGOUT:
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        if (jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                            Toast.makeText(getApplicationContext(), jsonObject1.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            setUserLoggedIn(false);
                            clearPreferences();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            logoutCall();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }

    public void logoutCall() {
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().logoutUser(APIServerResponse.LOGOUT, getAuthToken(), getUserID(), SplashActivity.this);
            } else {
                showSnack(Constants.INTERNET_CONNECTION);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
