package com.silvershield.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VirtualAcadmyActivity extends BaseActivity {


    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;


   /* @BindView(R.id.assigned_by_me_ll)
    LinearLayout assigned_by_me_ll;
    @BindView(R.id.assigned_to_me_ll)
    LinearLayout assigned_to_me_ll;
    @BindView(R.id.categories_ll)
    LinearLayout categories_ll;
    @BindView(R.id.open_ll)
    LinearLayout open_ll;*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_acadmy);
        ButterKnife.bind(this);
        title_tv.setText("Virtual Accadamy");
        logout_btn.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        VirtualAcadmyActivity.this.finish();
    }

    @OnClick(R.id.home_tv)
    public void backActivity() {
        VirtualAcadmyActivity.this.finish();
    }


    @OnClick(R.id.assigned_by_me_ll)
    public void assignByMeClick() {
        Intent intent=new Intent(getApplicationContext(),AssignedByMeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.assigned_to_me_ll)
    public void assignByToClick() {
        Intent intent=new Intent(getApplicationContext(),AssignedToMeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.categories_ll)
    public void categoriesClick() {
//CategoriesActivity
        Intent intent=new Intent(getApplicationContext(),CategoriesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.open_ll)
    public void openLibraryClick() {
        Intent intent=new Intent(getApplicationContext(),OpenLibraryActivity.class);
        startActivity(intent);
    }
}
