package com.silvershield.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonObject;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.silvershield.R;
import com.silvershield.adapter.EventSpinnerAdapter;
import com.silvershield.adapter.SpinnerCustomeAdapter;
import com.silvershield.custome_controls.Custome_Bold_TextView;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.EventResponseObject;
import com.silvershield.modal.Events;
import com.silvershield.modal.PrinterId;
import com.silvershield.modal.PrinterSettingResponse;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.printers.AllWifiPrinters;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/*import org.apache.commons.io.FileUtils;*/

public class VisitorInfoActivity extends BaseActivity implements APIServerResponse, ImagePickerCallback, EasyPermissions.PermissionCallbacks {

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int RC_CAMERA_PERM = 342;
    /*
     * returning image / video
     */
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "HelloCamera";
    private final int requestCode = 20;
    public int responseCode = 101;
    Dialog dialog;
    @BindView(R.id.client_name_tv)
    Custome_Bold_TextView client_name_tv;
    @BindView(R.id.first_name_tv)
    Custome_Regular_TextView first_name_tv;
    @BindView(R.id.last_name_tv)
    Custome_Regular_TextView last_name_tv;
    //middle_name_tv
    @BindView(R.id.middle_name_tv)
    Custome_Regular_TextView middle_name_tv;
    @BindView(R.id.curent_user_name)
    Custome_Regular_TextView curent_user_name;
    @BindView(R.id.checkin_time)
    Custome_Regular_TextView checkin_time;
    @BindView(R.id.logined_user_name)
    Custome_Regular_TextView logined_user_name;
    @BindView(R.id.purpose_of_visit)
    Custome_Regular_TextView purpose_of_visit;
    @BindView(R.id.email_et)
    EditText email_et;
    @BindView(R.id.phone_tv)
    EditText phone_et;
    @BindView(R.id.countryCode)
    TextView countryCode;
    @BindView(R.id.qb_switch)
    Switch qb_switch;
    @BindView(R.id.vip_switch)
    Switch vip_switch;
    @BindView(R.id.se_switch)
    Switch se_switch;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.print_btn)
    Button print_btn;
    @BindView(R.id.profile_img)
    AppCompatImageView profile_img;
    @BindView(R.id.circular_progress_bar)
    ProgressBar mProgress;
    String purposeSt = "N/A", visitinDept = "", timing = "", firstName = "", lastName = "",
            middleName = "", countryCodeSt = "", entered_by = "";
    JSONObject jsonObject;
    String visitorId = "0", isVip = "0", isSE = "0", logId = "0", is_qb = "0", email = "", phone = "", evenTitleSt, companyNameSt, eventNameSt, rsvpValueSt = "pending", eventIdSt, profileUrl = "";
    List<Events> eventsList = new ArrayList<>();
    List<String> rsArray = new ArrayList<>();
    List<PrinterId> printerIdList;
    Spinner rsvp_spn, eventName;
    EditText eventTitle, companyNameEt;
    ImageView closeImg;
    DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //HH for hour of the day (0 - 23)
    DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd h:mm a");
    private Uri fileUri;
    private String pickerPath = "";
    private CameraImagePicker cameraPicker;

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_info);
        ButterKnife.bind(this);

        home_img.setVisibility(View.GONE);
        logout_btn.setImageDrawable(getResources().getDrawable(R.drawable.close));
        title_tv.setText("Visitor Info");
        getEvents();
        if (getIntent().hasExtra("entered_by")) {
            entered_by = getIntent().getStringExtra("entered_by");
        }
        if (getIntent().hasExtra("object")) {
            try {
                String jsonResponse = getIntent().getStringExtra("object");
                profileUrl = "";
                jsonObject = new JSONObject(jsonResponse);
                visitorId = jsonObject.getString("id");

                if (jsonObject.has("logId")) {
                    logId = jsonObject.getString("logId");
                }
                if (jsonObject.has("is_vip")) {
                    isVip = jsonObject.getString("is_vip");
                }
                if (jsonObject.has("is_qb")) {
                    is_qb = jsonObject.getString("is_qb");
                }
                if (jsonObject.has("is_se")) {
                    isSE = jsonObject.getString("is_se");
                }
                if (jsonObject.has("log_id")) {
                    logId = jsonObject.getString("log_id");
                }

               /* if (pickerPath.equalsIgnoreCase("")) {
                    profile_img.setImageDrawable(getResources().getDrawable(R.drawable.dummy_users));
                } else {
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                    requestOptions.dontTransform();
                    requestOptions.placeholder(getResources().getDrawable(R.drawable.dummy_users));
                    Glide.with(getApplicationContext()).load(getIntent().getStringExtra("profileUrl")).apply(requestOptions).into(profile_img).onLoadFailed(getResources().getDrawable(R.drawable.dummy_users));
                }*/

                if (jsonObject.has("profile_pic_web")) {
                    profileUrl = jsonObject.getString("profile_pic_web");
                    new downloadImage().execute(jsonObject.getString("profile_pic_web"));
                } else {
                    if (getIntent().getStringExtra("profileUrl") != null) {
                        pickerPath = getIntent().getStringExtra("profileUrl");
                    } else {
                        profile_img.setImageDrawable(getResources().getDrawable(R.drawable.dummy_users));
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }


        //curent_user_name
        if (getIntent().hasExtra("visitor_individual")) {
            curent_user_name.setText(getIntent().getStringExtra("visitor_individual"));
        }

        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VisitorInfoActivity.this.finish();
            }
        });


        if (getIntent().hasExtra("purpose")) {
            purposeSt = getIntent().getStringExtra("purpose");
        }
        if (getIntent().hasExtra("visitinDept")) {
            visitinDept = getIntent().getStringExtra("visitinDept");
        }
        if (getIntent().hasExtra("timing")) {
            timing = getIntent().getStringExtra("timing");
        }
        if (getIntent().hasExtra("email")) {
            email = getIntent().getStringExtra("email");
        }
        if (getIntent().hasExtra("phone")) {
            phone = getIntent().getStringExtra("phone");
            if ((getIntent().getStringExtra("phone").contains("+")) && (getIntent().getStringExtra("phone").length() > 1)) {
                if ((getIntent().getStringExtra("phone").length()) > 10 && (getIntent().getStringExtra("phone").contains("+"))) {
                    String code = getIntent().getStringExtra("phone").substring(0, 3);
                    countryCode.setText(code);
                    phone = getIntent().getStringExtra("phone");
                    if (phone.length() == 13) {
                        phone = phone.substring(3, 13);
                    }
                } else {
                    countryCode.setText("+" + getCountryDialCode(this) + "");
                }
            } else {
                countryCode.setText("+" + getCountryDialCode(this) + "");
            }

        }

        if (getIntent().hasExtra("firstName")) {
            firstName = getIntent().getStringExtra("firstName");
        }

        if (getIntent().hasExtra("lastName")) {
            lastName = getIntent().getStringExtra("lastName");
        }

        if (getIntent().hasExtra("middleName")) {
            middleName = getIntent().getStringExtra("middleName");
        }


        if (getIntent().hasExtra("is_qb")) {
            is_qb = getIntent().getStringExtra("is_qb");
        }

        first_name_tv.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        last_name_tv.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        last_name_tv.setText(lastName + "");
        first_name_tv.setText(firstName + "");
        middle_name_tv.setText(middleName + "");
        client_name_tv.setText(getClientNameValue() + "");


        checkin_time.setText(convertTiming(timing) + "");

        logined_user_name.setText(getFirstName() + " " + getLastName() + "(" + getUserName() + ")");
        purpose_of_visit.setText(purposeSt + "");

        if (!phone.isEmpty()) {
            phone_et.setText(phone);
        } else {
            countryCode.setText("+" + getCountryDialCode(this) + "");
        }
        if (!email.isEmpty()) {
            email_et.setText(email);
        }
        if (isSE.equalsIgnoreCase("1")) {
            se_switch.setChecked(true);
        } else {
            se_switch.setChecked(false);
        }
        if (isVip.equalsIgnoreCase("1")) {
            vip_switch.setChecked(true);
        } else {
            vip_switch.setChecked(false);
        }
        if (is_qb.equalsIgnoreCase("1")) {
            qb_switch.setChecked(true);
        } else {
            qb_switch.setChecked(false);
        }

        qb_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked) {
                    is_qb = "1";
                    // Toast.makeText(getApplicationContext(), "is checked", Toast.LENGTH_SHORT).show();
                    if (phone_et.getText().equals("")) {

                    } else {
                        phone = phone_et.getText().toString();
                    }


                    if (email_et.getText().equals("")) {

                    } else {
                        email = email_et.getText().toString();
                    }


                    setQbClick();
                } else {
                    is_qb = "0";
                    if (phone_et.getText().equals("")) {

                    } else {
                        phone = phone_et.getText().toString();
                    }

                    if (email_et.getText().equals("")) {

                    } else {
                        email = email_et.getText().toString();
                    }

                    //  Toast.makeText(getApplicationContext(), "is not checked", Toast.LENGTH_SHORT).show();
                    setQbClick();
                }
            }
        });


        vip_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked) {
                    isVip = "1";
                    // Toast.makeText(getApplicationContext(), "is checked", Toast.LENGTH_SHORT).show();
                } else {
                    isVip = "0";
                    // Toast.makeText(getApplicationContext(), "is not checked", Toast.LENGTH_SHORT).show();
                }
            }
        });


        se_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked) {
                    isSE = "1";
                    if (phone_et.getText().equals("")) {
                    } else {
                        phone = phone_et.getText().toString();
                    }

                    if (email_et.getText().equals("")) {

                    } else {
                        email = email_et.getText().toString();
                    }

                    // Toast.makeText(getApplicationContext(), "is checked", Toast.LENGTH_SHORT).show();
                    showEventDialog();
                } else {
                    isSE = "0";
                    if (phone_et.getText().equals("")) {

                    } else {
                        phone = phone_et.getText().toString();
                    }


                    if (email_et.getText().equals("")) {

                    } else {
                        email = email_et.getText().toString();
                    }
                    showEventDialog();

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        VisitorInfoActivity.this.finish();
    }

    public void downloadImage(String profileUrl) {
        this.profileUrl = profileUrl;
        if (profileUrl.equalsIgnoreCase("")) {
        } else {
            pickerPath = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, this.profileUrl, new APIServerResponse() {
                @Override
                public void onSuccess(int tag, Response response) {
                    if (response.isSuccessful()) {
                        hideLoading();
                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.placeholder(R.drawable.dummy_users);
                        requestOptions.error(R.drawable.dummy_users);
                        try {
                            Glide.with(getApplicationContext()).load(jsonObject.getString("profile_pic_web")).apply(requestOptions).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    mProgress.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    mProgress.setVisibility(View.GONE);
                                    return false;
                                }
                            }).thumbnail(0.1f).into(profile_img);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                @Override
                public void onError(int tag, Throwable throwable) {
                    hideLoading();
                    throwable.printStackTrace();
                    alertWithSingleTitle("Alert", "Image is not getting download.Please try again.", "Error");
                }
            });
        }


    }

    @OnClick(R.id.profile_img)
    public void clickPicture() {
     /*   Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(photoCaptureIntent, requestCode);*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        hasPermissionInManifest(VisitorInfoActivity.this, Manifest.permission.CAMERA);
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();

        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                /* if (this.requestCode == requestCode && resultCode == RESULT_OK) {*/
                /*    profileUrl = data.getExtras().get("data").toString();*/
                /* }*/
                previewCapturedImage();
            }
        }
    }

    /**
     * ------------ Helper Methods ----------------------
     */

    /*
     * Creating file uri to store image/video
     */

    /*
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        try {
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            pickerPath = fileUri.getPath();
            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);

            profile_img.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    @OnClick(R.id.print_btn)
    public void getAllPrinterCall() {
        /*getAllPrinter();*/
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().createBadge(APIServerResponse.CREATE_BADGE, getAuthToken(), getUserID(),
                    visitorId, isVip, is_qb, isSE, logId, pickerPath,
                    email, "+" + getCountryDialCode(getApplicationContext()) + phone, VisitorInfoActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void getAllPrinter() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getPrinterSetting(APIServerResponse.GET_ALL_PRINTER, getAuthToken(), getClientId(), VisitorInfoActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void showEventDialog() {
        dialog = new Dialog(VisitorInfoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.event_entry_layout);
        dialog.setTitle(null);
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

        rsvp_spn = (Spinner) dialog.findViewById(R.id.rsvp_spn);
        companyNameEt = (EditText) dialog.findViewById(R.id.company_name);
        eventTitle = (EditText) dialog.findViewById(R.id.title_tv);
        eventName = (Spinner) dialog.findViewById(R.id.event_name_st);
        closeImg = (ImageView) dialog.findViewById(R.id.imageView_close);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        rsArray.clear();
        rsArray.add("pending");
        rsArray.add("Yes");
        rsArray.add("No");

        SpinnerCustomeAdapter customAdapter = new SpinnerCustomeAdapter(getApplicationContext(), /*eventsList*/rsArray);
        rsvp_spn.setAdapter(customAdapter);
        rsvp_spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), rsArray.get(i), Toast.LENGTH_LONG).show();
                rsvpValueSt = rsArray.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        if (eventsList.size() != 0) {
            EventSpinnerAdapter eventSpinnerAdapter = new EventSpinnerAdapter(getApplicationContext(), eventsList);
            eventName.setAdapter(eventSpinnerAdapter);

            eventName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Toast.makeText(getApplicationContext(), eventsList.get(i).getEvent_name(), Toast.LENGTH_LONG).show();
                    eventNameSt = eventsList.get(i).getEvent_name();
                    eventIdSt = eventsList.get(i).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
        Button saveBtn = (Button) dialog.findViewById(R.id.save_btn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                evenTitleSt = eventTitle.getText().toString();
                companyNameSt = companyNameEt.getText().toString();
                if (companyNameEt.getText().toString().equalsIgnoreCase("") || companyNameEt.getText().toString().isEmpty()) {

                    Toast.makeText(getApplicationContext(), "Company Name cannot be blank", Toast.LENGTH_SHORT).show();
                } else if (eventNameSt.equalsIgnoreCase("") || eventNameSt.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Event Name cannot be blank", Toast.LENGTH_SHORT).show();
                } else if (eventNameSt.equalsIgnoreCase("") || eventNameSt.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Event Name cannot be blank", Toast.LENGTH_SHORT).show();
                } else if (rsvpValueSt.equalsIgnoreCase("") || rsvpValueSt.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "rsvp should not be blank", Toast.LENGTH_SHORT).show();
                } else {
                    createGuestEvent();
                }
            }
        });

        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();

    }

    public void setQbClick() {


        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().setQb(APIServerResponse.SET_QB, getAuthToken(), getUserID(), visitorId, isVip, is_qb, isSE, logId, getUserID(), email, phone, pickerPath, VisitorInfoActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void getEvents() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllEvents(APIServerResponse.GET_ALL_EVENT, getAuthToken(), getUserID(), getClientId(), VisitorInfoActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void createGuestEvent() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().createEventGuest(APIServerResponse.CREATE_GUEST_EVENT, getAuthToken(), eventIdSt, firstName, lastName, evenTitleSt, companyNameSt, phone, email, rsvpValueSt, logId, VisitorInfoActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case SET_QB:
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").toString().equalsIgnoreCase("\"" + "OK" + "\"")) {
                            hideLoading();
                            Toast.makeText(VisitorInfoActivity.this, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VisitorInfoActivity.this, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case GET_ALL_EVENT:
                        EventResponseObject evenJsonObject = (EventResponseObject) response.body();
                        if (evenJsonObject.getStatus().equalsIgnoreCase("OK")) {
                            hideLoading();
                            eventsList = evenJsonObject.getEvents();
                        } else {
                            Toast.makeText(getApplicationContext(), "No Events found yet..", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case CREATE_GUEST_EVENT:
                        hideLoading();
                        JsonObject resObject = (JsonObject) response.body();
                        if (resObject.get("status").toString().equalsIgnoreCase("\"" + "OK" + "\"")) {
                            Toast.makeText(getApplicationContext(), "" + resObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            hideLoading();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), "" + resObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            hideLoading();
                            dialog.dismiss();
                        }
                        break;

                    case GET_ALL_PRINTER:
                        hideLoading();
                        PrinterSettingResponse printerSettingResponse = (PrinterSettingResponse) response.body();
                        if (printerSettingResponse.getStatus().equalsIgnoreCase("Ok")) {
                            printerIdList = printerSettingResponse.getPrinterId();
                            //-------------------gagan------------
//                            if (printerSettingResponse.getPrinterType().equalsIgnoreCase("IP")) {
//                                Intent intent = new Intent(VisitorInfoActivity.this, AllPrinterActivity.class);
//                                intent.putExtra("printerMainObject", printerSettingResponse);
//                                startActivity(intent);
//                            } else {
                            Intent intent = new Intent(getApplicationContext(), AllWifiPrinters/*PrinterMainActivity*/.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("eventId",logId);
                            bundle.putString("badge","badge");
                            intent.putExtras(bundle);
//                            intent.putExtra("badge", "badge");
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
//                            }
                            /*
                            AllPrinterAdapter allPrinterAdapter = new AllPrinterAdapter(getApplicationContext(), printerIdList);

                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                            printer_rv.setLayoutManager(linearLayoutManager);
                            printer_rv.setAdapter(allPrinterAdapter);
                            allPrinterAdapter.notifyDataSetChanged();*/
                        } else {
                            Toast.makeText(getApplicationContext(), "No printer found ", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case CREATE_BADGE:
                        hideLoading();
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        if (jsonObject1.get("status").toString().equalsIgnoreCase("\"" + "OK" + "\"")) {
                            Toast.makeText(getApplicationContext(), "" + jsonObject1.get("message").toString(), Toast.LENGTH_SHORT).show();
                            getAllPrinter();
                        } else {
                            Toast.makeText(getApplicationContext(), "" + jsonObject1.get("message").toString(), Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } catch (Exception ex) {
                hideLoading();
                ex.printStackTrace();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
        hideLoading();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {

    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    //for all setting
    private class downloadImage extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected String doInBackground(String... strings) {
            downloadImage(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {

        }
    }
}
