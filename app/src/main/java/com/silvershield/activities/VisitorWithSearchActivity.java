package com.silvershield.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.SearchWatchListAdapter;
import com.silvershield.modal.VipList;
import com.silvershield.modal.WatchList;
import com.silvershield.modal.requestModels.GetAllVisitorRequestModel;
import com.silvershield.modal.responseModels.GetAllVisitorResponseModel;
import com.silvershield.modal.responseModels.VisitorResponseModel1;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class VisitorWithSearchActivity extends BaseActivity implements APIServerResponse {

    private final int PAGE_NUMBER = 1;
    public String formattedDate, searchStringSt = "";
    @BindView(R.id.watch_list_rv)
    RecyclerView watch_list_rv;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.search_et)
    EditText search_et;
    @BindView(R.id.add_img)
    ImageView add_img;
    @BindView(R.id.null_layout)
    LinearLayout null_layout;
    @BindView(R.id.alert_img)
    ImageView alert_img;
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swip_refresh;
    @BindView(R.id.cancle_tv)
    TextView cancle_tv;
    //alert_img
    SearchWatchListAdapter watchSearchListAdapter;
    LinearLayoutManager linearLayoutManager;
    JsonObject jsonObjectSt;
    int visibleItemCount;
    EditText date_et;
    AlertDialog b;
    int i = 0;
    AlertDialog.Builder dialogBuilder;
    Calendar calendar = Calendar.getInstance();
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = PAGE_NUMBER;
    private int TOTAL_PAGES = 1;
    private ArrayList<VipList> vipLists;
    private ArrayList<WatchList> watchlist;
    private List<VisitorResponseModel1> modelList;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_with_search);
        ButterKnife.bind(this);
        if (showAlertIcon())
            alert_img.setVisibility(View.VISIBLE);
        else
            alert_img.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        title_tv.setText("Visitors");
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c.getTime());

        swip_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!searchStringSt.isEmpty()) {
                    searchString(searchStringSt);
                } else {
                    count = 0;
                    getAllWatcherList(formattedDate, count);
                }
            }
        });

        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchStringSt = charSequence.toString();
                searchString(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        initComponents();
    }

    private void initComponents() {
        modelList = new ArrayList<>();
        watchSearchListAdapter = new SearchWatchListAdapter(this, modelList, getViewVipList(), getViewWatchList());
        linearLayoutManager = new LinearLayoutManager(this);
        watch_list_rv.setLayoutManager(linearLayoutManager);
        watch_list_rv.setAdapter(watchSearchListAdapter);
        getAllVistors(count, formattedDate);

        watch_list_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                int LastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                Log.e("COUNT:>", visibleItemCount + "..." + firstVisibleItemPosition + "..." + LastVisibleItemPosition + "...." + totalItemCount);
                if (LastVisibleItemPosition + 1 == totalItemCount && totalItemCount == count) {
                    count = count + 10;
                    getAllVistors(count, formattedDate);
                }
            }
        });
    }

    private void getAllVistors(int count, String formattedDate) {
        if (isConnectedToInternet()) {
            showLoading();
            getAllWatcherList(formattedDate, count);
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.cancle_tv)
    public void clearSearch() {
        search_et.setText("");
    }

    @OnClick(R.id.add_img)
    public void addNewVisitor() {
        Intent intent = new Intent(getApplicationContext(), /*ScanUserDetailActivity*/AddNewVisitorActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.alert_img)
    public void notificationPageCall() {
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.eye_img)
    public void viewHistoryAccToDate() {
        dialogBuilder = new AlertDialog.Builder(VisitorWithSearchActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_layout_with_edittext, null);
        dialogBuilder.setTitle("View History");
        dialogBuilder.setView(dialogView);
        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        date_et = (EditText) dialogView.findViewById(R.id.edit1);
        date_et.setText(formattedDate);
        tvMsg.setText("Select the date you wish to see the history of ");
        date_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateDia();
            }
        });
        Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        Button cancleBtn = (Button) dialogView.findViewById(R.id.cancle_btn);
        //cancle_btn
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = 0;
                getAllWatcherList(formattedDate, count);
            }
        });

        cancleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.show();
    }

    public void openDateDia() {
        final Calendar cal = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("yyyy-MM-dd ").format(calendar.getTime());
                        Log.e("Date is", "Date is" + format);
                        formattedDate = format;
                        date_et.setText(format);

                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.setCancelable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    @OnClick(R.id.home_tv)
    public void backClicked() {
        VisitorWithSearchActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        VisitorWithSearchActivity.this.finish();
    }

    public void getAllWatcherList(String date, int count) {
        if (isConnectedToInternet()) {
            showLoading();
            String startTime = formattedDate + " 00:00:00";
            String endTimeTime = formattedDate + " 23:59:59";
            GetAllVisitorRequestModel requestModel = new GetAllVisitorRequestModel();
            requestModel.setClients_id(getClientId());
            requestModel.setUser_id(getUserID());
            requestModel.setParent_id(getParentIdValue());
            requestModel.setPage_index(count + "");
            requestModel.setHistory_view_date(date);
            requestModel.setStartTime(localToGMT(startTime));
            requestModel.setEndTime(localToGMT(endTimeTime));
            requestModel.setFlag_all("0");

            ServerAPI.getInstance().getAllVisitor(APIServerResponse.GETALLVISITOR, getAuthToken(), requestModel, this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void searchString(String stringToSearchSt) {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getSearchedWatchListVisitor(APIServerResponse.SEARCHED_WATCH_LIST, getAuthToken(), getUserID(), getClientId(), getParentIdValue(), stringToSearchSt, VisitorWithSearchActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (b != null) {
            b.dismiss();
        }
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case GETALLVISITOR:
                        GetAllVisitorResponseModel responseModel = (GetAllVisitorResponseModel) response.body();
                        if (swip_refresh.isRefreshing()) {
                            swip_refresh.setRefreshing(false);
                        }
                        if (responseModel.getStatus().equalsIgnoreCase("ok")) {
                            null_layout.setVisibility(View.GONE);
                            if (count == 0) {
                                modelList = responseModel.getVisitorsToday();
                            } else {
                                modelList.addAll(responseModel.getVisitorsToday());
                            }
                            watchSearchListAdapter.refreshData(modelList);
                        } else {
                            null_layout.setVisibility(View.VISIBLE);
                            watch_list_rv.setVisibility(View.GONE);
                            if (b != null) {
                                b.dismiss();
                            }
                        }
                        break;
                    case SEARCHED_WATCH_LIST:
                        JsonObject watchListReponse = (JsonObject) response.body();
                        if (swip_refresh.isRefreshing()) {
                            swip_refresh.setRefreshing(false);
                        }
                        if (watchListReponse.get("status").getAsString().equalsIgnoreCase("OK")) {
                            null_layout.setVisibility(View.GONE);
                            watch_list_rv.setVisibility(View.VISIBLE);
                            JsonArray jsonArray = watchListReponse.getAsJsonArray("users");
                            if (jsonArray.size() != 0) {
//                                watchSearchListAdapter = new SearchWatchListAdapter(VisitorWithSearchActivity.this, watchListReponse.toString(), getViewVipList(), getViewWatchList());
                                watch_list_rv.setAdapter(watchSearchListAdapter);
                            }
                        } else {
                            null_layout.setVisibility(View.VISIBLE);
                            watch_list_rv.setVisibility(View.GONE);
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        Toast.makeText(getBaseContext(), throwable.getMessage() + "", Toast.LENGTH_SHORT).show();
    }
}
