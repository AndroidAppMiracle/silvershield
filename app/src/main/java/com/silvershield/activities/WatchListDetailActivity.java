package com.silvershield.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.adapter.WatchCheckAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Offenders;
import com.silvershield.modal.SOResponse;
import com.silvershield.modal.StatusMessageModel;
import com.silvershield.modal.WatchListMatchModal;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;
import com.silvershield.utils.RecyclerViewClick;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class WatchListDetailActivity extends BaseActivity implements APIServerResponse, RecyclerViewClick {

    public static String firstName, lastName, ageSt = "", profileUrl = "", matchId = "", matchImage = "", emailSt = "", finalDate = "", selectedItem = "0";
    public static Boolean cbFlag = false, soFlag = false;
    @BindView(R.id.offender_name_tv)
    Custome_Regular_TextView offender_name_tv;
    @BindView(R.id.main_profile_img)
    ImageView main_profile_img;
    @BindView(R.id.list_criminal_rv)
    RecyclerView list_criminal_rv;
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swip_refresh;
    JsonObject fullJsonObject;
    List<WatchListMatchModal> offendersList;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    @BindView(R.id.title_tv)
    TextView title_tv;
    Dialog dialog;
    String phNumber = "", dobSt = "",
            entryFrom = "", purpose_id = "", suffixChoose = "", docTypeId = "",
            visitorIdNumber = "", visiting_individual_id = "", visiting_department_id = "",
            vms_visitor_entering_from_id = "", vms_visitor_purpose_id = "", timingST = "",
            country_idSt = "", eventCall = "";
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_list_detail);
        ButterKnife.bind(this);

        selectedItem = "0";
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));

        logout_btn.setImageDrawable(getResources().getDrawable(R.mipmap.red_notification));
        logout_btn.setVisibility(View.GONE);

        title_tv.setText("WatchList Check ");
        profileUrl = getIntent().getStringExtra("profileUrl");

        Bundle bundle = getIntent().getExtras();
        offendersList = (List<WatchListMatchModal>) bundle.getSerializable("match_list");
        firstName = bundle.getString("first_name");
        lastName = bundle.getString("last_name");
        ageSt = bundle.getString("age");

        purpose_id = bundle.getString("purpose_id");
        suffixChoose = bundle.getString("suffixChoose");
        docTypeId = bundle.getString("docTypeId");
        finalDate = bundle.getString("age");
        visitorIdNumber = bundle.getString("visitorIdNumber");
        visiting_individual_id = bundle.getString("visiting_individual_id");
        visiting_department_id = bundle.getString("visiting_department_id");
        vms_visitor_entering_from_id = bundle.getString("vms_visitor_entering_from_id");
        vms_visitor_purpose_id = bundle.getString("vms_visitor_purpose_id");
        timingST = bundle.getString("timing");
        country_idSt = bundle.getString("country_idSt");
        matchId = bundle.getString("log_id");
        cbFlag = bundle.getBoolean("cbFlag");
        soFlag = bundle.getBoolean("soFlag");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.dummy_users);
        requestOptions.error(R.drawable.dummy_users);
        offender_name_tv.setText(firstName + " " + lastName);
        if (getIntent().hasExtra("profileUrl")) {
            if (!getIntent().getStringExtra("profileUrl").equalsIgnoreCase("")) {
                profileUrl = getIntent().getStringExtra("profileUrl");
                Glide.with(this).load(getIntent().getStringExtra("profileUrl")).apply(requestOptions).into(main_profile_img).onLoadFailed(getResources().getDrawable(R.drawable.dummy_users));
            }
        }

        WatchCheckAdapter adapter = new WatchCheckAdapter(getApplicationContext(), offendersList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        list_criminal_rv.setLayoutManager(linearLayoutManager);
        list_criminal_rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.home_tv)
    public void backButtonClick() {
        WatchListDetailActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        WatchListDetailActivity.this.finish();
    }

    @OnClick(R.id.stop_btn)
    public void stopButtonClick() {
        try {
            if (selectedItem.equals("0")) {
                Toast.makeText(getApplicationContext(), "Please select one person from the array", Toast.LENGTH_SHORT).show();
            } else {
//                downloadImage();
                if (isConnectedToInternet()) {
                    showLoading();
                    ServerAPI.getInstance().stopWatchVisitor(APIServerResponse.STOP_VISITOR, getAuthToken(), getUserID(),
                            getParentIdValue(), getClientId(),
                            entryFrom, firstName + " " + lastName,
                            String.valueOf(offendersList.size()),
                            "WL", getFirstName() + " " + getLastName(),
                            firstName + " " + lastName,
                            matchImage, firstName,
                            lastName, emailSt, phNumber, dobSt,
                            profileUrl, matchId, WatchListDetailActivity.this);

                } else {
                    showSnack(Constants.INTERNET_CONNECTION);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(getApplicationContext(), "Please select one person from the array", Toast.LENGTH_SHORT).show();
        }
    }

    public void downloadImage() {
        showLoading();
        if (profileUrl.equalsIgnoreCase("")) {

        } else {
            profileUrl = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, profileUrl, WatchListDetailActivity.this);
        }
        hideLoading();

    }

    @OnClick(R.id.continue_btn)
    public void checkSexOffender() {
        if (!soFlag) {
            checkSO();
        } else {
            downloadImage();
            if (getIntent().getStringExtra("imageUrl") != null) {
                if (getIntent().getStringExtra("imageUrl").equalsIgnoreCase("")) {
                    createVisitor();
                } else {
                    createVisitorWIthUrl();
                }
            } else {
                createVisitor();
            }
        }
    }

    public void checkSO() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().checkSexOffenderExistence(APIServerResponse.CHECK_SEX_OFFENDER, getAuthToken(), firstName, lastName, ageSt, getClientId(), WatchListDetailActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    public void createVisitor() {
        ServerAPI.getInstance().createVisitor(APIServerResponse.CREATE_VISITOR, getAuthToken(),
                firstName, lastName, "",
                suffixChoose, emailSt, phNumber, finalDate,
                getUserID(), getParentIdValue(), docTypeId, visitorIdNumber,
                timingST, getUserID(), visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id,
                vms_visitor_purpose_id, "", profileUrl, getClientId(), "", "",
                "", country_idSt, "0", getUserID(), matchId, WatchListDetailActivity.this);
    }

    public void createVisitorWIthUrl() {
        ServerAPI.getInstance().createVisitorWithUrl(APIServerResponse.CREATE_VISITOR, getAuthToken(),
                firstName, lastName, "",
                suffixChoose, emailSt, phNumber, finalDate,
                getUserID(), getParentIdValue(), docTypeId, visitorIdNumber,
                timingST, getUserID(), visiting_individual_id, visiting_department_id, vms_visitor_entering_from_id,
                vms_visitor_purpose_id, "", profileUrl, getClientId(), "", "",
                "", country_idSt, "0", getUserID(), matchId, WatchListDetailActivity.this);
    }

    @OnClick(R.id.criminal_btn)
    public void checkSb() {
      /*  if (selectedItem == null || selectedItem.equals("0")) {
            Toast.makeText(getApplicationContext(), "Please select one person from the array", Toast.LENGTH_SHORT).show();
        } else {*/
        if (!cbFlag) {
            builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.add_new_user_alert_popup_string))
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().checkCB(APIServerResponse.CHECK_CB, getAuthToken(), firstName, lastName, dobSt,
                                        emailSt, phNumber, getUserID(), profileUrl, getClientId(),
                                        getParentIdValue(), getUserID(), WatchListDetailActivity.this);
                            } else {
                                showSnack(Constants.INTERNET_CONNECTION);
                            }
                        }
                    })
                    .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            Toast.makeText(getApplicationContext(), "CBC already check", Toast.LENGTH_SHORT).show();
        }
        /* }*/
    }

    @Override
    public void onSuccess(int tag, Response response) {
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case CHECK_SEX_OFFENDER:
                        SOResponse sexOffender = (SOResponse) response.body();
                        hideLoading();
                        if (sexOffender.getStatus().equalsIgnoreCase("OK")) {

                            if (sexOffender.getSOData().getOffenders().size() != 0) {

                                Intent intent = new Intent(getApplicationContext(), SoOffenderActivity.class);
                                List<Offenders> offendersList = sexOffender.getSOData().getOffenders();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("offender_array", (Serializable) offendersList);
                                bundle.putString("first_name", firstName);
                                bundle.putString("last_name", lastName);
                                bundle.putString("age", ageSt);
                                bundle.putString("profile_img", profileUrl);
                                bundle.putString("timing", timingST);
                                if (!profileUrl.equalsIgnoreCase("")) {
                                    bundle.putString("profileUrl", profileUrl);
                                } else {
                                    bundle.putString("profileUrl", "");
                                }
                                bundle.putString("log_id", matchId);
                                intent.putExtras(bundle);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                soFlag = true;
                                successPage("Sex Offender Check Cleared ", "SO");
                            }

                        } else {
                            dialog = new Dialog(WatchListDetailActivity.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_green_successfull);
                            dialog.setTitle(null);
                            TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);

                            Window window = dialog.getWindow();
                            window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
                            dialog.setCancelable(true);
                            dialog.setTitle(null);
                            dialog.show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    //open new activity
                                }
                            }, 2000);

                        }

                        break;
                    case CREATE_VISITOR:
                        JsonObject jsonObject1 = (JsonObject) response.body();
                        if (jsonObject1.get("status").getAsString().equalsIgnoreCase("OK")) {
                            Intent intent = new Intent(getApplicationContext(), VisitorInfoActivity.class);
                            intent.putExtra("entered_by", jsonObject1.get("entered_by").getAsString());
                            intent.putExtra("purpose", jsonObject1.get("visit_purpose").getAsString());
                            intent.putExtra("visitinDept", jsonObject1.get("department_name").getAsString());
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                            String myDate = format.format(new Date());
                            /*                        intent.putExtra("timing", localToGMT(myDate));*/
                            intent.putExtra("timing", jsonObject1.get("check_in").getAsString());
                            intent.putExtra("email", jsonObject1.get("email").getAsString());
                            intent.putExtra("phone", jsonObject1.get("phone").getAsString());
                            intent.putExtra("firstName", jsonObject1.get("first_name").getAsString());
                            intent.putExtra("lastName", jsonObject1.get("last_name").getAsString());
                            intent.putExtra("middleName", jsonObject1.get("middle_name").getAsString());
                            intent.putExtra("logId", jsonObject1.get("log_id").getAsString());
                            if (!profileUrl.equalsIgnoreCase("")) {
                                intent.putExtra("profileUrl", profileUrl);
                            } else {
                                intent.putExtra("profileUrl", "");
                            }
                            /*  DownloadImage(profileUrl);*/
                            intent.putExtra("object", String.valueOf(jsonObject1));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            alertWithSingleTitle("Alert", jsonObject1.get("message").getAsString(), "error");
                        }
                        break;

                    case STOP_VISITOR:
                        hideLoading();
                        StatusMessageModel jsonObject = (StatusMessageModel) response.body();
                        if (jsonObject.getStatus().equalsIgnoreCase("OK")) {
                            if (showAlertIcon()) {
                                Intent intent1 = new Intent(getApplicationContext(), NotificationActivity.class);
                                startActivity(intent1);
                            }else {
                                Toast.makeText(getApplicationContext(), jsonObject.getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "unable to stop ", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case CHECK_CB:
                        hideLoading();
                        fullJsonObject = (JsonObject) response.body();
                        cbFlag = true;
                        if (fullJsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                            successPage("Criminal Background Check is clear", "criminal");
                        } else {

                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                logoutCall();
            } else {
                alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
            }
        }
    }


    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    public void successPage(String textSt, String checkType) {
        dialog = new Dialog(WatchListDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_green_successfull);
        dialog.setTitle(null);
        TextView textView = (TextView) dialog.findViewById(R.id.txt_name_tv);
        textView.setText(textSt + "");
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setTitle(null);
        dialog.show();
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1000);

    }

    @Override
    public void productClick(View v, int position) {
        int itemClickedPostion = list_criminal_rv.getChildPosition(v);
        WatchListMatchModal watchListMatchModal = offendersList.get(itemClickedPostion);

        firstName = watchListMatchModal.getFirst_name();
        lastName = watchListMatchModal.getLast_name();
        emailSt = watchListMatchModal.getEmail();
        profileUrl = watchListMatchModal.getProfile_pic_web();


    }
}
