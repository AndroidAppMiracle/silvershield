package com.silvershield.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.fragments.VipListFragemt;
import com.silvershield.fragments.WatchListFragment;
import com.silvershield.modal.VipList;
import com.silvershield.modal.WatchList;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class WatchVipActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, APIServerResponse {

    @BindView(R.id.viewPager_friendsTab)
    ViewPager viewPager;
    @BindView(R.id.alert_img)
    ImageView alert_img;
    @BindView(R.id.tabs_friends)
    TabLayout tabLayout;
    JsonArray vipListJsonArray;
    JsonArray watchListJsonArray;
    JsonObject jsonObjectSt;
    @BindView(R.id.home_tv)
    ImageView home_img;
    @BindView(R.id.title_tv)
    TextView title_tv;
    @BindView(R.id.logout_btn)
    ImageView logout_btn;
    private ArrayList<VipList> vipLists;
    private ArrayList<WatchList> watchlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_vip);
        ButterKnife.bind(this);
        getAllWatcherList();
        title_tv.setText("Watch/Vip List");

        logout_btn.setVisibility(View.GONE);
        home_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));

        tabLayout.addTab(tabLayout.newTab().setText("Watch List"));
        tabLayout.addTab(tabLayout.newTab().setText("VIP List"));

        if (showAlertIcon())
            alert_img.setVisibility(View.VISIBLE);
        else
            alert_img.setVisibility(View.GONE);
    }

    @OnClick(R.id.home_tv)
    public void backPressed() {
       onBackPressed();
    }

    @Override
    public void onBackPressed() {
        WatchVipActivity.this.finish();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void getAllWatcherList() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllVipWatcherList(APIServerResponse.GET_VIP_WATCHER_LIST, getAuthToken(), getUserID(), getClientId(), getParentIdValue(), WatchVipActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
/*{"status":"OK",
"watchlist":[{"id":9352,"visitor_suffix":"","first_name":"jason","middle_name":"","last_name":"brown","ssn":"0","dob":"1986-07-07","phone":"+1436456","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"SO Match Found.","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/so_1515155362922id=IA19095.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2017-12-29 10:29:55","modified_at":"2018-01-05 07:29:25","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9326,"visitor_suffix":"","first_name":"Jason","middle_name":"","last_name":"brown","ssn":"0","dob":"1986-07-07","phone":"","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"CRIMINAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/so_1515072901791id=IA19095.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2017-12-28 09:45:45","modified_at":"2018-01-04 08:35:06","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9383,"visitor_suffix":"","first_name":"Steven","middle_name":"","last_name":"vanwagoner ","ssn":"0","dob":"1960-05-31","phone":"","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"CRIMINAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/so_1514983249229id=UT2303749.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2018-01-03 12:40:50","modified_at":"2018-01-03 07:40:50","visitor_id_type":0,"visitor_id_number":"","country_id":""}],"vip":[{"id":904,"visitor_suffix":"","first_name":"Amrinder","middle_name":"","last_name":"Singh","ssn":"0","dob":"","phone":"89789798","email":"test@test.com","is_vip":1,"is_watch":0,"is_qb":0,"is_se":0,"reason_watchlist_id":"","reason_viplist_id":"5","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/null","bar_code_image":"","quick_badge_image":"","created_by":670,"is_deleted":0,"updated_from":"","parent_id":670,"schools_id":"","clients_id":31,"created_at":"2017-06-26 13:46:44","modified_at":"2017-06-26 09:46:44","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":836,"visitor_suffix":"","first_name":"Steve","middle_name":"","last_name":"Job","ssn":"0","dob":"","phone":"","email":"","is_vip":1,"is_watch":0,"is_qb":0,"is_se":0,"reason_watchlist_id":"","reason_viplist_id":"1","reason_watchlist_notes":"","reason_viplist_notes":"He is key of apple computer and iphones","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/media.1498132441105.jpg","bar_code_image":"","quick_badge_image":"","created_by":670,"is_deleted":0,"updated_from":"","parent_id":670,"schools_id":"","clients_id":31,"created_at":"2017-06-22 11:54:01","modified_at":"2017-06-22 07:54:17","visitor_id_type":0,"visitor_id_number":"","country_id":""}]}*/
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GET_VIP_WATCHER_LIST:
                       /* VipWatchListReponse vipWatchListReponse = (VipWatchListReponse) response.body();
                        if (vipWatchListReponse.getStatus().equalsIgnoreCase("OK")) {
                            vipLists = vipWatchListReponse.getVipList();
                            watchlist = vipWatchListReponse.getWatchList();
                        }*/
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                            vipListJsonArray = jsonObject.getAsJsonArray("watchlist");
                            watchListJsonArray = jsonObject.getAsJsonArray("vip");

                            jsonObjectSt = jsonObject;
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    viewPager.setAdapter(new EventsAdapter(getSupportFragmentManager(), tabLayout.getTabCount()));
                                    viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                                    viewPager.setOffscreenPageLimit(0);
                                    tabLayout.setOnTabSelectedListener(WatchVipActivity.this);
                                    hideLoading();
                                }
                            }, 3000);
                        } else {

                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {

            Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
        Toast.makeText(getBaseContext(), throwable.getMessage() + "", Toast.LENGTH_SHORT).show();
    }

    public class EventsAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public EventsAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {

                case 0:
                    fragment = new WatchListFragment();
                    Bundle arg = new Bundle();
                    arg.putString("watch", String.valueOf(jsonObjectSt));
                    fragment.setArguments(arg);
                    return fragment;
                case 1:
                    fragment = new VipListFragemt();
                    Bundle arg1 = new Bundle();
                    arg1.putString("vip", String.valueOf(jsonObjectSt));
                    fragment.setArguments(arg1);
                    return fragment;
                default:
                    fragment = new WatchListFragment();
                    Bundle arg2 = new Bundle();
                    arg2.putString("vip", String.valueOf(jsonObjectSt));
                    fragment.setArguments(arg2);
                    return fragment;

            }
        }

        @Override
        public int getCount() {
            return 2;
        }

    }
}
