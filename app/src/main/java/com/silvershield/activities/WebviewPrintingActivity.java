package com.silvershield.activities;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.silvershield.R;
import com.silvershield.utils.BaseActivity;

public class WebviewPrintingActivity extends BaseActivity {

    WebView webView1;
    FloatingActionButton floatingActionButton;
    String eventId = "", badge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_printing);
        showLoading();
        floatingActionButton = (FloatingActionButton) findViewById(R.id.print_flt);
        if (getIntent() != null) {
            eventId = getIntent().getStringExtra("eventId");
            badge = getIntent().getStringExtra("badge");
        }

        webView1 = (WebView) findViewById(R.id.webView1);



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (badge!=null&&badge.equalsIgnoreCase("badge")) {
                    webView1.loadUrl("https://staging.silvershield.com/visitorlogs/printoutbadge?id=" + eventId);
                }
                createWebPrintJob(webView1);
                hideLoading();
            }
        }, 7000);


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        createWebPrintJob(webView1);
                                                    }
                                                }
        );
    }


    private void createWebPrintJob(WebView webView) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

            PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

            String jobName = getString(R.string.app_name) + " Print Test";

            printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
        }
    }
}
