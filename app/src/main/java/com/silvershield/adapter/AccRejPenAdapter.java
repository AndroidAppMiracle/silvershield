package com.silvershield.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 1/8/2018.
 */

public class AccRejPenAdapter extends RecyclerView.Adapter<AccRejPenAdapter.DataViewHolder> {

    Context context;
    JsonObject jsonObject;
    JsonArray jsonArray;

    public AccRejPenAdapter(Context context, JsonObject jsonObject) {
        this.context = context;
        this.jsonObject = jsonObject;
        try {
            jsonArray = jsonObject.getAsJsonArray("alertUsers");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_acc_rej_pen, parent, false);
        return new AccRejPenAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        JsonObject jsonObject = jsonArray.get(position).getAsJsonObject();
        if (jsonObject.has("profile_pic")) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(context.getResources().getDrawable(R.drawable.dummy_users));
            Glide.with(context).load(jsonObject.get("profile_pic").getAsString()).apply(requestOptions).into(holder.user_img);
        }
        holder.phone_email_tv.setVisibility(View.GONE);
        holder.type.setVisibility(View.GONE);
        holder.phone_email_tv.setText("Phone/Email: " + jsonObject.get("receiver_phone").getAsString());
        holder.type.setText("Type: " + jsonObject.get("alert_type").getAsString());

        holder.user_name_tv.setText("Name:" + jsonObject.get("first_name").getAsString() + " " + jsonObject.get("last_name").getAsString());
//        holder.message_tv.setText(jsonObject.get("message").getAsString() + "");
        holder.tvMsg.setText("Message "+jsonObject.get("message").getAsString() + "");

    }

    @Override
    public int getItemCount() {
        if (jsonArray.size() != 0) {
            return jsonArray.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_name_tv)
        Custome_Regular_TextView user_name_tv;
        @BindView(R.id.phone_email_tv)
        Custome_Regular_TextView phone_email_tv;
        @BindView(R.id.message_tv)
        Custome_Regular_TextView message_tv;
        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.tvMsg)
        TextView tvMsg;
        @BindView(R.id.user_img)
        ImageView user_img;
        @BindView(R.id.acp_card_cv)
        CardView acp_card_cv;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
