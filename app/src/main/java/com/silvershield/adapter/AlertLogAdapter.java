package com.silvershield.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.activities.AcceptRejectPendingListingActivity;
import com.silvershield.activities.AlertMapActivity;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.interfaces.AlertLongClick;
import com.silvershield.modal.responseModels.GetAlertLogResponseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 11/30/2017.
 */

public class AlertLogAdapter extends RecyclerView.Adapter<AlertLogAdapter.DataViewHolder> {
    private Activity context;
    private JsonObject jsonObject;
    private AlertLongClick alertLongClick;
    private List<GetAlertLogResponseModel> modelList;

    public AlertLogAdapter(Activity context, List<GetAlertLogResponseModel> modelList, AlertLongClick alertLongClick) {
        this.context = context;
        this.modelList = modelList;
        this.alertLongClick = alertLongClick;
    }

    public void refreshData(List<GetAlertLogResponseModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alert_log, parent, false);
        return new AlertLogAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        GetAlertLogResponseModel model = modelList.get(position);
        boolean isLongClickable = false;

        if (model.getReceiver_type() != null) {
            holder.title_logs.setText(model.getPendingCount() + " " + model.getAlert_type() + " Sent to " + model.getReceiver_type() + "\n" + model.getCreated_at() + "\nSent by " + model.getSentByUserName());
        } else {
            holder.title_logs.setText(model.getPendingCount() + "" + model.getAlert_type() + "\n" + model.getCreated_at() + "\nSent by " + model.getSentByUserName());
        }

        holder.description.setText("Message : " + model.getMessage());
        holder.acept_tv.setText(model.getAcceptCount() + " Accepted");
        holder.rejct_tv.setText(model.getRejectCount() + " Rejected");
        holder.pendng_tv.setText(model.getPendingCount() + " Pending");

        if (model.getMessage().contains("Location") && model.getTrigger_nature().equalsIgnoreCase("ALERT")) {
            holder.accp_rej_layout.setVisibility(View.GONE);
            if (model.getLocation_lat().isEmpty() || model.getLocation_lng().isEmpty()) {
                holder.map_layout.setVisibility(View.GONE);
            } else {
                holder.map_layout.setVisibility(View.VISIBLE);
            }
        } else if (model.getMessage().contains("Location") || model.getTrigger_nature().equalsIgnoreCase("CANCEL")) {
            holder.accp_rej_layout.setVisibility(View.GONE);
            if (model.getLocation_lat().isEmpty() || model.getLocation_lng().isEmpty()) {
                holder.map_layout.setVisibility(View.GONE);
            } else {
                holder.map_layout.setVisibility(View.VISIBLE);
            }
        } else if (model.getTrigger_nature().equalsIgnoreCase("ALERT")) {
            holder.accp_rej_layout.setVisibility(View.VISIBLE);
            isLongClickable = true;
            holder.map_layout.setVisibility(View.GONE);
        }

        holder.acept_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.getAcceptCount() == 0) {
                    Toast.makeText(context, " No Records found", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(context, AcceptRejectPendingListingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("model", model);
                    bundle.putString("status", "ACCEPT");
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            }
        });
        holder.rejct_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.getRejectCount() == 0) {
                    Toast.makeText(context, " No Records found", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(context, AcceptRejectPendingListingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("model", model);
                    bundle.putString("status", "REJECT");
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            }
        });
        holder.pendng_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.getPendingCount().equalsIgnoreCase("0")) {
                    Toast.makeText(context, " No Records found", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(context, AcceptRejectPendingListingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("model", model);
                    bundle.putString("status", "PENDING");
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            }
        });
        holder.map_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(context, AlertMapActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("model", model);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        holder.alert_cv.setLongClickable(true);
        boolean finalIsLongClickable = isLongClickable;
        holder.alert_cv.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                String dispatch_id = model.getDispatch_id();
                if (finalIsLongClickable)
                    alertLongClick.onLongClick(dispatch_id);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_logs)
        Custome_Regular_TextView title_logs;
        @BindView(R.id.description)
        Custome_Regular_TextView description;
        @BindView(R.id.acept_tv)
        TextView acept_tv;
        @BindView(R.id.rejct_tv)
        TextView rejct_tv;
        @BindView(R.id.pendng_tv)
        TextView pendng_tv;
        @BindView(R.id.accp_rej_layout)
        LinearLayout accp_rej_layout;
        @BindView(R.id.map_layout)
        LinearLayout map_layout;
        @BindView(R.id.alert_cv)
        CardView alert_cv;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}


