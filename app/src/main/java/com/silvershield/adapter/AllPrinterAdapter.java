package com.silvershield.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.activities.WebviewPrintingActivity;
import com.silvershield.modal.PrinterId;
import com.silvershield.observers.Observer;
import com.silvershield.services.FileDownloader;
import com.silvershield.utils.Constants;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/19/2017.
 */

public class AllPrinterAdapter extends RecyclerView.Adapter<AllPrinterAdapter.DataViewHolder> implements Observer {

    Activity context;
    List<PrinterId> printerIdList;
    String eventId = "";
    URL url = null;
    int port = 0;
    String ip;
    private Socket client;
    private FileInputStream fileInputStream;
    private BufferedInputStream bufferedInputStream;
    private OutputStream outputStream;
    private File pdfFile;
    private String externalStorageDirectory;

    public AllPrinterAdapter(Activity context, List<PrinterId> printerIdList, String eventId) {
        this.context = context;
        this.printerIdList = printerIdList;
        this.eventId = eventId;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new AllPrinterAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(printerIdList.get(position).getName() + "");
        ip = printerIdList.get(position).getIp();
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    /*--Cloud Printing */

                    Intent intent = new Intent(context, WebviewPrintingActivity.class);
                    intent.putExtra("eventId", eventId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);









              /*    Socket sock = new Socket(ip, 9100);
                   // new File("https://www.silvershield.com/vms/web/visitorlogs/getbadge?id=14909");
                    PrintWriter oStream = new PrintWriter(new File("https://www.silvershield.com/vms/web/visitorlogs/getbadge?id=14909"));
                    oStream.println("HI,test from Android Device");
                    oStream.println("\n\n\n");
                    oStream.close();
                    sock.close();*/

                 /*   URL url = null;
                    try {
                        url = new URL("https://www.silvershield.com/vms/web/visitorlogs/getbadge?id=14909");
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    String tDir = System.getProperty("java.io.tmpdir");
                    String path = tDir + "tmp" + ".pdf";
                    final File file = new File(path);
                    file.deleteOnExit();


                    final URL finalUrl = url;
                    Thread thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                *//*try {*//*
                                FileUtils.copyURLToFile(finalUrl, file);

                              *//*  Socket sock = new Socket(ip, 0);*//*
                                // new File("https://www.silvershield.com/vms/web/visitorlogs/getbadge?id=14909");
                               *//* PrintWriter oStream = new PrintWriter(file);
                                oStream.println("HI,test from Android Device");
                                oStream.println("\n\n\n");
                                oStream.close();
                                sock.close();*//*

                                try {
                                    Socket client = new Socket("http://172.16.0.139", 2468);

                                    byte[] mybytearray = new byte[(int) file.length()]; //create a byte array to file

                                    fileInputStream = new FileInputStream(file);
                                    bufferedInputStream = new BufferedInputStream(fileInputStream);

                                    bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file

                                    outputStream = client.getOutputStream();

                                    outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
                                    outputStream.flush();
                                    bufferedInputStream.close();
                                    outputStream.close();
                                    client.close();

                                    //  text.setText("File Sent");


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                              *//*  } catch (IOException e) {
                                    e.printStackTrace();
                                }*//*
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    thread.start();
*/





                  /*  ///ip printing
                    try {
                        url = new URL("https://www.silvershield.com/vms/web/visitorlogs/getbadge?id=" + eventId);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    String tDir = System.getProperty("java.io.tmpdir");
                    String path = tDir + "tmp" + ".pdf";
                    final File file = new File(path);
                    file.deleteOnExit();

                    final Handler handler = new Handler();
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                FileUtils.copyURLToFile(url, file);
                                Socket client = new Socket(printerIdList.get(position).getIp(), 9100);

                                byte[] mybytearray = new byte[(int) file.length()]; //create a byte array to file

                                fileInputStream = new FileInputStream(file);
                                bufferedInputStream = new BufferedInputStream(fileInputStream);

                                bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file

                                outputStream = client.getOutputStream();

                                outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
                                outputStream.flush();
                                bufferedInputStream.close();
                                outputStream.close();
                                client.close();
                                // Get a PrintManager instance
                                PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
                                printManager.print("jhdcsd", new MyPrintDocumentAdapter(context),
                                        null);
                                //  text.setText("File Sent");


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    thread.start();*/


//end printer


//                    Intent intent = new Intent(context, WebviewPrintingActivity.class);
//                    intent.putExtra("eventId", eventId);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);

                    //   downloadAndPrint("https://www.silvershield.com/vms/web/visitorlogs/getbadge?event_guest_id=54", "newPage");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void downloadAndPrint(String fileUrl, final String fileName) {
        new FileDownloader(context, fileUrl, fileName) {
            @Override
            protected void onPostExecute(Boolean result) {
                if (!result) {
                    mObservable.notifyObserver(true);
                } else {

                    // print flow will come here.

                    try {
                        externalStorageDirectory = Environment.getExternalStorageDirectory().toString();
                        File folder = new File(externalStorageDirectory, Constants.CONTROLLER_PDF_FOLDER);
                        pdfFile = new File(folder, fileName);
                    } catch (Exception e) {
                        mObservable.notifyObserver(true);
                        e.printStackTrace();
                    }

                    print(pdfFile);

                }
            }
        }.execute("");
    }


    public void print(final File pdfFile) {

        this.pdfFile = pdfFile;
        try {
            Socket client = new Socket("172.16.0.139", 2468);

            byte[] mybytearray = new byte[(int) pdfFile.length()]; //create a byte array to file

            fileInputStream = new FileInputStream(pdfFile);
            bufferedInputStream = new BufferedInputStream(fileInputStream);

            bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file

            outputStream = client.getOutputStream();

            outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
            outputStream.flush();
            bufferedInputStream.close();
            outputStream.close();
            client.close();

            //  text.setText("File Sent");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (printerIdList.size() != 0) {
            return printerIdList.size();
        } else {
            return 0;
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void updateObserver(boolean bool) {

    }

    @Override
    public void updateObserverProgress(int percentage) {

    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
