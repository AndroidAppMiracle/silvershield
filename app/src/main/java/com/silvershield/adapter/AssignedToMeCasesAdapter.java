package com.silvershield.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Tests;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 11/28/2017.
 */

public class AssignedToMeCasesAdapter extends RecyclerView.Adapter<AssignedToMeCasesAdapter.DataViewHolder> {
    List<Tests> testsList;
    Context context;

    public AssignedToMeCasesAdapter(Context context, List<Tests> testsList) {
        this.context = context;
        this.testsList = testsList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_assign_to_me, parent, false);
        return new AssignedToMeCasesAdapter.DataViewHolder(v);
    }

    @Override
    public int getItemCount() {
        if (testsList.size() != 0) {
            return testsList.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {

        int scoreRecord = 0;
        holder.title_tv.setText(testsList.get(position).getTitle() + "");
        holder.attempt_question.setText(testsList.get(position).getAttempt() + "");
        holder.attempt_question.setVisibility(View.GONE);
        holder.passing_grades.setText("Passing Grades : " + testsList.get(position).getQuesCount() + " %");

        if (testsList.get(position).getAttempts().getAttempt_1().getScore() != null || testsList.get(position).getAttempts().getAttempt_2().getScore() != null) {
            if (Integer.parseInt(testsList.get(position).getAttempts().getAttempt_1().getScore()) >= Integer.parseInt(testsList.get(position).getAttempts().getAttempt_1().getScore())) {
                scoreRecord = Integer.parseInt(testsList.get(position).getAttempts().getAttempt_1().getScore());
            } else if (Integer.parseInt(testsList.get(position).getAttempts().getAttempt_2().getScore()) >= Integer.parseInt(testsList.get(position).getAttempts().getAttempt_1().getScore())) {
                scoreRecord = Integer.parseInt(testsList.get(position).getAttempts().getAttempt_2().getScore());
            }
        } else {
            Log.d("null", " score");
        }
        holder.your_scors.setText("Your Score: " + scoreRecord + "%");
        if (Integer.parseInt(testsList.get(position).getMin_mark()) > scoreRecord) {
            holder.result_fail.setText("Your Result : Fail");
        } else {
            holder.result_fail.setText("Your Result : Pass");
        }

    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_tv)
        Custome_Regular_TextView title_tv;
        @BindView(R.id.attempt_question)
        Custome_Regular_TextView attempt_question;
        @BindView(R.id.passing_grades)
        Custome_Regular_TextView passing_grades;
        @BindView(R.id.your_scors)
        Custome_Regular_TextView your_scors;
        @BindView(R.id.result_fail)
        Custome_Regular_TextView result_fail;

        @BindView(R.id.proceed_img)
        ImageView proceed_img;
        @BindView(R.id.card_view)
        CardView card_view;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
