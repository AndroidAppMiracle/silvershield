package com.silvershield.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.silvershield.R;
import com.silvershield.activities.AssignedByMeActivity;
import com.silvershield.modal.CategoriesDetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 11/30/2017.
 */

public class CategoriesListingAdapter extends RecyclerView.Adapter<CategoriesListingAdapter.DataViewHolder> {
    List<CategoriesDetail> items;
    Context context;
    Dialog dialog;
    String mycate;
    EditText categories_et;

    public CategoriesListingAdapter(Context context, Dialog dialog, List<CategoriesDetail> items, String mycate, EditText categories_et) {
        this.context = context;
        this.items = items;
        this.dialog = dialog;
        this.mycate = mycate;
        this.categories_et = categories_et;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new CategoriesListingAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(items.get(position).getCategory_name() + "");
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mycate = items.get(position).getCategory_name();
                Toast.makeText(context, "categories selected " + mycate, Toast.LENGTH_SHORT).show();
                if (dialog != null) {
               /*     AssignedByMeActivity.categoriesName = items.get(position).getCategory_name();*/
                    if (categories_et!=null) {
                        categories_et.setText(mycate + "");
                    }
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


}
