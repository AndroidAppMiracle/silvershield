package com.silvershield.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.activities.AddNewVisitorActivity;
import com.silvershield.activities.EditUserActiity;
import com.silvershield.modal.Countries;
import com.silvershield.modal.Departments;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/5/2017.
 */

public class CountryNamesAdapter extends RecyclerView.Adapter<CountryNamesAdapter.DataViewHolder>{

    Context context;
    List<Countries> countriesList;
    EditText editText;
    Dialog dialog;


    public CountryNamesAdapter(Context context, Dialog dialog , List<Countries> countriesList, EditText editText)
    {
        this.context = context;
        this.countriesList = countriesList;
        this.editText=editText;
        this.dialog=dialog;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new CountryNamesAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(countriesList.get(position).getCountry_name() + "");
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(""+countriesList.get(position).getCountry_name());
                AddNewVisitorActivity.country_idSt=countriesList.get(position).getId();
                EditUserActiity.country_idSt=countriesList.get(position).getId();
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (countriesList.size()!= 0) {
            return countriesList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
