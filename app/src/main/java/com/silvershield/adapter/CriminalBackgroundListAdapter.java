package com.silvershield.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.silvershield.R;
import com.silvershield.activities.CriminalRecordActivity;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Offenders;
import com.silvershield.modal.WatchList;
import com.silvershield.utils.RecyclerViewClick;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/12/2017.
 */

public class CriminalBackgroundListAdapter extends RecyclerView.Adapter<CriminalBackgroundListAdapter.DataViewHolder> {
    Context context;
    /*List<Offenders> offendersList;*/
    JSONArray offendersList;
    int selected_position = -1;
    RecyclerViewClick rvClick;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    ;
    Dialog dialog;

    public CriminalBackgroundListAdapter(Context context, /*List<Offenders> offendersList*/ JSONArray offendersList, RecyclerViewClick rvClick) {
        this.context = context;
        this.offendersList = offendersList;
        this.rvClick = rvClick;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visitor, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvClick.productClick(v, viewType);
            }
        });
        return new DataViewHolder(v, rvClick);
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        try {
            JSONObject jsonObject = offendersList.getJSONObject(position);

            if (jsonObject.get("FullName") != null) {
                holder.visitor_name.setText(jsonObject.get("FullName").toString() + "");
            }
            if (jsonObject.has("Photo")) {
                if (jsonObject.get("Photo") != null) {
                    byte[] decodedString = Base64.decode(String.valueOf(jsonObject.get("Photo")), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.profile_img.setImageBitmap(decodedByte);
                }

            }
            if (jsonObject.has("Age")) {
                holder.age_tv.setText("Age:" + jsonObject.getString("Age") + "");
            }

            holder.client_here_offences.setVisibility(View.VISIBLE);
            holder.client_here_offences.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.custome_listview);
                        dialog.setTitle(null);
                        Window window = dialog.getWindow();
                        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

                        Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
                        JSONArray jsonArray = offendersList.getJSONObject(position).getJSONArray("Offenses");
                        RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                        cat_rv.setLayoutManager(linearLayoutManager);
                        OffencesAdapter offencesAdapter = new OffencesAdapter(context, jsonArray);
                        cat_rv.setAdapter(offencesAdapter);
                        cancle_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                        dialog.setCancelable(true);
                        dialog.setTitle(null);
                        dialog.show();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            });
            if (selected_position == position) {
                holder.checkbox_ll.setVisibility(View.VISIBLE);
            } else {
                holder.checkbox_ll.setVisibility(View.GONE);
            }
            //   holder.card_view.setSelected(selectedItems.get(position, false));
            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selected_position = position;
                    CriminalRecordActivity.selectedItem = "1";
                    notifyDataSetChanged();
                    try {
                       // CriminalRecordActivity.matchId=offendersList.getJSONObject(position).getString("id");
                        String filePath = downloadBase64(offendersList.getJSONObject(position).getString("Photo"));
                        CriminalRecordActivity.matchUrl = offendersList.getJSONObject(position).getString("Photo");
                        CriminalRecordActivity.jsonObject = offendersList.getJSONObject(position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (offendersList.length() != 0) {
            return offendersList.length();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.checkbox_ll)
        LinearLayout checkbox_ll;

        @BindView(R.id.visitor_name)
        Custome_Regular_TextView visitor_name;
        @BindView(R.id.profile_img)
        ImageView profile_img;
        @BindView(R.id.card_view)
        CardView card_view;
        @BindView(R.id.age_tv)
        Custome_Regular_TextView age_tv;
        @BindView(R.id.client_here_offences)
        Custome_Regular_TextView client_here_offences;
        //client_here_offences
        RecyclerViewClick recyclerClick;


        public DataViewHolder(View v, RecyclerViewClick recyclerClick) {
            super(v);
            ButterKnife.bind(this, v);
            this.recyclerClick = recyclerClick;
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
          /*  selected_position = getAdapterPosition();
            checkbox_ll.setVisibility(View.VISIBLE);
            notifyItemChanged(selected_position);
            recyclerClick.productClick(v, getLayoutPosition());
            notifyDataSetChanged();*/
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
            recyclerClick.productClick(v, getLayoutPosition());
            notifyDataSetChanged();
            if (selectedItems.get(getAdapterPosition(), false)) {
                selectedItems.delete(getAdapterPosition());
                checkbox_ll.setVisibility(View.VISIBLE);
            } else {
                selectedItems.put(getAdapterPosition(), true);
                checkbox_ll.setVisibility(View.GONE);
            }


        }
    }

    String filePath = "";
    String myDate = "";

    public String downloadBase64(String base64ImageData) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        myDate = format.format(new Date());
        FileOutputStream fos = null;
        try {
            if (base64ImageData != null) {
                /*fos = context.openFileOutput("imageName.png", Context.MODE_PRIVATE);
                byte[] decodedString = android.util.Base64.decode(base64ImageData, android.util.Base64.DEFAULT);*/

                File path = Environment.getExternalStorageDirectory();
                File file = new File(path, "/" + myDate);
                FileOutputStream fileOutputStream;
                fileOutputStream = new FileOutputStream(file);
                byte[] data = Base64.decode(base64ImageData, Base64.DEFAULT);
                IOUtils.write(data, fileOutputStream);
                filePath = file.getPath();
            }

        } catch (Exception e) {

        } finally {
            if (fos != null) {
                fos = null;
            }
        }
        return filePath;
    }
}
