package com.silvershield.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.activities.AddNewVisitorActivity;
import com.silvershield.activities.EditUserActiity;
import com.silvershield.modal.Departments;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/5/2017.
 */

public class DepartmentsAdapter extends RecyclerView.Adapter<DepartmentsAdapter.DataViewHolder> {

    Context context;
    List<Departments> departmentsList;
    EditText editText;
    Dialog dialog;
static String visiting_department_id="";
    public DepartmentsAdapter(Context context, Dialog dialog , List<Departments> departmentsList, EditText editText,String  visiting_department_id) {
        this.context = context;
        this.departmentsList = departmentsList;
        this.editText=editText;
        this.dialog=dialog;
        this.visiting_department_id=visiting_department_id;
    }
    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new DepartmentsAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(departmentsList.get(position).getName() + "");
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(""+departmentsList.get(position).getName());
                visiting_department_id=departmentsList.get(position).getId();
                AddNewVisitorActivity.visiting_department_id=departmentsList.get(position).getId();
                EditUserActiity.visiting_department_id=departmentsList.get(position).getId();
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (departmentsList.size()!= 0) {
            return departmentsList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
