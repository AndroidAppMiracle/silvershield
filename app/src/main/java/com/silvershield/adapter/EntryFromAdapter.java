package com.silvershield.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.silvershield.R;
import com.silvershield.activities.AddNewVisitorActivity;
import com.silvershield.activities.EditUserActiity;
import com.silvershield.modal.Docs_type;
import com.silvershield.modal.Entry_from;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/5/2017.
 */

public class EntryFromAdapter extends RecyclerView.Adapter<EntryFromAdapter.DataViewHolder> {

    Context context;
    List<Entry_from> entryfromsList;
    EditText editText;
    Dialog dialog;
    String vms_visitor_entering_from_id;


    public EntryFromAdapter(Context context, Dialog dialog, List<Entry_from> entryfromsList, EditText editText, String vms_visitor_entering_from_id) {
        this.context = context;
        this.entryfromsList = entryfromsList;
        this.editText = editText;
        this.dialog = dialog;
        this.vms_visitor_entering_from_id = vms_visitor_entering_from_id;
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new EntryFromAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(entryfromsList.get(position).getEntery_place() + "");
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (entryfromsList.get(position).getEntery_place().equalsIgnoreCase("select")) {
                    Toast.makeText(context, "Please select entery from..", Toast.LENGTH_SHORT).show();
                } else {
                    editText.setText("" + entryfromsList.get(position).getEntery_place());
                    vms_visitor_entering_from_id=entryfromsList.get(position).getId();
                    AddNewVisitorActivity.vms_visitor_entering_from_id = entryfromsList.get(position).getId();
                    EditUserActiity.vms_visitor_entering_from_id=entryfromsList.get(position).getId();
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (entryfromsList.size() != 0) {
            return entryfromsList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
