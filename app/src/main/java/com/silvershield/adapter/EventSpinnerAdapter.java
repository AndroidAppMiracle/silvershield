package com.silvershield.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Events;

import java.util.List;

/**
 * Created by satoti.garg on 12/19/2017.
 */

public class EventSpinnerAdapter extends BaseAdapter {
    Context context;
    List<Events> eventsList;
    LayoutInflater inflter;
    List<String> stList;


    public EventSpinnerAdapter(Context context, List<Events> eventsList) {
        this.context = context;
        this.eventsList = eventsList;
    }

    @Override
    public int getCount() {
        if (eventsList.size()!= 0) {
            return eventsList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            LayoutInflater lInflater = (LayoutInflater)context.getSystemService(
                    Activity.LAYOUT_INFLATER_SERVICE);

            view = lInflater.inflate(R.layout.item_single_tv, null);
        }

        Custome_Regular_TextView names = (Custome_Regular_TextView) view.findViewById(R.id.category_name_tv);
        names.setText(eventsList.get(i).getEvent_name()/*stList.get(i)*/+"");
        return view;
    }
}
