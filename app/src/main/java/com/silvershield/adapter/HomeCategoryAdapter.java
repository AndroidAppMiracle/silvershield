package com.silvershield.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.silvershield.R;
import com.silvershield.activities.BudgetScanActivity;
import com.silvershield.activities.ScaningActivity;
import com.silvershield.activities.SettingsActivity;
import com.silvershield.activities.VirtualAcadmyActivity;
import com.silvershield.activities.WatchVipActivity;
import com.silvershield.custome_controls.Custome_Regular_TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.DataViewHolder> {

    private List<String> namesDeptOption = new ArrayList<String>();
    private List<Integer> images = new ArrayList<Integer>();
    private String role = "";
    private Context context;

    public HomeCategoryAdapter(Context applicationContext, List<String> deptNameArray, List<Integer> images, String userRole) {
        this.context = applicationContext;
        this.namesDeptOption = deptNameArray;
        this.images = images;
        this.role = userRole;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_category_cv, parent, false);
        return new HomeCategoryAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(namesDeptOption.get(position) + "");
        Glide.with(context).load(images.get(position)).into(holder.cate_img);
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (namesDeptOption.get(position).equals("Watch/VIP list")) {
                    Intent intent = new Intent(context, WatchVipActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else if (namesDeptOption.get(position).equals("Visitors")) {
                    Intent intent = new Intent(context, ScaningActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else if (namesDeptOption.get(position).equals("Virtual Accademy")) {
                    Intent intent = new Intent(context, VirtualAcadmyActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else if (namesDeptOption.get(position).equals("Settings")) {
                    //SettingsActivity
                    Intent intent = new Intent(context, SettingsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else if (namesDeptOption.get(position).equals("Events")) {
                    //SettingsActivity
                    Intent intent = new Intent(context, BudgetScanActivity.class);
                    intent.putExtra("calling_type", "budge");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return namesDeptOption.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        Custome_Regular_TextView category_name_tv;

        @BindView(R.id.cate_img)
        ImageView cate_img;

        @BindView(R.id.card_view)
        CardView card_view;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
