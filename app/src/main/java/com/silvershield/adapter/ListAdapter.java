package com.silvershield.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.CategoriesDetail;

import java.util.List;

/**
 * Created by satoti.garg on 11/28/2017.
 */

public class ListAdapter extends ArrayAdapter<CategoriesDetail> {

    List<CategoriesDetail> items;
    Context context;

    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListAdapter(Context context, int resource, List<CategoriesDetail> items) {
        super(context, resource, items);
        this.items = items;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.itemlistrow, null);
        }

        CategoriesDetail p = getItem(position);

        if (p != null) {
            Custome_Regular_TextView tt1 = (Custome_Regular_TextView) v.findViewById(R.id.list_title_tv);
            if (tt1 != null) {
                tt1.setText(p.getCategory_name());
            }
        }

        return v;
    }

    @Override
    public int getCount() {
        return items.size();
    }
}
