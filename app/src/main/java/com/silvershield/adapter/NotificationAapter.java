package com.silvershield.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.interfaces.SuccessCodeCheck;
import com.silvershield.java.CautionarySettings;
import com.silvershield.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAapter extends RecyclerView.Adapter<NotificationAapter.DataViewHolder> {

    Activity context;
    Dialog dialog;
    private List<String> notificationTxt = new ArrayList<String>();
    private List<String> externalNotification = new ArrayList<String>();
    private String typeOfAlarms = "";
    private String callTypePerson = "", enteredCode = "";
    private SuccessCodeCheck successCodeCheck;
    private int size = 0;

    public NotificationAapter(Activity context, List<String> notificationTxt, List<String> externalNotification, String typeOfAlarms, SuccessCodeCheck successCodeCheck) {
        this.context = context;
        this.notificationTxt = notificationTxt;
        this.typeOfAlarms = typeOfAlarms;
        this.externalNotification = externalNotification;
        this.successCodeCheck = successCodeCheck;
        if (notificationTxt == null)
            size = size + externalNotification.size();
        else
            size = size + notificationTxt.size();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_text_layout_item, parent, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        String title = "";
        if (typeOfAlarms.equalsIgnoreCase("internal")) {
            title = notificationTxt.get(position);
            Log.d("TITLE:> ", title);
            String[] arr = title.split("\\(");
            holder.txt_name_tv.setText(arr[0]);
            if (arr.length > 1) {
                holder.below_txt.setVisibility(View.VISIBLE);
                holder.below_txt.setText(title.substring(arr[0].length()));
            } else {
                holder.below_txt.setVisibility(View.GONE);
            }
        } else if (typeOfAlarms.equalsIgnoreCase("external")) {
            title = externalNotification.get(position);
            Log.d("TITLE:> ", title);
            String[] arr = title.split("\\(");
            holder.txt_name_tv.setText(arr[0]);
            if (arr.length > 1) {
                holder.below_txt.setVisibility(View.VISIBLE);
                holder.below_txt.setText(title.substring(arr[0].length()));
            } else {
                holder.below_txt.setVisibility(View.GONE);
            }
        }
        holder.card_view.setCardBackgroundColor(getBackgroundColor(title));
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean playAlarm = false;
                if (typeOfAlarms.equalsIgnoreCase("internal")) {
                    if (notificationTxt.get(position).equalsIgnoreCase(Constants.ALERT_GUARD)) {
                        callTypePerson = "GUARD";
                    } else if (notificationTxt.get(position).equalsIgnoreCase(Constants.ALERT_ADMIN)) {
                        callTypePerson = "ADMIN";
                    } else if (notificationTxt.get(position).equalsIgnoreCase(Constants.ALERT_GUARD_ADMIN)) {
                        callTypePerson = "GUARD_ADMIN";
                    } else if (notificationTxt.get(position).equalsIgnoreCase(Constants.ALERT_STAFF)) {
                        callTypePerson = "STAFF";
                    } else if (notificationTxt.get(position).equalsIgnoreCase(Constants.ALERT_SOFT_LOCKDOWN)) {
                        callTypePerson = "INTERNAL_SOFT_LOCKDOWN";
                    } else if (notificationTxt.get(position).equalsIgnoreCase(Constants.HARD_LOCKDOWN_DRILL)) {
                        callTypePerson = "INTERNAL_HARD_LOCKDOWN_DRILL";
                        playAlarm = true;
                    } else if (notificationTxt.get(position).equalsIgnoreCase(Constants.ALERT_HARD_LOCKDOWN)) {
                        callTypePerson = "INTERNAL_HARD_LOCKDOWN";
                        playAlarm = true;
                    }
                } else {
                    if (notificationTxt.get(position).equalsIgnoreCase(Constants.CONTACT_POLICE_EMERGENCY)) {
                        callTypePerson = "EMERGENCY";
                        playAlarm = true;
                    } else if (notificationTxt.get(position).equalsIgnoreCase(Constants.CONTACT_POLICE_NON_EMERGENCY)) {
                        callTypePerson = "NONEMERGENCY";
                    }
                }
                CautionarySettings cautionarySettings = new CautionarySettings(context, callTypePerson, typeOfAlarms, successCodeCheck, playAlarm);
                cautionarySettings.getCautionary();
            }
        });
    }

    @Override
    public int getItemCount() {
        return size;
    }

    private int getBackgroundColor(String title) {
        int color = 0;
        switch (title) {
            case Constants.ALERT_GUARD:
            case Constants.ALERT_STAFF:
            case Constants.ALERT_ADMIN:
            case Constants.ALERT_GUARD_ADMIN:
            case Constants.HARD_LOCKDOWN_DRILL: {
                color = context.getResources().getColor(R.color.blue_bck);
                break;
            }
            case Constants.ALERT_SOFT_LOCKDOWN: {
                color = context.getResources().getColor(R.color.ylw_bck);
                break;
            }
            case Constants.ALERT_HARD_LOCKDOWN:
            case Constants.CONTACT_POLICE_EMERGENCY: {
                color = context.getResources().getColor(R.color.red_bck);
                break;
            }
            case Constants.CONTACT_POLICE_NON_EMERGENCY: {
                color = context.getResources().getColor(R.color.dark_blue_bck);
                break;
            }
        }
        return color;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_name_tv)
        Custome_Regular_TextView txt_name_tv;
        @BindView(R.id.below_txt)
        Custome_Regular_TextView below_txt;
        //below_txt
        @BindView(R.id.card_view)
        CardView card_view;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
