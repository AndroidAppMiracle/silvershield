package com.silvershield.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/21/2017.
 */

public class OffencesAdapter extends RecyclerView.Adapter<OffencesAdapter.DataViewHolder> {
    Context context;
    JSONArray jsonArray;

    public OffencesAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custome_text_layout, parent, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(position);
            holder.deposition_tv.setText("Disposition : "+jsonObject.getString("Disposition"));
            holder.description_tv.setText("Description : "+jsonObject.getString("Description"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (jsonArray.length() != 0) {
            return jsonArray.length();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.deposition_tv)
        Custome_Regular_TextView deposition_tv;

        @BindView(R.id.description_tv)
        Custome_Regular_TextView description_tv;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
