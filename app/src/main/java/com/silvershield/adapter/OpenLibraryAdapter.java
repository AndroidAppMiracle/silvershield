package com.silvershield.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 11/28/2017.
 */

public class OpenLibraryAdapter  extends RecyclerView.Adapter<OpenLibraryAdapter.DataViewHolder> {


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.video_title_tv)
        Custome_Regular_TextView video_title_tv;
        @BindView(R.id.video_category_tv)
        Custome_Regular_TextView video_category_tv;
        @BindView(R.id.video_status_tv)
        Custome_Regular_TextView video_status_tv;

        @BindView(R.id.card_view)
        CardView card_view;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
