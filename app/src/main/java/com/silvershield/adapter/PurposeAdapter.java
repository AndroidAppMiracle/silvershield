package com.silvershield.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.activities.AddNewVisitorActivity;
import com.silvershield.activities.EditUserActiity;
import com.silvershield.modal.Purpose;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/5/2017.
 */

public class PurposeAdapter extends RecyclerView.Adapter<PurposeAdapter.DataViewHolder> {

    Context context;
    List<Purpose> purposeList;
    EditText editText;
    Dialog dialog;
    static String vms_visitor_purpose_id = "";

    public PurposeAdapter(Context context, Dialog dialog, List<Purpose> purposeList, EditText editText,String vms_visitor_purpose_id ) {
        this.context = context;
        this.purposeList = purposeList;
        this.editText = editText;
        this.dialog = dialog;
        this.vms_visitor_purpose_id=vms_visitor_purpose_id;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new PurposeAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(purposeList.get(position).getVisit_purpose() + "");
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("" + purposeList.get(position).getVisit_purpose());
                vms_visitor_purpose_id=purposeList.get(position).getId();
                AddNewVisitorActivity.vms_visitor_purpose_id = purposeList.get(position).getId();
                EditUserActiity.vms_visitor_purpose_id = purposeList.get(position).getId();
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (purposeList.size() != 0) {
            return purposeList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
