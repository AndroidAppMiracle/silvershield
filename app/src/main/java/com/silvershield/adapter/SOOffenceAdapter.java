package com.silvershield.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Convictions;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SOOffenceAdapter extends RecyclerView.Adapter<SOOffenceAdapter.DataViewHolder> {
    Context context;
    Convictions[] jsonArray;

    public SOOffenceAdapter(Context context, Convictions[] jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custome_text_layout, parent, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            holder.description_tv.setText("Disposition : " + jsonArray[position].getCharge());
            holder.deposition_tv.setText("Description : Sex Offence ");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (jsonArray != null) {
            return jsonArray.length;
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.deposition_tv)
        Custome_Regular_TextView deposition_tv;

        @BindView(R.id.description_tv)
        Custome_Regular_TextView description_tv;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
