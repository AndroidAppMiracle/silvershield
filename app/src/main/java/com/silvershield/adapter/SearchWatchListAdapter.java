package com.silvershield.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.activities.EditUserActiity;
import com.silvershield.activities.VisitorInfoActivity;
import com.silvershield.activities.VisitorWithSearchActivity;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.responseModels.VisitorResponseModel1;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 1/3/2018.
 */

public class SearchWatchListAdapter extends RecyclerView.Adapter<SearchWatchListAdapter.ViewHolder> {
    String is_vip, is_watch;
    List<VisitorResponseModel1> modelList;
    private String checkout = "0";
    private int selected_position = 0;
    private DateFormat f1 = new SimpleDateFormat("HH:mm:ss");

    private DateFormat f2 = new SimpleDateFormat("hh:mm a");
    private String jsonObjectSt;
    private boolean editPageCall;
    private Activity context;
    private String profileUrl = "", pickerPath = "";
    private String showVip, showWatchList;

    public SearchWatchListAdapter(Activity context, List<VisitorResponseModel1> modelList, String showVip, String showWatchList) {
        this.context = context;
        this.modelList = modelList;
        this.showVip = showVip;
        this.showWatchList = showWatchList;
    }

    public void refreshData(List<VisitorResponseModel1> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_watch, parent, false);
        return new SearchWatchListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        VisitorResponseModel1 model = modelList.get(position);
        try {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(context.getResources().getDrawable(R.drawable.dummy_users));
            Glide.with(context).load(model.getProfile_pic_web()).apply(requestOptions).into(holder.user_img);
            holder.uername_tv.setText(model.getLast_name() + " " + model.getMiddle_name() + " " + model.getFirst_name());
            String phone_no = model.getPhone();
            if (phone_no != null && phone_no.length() >= 10) {
                holder.phone_tv.setVisibility(View.VISIBLE);
                holder.phone_tv.setText(phone_no);
            } else
                holder.phone_tv.setVisibility(View.GONE);

            if (model.getIs_vip() == 0)
                holder.is_vip_img.setVisibility(View.GONE);
            else {
                holder.is_vip_img.setVisibility(View.VISIBLE);
                holder.is_vip_img.setImageDrawable(context.getResources().getDrawable(R.drawable.vip_badge));
            }
            if (model.getIs_watch() == 0)
                holder.is_watch_img.setVisibility(View.GONE);
            else {
                holder.is_watch_img.setVisibility(View.VISIBLE);
                holder.is_watch_img.setImageDrawable(context.getResources().getDrawable(R.drawable.red_micro));
            }
            if (model.getCheck_out().equalsIgnoreCase("0000-00-00 00:00:00")) {
                holder.check_in_tv.setTextColor(context.getResources().getColor(R.color.Red));
                holder.check_in_tv.setText("Checked in");
                String time1Array[] = model.getCheck_in().split(" ");
                String timingSt = time1Array[1];
                String s = timingSt;
                Date d = null;
                d = f1.parse(s);
                String timing = f2.format(d).toLowerCase();
                holder.time_tv.setText("" + timing);
            } else {
                holder.check_in_tv.setTextColor(context.getResources().getColor(R.color.Black));
                String time1Array[] = model.getCheck_in().split(" ");
                String s = time1Array[1];
                Date d = null;
                d = f1.parse(s);
                String timing = f2.format(d).toUpperCase();
                holder.check_in_tv.setText("Check in :" + timing);
                String time2Array[] = model.getCheck_out().split(" ");
                Date d2 = null;
                d2 = f1.parse(time2Array[1]);
                String checkoutTime = f2.format(d2).toUpperCase();
                holder.time_tv.setText("Check out :" + checkoutTime);
            }
        } catch (Exception e) {

        }
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(context, EditUserActiity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("profileUrl", model.getProfile_pic_web());
                    bundle.putSerializable("jsonObject", model);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        holder.card_view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                builderSingle.setIcon(R.drawable.ic_launcher_background);
                builderSingle.setTitle(null);
                selected_position = position;
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
                arrayAdapter.clear();
                if (model.getCheck_out().equalsIgnoreCase("0000-00-00 00:00:00")) {
                    arrayAdapter.add("Checkout Visitor");
                }
                arrayAdapter.add("Create Badge");
                if (model.getIs_vip() == 0 && showVip.equalsIgnoreCase("1")) {
                    arrayAdapter.add("Add to VIP");
                }
                if (model.getIs_watch() == 0 && showWatchList.equalsIgnoreCase("1")) {
                    arrayAdapter.add("Add to Watch List");
                }

                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        if (strName.equalsIgnoreCase("Checkout Visitor")) {
                            try {
                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().checkoutVisitor(APIServerResponse.CHECK_OUT_VISITOR, ((BaseActivity) context).getAuthToken(), ((BaseActivity) context).getUserID(), "" + model.getId(), new APIServerResponse() {
                                        @Override
                                        public void onSuccess(int tag, Response response) {
                                            ((BaseActivity) context).hideLoading();
                                            JsonObject jsonObject = (JsonObject) response.body();
                                            if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                                                Toast.makeText(context, "Check out Successfully", Toast.LENGTH_SHORT).show();
                                                arrayAdapter.remove("Checkout Visitor");
                                                checkout = "1";
                                                try {
                                                    String time1Array[] = model.getCheck_in().split(" ");
                                                    String s = time1Array[1];
                                                    Date d = f1.parse(s);
                                                    String timing = f2.format(d).toUpperCase();
                                                    holder.check_in_tv.setText("Check in :" + timing);
                                                    holder.check_in_tv.setTextColor(context.getResources().getColor(R.color.Black));

                                                    SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
                                                    String currentDateandTime = sdf.format(new Date());
                                                    holder.time_tv.setText("Check out :" + currentDateandTime.toUpperCase());

                                                    ((VisitorWithSearchActivity) context).getAllWatcherList(((VisitorWithSearchActivity) context).formattedDate, 0);
                                                } catch (Exception exc) {
                                                    exc.printStackTrace();
                                                }
                                                notifyDataSetChanged();
                                            }
                                        }

                                        @Override
                                        public void onError(int tag, Throwable throwable) {
                                            ((BaseActivity) context).hideLoading();
                                            throwable.printStackTrace();
                                        }
                                    });
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        } else if (strName.equalsIgnoreCase("Create Badge")) {
                            ((BaseActivity) context).showLoading();
                            try {
                                profileUrl = model.getProfile_pic_web();
                                ((BaseActivity) context).hideLoading();
                                Intent intent = new Intent(context, VisitorInfoActivity.class);
                                intent.putExtra("purpose", model.getVisit_purpose());
                                intent.putExtra("visitinDept", model.getDepartment_name());
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String myDate = format.format(new Date());
                                intent.putExtra("timing", myDate);
                                intent.putExtra("email", model.getEmail());
                                intent.putExtra("phone", model.getPhone());
                                intent.putExtra("firstName", model.getFirst_name());
                                intent.putExtra("lastName", model.getLast_name());
                                intent.putExtra("middleName", model.getMiddle_name());
                                intent.putExtra("logId", model.getLog_id());
                                intent.putExtra("profileUrl", profileUrl);

                                if (!pickerPath.equalsIgnoreCase("")) {
                                    intent.putExtra("profileUrl", pickerPath);
                                } else {
                                    intent.putExtra("profileUrl", "");
                                }
//                                intent.putExtra("object", String.valueOf(jsonArray.getJSONObject(position)));
                                context.startActivity(intent);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        } else if (strName.equalsIgnoreCase("Add to Watch List")) {
                            /*  updateStatusWatchList();*/
                            try {
                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().updateWatchList(APIServerResponse.UPDATE_WATCHLIST_STATUS, ((BaseActivity) context).getAuthToken(), ((BaseActivity) context).getUserID(), "" + model.getId(), "1", new APIServerResponse() {
                                        @Override
                                        public void onSuccess(int tag, Response response) {
                                            ((BaseActivity) context).hideLoading();
                                            JsonObject jsonObject = (JsonObject) response.body();
                                            if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                                                is_watch = "1";
                                                holder.user_img.setVisibility(View.VISIBLE);
                                                holder.is_watch_img.setImageDrawable(context.getResources().getDrawable(R.drawable.red_micro));
                                                ((VisitorWithSearchActivity) context).getAllWatcherList(((VisitorWithSearchActivity) context).formattedDate, 0);
                                                Toast.makeText(context, "" + jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                                notifyDataSetChanged();
                                            }
                                        }

                                        @Override
                                        public void onError(int tag, Throwable throwable) {
                                            ((BaseActivity) context).hideLoading();
                                            throwable.printStackTrace();
                                        }
                                    });
                                } else {
                                    Toast.makeText(context, "Please check Internet Connection", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        } else if (strName.equalsIgnoreCase("Add to VIP")) {
                            try {
                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().updateVipList(APIServerResponse.UPDATE_VIP_STATUS, ((BaseActivity) context).getAuthToken(), ((BaseActivity) context).getUserID(), model.getId() + "", "1", new APIServerResponse() {
                                        @Override
                                        public void onSuccess(int tag, Response response) {
                                            ((BaseActivity) context).hideLoading();
                                            JsonObject vipObject = (JsonObject) response.body();
                                            try {
                                                if (vipObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                                                    is_vip = "1";
                                                    holder.is_vip_img.setVisibility(View.VISIBLE);
                                                    arrayAdapter.remove("Add to Watch List");
                                                    arrayAdapter.remove("Add to VIP");
                                                    ((VisitorWithSearchActivity) context).getAllWatcherList(((VisitorWithSearchActivity) context).formattedDate, 0);
                                                    Toast.makeText(context, "You have successfully added into vip list", Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onError(int tag, Throwable throwable) {
                                            ((BaseActivity) context).hideLoading();
                                            throwable.printStackTrace();
                                        }
                                    });
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                });

                if (arrayAdapter.getCount() != 0) {
                    builderSingle.show();
                }
                return false;
            }
        });
    }

    public void downloadImage(final String profileUrl, String jsonObject, boolean flag) {
        jsonObjectSt = jsonObject;
        editPageCall = flag;
        if (profileUrl.equalsIgnoreCase("")) {

        } else {
            pickerPath = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, profileUrl, new APIServerResponse() {
                @Override
                public void onSuccess(int tag, Response response) {
                    if (response.isSuccessful()) {
                        if (editPageCall) {
                            ((BaseActivity) context).hideLoading();
                            Intent intent = new Intent(context, EditUserActiity.class);
                            intent.putExtra("profileUrl", profileUrl);
                            intent.putExtra("jsonObject", jsonObjectSt.toString());
                            context.startActivity(intent);
                        } else {
                            try {
                                ((BaseActivity) context).hideLoading();
                                Intent intent = new Intent(context, VisitorInfoActivity.class);
                                JSONObject jsonObject1 = new JSONObject(jsonObjectSt);
                                intent.putExtra("purpose", jsonObject1.getString("visit_purpose"));
                                intent.putExtra("visitinDept", jsonObject1.getString("department_name"));
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String myDate = format.format(new Date());
                                intent.putExtra("timing", myDate);
                                intent.putExtra("email", jsonObject1.getString("email"));
                                intent.putExtra("phone", jsonObject1.getString("phone"));
                                intent.putExtra("firstName", jsonObject1.getString("first_name"));
                                intent.putExtra("lastName", jsonObject1.getString("last_name"));
                                intent.putExtra("middleName", jsonObject1.getString("middle_name"));
                                intent.putExtra("logId", jsonObject1.getString("log_id"));
                                if (!pickerPath.equalsIgnoreCase("")) {
                                    intent.putExtra("profileUrl", pickerPath);
                                } else {
                                    intent.putExtra("profileUrl", "");
                                }
                                intent.putExtra("object", jsonObjectSt);
                                context.startActivity(intent);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onError(int tag, Throwable throwable) {
                    throwable.printStackTrace();
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        if (modelList != null) {
            return modelList.size();
        } else {
            return 0;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_name_tv)
        Custome_Regular_TextView uername_tv;
        @BindView(R.id.phone_tv)
        Custome_Regular_TextView phone_tv;
        @BindView(R.id.check_in_tv)
        Custome_Regular_TextView check_in_tv;
        @BindView(R.id.time_tv)
        Custome_Regular_TextView time_tv;
        @BindView(R.id.is_vip_img)
        ImageView is_vip_img;
        @BindView(R.id.is_watch_img)
        ImageView is_watch_img;
        @BindView(R.id.user_img)
        ImageView user_img;

        @BindView(R.id.card_view)
        CardView card_view;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
