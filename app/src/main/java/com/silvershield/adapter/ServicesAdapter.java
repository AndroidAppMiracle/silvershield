package com.silvershield.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.services.OnServiceSelectedListener;

import java.util.ArrayList;
import java.util.List;

import it.ennova.zerxconf.model.NetworkServiceDiscoveryInfo;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ServiceViewHolder>  {

    private List<NetworkServiceDiscoveryInfo> services = new ArrayList<>();
    private OnServiceSelectedListener serviceSelectedListener;

    public ServicesAdapter(OnServiceSelectedListener serviceSelectedListener) {
        this.serviceSelectedListener = serviceSelectedListener;
    }

    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ServiceViewHolder(inflater.inflate(R.layout.service_item_layout, parent, false), serviceSelectedListener);
    }

    @Override
    public void onBindViewHolder(ServiceViewHolder holder, int position) {
        holder.bindTo(services.get(position));
    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public void addService(@NonNull NetworkServiceDiscoveryInfo service) {
        services.add(service);
        notifyItemInserted(services.size() - 1);
        notifyDataSetChanged();
    }

    public void removeService(@NonNull NetworkServiceDiscoveryInfo service) {
        int foundIndex = -1;
        int listSize = services.size();

        for (int i = 0; i < listSize && (foundIndex == -1); i++) {
            if (services.get(i).equals(service)) {
                foundIndex = i;
            }
        }

        services.remove(foundIndex);
        notifyItemRemoved(foundIndex);
        notifyDataSetChanged();
    }



     class ServiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView serviceName, serviceAddress;
        private OnServiceSelectedListener serviceSelectedListener;
        private NetworkServiceDiscoveryInfo info;

        public ServiceViewHolder(View itemView, OnServiceSelectedListener serviceSelectedListener) {
            super(itemView);
            this.serviceSelectedListener = serviceSelectedListener;
            serviceName = (TextView) itemView.findViewById(R.id.txtServiceName);
            serviceAddress = (TextView) itemView.findViewById(R.id.txtServiceType);

            itemView.setOnClickListener(this);
        }

        public void bindTo(@NonNull NetworkServiceDiscoveryInfo info) {
            this.info = info;
            serviceName.setText(String.format("%s (%s)", info.getServiceName(), info.getServiceLayer()));
            serviceAddress.setText(String.format("%s:%s", getAddressFrom(info), info.getServicePort()));
        }

        private String getAddressFrom(@NonNull NetworkServiceDiscoveryInfo info) {
            String address;
            if (info.getAddress() != null) {
                address = info.getAddress().getHostAddress();
            } else {
                address = "N/A";
            }
            return address;
        }

        @Override
        public void onClick(View v) {
            serviceSelectedListener.onServiceSelected(info);
        }


    }

}
