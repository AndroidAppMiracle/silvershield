package com.silvershield.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.modal.SettingsModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 11/30/2017.
 */

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.DataViewHolder> {

    private Context context;
    private List<SettingsModel> settingsList;

    public SettingAdapter(Context context, List<SettingsModel> settingsList) {
        this.context = context;
        this.settingsList = settingsList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_setting_layout, parent, false);
        return new SettingAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.atv_general_setting_name.setText(settingsList.get(position).getLabel());
        if (settingsList.get(position).getState() == 0) {
            holder.switchOnOff.setChecked(false);
        } else {
            holder.switchOnOff.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return settingsList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_general_setting_name)
        TextView atv_general_setting_name;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        @BindView(R.id.switch1)
        Switch switchOnOff;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
