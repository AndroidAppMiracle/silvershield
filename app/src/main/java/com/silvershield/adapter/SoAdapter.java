package com.silvershield.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.silvershield.R;
import com.silvershield.activities.SoOffenderActivity;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Convictions;
import com.silvershield.modal.Offenders;
import com.silvershield.modal.WatchListMatchModal;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class SoAdapter extends RecyclerView.Adapter<SoAdapter.DataViewHolder> implements APIServerResponse {

    Activity context;
    List<Offenders> offendersList;
    int selectedItem;
    Dialog dialog;

    public SoAdapter(Activity context, List<Offenders> offendersList) {
        this.context = context;
        this.offendersList = offendersList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visitor, parent, false);
        return new SoAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        final Offenders offenders = offendersList.get(position);
        Glide.with(context).load(offenders.getPhoto()).into(holder.profile_img);
        holder.reason_available.setVisibility(View.VISIBLE);
        holder.visitor_name.setText(offenders.getFirstname() + "" + offenders.getLastname());
        holder.reason_available.setVisibility(View.GONE);
        if(!offenders.getAge().equalsIgnoreCase("")) {
            holder.age_tv.setVisibility(View.VISIBLE);
            holder.age_tv.setText("AGE = " + offenders.getAge());
        }else
        {
            holder.age_tv.setVisibility(View.GONE);
        }
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SoOffenderActivity.selectedItem = "1";
                SoOffenderActivity.soFlag = true;
                SoOffenderActivity.offenderDetail=offenders;
                SoOffenderActivity.matchId=offenders.getOffenderid();
                SoOffenderActivity.matchedPhoto=offenders.getPhoto().replace("\"","");
                /*if (offenders.getPhoto()!=null) {
                  *//*  SoOffenderActivity.matchedPhoto = offenders.getPhoto();*//*
                    downloadImage(offenders.getPhoto()+"");
                }*/
                if (position == selectedItem) {
                    holder.checkbox_ll.setVisibility(View.VISIBLE);
                } else {
                    holder.checkbox_ll.setVisibility(View.GONE);
                }
                notifyDataSetChanged();
            }
        });


        holder.client_here_offences.setVisibility(View.VISIBLE);
        holder.client_here_offences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.custome_listview);
                    dialog.setTitle(null);
                    Window window = dialog.getWindow();
                    window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);

                    Button cancle_btn = (Button) dialog.findViewById(R.id.cancle_btn);
                    Convictions[] jsonArray = offendersList.get(position).getConvictions();
                    RecyclerView cat_rv = (RecyclerView) dialog.findViewById(R.id.cat_rv);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                    cat_rv.setLayoutManager(linearLayoutManager);
                    SOOffenceAdapter offencesAdapter = new SOOffenceAdapter(context, jsonArray);
                    cat_rv.setAdapter(offencesAdapter);
                    cancle_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    dialog.setCancelable(true);
                    dialog.setTitle(null);
                    dialog.show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });


    }


    public void downloadImage(String matchImg) {
        ((BaseActivity)context).showLoading();
        if (matchImg.equalsIgnoreCase("")) {

        } else {
            SoOffenderActivity.matchedPhoto = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, matchImg, SoAdapter.this);
        }
        ((BaseActivity)context).hideLoading();

    }
    @Override
    public int getItemCount() {
        return offendersList.size();
    }

    @Override
    public void onSuccess(int tag, Response response) {

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.visitor_name)
        Custome_Regular_TextView visitor_name;
        @BindView(R.id.profile_img)
        ImageView profile_img;
        @BindView(R.id.card_view)
        CardView card_view;
        @BindView(R.id.age_tv)
        Custome_Regular_TextView age_tv;
        //reason_available
        @BindView(R.id.reason_available)
        Custome_Regular_TextView reason_available;

        @BindView(R.id.checkbox_ll)
        LinearLayout checkbox_ll;
        @BindView(R.id.client_here_offences)
        Custome_Regular_TextView client_here_offences;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            checkbox_ll.setVisibility(View.GONE);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Redraw the old selection and the new
                    notifyItemChanged(selectedItem);
                    selectedItem = getLayoutPosition();
                    notifyItemChanged(selectedItem);
                }
            });
        }

    }
}
