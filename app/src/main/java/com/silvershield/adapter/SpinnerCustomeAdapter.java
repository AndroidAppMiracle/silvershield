package com.silvershield.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Bold_TextView;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Events;

import java.util.List;

/**
 * Created by satoti.garg on 12/18/2017.
 */

public class SpinnerCustomeAdapter extends BaseAdapter {

    Context context;
    List<Events> eventsList;
    LayoutInflater inflter;
    List<String> stList;

    public SpinnerCustomeAdapter(Context context, /*List<Events> eventsList*/  List<String> stList) {
        this.context=context;
        this.eventsList = eventsList;
        this.stList=stList;
    }

    @Override
    public int getCount() {
        if (stList.size()!= 0) {
            return stList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
       /* view = inflter.inflate(R.layout.item_single_tv, null);*/

        if(view == null){
            LayoutInflater lInflater = (LayoutInflater)context.getSystemService(
                    Activity.LAYOUT_INFLATER_SERVICE);

            view = lInflater.inflate(R.layout.item_single_tv, null);
        }


        Custome_Regular_TextView names = (Custome_Regular_TextView) view.findViewById(R.id.category_name_tv);
        names.setText(/*eventsList.get(i).getEvent_name()*/stList.get(i)+"");
        return view;
    }
}
