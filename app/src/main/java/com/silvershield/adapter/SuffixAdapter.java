package com.silvershield.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.activities.AddNewVisitorActivity;
import com.silvershield.activities.EditUserActiity;
import com.silvershield.modal.Suffix;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class SuffixAdapter extends RecyclerView.Adapter<SuffixAdapter.DataViewHolder> {

    Context context;
    List<Suffix> suffixList;
    Dialog dialog;
    EditText editText;
   public static String suffixId;

    public SuffixAdapter(Context context, Dialog dialog, List<Suffix> suffixList, EditText editText,String suffixId) {
        this.context = context;
        this.dialog = dialog;
        this.suffixList = suffixList;
        this.editText = editText;
        this.suffixId=suffixId;

    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new SuffixAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder,final int position) {
        holder.category_name_tv.setText(suffixList.get(position).getSuffix_name() + "");
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(""+suffixList.get(position).getSuffix_name());
                suffixId=suffixList.get(position).getId();
                AddNewVisitorActivity.suffixId=suffixList.get(position).getId();
                EditUserActiity.suffixId=suffixList.get(position).getId();
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (suffixList.size() != 0) {
            return suffixList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
