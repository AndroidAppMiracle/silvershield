package com.silvershield.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Matches;
import com.silvershield.utils.RecyclerViewClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/8/2017.
 */

public class SuggessionAdapter extends RecyclerView.Adapter<SuggessionAdapter.DataViewHolder> {

    List<Matches> matchList;
    int selected_position = -1;
    Activity context;
    RecyclerViewClick rvClick;

    public SuggessionAdapter(Activity context, List<Matches> matchList, RecyclerViewClick rvClick) {
        this.context = context;
        this.matchList = matchList;
        this.rvClick = rvClick;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_suggestion_dialog, parent, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        Matches matches = matchList.get(position);
        holder.user_full_name_tv.setText(matches.getFirst_name() + " " + matches.getMiddle_name() + " " + matches.getLast_name());
        if (matches.getDob().isEmpty()) {
            holder.dob_tv.setVisibility(View.GONE);
        } else {
            holder.dob_tv.setVisibility(View.VISIBLE);
            holder.dob_tv.setText(matches.getDob() + "");
        }
        if (matches.getPhone().isEmpty() || matches.getPhone().length() < 7) {
            holder.ph_number_tv.setVisibility(View.GONE);
        } else {
            holder.ph_number_tv.setVisibility(View.VISIBLE);
            holder.ph_number_tv.setText(matches.getPhone() + "");
        }
        if (matches.getEmail().isEmpty()) {
            holder.email_tv.setVisibility(View.GONE);
        } else {
            holder.email_tv.setVisibility(View.VISIBLE);
            holder.email_tv.setText(matches.getEmail() + "");
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.icon_profile);
        requestOptions.error(R.drawable.icon_profile);

        if (!matches.getProfile_pic_web().isEmpty()) {
            Glide.with(context).applyDefaultRequestOptions(requestOptions).load(matches.getProfile_pic_web()).into(holder.profile_img);
        }

        if (matches.getIs_vip().equalsIgnoreCase("1")) {
            holder.vip_img.setVisibility(View.VISIBLE);
        } else {
            holder.vip_img.setVisibility(View.GONE);
        }

        if (selected_position == position) {
            holder.tick_img.setVisibility(View.VISIBLE);
        } else {
            holder.tick_img.setVisibility(View.GONE);
        }

        holder.cv_sug_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected_position = position;
                notifyDataSetChanged();
                rvClick.productClick(view, position);
//                holder.tick_img.setVisibility(View.VISIBLE);

//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions.placeholder(R.drawable.icon_profile);
//                requestOptions.error(R.drawable.icon_profile);
//                if (((BaseActivity) context).isConnectedToInternet()) {
//                    ((BaseActivity) context).showLoading();
//                    AddNewVisitorActivity.profileUrl = matches.getProfile_pic_web();
//                    AddNewVisitorActivity.pickerPath = ServerAPI.getInstance().download(APIServerResponse.DOWNLOAD_FILE, matches.getProfile_pic_web(), new APIServerResponse() {
//                        @Override
//                        public void onSuccess(int tag, Response response) {
//                            ((BaseActivity) context).hideLoading();
//                            Log.e("image_response", response.body() + "");
//                        }
//
//                        @Override
//                        public void onError(int tag, Throwable throwable) {
//                            ((BaseActivity) context).hideLoading();
//                            throwable.printStackTrace();
//                        }
//                    });
//                }
//                Glide.with(context).applyDefaultRequestOptions(requestOptions).load(matches.getProfile_pic_web()).into(AddNewVisitorActivity.profile_img);
//                AddNewVisitorActivity.matchesObj = matches;
//                AddNewVisitorActivity.matchId = matches.getId();
//                AddNewVisitorActivity.profileUrl = matches.getProfile_pic_web();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (matchList.size() != 0) {
            return matchList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_full_name_tv)
        Custome_Regular_TextView user_full_name_tv;
        @BindView(R.id.dob_tv)
        Custome_Regular_TextView dob_tv;
        @BindView(R.id.ph_number_tv)
        Custome_Regular_TextView ph_number_tv;
        @BindView(R.id.email_tv)
        Custome_Regular_TextView email_tv;
        @BindView(R.id.profile_img)
        ImageView profile_img;
        //vip_img
        @BindView(R.id.vip_img)
        ImageView vip_img;
        @BindView(R.id.tick_img)
        ImageView tick_img;
        @BindView(R.id.cv_sug_item)
        CardView cv_sug_item;

        public DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
