package com.silvershield.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.Individuals;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satoti.garg on 12/5/2017.
 */

public class VisitIndividualAdapter extends ArrayAdapter<Individuals> {
    Context context;
    int resource, textViewResourceId;
    List<Individuals> items, tempItems, suggestions;

    public VisitIndividualAdapter(Context context, int resource, int textViewResourceId, List<Individuals> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<Individuals>(items); // this makes the difference.
        suggestions = new ArrayList<Individuals>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_single_tv, parent, false);
        }
        Individuals people = items.get(position);
        if (people != null) {
            Custome_Regular_TextView lblName = (Custome_Regular_TextView) view.findViewById(R.id.category_name_tv);
            if (lblName != null)
                lblName.setText(people.getUsername());
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((Individuals) resultValue).getUsername();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Individuals people : tempItems) {
                    if (people.getUsername().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Individuals> filterList = (ArrayList<Individuals>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Individuals people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
