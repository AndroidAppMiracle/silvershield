package com.silvershield.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.silvershield.R;
import com.silvershield.modal.Departments;
import com.silvershield.modal.Docs_type;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class VisitingDeptAdapter extends RecyclerView.Adapter<VisitingDeptAdapter.DataViewHolder> {

    Context context;
    List<Docs_type> departmentsList;
    EditText editText;
    Dialog dialog;

    public VisitingDeptAdapter(Context context, Dialog dialog , List<Docs_type> departmentsList, EditText editText) {
        this.context = context;
        this.departmentsList = departmentsList;
        this.editText=editText;
        this.dialog=dialog;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_tv, parent, false);
        return new VisitingDeptAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.category_name_tv.setText(departmentsList.get(position).getDocument_name() + "");
        holder.cv_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(""+departmentsList.get(position).getDocument_name());
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (departmentsList.size()!= 0) {
            return departmentsList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name_tv)
        TextView category_name_tv;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
