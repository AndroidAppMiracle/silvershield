package com.silvershield.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.silvershield.R;
import com.silvershield.activities.WatchListDetailActivity;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.WatchListMatchModal;
import com.silvershield.utils.RecyclerViewClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/12/2017.
 */

public class WatchCheckAdapter extends RecyclerView.Adapter<WatchCheckAdapter.DataViewHolder> {
    Context context;
    List<WatchListMatchModal> offendersList;
    int selectedItem;
    RecyclerViewClick rvClick;
    int selected_position = -1;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    public WatchCheckAdapter(Context context, List<WatchListMatchModal> offendersList, RecyclerViewClick rvClick) {
        this.context = context;
        this.rvClick = rvClick;
        this.offendersList = offendersList;
    }

    @Override
    public WatchCheckAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visitor, parent, false);
        return new WatchCheckAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final WatchCheckAdapter.DataViewHolder holder, final int position) {
        final WatchListMatchModal offenders = offendersList.get(position);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.dummy_users);
        requestOptions.error(R.drawable.dummy_users);
        Glide.with(context).setDefaultRequestOptions(requestOptions).load(offenders.getProfile_pic_web()).into(holder.profile_img);
        holder.reason_available.setVisibility(View.VISIBLE);
        holder.visitor_name.setText(offenders.getFirst_name() + " " + offenders.getLast_name());
        if (offenders.getWl_reason().equalsIgnoreCase("SO")) {
            holder.reason_available.setText("Reason: " + "Sex Offender");
        } else if (offenders.getWl_reason().equalsIgnoreCase("CB")) {
            holder.reason_available.setText("Reason: " + "Criminal");
        } else {
            holder.reason_available.setText("Reason: " + offenders.getWl_reason() + "");
        }
        if (selected_position == position) {
            holder.checkbox_ll.setVisibility(View.VISIBLE);
        } else {
            holder.checkbox_ll.setVisibility(View.GONE);
        }
        if (offenders.getDob().equals("")) {
            holder.age_tv.setVisibility(View.GONE);
        } else {
            holder.age_tv.setVisibility(View.VISIBLE);
            holder.age_tv.setText("DOB = " + offenders.getDob());
        }

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected_position = position;
                notifyDataSetChanged();
                WatchListDetailActivity.selectedItem = "1";
                WatchListDetailActivity.matchId = offendersList.get(position).getId();
                if (offendersList.get(position).getProfile_pic_web().equalsIgnoreCase("") || offendersList.get(position).getProfile_pic_web().equalsIgnoreCase(null)) {
                    WatchListDetailActivity.matchImage = "";
                } else {
                    WatchListDetailActivity.matchImage = offendersList.get(position).getProfile_pic_web();
                }
                WatchListDetailActivity.soFlag = false;


                if (position == selectedItem) {
                } else {
                    //holder.checkbox_ll.setVisibility(View.GONE);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        if (offendersList.size() != 0) {
            return offendersList.size();
        } else {
            return 0;
        }
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.visitor_name)
        Custome_Regular_TextView visitor_name;
        @BindView(R.id.profile_img)
        ImageView profile_img;
        @BindView(R.id.card_view)
        CardView card_view;
        @BindView(R.id.age_tv)
        Custome_Regular_TextView age_tv;
        //reason_available
        @BindView(R.id.reason_available)
        Custome_Regular_TextView reason_available;

        @BindView(R.id.checkbox_ll)
        LinearLayout checkbox_ll;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }
}
