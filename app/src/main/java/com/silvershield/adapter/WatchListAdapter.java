package com.silvershield.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.silvershield.R;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.WatchList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 11/23/2017.
 */

public class WatchListAdapter extends RecyclerView.Adapter<WatchListAdapter.DataViewHolder> {
    private Context context;
    private ArrayList<WatchList> watchList;
    private String watchListArray;
    private JSONArray jsonArray;

    public WatchListAdapter(Context context,/* ArrayList<WatchList> watchList*/String watchListArray) {
        this.context = context;
      /*  this.watchList = watchList;*/
        this.watchListArray = watchListArray;
        try {
            JSONObject json = new JSONObject(watchListArray);
            if (json.has("watchlist")) {
                jsonArray = json.getJSONArray("watchlist");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_watch_vip_list, parent, false);
        return new WatchListAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            JSONObject json = jsonArray.getJSONObject(position);
            holder.phone_number.setText(json.getString("phone"));
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(context.getResources().getDrawable(R.drawable.dummy_users));
            Glide.with(context).load(json.getString("profile_pic_web")).apply(requestOptions).into(holder.user_img);
            holder.uername_tv.setText(json.getString("first_name") + "" + json.getString("last_name"));
            holder.date_time_tv.setText(json.getString("dob") + "");
            //date_time_tv
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //WatchList watchLists = watchList.get(position);
    }

    @Override
    public int getItemCount() {
        if (jsonArray.length() != 0) {
            return jsonArray.length();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.phone_number)
        Custome_Regular_TextView phone_number;
        @BindView(R.id.uername_tv)
        Custome_Regular_TextView uername_tv;
        @BindView(R.id.user_img)
        ImageView user_img;
        @BindView(R.id.card_view)
        CardView card_view;
        @BindView(R.id.date_time_tv)
        Custome_Regular_TextView date_time_tv;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
