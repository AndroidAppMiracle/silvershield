package com.silvershield.custome_controls;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class Custome_Bold_TextView  extends AppCompatTextView {

    public Custome_Bold_TextView(Context context)
    {
        super(context); setFont();
    }

    public Custome_Bold_TextView(Context context,AttributeSet set)
    {
        super(context,set); setFont();
    }

    public Custome_Bold_TextView(Context context,AttributeSet set,int defaultStyle)
    {
        super(context,set,defaultStyle); setFont();
    }

    private void setFont() {

        Typeface typeface=Typeface.createFromAsset(getContext().getAssets(),"Raleway-Bold.ttf");
        setTypeface(typeface); //function used to set font

    }
}
