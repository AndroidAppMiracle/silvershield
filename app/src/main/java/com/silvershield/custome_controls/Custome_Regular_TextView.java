package com.silvershield.custome_controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class Custome_Regular_TextView extends AppCompatTextView {

    public Custome_Regular_TextView(Context context)
    {
        super(context); setFont();
    }

    public Custome_Regular_TextView(Context context,AttributeSet set)
    {
        super(context,set); setFont();
    }

    public Custome_Regular_TextView(Context context,AttributeSet set,int defaultStyle)
    {
        super(context,set,defaultStyle); setFont();
    }

    private void setFont() {

        Typeface typeface=Typeface.createFromAsset(getContext().getAssets(),"Raleway-Regular.ttf");
        setTypeface(typeface); //function used to set font

    }
}
