package com.silvershield.dialog_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.silvershield.R;
import com.silvershield.adapter.SuggessionAdapter;
import com.silvershield.interfaces.SuggestionDialogClick;
import com.silvershield.modal.Matches;
import com.silvershield.utils.RecyclerViewClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SuggestionDialogFragment extends DialogFragment implements RecyclerViewClick {

    @BindView(R.id.imageView_close)
    ImageView imageView_close;
    @BindView(R.id.suggestion_user_rv)
    RecyclerView suggestion_user_rv;
    @BindView(R.id.submit_btn)
    Button submit_btn;
    SuggestionDialogClick suggestionDialogClick;
    private List<Matches> matchesList;
    private Matches selected_matches;

    public SuggestionDialogFragment() {

    }

    public static SuggestionDialogFragment newInstance(String param1, String param2) {
        SuggestionDialogFragment fragment = new SuggestionDialogFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public void setData(List<Matches> matchesList, SuggestionDialogClick suggestionDialogClick) {
        this.matchesList = matchesList;
        this.suggestionDialogClick = suggestionDialogClick;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.suggesion_custome_layout, container, false);
        ButterKnife.bind(this, view);
        initComponents();
        return view;
    }

    private void initComponents() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        suggestion_user_rv.setLayoutManager(linearLayoutManager);
        SuggessionAdapter suggessionAdapter = new SuggessionAdapter(getActivity(), matchesList, this);
        suggestion_user_rv.setAdapter(suggessionAdapter);
        submit_btn.setText(getString(R.string.continueText));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick({R.id.imageView_close, R.id.submit_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_close: {
                dismissFragment();
                break;
            }
            case R.id.submit_btn: {
                suggestionDialogClick.onClick(selected_matches);
                dismissFragment();
                break;
            }
        }
    }

    @Override
    public void productClick(View v, int position) {
        selected_matches = matchesList.get(position);
    }

    public void dismissFragment() {
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("suggestion_list");
        if (prev != null) {
            DialogFragment df = (DialogFragment) prev;
            df.dismiss();
        }
    }
}
