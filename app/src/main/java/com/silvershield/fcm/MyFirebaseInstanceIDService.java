package com.silvershield.fcm;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.silvershield.utils.Constants;

/**
 * Created by satoti.garg on 1/6/2018.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    public static String refreshedToken;
    private SharedPreferences mPrefs;

    public static FirebaseInstanceIdService getInstance() {
        return new FirebaseInstanceIdService();
    }

    @Override
    public void onTokenRefresh() {
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("", "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        Log.e("Token", "---" + token);
        mPrefs = getSharedPreferences(Constants.DEVICE_ID, MODE_PRIVATE);
        mPrefs.edit().putString(Constants.FCM_TOKEN, token).commit();
    }
}
