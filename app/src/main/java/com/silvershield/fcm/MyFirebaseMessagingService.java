package com.silvershield.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.silvershield.R;
import com.silvershield.activities.AlertMapActivity;
import com.silvershield.activities.SplashActivity;
import com.silvershield.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by satoti.garg on 1/6/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFcmListenerService";
    static boolean showAcceptReject = false;
    public long NOTIFICATION_ID;
    SharedPreferences sp;
    String userId;
    String message;


    String alertId = "", messageSt = "";
    double location_lng = 0.0, location_lat = 0.0;
    Intent intent;
    private SharedPreferences mPrefs;

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        mPrefs = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message_data_payload: " + remoteMessage.getData());
            Log.d(TAG, "Message_data_payload: " + remoteMessage.getNotification().getSound());
            NOTIFICATION_ID = System.currentTimeMillis();
            try {
                Bundle bundle = new Bundle();
                for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
                    bundle.putString(entry.getKey(), entry.getValue());
                }
                alertId = String.valueOf(bundle.get("status_key"));
                if (alertId == null)
                    alertId = "";
                if (bundle.containsKey("location_lng")) {
                    location_lng = Double.parseDouble(bundle.get("location_lng") + "");
                }
                if (bundle.containsKey("location_lat")) {
                    location_lat = Double.parseDouble(bundle.get("location_lat") + "");
                }
                Log.d("alertId", alertId);
                Constants.ALERT_ID = alertId;
                String cat = bundle.getString("category");
                if (cat == null) {
                    cat = "";
                }
                noto2(bundle.get("message").toString(), cat, alertId);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                //                Log.e("app",appPackageName);
                return true;
            }
        }
        return false;
    }

    public void noto2(String messageSt, String category, String alertId) // paste in activity
    {
        if (category.equalsIgnoreCase("LOCATION_MAP")) {
            intent = new Intent(getApplicationContext(), AlertMapActivity.class);
            intent.putExtra("NOTIFICATION_MESSAGE", messageSt);
            intent.putExtra("category", category);
            intent.putExtra("alertId", alertId);
            intent.putExtra("location_lat", location_lat);
            intent.putExtra("location_lng", location_lng);
        } else if (!category.isEmpty()) {
            intent = new Intent(getApplicationContext(), NotifyAlertDialog.class);
            intent.putExtra("NOTIFICATION_MESSAGE", messageSt);
            intent.putExtra("category", category);
            intent.putExtra("alertId", alertId);
            intent.putExtra("location_lat", location_lat);
            intent.putExtra("location_lng", location_lng);
        } else {
            intent = new Intent(getApplicationContext(), SplashActivity.class);
        }

        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Notification.Builder notif;

        notif = new Notification.Builder(getApplicationContext());
        notif.setSmallIcon(R.mipmap.ic_launcher);
        notif.setStyle(new Notification.BigTextStyle().bigText(messageSt));
        notif.setContentText(messageSt);
        notif.setContentIntent(pIntent);
//        notif.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
        notif.setContentTitle("Silver Shield");
        notif.setAutoCancel(true);
        Uri path = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.warning);
        notif.setSound(path);
        notif.setOngoing(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int m = ((int) System.currentTimeMillis());
        notificationManager.notify(m, notif.build());

//        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        Notification notification = notif.getNotification();
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//        nm.notify((int) NOTIFICATION_ID, notification);


//        Intent yesReceive = new Intent();
//        yesReceive.setAction(Constants.YES_ACTION);
//        yesReceive.putExtra("message", messageSt);
//        yesReceive.putExtra("category", category);
//        yesReceive.putExtra("alertId", alertId);
//        yesReceive.putExtra("location_lat", location_lat);
//        yesReceive.putExtra("location_lng", location_lng);
//        yesReceive.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntentYes = PendingIntent.getBroadcast(this, 12345, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
//        notif.addAction(0, "View ", pendingIntentYes);

//        Intent yesReceive2 = new Intent();
//        yesReceive2.setAction(Constants.STOP_ACTION);
//        yesReceive2.putExtra("message", messageSt);
//        yesReceive2.putExtra("category", category);
//        yesReceive2.putExtra("alertId", alertId);
//        yesReceive2.putExtra("location_lat", location_lat);
//        yesReceive2.putExtra("location_lng", location_lng);
//        PendingIntent pendingIntentYes2 = PendingIntent.getBroadcast(this, 12345, yesReceive2, PendingIntent.FLAG_UPDATE_CURRENT);
//        notif.addAction(0, "Cancel", pendingIntentYes2);

//        notificationManager.notify(10, notif.getNotification());
//        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
//        this.sendBroadcast(it);
    }
}

