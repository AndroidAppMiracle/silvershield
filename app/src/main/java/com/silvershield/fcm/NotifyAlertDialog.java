package com.silvershield.fcm;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.activities.LoginActivity;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import retrofit2.Response;

public class NotifyAlertDialog extends BaseActivity implements View.OnClickListener {

    Button mBtnAccept, mBtnReject;
    TextView tv_accept_reject_title;
    String category = "", alertID = "";
    TextView titleTxt;
    double location_lng = 0.0, location_lat = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_alert_dialog);
        titleTxt = (TextView) findViewById(R.id.titleTxt);
        mBtnAccept = (Button) findViewById(R.id.btnAccept);
        mBtnReject = (Button) findViewById(R.id.btnReject);

        if (getUserLoggedIn()) {
            Constants.ACCESS_TOKEN = getAuthToken();
            Constants.USER_ID = getUserID();
            Constants.USER_NAME = getFirstName() + "" + getLastName();
        }
        mBtnAccept.setOnClickListener(this);
        mBtnReject.setOnClickListener(this);

        tv_accept_reject_title = (TextView) findViewById(R.id.tv_accept_reject_title);
        try {
            if (getIntent().hasExtra("NOTIFICATION_MESSAGE")) {
                tv_accept_reject_title.setMovementMethod(LinkMovementMethod.getInstance());
                tv_accept_reject_title.setText(getIntent().getExtras().getString("NOTIFICATION_MESSAGE"));
            }
            if (getIntent().hasExtra("category")) {
                category = getIntent().getStringExtra("category");
            }
            if (getIntent().hasExtra("alertId")) {
                alertID = getIntent().getStringExtra("alertId");
            }
            if (getIntent().hasExtra("location_lng")) {
                location_lng = getIntent().getDoubleExtra("location_lng", 0.0);
            }
            if (getIntent().hasExtra("location_lat")) {
                location_lat = getIntent().getDoubleExtra("location_lat", 0.0);
            }
            if (category.equalsIgnoreCase("TIMER_EXPIRED")) {
                titleTxt.setText("Notification Alert");
                mBtnAccept.setVisibility(View.VISIBLE);
                mBtnReject.setVisibility(View.VISIBLE);
            } else {
                titleTxt.setText("Notification Location");
                mBtnAccept.setVisibility(View.GONE);
                mBtnReject.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAccept:
                accept();
                break;
            case R.id.btnReject:
                reject();
                this.finish();
                break;
        }
    }

    public void accept() {
        showLoading();
        try {
            if (getUserLoggedIn()) {
                ServerAPI.getInstance().acceptRequest(APIServerResponse.ACCEPT_REJECT_ALARAM, Constants.ACCESS_TOKEN, alertID, "accept", Constants.USER_NAME, Constants.USER_ID, new APIServerResponse() {
                    @Override
                    public void onSuccess(int tag, Response response) {
                        hideLoading();
                        if (response.isSuccessful()) {
                            JsonObject jsonObject = (JsonObject) response.body();
                            if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                                hideLoading();
                                Toast.makeText(getApplicationContext(), jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                NotifyAlertDialog.this.finish();
                            } else {
                                hideLoading();
                                NotifyAlertDialog.this.finish();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Please check your Internet Connectivity", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(int tag, Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
            } else {
                Intent gotoMain = new Intent(this, LoginActivity.class);
                startActivity(gotoMain);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void reject() {
        showLoading();
        try {
            if (getUserLoggedIn()) {
                ServerAPI.getInstance().acceptRequest(APIServerResponse.ACCEPT_REJECT_ALARAM, Constants.ACCESS_TOKEN, alertID, "reject", Constants.USER_NAME, Constants.USER_ID, new APIServerResponse() {
                    @Override
                    public void onSuccess(int tag, Response response) {
                        showLoading();
                        if (response.isSuccessful()) {
                            JsonObject jsonObject = (JsonObject) response.body();
                            if (jsonObject.get("status").getAsString().equalsIgnoreCase("OK")) {
                                hideLoading();
                                Toast.makeText(getApplicationContext(), jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                NotifyAlertDialog.this.finish();
                            } else {
                                NotifyAlertDialog.this.finish();
                                hideLoading();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Please check your Internet Connectivity", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(int tag, Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
            } else {
                Intent gotoMain = new Intent(this, LoginActivity.class);
                startActivity(gotoMain);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

