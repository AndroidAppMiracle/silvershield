package com.silvershield.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvershield.R;
import com.silvershield.adapter.VipAdapter;
import com.silvershield.adapter.WatchListAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;
import com.silvershield.modal.VipList;
import com.silvershield.modal.WatchList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class VipListFragemt extends Fragment {

    @BindView(R.id.vip_list_rv)
    RecyclerView vip_list_rv;

    @BindView(R.id.null_tv)
    Custome_Regular_TextView null_tv;
    String watchListArray;
    ArrayList<VipList> vipLists;
    VipAdapter vipListsAdapter;
    LinearLayoutManager linearLayoutManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vip_list_fragemt, container, false);
        ButterKnife.bind(this, view);

        linearLayoutManager = new LinearLayoutManager(getContext());
        vip_list_rv.setLayoutManager(linearLayoutManager);

        if (getArguments()!=null) {
            watchListArray=getArguments().getString("vip");
            if(!watchListArray.isEmpty())
            {
                vip_list_rv.setVisibility(View.VISIBLE);
                null_tv.setVisibility(View.GONE);
                vipListsAdapter = new VipAdapter(getContext(), watchListArray);
                vip_list_rv.setAdapter(vipListsAdapter);
            }else
            {
                vip_list_rv.setVisibility(View.VISIBLE);
                null_tv.setVisibility(View.GONE);
            }
        }
        return view;
    }


}
