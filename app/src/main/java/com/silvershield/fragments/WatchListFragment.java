package com.silvershield.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvershield.R;
import com.silvershield.adapter.WatchListAdapter;
import com.silvershield.custome_controls.Custome_Regular_TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WatchListFragment extends Fragment {

    @BindView(R.id.watch_list_rv)
    RecyclerView watch_list_rv;

    @BindView(R.id.null_tv)
    Custome_Regular_TextView null_tv;

    /*  ArrayList<WatchList> watchList;*/
    String watchListArray;
    WatchListAdapter watchListAdapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_watch_list, container, false);
        ButterKnife.bind(this, rootView);
        linearLayoutManager = new LinearLayoutManager(getContext());
        watch_list_rv.setLayoutManager(linearLayoutManager);
/*{"status":"OK","watchlist":[{"id":6788,"visitor_suffix":"","first_name":"eduardo","middle_name":"","last_name":"ravelo","ssn":"0","dob":"1968-10-13","phone":"789987","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":1,"reason_watchlist_id":"","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"","bar_code_image":"","quick_badge_image":"","created_by":337,"is_deleted":0,"updated_from":"","parent_id":337,"schools_id":"","clients_id":31,"created_at":"2017-11-01 09:43:09","modified_at":"2018-01-10 04:15:22","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9420,"visitor_suffix":"","first_name":"eduardo","middle_name":"","last_name":"ravelo","ssn":"0","dob":"1968-10-13","phone":"","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"53","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"CRIMINAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/ffjD1N1dcfW7.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2018-01-08 13:10:29","modified_at":"2018-01-08 08:10:29","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9419,"visitor_suffix":"","first_name":"Jason","middle_name":"","last_name":"Brown","ssn":"0","dob":"1986-07-07","phone":"","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"SO Match Found.","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/offender.1515416806371.jpeg","bar_code_image":"","quick_badge_image":"","created_by":1,"is_deleted":0,"updated_from":"","parent_id":670,"schools_id":"","clients_id":31,"created_at":"2018-01-08 13:06:46","modified_at":"2018-01-08 08:06:46","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9314,"visitor_suffix":"","first_name":"Jason","middle_name":"","last_name":"Brown","ssn":"0","dob":"1986-07-07","phone":"9876543210","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":1,"reason_watchlist_id":"","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/i1515567071012.png","bar_code_image":"","quick_badge_image":"","created_by":337,"is_deleted":0,"updated_from":"","parent_id":337,"schools_id":"","clients_id":31,"created_at":"2017-12-27 13:22:26","modified_at":"2018-01-08 07:12:33","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9348,"visitor_suffix":"","first_name":"Jason","middle_name":"","last_name":"Brown","ssn":"0","dob":"1986-07-07","phone":"+17042118918","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"SO Match Found.","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/so_1515412591988id=IA19095.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2017-12-29 09:45:57","modified_at":"2018-01-08 06:56:36","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9411,"visitor_suffix":"","first_name":"eduardo","middle_name":"","last_name":"ravelo","ssn":"0","dob":"1968-10-13","phone":"","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"53","reason_viplist_id":"","reason_watchlist_notes":"CB Match Found.","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/offender.1515405040771.jpeg","bar_code_image":"","quick_badge_image":"","created_by":1,"is_deleted":0,"updated_from":"","parent_id":670,"schools_id":"","clients_id":31,"created_at":"2018-01-08 09:50:40","modified_at":"2018-01-08 04:50:40","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9352,"visitor_suffix":"","first_name":"jason","middle_name":"","last_name":"brown","ssn":"0","dob":"1986-07-07","phone":"+1436456","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"SO Match Found.","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/so_1515155362922id=IA19095.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2017-12-29 10:29:55","modified_at":"2018-01-05 07:29:25","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9358,"visitor_suffix":"","first_name":"Eduardo","middle_name":"","last_name":"Ravelo","ssn":"0","dob":"1968-10-13","phone":"9872279610","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":1,"reason_watchlist_id":"","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/null","bar_code_image":"","quick_badge_image":"","created_by":337,"is_deleted":0,"updated_from":"","parent_id":337,"schools_id":"","clients_id":31,"created_at":"2017-12-29 11:57:45","modified_at":"2018-01-05 05:00:28","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9326,"visitor_suffix":"","first_name":"Jason","middle_name":"","last_name":"brown","ssn":"0","dob":"1986-07-07","phone":"","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"CRIMINAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/so_1515072901791id=IA19095.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2017-12-28 09:45:45","modified_at":"2018-01-04 08:35:06","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":9383,"visitor_suffix":"","first_name":"Steven","middle_name":"","last_name":"vanwagoner ","ssn":"0","dob":"1960-05-31","phone":"","email":"","is_vip":0,"is_watch":1,"is_qb":0,"is_se":0,"reason_watchlist_id":"27","reason_viplist_id":"","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"CRIMINAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/so_1514983249229id=UT2303749.png","bar_code_image":"","quick_badge_image":"","created_by":1447,"is_deleted":0,"updated_from":"","parent_id":326,"schools_id":"","clients_id":31,"created_at":"2018-01-03 12:40:50","modified_at":"2018-01-03 07:40:50","visitor_id_type":0,"visitor_id_number":"","country_id":""}],"vip":[{"id":904,"visitor_suffix":"","first_name":"Amrinder","middle_name":"","last_name":"Singh","ssn":"0","dob":"","phone":"89789798","email":"test@test.com","is_vip":1,"is_watch":0,"is_qb":0,"is_se":0,"reason_watchlist_id":"","reason_viplist_id":"5","reason_watchlist_notes":"","reason_viplist_notes":"","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/null","bar_code_image":"","quick_badge_image":"","created_by":670,"is_deleted":0,"updated_from":"","parent_id":670,"schools_id":"","clients_id":31,"created_at":"2017-06-26 13:46:44","modified_at":"2017-06-26 09:46:44","visitor_id_type":0,"visitor_id_number":"","country_id":""},{"id":836,"visitor_suffix":"","first_name":"Steve","middle_name":"","last_name":"Job","ssn":"0","dob":"","phone":"","email":"","is_vip":1,"is_watch":0,"is_qb":0,"is_se":0,"reason_watchlist_id":"","reason_viplist_id":"1","reason_watchlist_notes":"","reason_viplist_notes":"He is key of apple computer and iphones","watch_list_reason":"MANUAL","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/media.1498132441105.jpg","bar_code_image":"","quick_badge_image":"","created_by":670,"is_deleted":0,"updated_from":"","parent_id":670,"schools_id":"","clients_id":31,"created_at":"2017-06-22 11:54:01","modified_at":"2017-06-22 07:54:17","visitor_id_type":0,"visitor_id_number":"","country_id":""}]}*/

        if (getArguments() != null) {
            //   watchList = getArguments().getParcelableArrayList("watch");
            watchListArray = getArguments().getString("watch");
            if (!watchListArray.isEmpty()) {
                watch_list_rv.setVisibility(View.VISIBLE);
                null_tv.setVisibility(View.GONE);
                watchListAdapter = new WatchListAdapter(getContext(), watchListArray);
                watch_list_rv.setAdapter(watchListAdapter);
            } else {
                watch_list_rv.setVisibility(View.VISIBLE);
                null_tv.setVisibility(View.GONE);
            }
        }
        return rootView;
    }


}
