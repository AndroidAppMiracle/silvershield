package com.silvershield.interfaces;

/**
 * Created by gagandeep.bhutani on 3/21/2018.
 */

public interface AlertLongClick {

    void onLongClick(String dispatch_id);

    void onReloadPage();

}
