package com.silvershield.interfaces;

/**
 * Created by gagandeep.bhutani on 3/21/2018.
 */

public interface SuccessCodeCheck {

    void onCodeVerify(String message, String enteredCode, String typeOfAlarms,String callTypePerson);
}
