package com.silvershield.interfaces;

import com.silvershield.modal.Matches;

/**
 * Created by gagandeep.bhutani on 3/27/2018.
 */

public interface SuggestionDialogClick {

    public void onClick(Matches matches);
}
