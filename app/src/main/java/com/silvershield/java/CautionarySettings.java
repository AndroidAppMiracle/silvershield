package com.silvershield.java;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.interfaces.AlertLongClick;
import com.silvershield.interfaces.SuccessCodeCheck;
import com.silvershield.modal.requestModels.VerifyCautioneryPassRequestModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;

import retrofit2.Response;

/**
 * Created by gagandeep.bhutani on 3/22/2018.
 */

public class CautionarySettings implements APIServerResponse {

    String enteredCode;
    private DialogInterface.OnClickListener noClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
        }
    };
    private AlertDialog codeAlertDialog;
    private Context context;
    private Boolean cautionary_confirm = false, cautionary_code = false, cautionaryCheck = false, codeCheck = false;
    private String callTypePerson, typeOfAlarms;
    private SuccessCodeCheck successCodeCheck;
    private boolean cautionaryClear;
    private int clear_step = 0;
    private DialogInterface.OnClickListener noClearAlert = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    };
    private String dispatch_id;
    private DialogInterface.OnClickListener yesClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            cautionaryCheck = true;
            if (cautionary_code) {
                codeTrue(context.getResources().getString(R.string.warning_msg));
            } else {
                callAlarm("");
            }
        }
    };
    private DialogInterface.OnClickListener clearAlert = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            if (!cautionaryClear)
                setCautionaryClear();
            else {
                codeTrue(context.getResources().getString(R.string.clear_alert_msg1));
                clear_step = 1;
            }
        }
    };
    private AlertLongClick alertLongClick;
    private boolean playAlarm = false;
    private MediaPlayer mp;

    public CautionarySettings(Context context, String callTypePerson, String typeOfAlarms, SuccessCodeCheck successCodeCheck, boolean playAlarm) {
        this.context = context;
        this.callTypePerson = callTypePerson;
        this.typeOfAlarms = typeOfAlarms;
        this.successCodeCheck = successCodeCheck;
        this.playAlarm = playAlarm;
    }

    public CautionarySettings(Context context, String dispatch_id, AlertLongClick alertLongClick) {
        this.context = context;
        this.alertLongClick = alertLongClick;
        this.dispatch_id = dispatch_id;
    }

    public void clearAlert(String title, String msg) {
        ((BaseActivity) context).showAlertDialog(context, title, msg, "Yes", "No", clearAlert, noClearAlert);
    }

    public void getCautionary() {
        if (((BaseActivity) context).isConnectedToInternet()) {
            ((BaseActivity) context).showLoading();
            ServerAPI.getInstance().getCautionaryDetail(APIServerResponse.GETCAUTIONARY, ((BaseActivity) context).getAuthToken(), ((BaseActivity) context).getClientId(), this);
        } else {
            ((BaseActivity) context).noInternetMessage();
        }
    }

    private void setCautionaryClear() {
        if (((BaseActivity) context).isConnectedToInternet()) {
            ((BaseActivity) context).showLoading();
            ServerAPI.getInstance().setCautionaryClear(APIServerResponse.GETCAUTIONARYCLEAR, ((BaseActivity) context).getAuthToken(), ((BaseActivity) context).getClientId(), this);
        } else {
            ((BaseActivity) context).noInternetMessage();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        ((BaseActivity) context).hideLoading();
        if (response.isSuccessful()) {
            switch (tag) {
                case CLEARALERT: {
                    JsonObject jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                        codeAlertDialog.dismiss();
                        Toast.makeText(context, "" + jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        alertLongClick.onReloadPage();

                    } else {
                        Toast.makeText(context, "" + jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case VERIFYCAUTIONERYPASSWORD: {
                    JsonObject jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                        clear_step = 2;
                        codeAlertDialog.dismiss();
                        codeTrue(context.getResources().getString(R.string.clear_alert_msg2));

                    } else {
                        Toast.makeText(context, "" + jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case GETCAUTIONARYCLEAR: {
                    JsonObject jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                        JsonObject settings = jsonObject.getAsJsonObject("settings");
                        if (settings.has("cautionary_confirm")) {
                            cautionary_confirm = settings.get("cautionary_confirm").getAsBoolean();
                        }
                        if (settings.has("cautionary_code")) {
                            cautionary_code = settings.get("cautionary_code").getAsBoolean();
                        }
                        cautionaryClear = true;

                        if (cautionary_confirm) {
                            clearAlert("Are you sure?", "Cautionary Alarms secruity is ON");
                        } else if (cautionary_code) {
                            codeTrue(context.getResources().getString(R.string.warning_msg));
                        } else {
                            callAlarm("");
                        }

                    } else {
                        Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case GETCAUTIONARY: {
                    JsonObject jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                        JsonObject settings = jsonObject.getAsJsonObject("settings");
                        if (settings.has("cautionary_confirm")) {
                            cautionary_confirm = settings.get("cautionary_confirm").getAsBoolean();
                        }
                        if (settings.has("cautionary_code")) {
                            cautionary_code = settings.get("cautionary_code").getAsBoolean();
                        }

                        if (cautionary_confirm) {
                            cautionaryTrue();
                        } else if (cautionary_code) {
                            codeTrue(context.getResources().getString(R.string.warning_msg));
                        } else {
                            callAlarm("");
                        }

                    } else {
                        Toast.makeText(context, "" + jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case CALL_ALARM: {
                    JsonObject jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("status").getAsString().equalsIgnoreCase("ok")) {
                        if (cautionary_code) {
                            if (codeCheck) {
                                successCodeCheck.onCodeVerify(jsonObject.get("message").getAsString(), enteredCode, typeOfAlarms, callTypePerson);
                            }
                        } else {
                            successCodeCheck.onCodeVerify(jsonObject.get("message").getAsString(), enteredCode, typeOfAlarms, callTypePerson);
                        }
                        if (playAlarm) {
                            playAlarm();
                        }
                    } else {
                        Toast.makeText(context, "" + jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case CALL_LOCK_EMERGENCY: {
                    JsonObject jsonObjectEmergence = (JsonObject) response.body();
                    if (jsonObjectEmergence.get("status").getAsString().equalsIgnoreCase("ok")) {
                        successCodeCheck.onCodeVerify(jsonObjectEmergence.get("message").getAsString(), enteredCode, typeOfAlarms, callTypePerson);
                        if (playAlarm) {
                            playAlarm();
                        }
                    } else {
                        Toast.makeText(context, "" + jsonObjectEmergence.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        if (codeAlertDialog != null) {
                            codeAlertDialog.dismiss();
                        }
                    }
                    break;
                }
                case CALL_LOCK_NON_EMERGENCY: {
                    JsonObject jsonObjectNonEmergence = (JsonObject) response.body();
                    if (jsonObjectNonEmergence.get("status").getAsString().equalsIgnoreCase("ok")) {
                        successCodeCheck.onCodeVerify(jsonObjectNonEmergence.get("message").getAsString(), enteredCode, typeOfAlarms, callTypePerson);
                    } else {
                        Toast.makeText(context, "" + jsonObjectNonEmergence.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        if (codeAlertDialog != null) {
                            codeAlertDialog.dismiss();
                        }
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        ((BaseActivity) context).hideLoading();
        throwable.printStackTrace();
    }

    private void playAlarm() {
        PlaySound playSound = new PlaySound(context);
        playSound.playSound();
    }

    private void cautionaryTrue() {
        ((BaseActivity) context).showAlertDialog(context, "Alert", context.getResources().getString(R.string.sure_to_alarm), "Yes", "No", yesClick, noClick);
    }

    private void codeTrue(String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((BaseActivity) context).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custome_layout_with_edittext, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        final EditText edittext = (EditText) dialogView.findViewById(R.id.edit1);
        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        Button cancle_btn = (Button) dialogView.findViewById(R.id.cancle_btn);
        tvMsg.setText(message);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enteredCode = edittext.getText().toString();
                if (enteredCode.isEmpty()) {
                    Toast.makeText(context, "Enter password first.", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!cautionaryClear) {
                    codeCheck = true;
                    codeAlertDialog.dismiss();
                    callAlarm(enteredCode);
                } else {
//                    codeAlertDialog.dismiss();
                    if (clear_step == 1) {
                        codeCheck = true;
                        verifyCautioneryAlarm(enteredCode);
                    } else if (clear_step == 2) {
                        clearAlert(enteredCode);
                    }
                }
            }
        });
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codeAlertDialog.dismiss();
            }
        });
        codeAlertDialog = dialogBuilder.create();
        codeAlertDialog.show();
    }

    private void verifyCautioneryAlarm(String enteredCode) {
        if (((BaseActivity) context).isConnectedToInternet()) {
            ((BaseActivity) context).showLoading();
            VerifyCautioneryPassRequestModel model = new VerifyCautioneryPassRequestModel();
            model.setCautionary_code(enteredCode);
            ServerAPI.getInstance().verifyCautionaryPassword(APIServerResponse.VERIFYCAUTIONERYPASSWORD, model, this);
        } else {
            ((BaseActivity) context).noInternetMessage();
        }
    }

    private void clearAlert(String enteredCode) {
        if (((BaseActivity) context).isConnectedToInternet()) {
            ((BaseActivity) context).showLoading();
            VerifyCautioneryPassRequestModel model = new VerifyCautioneryPassRequestModel();
            model.setCode(enteredCode);
            model.setDispatch_id(dispatch_id);
            ServerAPI.getInstance().clearAlert(APIServerResponse.CLEARALERT, model, this);
        } else {
            ((BaseActivity) context).noInternetMessage();
        }
    }

    private void callAlarm(String codeSt) {
        if (((BaseActivity) context).isConnectedToInternet()) {
            ((BaseActivity) context).showLoading();
            if (callTypePerson.equalsIgnoreCase("EMERGENCY")) {
                ServerAPI.getInstance().callLockDownAlarm(APIServerResponse.CALL_LOCK_EMERGENCY, ((BaseActivity) context).getAuthToken(), ((BaseActivity) context).getUserID(), ((BaseActivity) context).getParentIdValue(), ((BaseActivity) context).getClientId(), "SMS", "", codeSt, this);
            } else if (callTypePerson.equalsIgnoreCase("NONEMERGENCY")) {
                ServerAPI.getInstance().callNonEmergenceyAlarm(APIServerResponse.CALL_LOCK_NON_EMERGENCY, ((BaseActivity) context).getAuthToken(), ((BaseActivity) context).getUserID(), ((BaseActivity) context).getParentIdValue(), ((BaseActivity) context).getClientId(), "SMS", "", codeSt, this);
            } else {
                ServerAPI.getInstance().callAlarm(APIServerResponse.CALL_ALARM, ((BaseActivity) context).getAuthToken(),/* getClientId()*/ ((BaseActivity) context).getUserID(),
                        ((BaseActivity) context).getClientId(), ((BaseActivity) context).getParentIdValue(),
                        callTypePerson, "SMS",
                        ((BaseActivity) context).getFirstName() + ((BaseActivity) context).getLastName(),
                        "", codeSt, "", "", "", "", this);
            }
        } else {
            ((BaseActivity) context).noInternetMessage();
        }
    }
}
