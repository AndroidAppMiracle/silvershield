package com.silvershield.java;

import android.content.Context;
import android.media.MediaPlayer;

import com.silvershield.R;

/**
 * Created by gagandeep.bhutani on 3/26/2018.
 */

public class PlaySound {
    private MediaPlayer mp;
    private Context context;

    public PlaySound(Context context) {
        this.context = context;
    }

    public synchronized void playSound() {
        mp = MediaPlayer.create(context, R.raw.warning);
        try {
            if (mp.isPlaying()) {
                mp.stop();
                mp.release();
                mp = MediaPlayer.create(context, R.raw.warning);
            }
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
