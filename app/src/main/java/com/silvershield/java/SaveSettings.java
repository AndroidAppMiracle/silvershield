package com.silvershield.java;

import com.silvershield.modal.SettingMainResponse;
import com.silvershield.modal.SettingsModel;
import com.silvershield.utils.BaseActivity;

import java.util.List;

/**
 * Created by gagandeep.bhutani on 3/27/2018.
 */

public class SaveSettings extends BaseActivity{
    public SaveSettings() {

    }

    public void saveSettings(List<SettingsModel> settings) {
        for (int i = 0; i < settings.size(); i++) {
            if (settings.get(i).getTitle().equalsIgnoreCase("alertAdminButton")) {
                setAlertAdmin(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertGuardAndAdminButton")) {
                setGuardAndAdmin(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertLockdownButton")) {
                setLockdown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertPoliceEmergencyButton")) {
                setalertPoliceEmergency(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertPoliceNonEmergencyButton")) {
                setAlertPoliceNonEmergencyButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("checkInVisitors")) {
                setCheckInVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("checkOutVisitors")) {
                setCheckOutVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("createQuickBadge")) {
                setCreateQuickBadge(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("createVipList")) {
                setCreateVipList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("createWatchList")) {
                setCreateWatchList(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("sexOffenderCheck")) {
                setSexOffenderCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("criminalBackgroundCheck")) {
                setCriminalBackgroundCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("eventSexOffenderCheck")) {
                setEventSexOffenderCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("eventCriminalBackgroundCheck")) {
                setEventCbcCheck(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("allowCallPolice")) {
                setAllowCallPolice(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("editPoliceContactNumbers")) {
                setEditPoliceContactNumbers(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageEvents")) {
                setManageEvent(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageBadges")) {
                setManageBadges(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageStaff")) {
                setManageStaff(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageGuards")) {
                setManageGuards(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageVisitors")) {
                setManageVisitors(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageTeachers")) {
                setManageTeachers(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageStudents")) {
                setManageStudents(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageParents")) {
                setManageParents(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageVideos")) {
                setManageVideos(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertGuardsButton")) {
                setAlertGuardsButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertStaffButton")) {
                setAlertStaffButton(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("manageSelfSignupRequests")) {
                setManageSelfSignupRequests(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertHardLockdownButton")) {
                setHardLockDown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertSoftLockdownButton")) {
                setSoftLockDown(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertHardLockdownButtonDrill")) {
                setAlertHardLockdownButtonDrill(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            } else if (settings.get(i).getTitle().equalsIgnoreCase("alertScreen")) {
                setAlertScreen(checkState(settings.get(i).getFlag(), settings.get(i).getState()));
            }
        }
    }

    public String checkState(int flag, int state) {
        if (flag == 1 && state == 1) {
            return "1";
        }
        return "0";
    }
}
