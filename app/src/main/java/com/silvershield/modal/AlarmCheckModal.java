package com.silvershield.modal;

/**
 * Created by satoti.garg on 11/30/2017.
 */

public class AlarmCheckModal {
    private String status;

    private String policeNonEmerg;

    private String policeEmerg;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getPoliceNonEmerg ()
    {
        return policeNonEmerg;
    }

    public void setPoliceNonEmerg (String policeNonEmerg)
    {
        this.policeNonEmerg = policeNonEmerg;
    }

    public String getPoliceEmerg ()
    {
        return policeEmerg;
    }

    public void setPoliceEmerg (String policeEmerg)
    {
        this.policeEmerg = policeEmerg;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", policeNonEmerg = "+policeNonEmerg+", policeEmerg = "+policeEmerg+"]";
    }
}
