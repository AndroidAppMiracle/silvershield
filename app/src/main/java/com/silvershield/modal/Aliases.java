package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class Aliases implements Parcelable {
    private String MiddleName;

    private String FullName;

    private String FirstName;

    private String LastName;

    protected Aliases(Parcel in) {
        MiddleName = in.readString();
        FullName = in.readString();
        FirstName = in.readString();
        LastName = in.readString();
    }


    public static final Creator<Aliases> CREATOR = new Creator<Aliases>() {
        @Override
        public Aliases createFromParcel(Parcel in) {
            return new Aliases(in);
        }

        @Override
        public Aliases[] newArray(int size) {
            return new Aliases[size];
        }
    };

    public String getMiddleName ()
    {
        return MiddleName;
    }

    public void setMiddleName (String MiddleName)
    {
        this.MiddleName = MiddleName;
    }

    public String getFullName ()
    {
        return FullName;
    }

    public void setFullName (String FullName)
    {
        this.FullName = FullName;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [MiddleName = "+MiddleName+", FullName = "+FullName+", FirstName = "+FirstName+", LastName = "+LastName+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(MiddleName);
        parcel.writeString(FullName);
        parcel.writeString(FirstName);
        parcel.writeString(LastName);
    }
}
