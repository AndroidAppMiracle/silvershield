package com.silvershield.modal;

import java.util.List;

/**
 * Created by satoti.garg on 11/28/2017.
 */

public class AssignToMeReponse {

    private List<Tests> tests;

    private String status;

    public List<Tests> getTests() {
        return tests;
    }

    public void setTests(List<Tests> tests) {
        this.tests = tests;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [tests = " + tests + ", status = " + status + "]";
    }
}
