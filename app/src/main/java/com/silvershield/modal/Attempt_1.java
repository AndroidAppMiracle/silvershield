package com.silvershield.modal;

/**
 * Created by satoti.garg on 11/28/2017.
 */

public class Attempt_1 {

    private String id;

    private String record;

    private String score;

    private String video_id;

    private String attemp_no;

    private String test_status;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getRecord ()
    {
        return record;
    }

    public void setRecord (String record)
    {
        this.record = record;
    }

    public String getScore ()
    {
        return score;
    }

    public void setScore (String score)
    {
        this.score = score;
    }

    public String getVideo_id ()
    {
        return video_id;
    }

    public void setVideo_id (String video_id)
    {
        this.video_id = video_id;
    }

    public String getAttemp_no ()
    {
        return attemp_no;
    }

    public void setAttemp_no (String attemp_no)
    {
        this.attemp_no = attemp_no;
    }

    public String getTest_status ()
    {
        return test_status;
    }

    public void setTest_status (String test_status)
    {
        this.test_status = test_status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", record = "+record+", score = "+score+", video_id = "+video_id+", attemp_no = "+attemp_no+", test_status = "+test_status+"]";
    }
}
