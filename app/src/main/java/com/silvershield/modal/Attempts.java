package com.silvershield.modal;

/**
 * Created by satoti.garg on 11/28/2017.
 */

public class Attempts {
    private Attempt_1 attempt_1;

    private Attempt_2 attempt_2;

    public Attempt_1 getAttempt_1 ()
    {
        return attempt_1;
    }

    public void setAttempt_1 (Attempt_1 attempt_1)
    {
        this.attempt_1 = attempt_1;
    }

    public Attempt_2 getAttempt_2 ()
    {
        return attempt_2;
    }

    public void setAttempt_2 (Attempt_2 attempt_2)
    {
        this.attempt_2 = attempt_2;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [attempt_1 = "+attempt_1+", attempt_2 = "+attempt_2+"]";
    }
}
