package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 11/27/2017.
 */

public class CategoriesDetail implements Parcelable{

    private String id;

    private String category_name;

    protected CategoriesDetail(Parcel in) {
        id = in.readString();
        category_name = in.readString();
    }

    public static final Creator<CategoriesDetail> CREATOR = new Creator<CategoriesDetail>() {
        @Override
        public CategoriesDetail createFromParcel(Parcel in) {
            return new CategoriesDetail(in);
        }

        @Override
        public CategoriesDetail[] newArray(int size) {
            return new CategoriesDetail[size];
        }
    };

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCategory_name ()
    {
        return category_name;
    }

    public void setCategory_name (String category_name)
    {
        this.category_name = category_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", category_name = "+category_name+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(category_name);
    }
}
