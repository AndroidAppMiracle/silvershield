package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

public class Convictions implements Parcelable {

    private String statute;

    private String victimage;

    private String charge;

    private String victimsex;

    private String convictionstate;

    protected Convictions(Parcel in) {
        statute = in.readString();
        victimage = in.readString();
        charge = in.readString();
        victimsex = in.readString();
        convictionstate = in.readString();
    }

    public static final Creator<Convictions> CREATOR = new Creator<Convictions>() {
        @Override
        public Convictions createFromParcel(Parcel in) {
            return new Convictions(in);
        }

        @Override
        public Convictions[] newArray(int size) {
            return new Convictions[size];
        }
    };

    public String getStatute ()
    {
        return statute;
    }

    public void setStatute (String statute)
    {
        this.statute = statute;
    }

    public String getVictimage ()
    {
        return victimage;
    }

    public void setVictimage (String victimage)
    {
        this.victimage = victimage;
    }

    public String getCharge ()
    {
        return charge;
    }

    public void setCharge (String charge)
    {
        this.charge = charge;
    }

    public String getVictimsex ()
    {
        return victimsex;
    }

    public void setVictimsex (String victimsex)
    {
        this.victimsex = victimsex;
    }

    public String getConvictionstate ()
    {
        return convictionstate;
    }

    public void setConvictionstate (String convictionstate)
    {
        this.convictionstate = convictionstate;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statute = "+statute+", victimage = "+victimage+", charge = "+charge+", victimsex = "+victimsex+", convictionstate = "+convictionstate+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(statute);
        parcel.writeString(victimage);
        parcel.writeString(charge);
        parcel.writeString(victimsex);
        parcel.writeString(convictionstate);
    }
}
