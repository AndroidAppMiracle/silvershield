package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class Countries {
    private String id;

    private String country_name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCountry_name ()
    {
        return country_name;
    }

    public void setCountry_name (String country_name)
    {
        this.country_name = country_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", country_name = "+country_name+"]";
    }
}
