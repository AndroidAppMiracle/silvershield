package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class CriminalOfferses implements Parcelable{
    private String Description;

    private String AgeOfVictim;

    private String SexOfVictim;

    private String Title;

    private String Category;


    private String Disposition;//ChargeFilingDate

    private String CaseNumber;



    private String ChargeFilingDate;

    protected CriminalOfferses(Parcel in) {
        Description = in.readString();
        AgeOfVictim = in.readString();
        SexOfVictim = in.readString();
        Title = in.readString();
        Category = in.readString();
        Disposition = in.readString();
        CaseNumber = in.readString();
        ChargeFilingDate = in.readString();
    }

    public static final Creator<CriminalOfferses> CREATOR = new Creator<CriminalOfferses>() {
        @Override
        public CriminalOfferses createFromParcel(Parcel in) {
            return new CriminalOfferses(in);
        }

        @Override
        public CriminalOfferses[] newArray(int size) {
            return new CriminalOfferses[size];
        }
    };

    public String getChargeFilingDate() {
        return ChargeFilingDate;
    }

    public void setChargeFilingDate(String chargeFilingDate) {
        ChargeFilingDate = chargeFilingDate;
    }
    public String getDisposition() {
        return Disposition;
    }

    public void setDisposition(String disposition) {
        Disposition = disposition;
    }


    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getCaseNumber() {
        return CaseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        CaseNumber = caseNumber;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getAgeOfVictim ()
    {
        return AgeOfVictim;
    }

    public void setAgeOfVictim (String AgeOfVictim)
    {
        this.AgeOfVictim = AgeOfVictim;
    }

    public String getSexOfVictim ()
    {
        return SexOfVictim;
    }

    public void setSexOfVictim (String SexOfVictim)
    {
        this.SexOfVictim = SexOfVictim;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", AgeOfVictim = "+AgeOfVictim+", SexOfVictim = "+SexOfVictim+", Title = "+Title+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Description);
        parcel.writeString(AgeOfVictim);
        parcel.writeString(SexOfVictim);
        parcel.writeString(Title);
        parcel.writeString(Category);
        parcel.writeString(Disposition);
        parcel.writeString(CaseNumber);
        parcel.writeString(ChargeFilingDate);
    }
}
