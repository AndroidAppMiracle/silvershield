package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class CriminalResponse implements Parcelable {

    private String message;

    private String status;

    private Data data;

    protected CriminalResponse(Parcel in) {
        message = in.readString();
        status = in.readString();
    }

    public static final Creator<CriminalResponse> CREATOR = new Creator<CriminalResponse>() {
        @Override
        public CriminalResponse createFromParcel(Parcel in) {
            return new CriminalResponse(in);
        }

        @Override
        public CriminalResponse[] newArray(int size) {
            return new CriminalResponse[size];
        }
    };

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(message);
        parcel.writeString(status);
    }
}
