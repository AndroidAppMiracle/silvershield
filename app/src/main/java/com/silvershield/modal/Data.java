package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class Data implements Parcelable{
    private Results Results;

    protected Data(Parcel in) {
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public Results getResults ()
    {
        return Results;
    }

    public void setResults (Results Results)
    {
        this.Results = Results;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Results = "+Results+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
