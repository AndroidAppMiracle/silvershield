package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class Docs_type {

    private String id;

    private String document_name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDocument_name ()
    {
        return document_name;
    }

    public void setDocument_name (String document_name)
    {
        this.document_name = document_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", document_name = "+document_name+"]";
    }
}
