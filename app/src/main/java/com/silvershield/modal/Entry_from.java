package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class Entry_from {
    private String id;

    private String entery_place;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getEntery_place ()
    {
        return entery_place;
    }

    public void setEntery_place (String entery_place)
    {
        this.entery_place = entery_place;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", entery_place = "+entery_place+"]";
    }
}
