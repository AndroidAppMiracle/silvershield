package com.silvershield.modal;

import java.util.List;

/**
 * Created by satoti.garg on 12/18/2017.
 */

public class EventResponseObject {
    private String status;

    private List<Events> events;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<Events> getEvents ()
    {
        return events;
    }

    public void setEvents (List<Events> events)
    {
        this.events = events;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", events = "+events+"]";
    }
}
