package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/18/2017.
 */

public class Events {
    private String id;

    private String event_name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getEvent_name ()
    {
        return event_name;
    }

    public void setEvent_name (String event_name)
    {
        this.event_name = event_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", event_name = "+event_name+"]";
    }
}
