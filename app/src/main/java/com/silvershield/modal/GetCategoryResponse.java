package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 11/27/2017.
 */

public class GetCategoryResponse implements Parcelable{

    private String status;

    private List<CategoriesDetail> categories;

    protected GetCategoryResponse(Parcel in) {
        status = in.readString();
    }

    public static final Creator<GetCategoryResponse> CREATOR = new Creator<GetCategoryResponse>() {
        @Override
        public GetCategoryResponse createFromParcel(Parcel in) {
            return new GetCategoryResponse(in);
        }

        @Override
        public GetCategoryResponse[] newArray(int size) {
            return new GetCategoryResponse[size];
        }
    };

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<CategoriesDetail> getCategories ()
    {
        return categories;
    }

    public void setCategories (List<CategoriesDetail> categories)
    {
        this.categories = categories;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", categories = "+categories+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(status);
    }
}
