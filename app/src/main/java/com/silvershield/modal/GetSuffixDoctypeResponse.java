package com.silvershield.modal;

import java.util.List;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class GetSuffixDoctypeResponse {

    private List<Docs_type> docs_type;

    private String status;

    private List<Entry_from> entry_from;

    private List<Departments> departments;

    private List<Purpose> purpose;

    private List<Countries> countries;

    private List<Suffix> suffix;

    private List<Individuals> individuals;

    public List<Docs_type> getDocs_type ()
    {
        return docs_type;
    }

    public void setDocs_type (List<Docs_type> docs_type)
    {
        this.docs_type = docs_type;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<Entry_from> getEntry_from ()
    {
        return entry_from;
    }

    public void setEntry_from (List<Entry_from> entry_from)
    {
        this.entry_from = entry_from;
    }

    public List<Departments> getDepartments ()
    {
        return departments;
    }

    public void setDepartments (List<Departments> departments)
    {
        this.departments = departments;
    }

    public List<Purpose> getPurpose ()
    {
        return purpose;
    }

    public void setPurpose (List<Purpose> purpose)
    {
        this.purpose = purpose;
    }

    public List<Countries> getCountries ()
    {
        return countries;
    }

    public void setCountries (List<Countries> countries)
    {
        this.countries = countries;
    }

    public List<Suffix> getSuffix ()
    {
        return suffix;
    }

    public void setSuffix (List<Suffix> suffix)
    {
        this.suffix = suffix;
    }

    public List<Individuals> getIndividuals ()
    {
        return individuals;
    }

    public void setIndividuals (List<Individuals> individuals)
    {
        this.individuals = individuals;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [docs_type = "+docs_type+", status = "+status+", entry_from = "+entry_from+", departments = "+departments+", purpose = "+purpose+", countries = "+countries+", suffix = "+suffix+", individuals = "+individuals+"]";
    }

}
