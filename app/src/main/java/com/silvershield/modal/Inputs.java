package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class Inputs {
    private String OnlyPhotos;

    private String DOB;

    private String Account;

    private String ExactMatch;

    private String FirstName;

    private String LastName;

    private String Limit;

    public String getOnlyPhotos ()
    {
        return OnlyPhotos;
    }

    public void setOnlyPhotos (String OnlyPhotos)
    {
        this.OnlyPhotos = OnlyPhotos;
    }

    public String getDOB ()
    {
        return DOB;
    }

    public void setDOB (String DOB)
    {
        this.DOB = DOB;
    }

    public String getAccount ()
    {
        return Account;
    }

    public void setAccount (String Account)
    {
        this.Account = Account;
    }

    public String getExactMatch ()
    {
        return ExactMatch;
    }

    public void setExactMatch (String ExactMatch)
    {
        this.ExactMatch = ExactMatch;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getLimit ()
    {
        return Limit;
    }

    public void setLimit (String Limit)
    {
        this.Limit = Limit;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [OnlyPhotos = "+OnlyPhotos+", DOB = "+DOB+", Account = "+Account+", ExactMatch = "+ExactMatch+", FirstName = "+FirstName+", LastName = "+LastName+", Limit = "+Limit+"]";
    }
}
