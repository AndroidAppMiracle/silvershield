package com.silvershield.modal;

import java.io.Serializable;
import java.util.List;

/**
 * Created by satoti.garg on 12/7/2017.
 */

public class Matches implements Serializable {

    private String id;

    private String first_name;

    private String profile_pic_web;

    private String phone;

    private String visitor_suffix;

    private String is_vip;

    private String email;

    private String middle_name;

    private String is_watch;

    private String dob;

    private String last_name;

    private String profile_pic;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getProfile_pic_web ()
    {
        return profile_pic_web;
    }

    public void setProfile_pic_web (String profile_pic_web)
    {
        this.profile_pic_web = profile_pic_web;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getVisitor_suffix ()
    {
        return visitor_suffix;
    }

    public void setVisitor_suffix (String visitor_suffix)
    {
        this.visitor_suffix = visitor_suffix;
    }

    public String getIs_vip ()
    {
        return is_vip;
    }

    public void setIs_vip (String is_vip)
    {
        this.is_vip = is_vip;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getMiddle_name ()
    {
        return middle_name;
    }

    public void setMiddle_name (String middle_name)
    {
        this.middle_name = middle_name;
    }

    public String getIs_watch ()
    {
        return is_watch;
    }

    public void setIs_watch (String is_watch)
    {
        this.is_watch = is_watch;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getProfile_pic ()
    {
        return profile_pic;
    }

    public void setProfile_pic (String profile_pic)
    {
        this.profile_pic = profile_pic;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", first_name = "+first_name+", profile_pic_web = "+profile_pic_web+", phone = "+phone+", visitor_suffix = "+visitor_suffix+", is_vip = "+is_vip+", email = "+email+", middle_name = "+middle_name+", is_watch = "+is_watch+", dob = "+dob+", last_name = "+last_name+", profile_pic = "+profile_pic+"]";
    }
}
