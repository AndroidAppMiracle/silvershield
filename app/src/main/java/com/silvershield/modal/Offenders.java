package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/12/2017.
 */

public class Offenders implements Parcelable{
    private String[] possiblenicknames;

    private String sex;

    private String matchtype;

    private String weight;

    private String state;

    private String lastname;

    private Aliases[] aliases;

    private String race;

    private String city;

    private String height;

    private String age;

    private String name;

    private String longitude;

    private Convictions[] convictions;

    private String hair;

    private Markings[] markings;

    private String eye;

    private String street2;

    private String street1;

    private String middlename;

    private String zipcode;

    private String firstname;

    private String suffix;

    private String photo;

    private String county;

    private String dob;

    private String convictiontype;

    private String[] otheraddresses;

    private String latitude;

    private String[] birthdates;

    private String offenderid;

    protected Offenders(Parcel in) {
        possiblenicknames = in.createStringArray();
        sex = in.readString();
        matchtype = in.readString();
        weight = in.readString();
        state = in.readString();
        lastname = in.readString();
        aliases = in.createTypedArray(Aliases.CREATOR);
        race = in.readString();
        city = in.readString();
        height = in.readString();
        age = in.readString();
        name = in.readString();
        longitude = in.readString();
        convictions = in.createTypedArray(Convictions.CREATOR);
        hair = in.readString();
        eye = in.readString();
        street2 = in.readString();
        street1 = in.readString();
        middlename = in.readString();
        zipcode = in.readString();
        firstname = in.readString();
        suffix = in.readString();
        photo = in.readString();
        county = in.readString();
        dob = in.readString();
        convictiontype = in.readString();
        otheraddresses = in.createStringArray();
        latitude = in.readString();
        birthdates = in.createStringArray();
        offenderid = in.readString();
    }

    public static final Creator<Offenders> CREATOR = new Creator<Offenders>() {
        @Override
        public Offenders createFromParcel(Parcel in) {
            return new Offenders(in);
        }

        @Override
        public Offenders[] newArray(int size) {
            return new Offenders[size];
        }
    };

    public String[] getPossiblenicknames ()
    {
        return possiblenicknames;
    }

    public void setPossiblenicknames (String[] possiblenicknames)
    {
        this.possiblenicknames = possiblenicknames;
    }

    public String getSex ()
    {
        return sex;
    }

    public void setSex (String sex)
    {
        this.sex = sex;
    }

    public String getMatchtype ()
    {
        return matchtype;
    }

    public void setMatchtype (String matchtype)
    {
        this.matchtype = matchtype;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String weight)
    {
        this.weight = weight;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public Aliases[] getAliases ()
    {
        return aliases;
    }

    public void setAliases (Aliases[] aliases)
    {
        this.aliases = aliases;
    }

    public String getRace ()
    {
        return race;
    }

    public void setRace (String race)
    {
        this.race = race;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public String getAge ()
    {
        return age;
    }

    public void setAge (String age)
    {
        this.age = age;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public Convictions[] getConvictions ()
    {
        return convictions;
    }

    public void setConvictions (Convictions[] convictions)
    {
        this.convictions = convictions;
    }

    public String getHair ()
    {
        return hair;
    }

    public void setHair (String hair)
    {
        this.hair = hair;
    }

    public Markings[] getMarkings ()
    {
        return markings;
    }

    public void setMarkings (Markings[] markings)
    {
        this.markings = markings;
    }

    public String getEye ()
    {
        return eye;
    }

    public void setEye (String eye)
    {
        this.eye = eye;
    }

    public String getStreet2 ()
    {
        return street2;
    }

    public void setStreet2 (String street2)
    {
        this.street2 = street2;
    }

    public String getStreet1 ()
    {
        return street1;
    }

    public void setStreet1 (String street1)
    {
        this.street1 = street1;
    }

    public String getMiddlename ()
    {
        return middlename;
    }

    public void setMiddlename (String middlename)
    {
        this.middlename = middlename;
    }

    public String getZipcode ()
    {
        return zipcode;
    }

    public void setZipcode (String zipcode)
    {
        this.zipcode = zipcode;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getSuffix ()
    {
        return suffix;
    }

    public void setSuffix (String suffix)
    {
        this.suffix = suffix;
    }

    public String getPhoto ()
    {
        return photo;
    }

    public void setPhoto (String photo)
    {
        this.photo = photo;
    }

    public String getCounty ()
    {
        return county;
    }

    public void setCounty (String county)
    {
        this.county = county;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getConvictiontype ()
    {
        return convictiontype;
    }

    public void setConvictiontype (String convictiontype)
    {
        this.convictiontype = convictiontype;
    }

    public String[] getOtheraddresses ()
    {
        return otheraddresses;
    }

    public void setOtheraddresses (String[] otheraddresses)
    {
        this.otheraddresses = otheraddresses;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String[] getBirthdates ()
    {
        return birthdates;
    }

    public void setBirthdates (String[] birthdates)
    {
        this.birthdates = birthdates;
    }

    public String getOffenderid ()
    {
        return offenderid;
    }

    public void setOffenderid (String offenderid)
    {
        this.offenderid = offenderid;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [possiblenicknames = "+possiblenicknames+", sex = "+sex+", matchtype = "+matchtype+", weight = "+weight+", state = "+state+", lastname = "+lastname+", aliases = "+aliases+", race = "+race+", city = "+city+", height = "+height+", age = "+age+", name = "+name+", longitude = "+longitude+", convictions = "+convictions+", hair = "+hair+", markings = "+markings+", eye = "+eye+", street2 = "+street2+", street1 = "+street1+", middlename = "+middlename+", zipcode = "+zipcode+", firstname = "+firstname+", suffix = "+suffix+", photo = "+photo+", county = "+county+", dob = "+dob+", convictiontype = "+convictiontype+", otheraddresses = "+otheraddresses+", latitude = "+latitude+", birthdates = "+birthdates+", offenderid = "+offenderid+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(possiblenicknames);
        parcel.writeString(sex);
        parcel.writeString(matchtype);
        parcel.writeString(weight);
        parcel.writeString(state);
        parcel.writeString(lastname);
        parcel.writeTypedArray(aliases, i);
        parcel.writeString(race);
        parcel.writeString(city);
        parcel.writeString(height);
        parcel.writeString(age);
        parcel.writeString(name);
        parcel.writeString(longitude);
        parcel.writeTypedArray(convictions, i);
        parcel.writeString(hair);
        parcel.writeString(eye);
        parcel.writeString(street2);
        parcel.writeString(street1);
        parcel.writeString(middlename);
        parcel.writeString(zipcode);
        parcel.writeString(firstname);
        parcel.writeString(suffix);
        parcel.writeString(photo);
        parcel.writeString(county);
        parcel.writeString(dob);
        parcel.writeString(convictiontype);
        parcel.writeStringArray(otheraddresses);
        parcel.writeString(latitude);
        parcel.writeStringArray(birthdates);
        parcel.writeString(offenderid);
    }
}
