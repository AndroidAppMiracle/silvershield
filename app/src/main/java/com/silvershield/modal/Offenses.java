package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class Offenses {

    private String Category;

    private String Description;

    private String ChargeFilingDate;

    private String Disposition;

    private String CaseNumber;

    public String getCategory ()
    {
        return Category;
    }

    public void setCategory (String Category)
    {
        this.Category = Category;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getChargeFilingDate ()
    {
        return ChargeFilingDate;
    }

    public void setChargeFilingDate (String ChargeFilingDate)
    {
        this.ChargeFilingDate = ChargeFilingDate;
    }

    public String getDisposition ()
    {
        return Disposition;
    }

    public void setDisposition (String Disposition)
    {
        this.Disposition = Disposition;
    }

    public String getCaseNumber ()
    {
        return CaseNumber;
    }

    public void setCaseNumber (String CaseNumber)
    {
        this.CaseNumber = CaseNumber;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Category = "+Category+", Description = "+Description+", ChargeFilingDate = "+ChargeFilingDate+", Disposition = "+Disposition+", CaseNumber = "+CaseNumber+"]";
    }
}
