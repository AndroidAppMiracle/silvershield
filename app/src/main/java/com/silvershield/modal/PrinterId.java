package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/19/2017.
 */

public class PrinterId implements Parcelable {
    private String name;

    private String ip;

    protected PrinterId(Parcel in) {
        name = in.readString();
        ip = in.readString();
    }

    public static final Creator<PrinterId> CREATOR = new Creator<PrinterId>() {
        @Override
        public PrinterId createFromParcel(Parcel in) {
            return new PrinterId(in);
        }

        @Override
        public PrinterId[] newArray(int size) {
            return new PrinterId[size];
        }
    };

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getIp ()
    {
        return ip;
    }

    public void setIp (String ip)
    {
        this.ip = ip;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", ip = "+ip+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(ip);
    }
}
