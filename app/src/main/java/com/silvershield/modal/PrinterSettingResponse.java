package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 12/19/2017.
 */

public class PrinterSettingResponse implements Parcelable{
    private String status;

    private List<PrinterId> printerId;

    private String printerType;

    protected PrinterSettingResponse(Parcel in) {
        status = in.readString();
        printerId = in.createTypedArrayList(PrinterId.CREATOR);
        printerType = in.readString();
    }

    public static final Creator<PrinterSettingResponse> CREATOR = new Creator<PrinterSettingResponse>() {
        @Override
        public PrinterSettingResponse createFromParcel(Parcel in) {
            return new PrinterSettingResponse(in);
        }

        @Override
        public PrinterSettingResponse[] newArray(int size) {
            return new PrinterSettingResponse[size];
        }
    };

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<PrinterId> getPrinterId ()
    {
        return printerId;
    }

    public void setPrinterId (List<PrinterId> printerId)
    {
        this.printerId = printerId;
    }

    public String getPrinterType ()
    {
        return printerType;
    }

    public void setPrinterType (String printerType)
    {
        this.printerType = printerType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", printerId = "+printerId+", printerType = "+printerType+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(status);
        parcel.writeTypedList(printerId);
        parcel.writeString(printerType);
    }
}
