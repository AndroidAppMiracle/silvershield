package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class Purpose {

    private String id;

    private String visit_purpose;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getVisit_purpose ()
    {
        return visit_purpose;
    }

    public void setVisit_purpose (String visit_purpose)
    {
        this.visit_purpose = visit_purpose;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", visit_purpose = "+visit_purpose+"]";
    }

}
