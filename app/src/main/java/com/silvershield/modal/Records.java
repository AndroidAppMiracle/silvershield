package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class Records implements Parcelable {
    private List<Aliases> Aliases;

    private List<Addresses> Addresses;

    private String RecordId;

    private String DOB;

    private String Age;

    private String Race;

    private String Height;

    private String LastName;

    private String Photo;

    private List<CriminalOfferses> Offenses;

    private String EyeColor;

    private String Category;

    private String Weight;

    private String SourceorJurisdiction;

    private String FullName;

    private String FirstName;

    private String Score;

    private String Sex;

    protected Records(Parcel in) {
        Aliases = in.createTypedArrayList(com.silvershield.modal.Aliases.CREATOR);
        Addresses = in.createTypedArrayList(com.silvershield.modal.Addresses.CREATOR);
        RecordId = in.readString();
        DOB = in.readString();
        Age = in.readString();
        Race = in.readString();
        Height = in.readString();
        LastName = in.readString();
        Photo = in.readString();
        Offenses = in.createTypedArrayList(CriminalOfferses.CREATOR);
        EyeColor = in.readString();
        Category = in.readString();
        Weight = in.readString();
        SourceorJurisdiction = in.readString();
        FullName = in.readString();
        FirstName = in.readString();
        Score = in.readString();
        Sex = in.readString();
    }

    public static final Creator<Records> CREATOR = new Creator<Records>() {
        @Override
        public Records createFromParcel(Parcel in) {
            return new Records(in);
        }

        @Override
        public Records[] newArray(int size) {
            return new Records[size];
        }
    };

    public List<Aliases> getAliases ()
    {
        return Aliases;
    }

    public void setAliases (List<Aliases> Aliases)
    {
        this.Aliases = Aliases;
    }

    public List<Addresses> getAddresses ()
    {
        return Addresses;
    }

    public void setAddresses (List<Addresses> Addresses)
    {
        this.Addresses = Addresses;
    }

    public String getRecordId ()
    {
        return RecordId;
    }

    public void setRecordId (String RecordId)
    {
        this.RecordId = RecordId;
    }

    public String getDOB ()
    {
        return DOB;
    }

    public void setDOB (String DOB)
    {
        this.DOB = DOB;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getRace ()
    {
        return Race;
    }

    public void setRace (String Race)
    {
        this.Race = Race;
    }

    public String getHeight ()
    {
        return Height;
    }

    public void setHeight (String Height)
    {
        this.Height = Height;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getPhoto ()
    {
        return Photo;
    }

    public void setPhoto (String Photo)
    {
        this.Photo = Photo;
    }

    public List<CriminalOfferses>  getOffenses ()
    {
        return Offenses;
    }

    public void setOffenses (List<CriminalOfferses>  Offenses)
    {
        this.Offenses = Offenses;
    }

    public String getEyeColor ()
    {
        return EyeColor;
    }

    public void setEyeColor (String EyeColor)
    {
        this.EyeColor = EyeColor;
    }

    public String getCategory ()
    {
        return Category;
    }

    public void setCategory (String Category)
    {
        this.Category = Category;
    }

    public String getWeight ()
    {
        return Weight;
    }

    public void setWeight (String Weight)
    {
        this.Weight = Weight;
    }

    public String getSourceorJurisdiction ()
    {
        return SourceorJurisdiction;
    }

    public void setSourceorJurisdiction (String SourceorJurisdiction)
    {
        this.SourceorJurisdiction = SourceorJurisdiction;
    }

    public String getFullName ()
    {
        return FullName;
    }

    public void setFullName (String FullName)
    {
        this.FullName = FullName;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getScore ()
    {
        return Score;
    }

    public void setScore (String Score)
    {
        this.Score = Score;
    }

    public String getSex ()
    {
        return Sex;
    }

    public void setSex (String Sex)
    {
        this.Sex = Sex;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Aliases = "+Aliases+", Addresses = "+Addresses+", RecordId = "+RecordId+", DOB = "+DOB+", Race = "+Race+", Height = "+Height+", LastName = "+LastName+", Photo = "+Photo+", Offenses = "+Offenses+", EyeColor = "+EyeColor+", Category = "+Category+", Weight = "+Weight+", SourceorJurisdiction = "+SourceorJurisdiction+", FullName = "+FullName+", FirstName = "+FirstName+", Score = "+Score+", Sex = "+Sex+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(Aliases);
        parcel.writeTypedList(Addresses);
        parcel.writeString(RecordId);
        parcel.writeString(DOB);
        parcel.writeString(Age);
        parcel.writeString(Race);
        parcel.writeString(Height);
        parcel.writeString(LastName);
        parcel.writeString(Photo);
        parcel.writeTypedList(Offenses);
        parcel.writeString(EyeColor);
        parcel.writeString(Category);
        parcel.writeString(Weight);
        parcel.writeString(SourceorJurisdiction);
        parcel.writeString(FullName);
        parcel.writeString(FirstName);
        parcel.writeString(Score);
        parcel.writeString(Sex);
    }
}
