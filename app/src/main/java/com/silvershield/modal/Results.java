package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 12/13/2017.
 */

public class Results implements Parcelable{
    private List<Records>  Records;

    private String Time;

    private String Product;

    private String Message;

    private String grandTotalRecords;

    private Inputs Inputs;

    private String TotalRecords;

    protected Results(Parcel in) {
        Time = in.readString();
        Product = in.readString();
        Message = in.readString();
        grandTotalRecords = in.readString();
        TotalRecords = in.readString();
    }

    public static final Creator<Results> CREATOR = new Creator<Results>() {
        @Override
        public Results createFromParcel(Parcel in) {
            return new Results(in);
        }

        @Override
        public Results[] newArray(int size) {
            return new Results[size];
        }
    };

    public List<Records> getRecords ()
    {
        return Records;
    }

    public void setRecords ( List<Records>  Records)
    {
        this.Records = Records;
    }

    public String getTime ()
    {
        return Time;
    }

    public void setTime (String Time)
    {
        this.Time = Time;
    }

    public String getProduct ()
    {
        return Product;
    }

    public void setProduct (String Product)
    {
        this.Product = Product;
    }

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public String getGrandTotalRecords ()
    {
        return grandTotalRecords;
    }

    public void setGrandTotalRecords (String grandTotalRecords)
    {
        this.grandTotalRecords = grandTotalRecords;
    }

    public Inputs getInputs ()
    {
        return Inputs;
    }

    public void setInputs (Inputs Inputs)
    {
        this.Inputs = Inputs;
    }

    public String getTotalRecords ()
    {
        return TotalRecords;
    }

    public void setTotalRecords (String TotalRecords)
    {
        this.TotalRecords = TotalRecords;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Records = "+Records+", Time = "+Time+", Product = "+Product+", Message = "+Message+", grandTotalRecords = "+grandTotalRecords+", Inputs = "+Inputs+", TotalRecords = "+TotalRecords+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Time);
        parcel.writeString(Product);
        parcel.writeString(Message);
        parcel.writeString(grandTotalRecords);
        parcel.writeString(TotalRecords);
    }
}
