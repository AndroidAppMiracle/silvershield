package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 12/12/2017.
 */

public class SOData implements Parcelable {
    private String searchtype;

    private String status;

    private String vers;

    private List<Offenders> offenders;

    protected SOData(Parcel in) {
        searchtype = in.readString();
        status = in.readString();
        vers = in.readString();
        offenders = in.createTypedArrayList(Offenders.CREATOR);
    }

    public static final Creator<SOData> CREATOR = new Creator<SOData>() {
        @Override
        public SOData createFromParcel(Parcel in) {
            return new SOData(in);
        }

        @Override
        public SOData[] newArray(int size) {
            return new SOData[size];
        }
    };

    public String getSearchtype ()
    {
        return searchtype;
    }

    public void setSearchtype (String searchtype)
    {
        this.searchtype = searchtype;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getVers ()
    {
        return vers;
    }

    public void setVers (String vers)
    {
        this.vers = vers;
    }

    public List<Offenders> getOffenders ()
    {
        return offenders;
    }

    public void setOffenders (List<Offenders> offenders)
    {
        this.offenders = offenders;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [searchtype = "+searchtype+", status = "+status+", vers = "+vers+", offenders = "+offenders+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(searchtype);
        parcel.writeString(status);
        parcel.writeString(vers);
        parcel.writeTypedList(offenders);
    }
}

