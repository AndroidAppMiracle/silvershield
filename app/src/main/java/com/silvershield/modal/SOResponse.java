package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/12/2017.
 */

public class SOResponse implements Parcelable {
    private String status;

    private SOData data;

    protected SOResponse(Parcel in) {
        status = in.readString();
        data = in.readParcelable(SOData.class.getClassLoader());
    }

    public static final Creator<SOResponse> CREATOR = new Creator<SOResponse>() {
        @Override
        public SOResponse createFromParcel(Parcel in) {
            return new SOResponse(in);
        }

        @Override
        public SOResponse[] newArray(int size) {
            return new SOResponse[size];
        }
    };

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public SOData getSOData ()
    {
        return data;
    }

    public void setSOData (SOData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(status);
        parcel.writeParcelable(data, i);
    }
}
