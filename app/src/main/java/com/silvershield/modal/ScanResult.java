package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/4/2017.
 */

public class ScanResult {
    private String status;
    private String message;
    private ScanUserDetail user;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ScanUserDetail getScanUserDetail ()
    {
        return user;
    }

    public void setScanUserDetail (ScanUserDetail user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", user = "+user+"]";
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
