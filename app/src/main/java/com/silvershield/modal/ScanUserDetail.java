package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/4/2017.
 */

public class ScanUserDetail implements Parcelable {

    private String visiting_department_id;

    private String visiting_individual;

    private String phone;

    private String visitor_suffix;

    private String is_vip;

    private String bar_code_image;

    private String entered_by;

    private String ssn;

    private String entered_by_lastname;

    private String country_name;

    private String vms_visitor_entering_from_id;

    private String id;

    private String visit_purpose;

    private String first_name;

    private String profile_pic_web;

    private String visiting_individual_firstname;

    private String is_watch;

    private String visiting_individual_id;

    private String updated_by;

    private String profile_pic;

    private String department_name;

    private String log_id;

    private String parent_id;

    private String visiting_individual_lastname;

    private String visitor_id_document_type;

    private String is_qb;

    private String middle_name;

    private String entered_by_firstname;

    private String country_id;

    private String visitor_id_document_types_id;

    private String check_in;

    private String check_out;

    private String country_code;

    private String is_deleted;

    private String watch_list_reason;

    private String email;

    private String dob;

    private String last_name;

    private String visitor_id_number;

    private String purpose_other;

    private String entery_place;

    protected ScanUserDetail(Parcel in) {
        visiting_department_id = in.readString();
        visiting_individual = in.readString();
        phone = in.readString();
        visitor_suffix = in.readString();
        is_vip = in.readString();
        bar_code_image = in.readString();
        entered_by = in.readString();
        ssn = in.readString();
        entered_by_lastname = in.readString();
        country_name = in.readString();
        vms_visitor_entering_from_id = in.readString();
        id = in.readString();
        visit_purpose = in.readString();
        first_name = in.readString();
        profile_pic_web = in.readString();
        visiting_individual_firstname = in.readString();
        is_watch = in.readString();
        visiting_individual_id = in.readString();
        updated_by = in.readString();
        profile_pic = in.readString();
        department_name = in.readString();
        log_id = in.readString();
        parent_id = in.readString();
        visiting_individual_lastname = in.readString();
        visitor_id_document_type = in.readString();
        is_qb = in.readString();
        middle_name = in.readString();
        entered_by_firstname = in.readString();
        country_id = in.readString();
        visitor_id_document_types_id = in.readString();
        check_in = in.readString();
        check_out = in.readString();
        country_code = in.readString();
        is_deleted = in.readString();
        watch_list_reason = in.readString();
        email = in.readString();
        dob = in.readString();
        last_name = in.readString();
        visitor_id_number = in.readString();
        purpose_other = in.readString();
        entery_place = in.readString();
    }

    public static final Creator<ScanUserDetail> CREATOR = new Creator<ScanUserDetail>() {
        @Override
        public ScanUserDetail createFromParcel(Parcel in) {
            return new ScanUserDetail(in);
        }

        @Override
        public ScanUserDetail[] newArray(int size) {
            return new ScanUserDetail[size];
        }
    };

    public String getVisiting_department_id ()
    {
        return visiting_department_id;
    }

    public void setVisiting_department_id (String visiting_department_id)
    {
        this.visiting_department_id = visiting_department_id;
    }

    public String getVisiting_individual ()
    {
        return visiting_individual;
    }

    public void setVisiting_individual (String visiting_individual)
    {
        this.visiting_individual = visiting_individual;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getVisitor_suffix ()
    {
        return visitor_suffix;
    }

    public void setVisitor_suffix (String visitor_suffix)
    {
        this.visitor_suffix = visitor_suffix;
    }

    public String getIs_vip ()
    {
        return is_vip;
    }

    public void setIs_vip (String is_vip)
    {
        this.is_vip = is_vip;
    }

    public String getBar_code_image ()
    {
        return bar_code_image;
    }

    public void setBar_code_image (String bar_code_image)
    {
        this.bar_code_image = bar_code_image;
    }

    public String getEntered_by ()
    {
        return entered_by;
    }

    public void setEntered_by (String entered_by)
    {
        this.entered_by = entered_by;
    }

    public String getSsn ()
    {
        return ssn;
    }

    public void setSsn (String ssn)
    {
        this.ssn = ssn;
    }

    public String getEntered_by_lastname ()
    {
        return entered_by_lastname;
    }

    public void setEntered_by_lastname (String entered_by_lastname)
    {
        this.entered_by_lastname = entered_by_lastname;
    }

    public String getCountry_name ()
    {
        return country_name;
    }

    public void setCountry_name (String country_name)
    {
        this.country_name = country_name;
    }

    public String getVms_visitor_entering_from_id ()
    {
        return vms_visitor_entering_from_id;
    }

    public void setVms_visitor_entering_from_id (String vms_visitor_entering_from_id)
    {
        this.vms_visitor_entering_from_id = vms_visitor_entering_from_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getVisit_purpose ()
    {
        return visit_purpose;
    }

    public void setVisit_purpose (String visit_purpose)
    {
        this.visit_purpose = visit_purpose;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getProfile_pic_web ()
    {
        return profile_pic_web;
    }

    public void setProfile_pic_web (String profile_pic_web)
    {
        this.profile_pic_web = profile_pic_web;
    }

    public String getVisiting_individual_firstname ()
    {
        return visiting_individual_firstname;
    }

    public void setVisiting_individual_firstname (String visiting_individual_firstname)
    {
        this.visiting_individual_firstname = visiting_individual_firstname;
    }

    public String getIs_watch ()
    {
        return is_watch;
    }

    public void setIs_watch (String is_watch)
    {
        this.is_watch = is_watch;
    }

    public String getVisiting_individual_id ()
    {
        return visiting_individual_id;
    }

    public void setVisiting_individual_id (String visiting_individual_id)
    {
        this.visiting_individual_id = visiting_individual_id;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public String getProfile_pic ()
    {
        return profile_pic;
    }

    public void setProfile_pic (String profile_pic)
    {
        this.profile_pic = profile_pic;
    }

    public String getDepartment_name ()
    {
        return department_name;
    }

    public void setDepartment_name (String department_name)
    {
        this.department_name = department_name;
    }

    public String getLog_id ()
    {
        return log_id;
    }

    public void setLog_id (String log_id)
    {
        this.log_id = log_id;
    }

    public String getParent_id ()
    {
        return parent_id;
    }

    public void setParent_id (String parent_id)
    {
        this.parent_id = parent_id;
    }

    public String getVisiting_individual_lastname ()
    {
        return visiting_individual_lastname;
    }

    public void setVisiting_individual_lastname (String visiting_individual_lastname)
    {
        this.visiting_individual_lastname = visiting_individual_lastname;
    }

    public String getVisitor_id_document_type ()
    {
        return visitor_id_document_type;
    }

    public void setVisitor_id_document_type (String visitor_id_document_type)
    {
        this.visitor_id_document_type = visitor_id_document_type;
    }

    public String getIs_qb ()
    {
        return is_qb;
    }

    public void setIs_qb (String is_qb)
    {
        this.is_qb = is_qb;
    }

    public String getMiddle_name ()
    {
        return middle_name;
    }

    public void setMiddle_name (String middle_name)
    {
        this.middle_name = middle_name;
    }

    public String getEntered_by_firstname ()
    {
        return entered_by_firstname;
    }

    public void setEntered_by_firstname (String entered_by_firstname)
    {
        this.entered_by_firstname = entered_by_firstname;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    public String getVisitor_id_document_types_id ()
    {
        return visitor_id_document_types_id;
    }

    public void setVisitor_id_document_types_id (String visitor_id_document_types_id)
    {
        this.visitor_id_document_types_id = visitor_id_document_types_id;
    }

    public String getCheck_in ()
    {
        return check_in;
    }

    public void setCheck_in (String check_in)
    {
        this.check_in = check_in;
    }

    public String getCheck_out ()
    {
        return check_out;
    }

    public void setCheck_out (String check_out)
    {
        this.check_out = check_out;
    }

    public String getCountry_code ()
    {
        return country_code;
    }

    public void setCountry_code (String country_code)
    {
        this.country_code = country_code;
    }

    public String getIs_deleted ()
    {
        return is_deleted;
    }

    public void setIs_deleted (String is_deleted)
    {
        this.is_deleted = is_deleted;
    }

    public String getWatch_list_reason ()
    {
        return watch_list_reason;
    }

    public void setWatch_list_reason (String watch_list_reason)
    {
        this.watch_list_reason = watch_list_reason;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getVisitor_id_number ()
    {
        return visitor_id_number;
    }

    public void setVisitor_id_number (String visitor_id_number)
    {
        this.visitor_id_number = visitor_id_number;
    }

    public String getPurpose_other ()
    {
        return purpose_other;
    }

    public void setPurpose_other (String purpose_other)
    {
        this.purpose_other = purpose_other;
    }

    public String getEntery_place ()
    {
        return entery_place;
    }

    public void setEntery_place (String entery_place)
    {
        this.entery_place = entery_place;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [visiting_department_id = "+visiting_department_id+", visiting_individual = "+visiting_individual+", phone = "+phone+", visitor_suffix = "+visitor_suffix+", is_vip = "+is_vip+", bar_code_image = "+bar_code_image+", entered_by = "+entered_by+", ssn = "+ssn+", entered_by_lastname = "+entered_by_lastname+", country_name = "+country_name+", vms_visitor_entering_from_id = "+vms_visitor_entering_from_id+", id = "+id+", visit_purpose = "+visit_purpose+", first_name = "+first_name+", profile_pic_web = "+profile_pic_web+", visiting_individual_firstname = "+visiting_individual_firstname+", is_watch = "+is_watch+", visiting_individual_id = "+visiting_individual_id+", updated_by = "+updated_by+", profile_pic = "+profile_pic+", department_name = "+department_name+", log_id = "+log_id+", parent_id = "+parent_id+", visiting_individual_lastname = "+visiting_individual_lastname+", visitor_id_document_type = "+visitor_id_document_type+", is_qb = "+is_qb+", middle_name = "+middle_name+", entered_by_firstname = "+entered_by_firstname+", country_id = "+country_id+", visitor_id_document_types_id = "+visitor_id_document_types_id+", check_in = "+check_in+", check_out = "+check_out+", country_code = "+country_code+", is_deleted = "+is_deleted+", watch_list_reason = "+watch_list_reason+", email = "+email+", dob = "+dob+", last_name = "+last_name+", visitor_id_number = "+visitor_id_number+", purpose_other = "+purpose_other+", entery_place = "+entery_place+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(visiting_department_id);
        parcel.writeString(visiting_individual);
        parcel.writeString(phone);
        parcel.writeString(visitor_suffix);
        parcel.writeString(is_vip);
        parcel.writeString(bar_code_image);
        parcel.writeString(entered_by);
        parcel.writeString(ssn);
        parcel.writeString(entered_by_lastname);
        parcel.writeString(country_name);
        parcel.writeString(vms_visitor_entering_from_id);
        parcel.writeString(id);
        parcel.writeString(visit_purpose);
        parcel.writeString(first_name);
        parcel.writeString(profile_pic_web);
        parcel.writeString(visiting_individual_firstname);
        parcel.writeString(is_watch);
        parcel.writeString(visiting_individual_id);
        parcel.writeString(updated_by);
        parcel.writeString(profile_pic);
        parcel.writeString(department_name);
        parcel.writeString(log_id);
        parcel.writeString(parent_id);
        parcel.writeString(visiting_individual_lastname);
        parcel.writeString(visitor_id_document_type);
        parcel.writeString(is_qb);
        parcel.writeString(middle_name);
        parcel.writeString(entered_by_firstname);
        parcel.writeString(country_id);
        parcel.writeString(visitor_id_document_types_id);
        parcel.writeString(check_in);
        parcel.writeString(check_out);
        parcel.writeString(country_code);
        parcel.writeString(is_deleted);
        parcel.writeString(watch_list_reason);
        parcel.writeString(email);
        parcel.writeString(dob);
        parcel.writeString(last_name);
        parcel.writeString(visitor_id_number);
        parcel.writeString(purpose_other);
        parcel.writeString(entery_place);
    }
}
