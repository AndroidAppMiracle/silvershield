package com.silvershield.modal;

import java.util.List;

public class SettingMainResponse {

    private String status;

    private List<SettingsModel> settings;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SettingsModel> getSettings() {
        return settings;
    }

    public void setSettings(List<SettingsModel> settings) {
        this.settings = settings;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", settings = " + settings+ "]";
    }
}
