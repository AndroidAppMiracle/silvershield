package com.silvershield.modal;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class SettingsModel{
    private String title;

    private int flag;

    private int state;

    private String label;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [title = "+title+", flag = "+flag+", state = "+state+", label = "+label+"]";
    }
}
