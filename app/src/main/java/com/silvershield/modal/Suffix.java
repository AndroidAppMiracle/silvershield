package com.silvershield.modal;

/**
 * Created by satoti.garg on 12/1/2017.
 */

public class Suffix {
    private String id;

    private String suffix_name;

    private String suffix_title;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSuffix_name ()
    {
        return suffix_name;
    }

    public void setSuffix_name (String suffix_name)
    {
        this.suffix_name = suffix_name;
    }

    public String getSuffix_title ()
    {
        return suffix_title;
    }

    public void setSuffix_title (String suffix_title)
    {
        this.suffix_title = suffix_title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", suffix_name = "+suffix_name+", suffix_title = "+suffix_title+"]";
    }
}
