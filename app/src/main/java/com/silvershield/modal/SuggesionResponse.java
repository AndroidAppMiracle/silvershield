package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 12/7/2017.
 */

public class SuggesionResponse implements Parcelable{

    private String message;

    private List<Matches> matches;

    private String status;

    protected SuggesionResponse(Parcel in) {
        message = in.readString();
        status = in.readString();
    }

    public static final Creator<SuggesionResponse> CREATOR = new Creator<SuggesionResponse>() {
        @Override
        public SuggesionResponse createFromParcel(Parcel in) {
            return new SuggesionResponse(in);
        }

        @Override
        public SuggesionResponse[] newArray(int size) {
            return new SuggesionResponse[size];
        }
    };

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Matches> getMatches ()
    {
        return matches;
    }

    public void setMatches (List<Matches> matches)
    {
        this.matches = matches;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", matches = "+matches+", status = "+status+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(status);
    }
}
