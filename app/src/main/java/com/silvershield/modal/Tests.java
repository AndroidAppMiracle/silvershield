package com.silvershield.modal;

/**
 * Created by satoti.garg on 11/28/2017.
 */

public class Tests {
    private String attempt;

    private String id;

    private String quesCount;

    private String title;

    private Attempts attempts;

    private String min_mark;

    private String tag_name;

    private String url;

    public String getAttempt ()
    {
        return attempt;
    }

    public void setAttempt (String attempt)
    {
        this.attempt = attempt;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getQuesCount ()
    {
        return quesCount;
    }

    public void setQuesCount (String quesCount)
    {
        this.quesCount = quesCount;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public Attempts getAttempts ()
    {
        return attempts;
    }

    public void setAttempts (Attempts attempts)
    {
        this.attempts = attempts;
    }

    public String getMin_mark ()
    {
        return min_mark;
    }

    public void setMin_mark (String min_mark)
    {
        this.min_mark = min_mark;
    }

    public String getTag_name ()
    {
        return tag_name;
    }

    public void setTag_name (String tag_name)
    {
        this.tag_name = tag_name;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [attempt = "+attempt+", id = "+id+", quesCount = "+quesCount+", title = "+title+", attempts = "+attempts+", min_mark = "+min_mark+", tag_name = "+tag_name+", url = "+url+"]";
    }
}
