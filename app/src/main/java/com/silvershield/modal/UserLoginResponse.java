package com.silvershield.modal;

import java.util.List;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class UserLoginResponse {
    private Userdata userdata;

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    private String authtoken;
    private String status;

    private List<SettingsModel> settings;

    private String message;
    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }


    public Userdata getUserdata ()
    {
        return userdata;
    }

    public void setUserdata (Userdata userdata)
    {
        this.userdata = userdata;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<SettingsModel> getSettings() {
        return settings;
    }

    public void setSettings(List<SettingsModel> settings) {
        this.settings = settings;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [userdata = "+userdata+", status = "+status+", settings = "+settings+"]";
    }
}
