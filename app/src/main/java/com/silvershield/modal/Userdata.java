package com.silvershield.modal;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class Userdata {
    private String created_by;

    private String phone_number;

    private String status;

    private String department;

    private String department_id;

    private String first_name;

    private String is_deleted;

    private String username;

    private String timezone;

    private String email;

    private String clients_id;

    private String name;

    private String last_name;

    private String role;

    private String user_id;

    private String profile_pic;

    private String department_name;

    private String client_name;

    private String parent_id;

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public String getPhone_number ()
    {
        return phone_number;
    }

    public void setPhone_number (String phone_number)
    {
        this.phone_number = phone_number;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getDepartment ()
    {
        return department;
    }

    public void setDepartment (String department)
    {
        this.department = department;
    }

    public String getDepartment_id ()
    {
        return department_id;
    }

    public void setDepartment_id (String department_id)
    {
        this.department_id = department_id;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getIs_deleted ()
    {
        return is_deleted;
    }

    public void setIs_deleted (String is_deleted)
    {
        this.is_deleted = is_deleted;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getTimezone ()
    {
        return timezone;
    }

    public void setTimezone (String timezone)
    {
        this.timezone = timezone;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getClients_id ()
    {
        return clients_id;
    }

    public void setClients_id (String clients_id)
    {
        this.clients_id = clients_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getRole ()
    {
        return role;
    }

    public void setRole (String role)
    {
        this.role = role;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getProfile_pic ()
    {
        return profile_pic;
    }

    public void setProfile_pic (String profile_pic)
    {
        this.profile_pic = profile_pic;
    }

    public String getDepartment_name ()
    {
        return department_name;
    }

    public void setDepartment_name (String department_name)
    {
        this.department_name = department_name;
    }

    public String getClient_name ()
    {
        return client_name;
    }

    public void setClient_name (String client_name)
    {
        this.client_name = client_name;
    }

    public String getParent_id ()
    {
        return parent_id;
    }

    public void setParent_id (String parent_id)
    {
        this.parent_id = parent_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", phone_number = "+phone_number+", status = "+status+", department = "+department+", department_id = "+department_id+", first_name = "+first_name+", is_deleted = "+is_deleted+", username = "+username+", timezone = "+timezone+", email = "+email+", clients_id = "+clients_id+", name = "+name+", last_name = "+last_name+", role = "+role+", user_id = "+user_id+", profile_pic = "+profile_pic+", department_name = "+department_name+", client_name = "+client_name+", parent_id = "+parent_id+"]";
    }
}
