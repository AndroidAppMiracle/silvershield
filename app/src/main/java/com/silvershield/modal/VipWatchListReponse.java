package com.silvershield.modal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satoti.garg on 11/23/2017.
 */

public class VipWatchListReponse {
    private ArrayList<VipList> vip;

    private ArrayList<WatchList> watchlist;

    private String status;

    public ArrayList<VipList> getVipList ()
    {
        return vip;
    }

    public void setVipList (ArrayList<VipList> vip)
    {
        this.vip = vip;
    }

    public ArrayList<WatchList> getWatchList ()
    {
        return watchlist;
    }

    public void setWatchList (ArrayList<WatchList> watchlist)
    {
        this.watchlist = watchlist;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [vip = "+vip+", watchlist = "+watchlist+", status = "+status+"]";
    }
}
