package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 12/11/2017.
 */

public class WatchListCheck implements Parcelable {

    public static final Creator<WatchListCheck> CREATOR = new Creator<WatchListCheck>() {
        @Override
        public WatchListCheck createFromParcel(Parcel in) {
            return new WatchListCheck(in);
        }

        @Override
        public WatchListCheck[] newArray(int size) {
            return new WatchListCheck[size];
        }
    };
    private String message;
    private List<WatchListMatchModal> matches;
    private String status;

    protected WatchListCheck(Parcel in) {
        message = in.readString();
        matches = in.createTypedArrayList(WatchListMatchModal.CREATOR);
        status = in.readString();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WatchListMatchModal> getMatches() {
        return matches;
    }

    public void setMatches(List<WatchListMatchModal> matches) {
        this.matches = matches;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", matches = " + matches + ", status = " + status + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(message);
        parcel.writeTypedList(matches);
        parcel.writeString(status);
    }
}
