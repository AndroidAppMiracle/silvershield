package com.silvershield.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 12/11/2017.
 */

public class WatchListMatchModal implements Parcelable {

    public static final Creator<WatchListMatchModal> CREATOR = new Creator<WatchListMatchModal>() {
        @Override
        public WatchListMatchModal createFromParcel(Parcel in) {
            return new WatchListMatchModal(in);
        }

        @Override
        public WatchListMatchModal[] newArray(int size) {
            return new WatchListMatchModal[size];
        }
    };
    private String phone;
    private String visitor_suffix;
    private String quick_badge_image;
    private String is_vip;
    private String reason_viplist_id;
    private String bar_code_image;
    private String ssn;
    private String id;
    private String reason_watchlist_id;
    private String first_name;
    private String profile_pic_web;
    private String clients_id;
    private String is_watch;
    private String created_at;
    private String profile_pic;
    private String wl_reason;
    private String parent_id;
    private String reason_viplist_notes;
    private String created_by;
    private String is_qb;
    private String middle_name;
    private String country_id;
    private String schools_id;
    private String is_se;
    private String reason_watchlist_notes;
    private String visitor_id_type;
    private String updated_from;
    private String watch_list_reason;
    private String is_deleted;
    private String email;
    private String dob;
    private String last_name;
    private String visitor_id_number;
    private String modified_at;

    protected WatchListMatchModal(Parcel in) {
        phone = in.readString();
        visitor_suffix = in.readString();
        quick_badge_image = in.readString();
        is_vip = in.readString();
        reason_viplist_id = in.readString();
        bar_code_image = in.readString();
        ssn = in.readString();
        id = in.readString();
        reason_watchlist_id = in.readString();
        first_name = in.readString();
        profile_pic_web = in.readString();
        clients_id = in.readString();
        is_watch = in.readString();
        created_at = in.readString();
        profile_pic = in.readString();
        wl_reason = in.readString();
        parent_id = in.readString();
        reason_viplist_notes = in.readString();
        created_by = in.readString();
        is_qb = in.readString();
        middle_name = in.readString();
        country_id = in.readString();
        schools_id = in.readString();
        is_se = in.readString();
        reason_watchlist_notes = in.readString();
        visitor_id_type = in.readString();
        updated_from = in.readString();
        watch_list_reason = in.readString();
        is_deleted = in.readString();
        email = in.readString();
        dob = in.readString();
        last_name = in.readString();
        visitor_id_number = in.readString();
        modified_at = in.readString();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVisitor_suffix() {
        return visitor_suffix;
    }

    public void setVisitor_suffix(String visitor_suffix) {
        this.visitor_suffix = visitor_suffix;
    }

    public String getQuick_badge_image() {
        return quick_badge_image;
    }

    public void setQuick_badge_image(String quick_badge_image) {
        this.quick_badge_image = quick_badge_image;
    }

    public String getIs_vip() {
        return is_vip;
    }

    public void setIs_vip(String is_vip) {
        this.is_vip = is_vip;
    }

    public String getReason_viplist_id() {
        return reason_viplist_id;
    }

    public void setReason_viplist_id(String reason_viplist_id) {
        this.reason_viplist_id = reason_viplist_id;
    }

    public String getBar_code_image() {
        return bar_code_image;
    }

    public void setBar_code_image(String bar_code_image) {
        this.bar_code_image = bar_code_image;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason_watchlist_id() {
        return reason_watchlist_id;
    }

    public void setReason_watchlist_id(String reason_watchlist_id) {
        this.reason_watchlist_id = reason_watchlist_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getProfile_pic_web() {
        return profile_pic_web;
    }

    public void setProfile_pic_web(String profile_pic_web) {
        this.profile_pic_web = profile_pic_web;
    }

    public String getClients_id() {
        return clients_id;
    }

    public void setClients_id(String clients_id) {
        this.clients_id = clients_id;
    }

    public String getIs_watch() {
        return is_watch;
    }

    public void setIs_watch(String is_watch) {
        this.is_watch = is_watch;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getWl_reason() {
        return wl_reason;
    }

    public void setWl_reason(String wl_reason) {
        this.wl_reason = wl_reason;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getReason_viplist_notes() {
        return reason_viplist_notes;
    }

    public void setReason_viplist_notes(String reason_viplist_notes) {
        this.reason_viplist_notes = reason_viplist_notes;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getIs_qb() {
        return is_qb;
    }

    public void setIs_qb(String is_qb) {
        this.is_qb = is_qb;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getSchools_id() {
        return schools_id;
    }

    public void setSchools_id(String schools_id) {
        this.schools_id = schools_id;
    }

    public String getIs_se() {
        return is_se;
    }

    public void setIs_se(String is_se) {
        this.is_se = is_se;
    }

    public String getReason_watchlist_notes() {
        return reason_watchlist_notes;
    }

    public void setReason_watchlist_notes(String reason_watchlist_notes) {
        this.reason_watchlist_notes = reason_watchlist_notes;
    }

    public String getVisitor_id_type() {
        return visitor_id_type;
    }

    public void setVisitor_id_type(String visitor_id_type) {
        this.visitor_id_type = visitor_id_type;
    }

    public String getUpdated_from() {
        return updated_from;
    }

    public void setUpdated_from(String updated_from) {
        this.updated_from = updated_from;
    }

    public String getWatch_list_reason() {
        return watch_list_reason;
    }

    public void setWatch_list_reason(String watch_list_reason) {
        this.watch_list_reason = watch_list_reason;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getVisitor_id_number() {
        return visitor_id_number;
    }

    public void setVisitor_id_number(String visitor_id_number) {
        this.visitor_id_number = visitor_id_number;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    @Override
    public String toString() {
        return "ClassPojo [phone = " + phone + ", visitor_suffix = " + visitor_suffix + ", quick_badge_image = " + quick_badge_image + ", is_vip = " + is_vip + ", reason_viplist_id = " + reason_viplist_id + ", bar_code_image = " + bar_code_image + ", ssn = " + ssn + ", id = " + id + ", reason_watchlist_id = " + reason_watchlist_id + ", first_name = " + first_name + ", profile_pic_web = " + profile_pic_web + ", clients_id = " + clients_id + ", is_watch = " + is_watch + ", created_at = " + created_at + ", profile_pic = " + profile_pic + ", wl_reason = " + wl_reason + ", parent_id = " + parent_id + ", reason_viplist_notes = " + reason_viplist_notes + ", created_by = " + created_by + ", is_qb = " + is_qb + ", middle_name = " + middle_name + ", country_id = " + country_id + ", schools_id = " + schools_id + ", is_se = " + is_se + ", reason_watchlist_notes = " + reason_watchlist_notes + ", visitor_id_type = " + visitor_id_type + ", updated_from = " + updated_from + ", watch_list_reason = " + watch_list_reason + ", is_deleted = " + is_deleted + ", email = " + email + ", dob = " + dob + ", last_name = " + last_name + ", visitor_id_number = " + visitor_id_number + ", modified_at = " + modified_at + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(phone);
        parcel.writeString(visitor_suffix);
        parcel.writeString(quick_badge_image);
        parcel.writeString(is_vip);
        parcel.writeString(reason_viplist_id);
        parcel.writeString(bar_code_image);
        parcel.writeString(ssn);
        parcel.writeString(id);
        parcel.writeString(reason_watchlist_id);
        parcel.writeString(first_name);
        parcel.writeString(profile_pic_web);
        parcel.writeString(clients_id);
        parcel.writeString(is_watch);
        parcel.writeString(created_at);
        parcel.writeString(profile_pic);
        parcel.writeString(wl_reason);
        parcel.writeString(parent_id);
        parcel.writeString(reason_viplist_notes);
        parcel.writeString(created_by);
        parcel.writeString(is_qb);
        parcel.writeString(middle_name);
        parcel.writeString(country_id);
        parcel.writeString(schools_id);
        parcel.writeString(is_se);
        parcel.writeString(reason_watchlist_notes);
        parcel.writeString(visitor_id_type);
        parcel.writeString(updated_from);
        parcel.writeString(watch_list_reason);
        parcel.writeString(is_deleted);
        parcel.writeString(email);
        parcel.writeString(dob);
        parcel.writeString(last_name);
        parcel.writeString(visitor_id_number);
        parcel.writeString(modified_at);
    }
}
