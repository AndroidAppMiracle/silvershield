package com.silvershield.modal.requestModels;

import java.io.Serializable;

/**
 * Created by gagandeep.bhutani on 3/19/2018.
 */

public class CreateVisitorRequestModel implements Serializable {
    private String loggedin_user, first_name, middle_name, last_name, suffix_name, email, phone, dob, created_by, parent_id, visitor_id_document_types_id, visitor_id_number, check_in, entered_by, visiting_individual_id, visiting_department_id;
    private String vms_visitor_entering_from_id, vms_visitor_purpose_id, vms_visitor_other_purpose, clients_id, visitor_title, sexOffenderCheckSetting, sexOffenderChecked, country_id, vipId, match_id;

    public String getLoggedin_user() {
        return loggedin_user;
    }

    public void setLoggedin_user(String loggedin_user) {
        this.loggedin_user = loggedin_user;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSuffix_name() {
        return suffix_name;
    }

    public void setSuffix_name(String suffix_name) {
        this.suffix_name = suffix_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getVisitor_id_document_types_id() {
        return visitor_id_document_types_id;
    }

    public void setVisitor_id_document_types_id(String visitor_id_document_types_id) {
        this.visitor_id_document_types_id = visitor_id_document_types_id;
    }

    public String getVisitor_id_number() {
        return visitor_id_number;
    }

    public void setVisitor_id_number(String visitor_id_number) {
        this.visitor_id_number = visitor_id_number;
    }

    public String getCheck_in() {
        return check_in;
    }

    public void setCheck_in(String check_in) {
        this.check_in = check_in;
    }

    public String getEntered_by() {
        return entered_by;
    }

    public void setEntered_by(String entered_by) {
        this.entered_by = entered_by;
    }

    public String getVisiting_individual_id() {
        return visiting_individual_id;
    }

    public void setVisiting_individual_id(String visiting_individual_id) {
        this.visiting_individual_id = visiting_individual_id;
    }

    public String getVisiting_department_id() {
        return visiting_department_id;
    }

    public void setVisiting_department_id(String visiting_department_id) {
        this.visiting_department_id = visiting_department_id;
    }

    public String getVms_visitor_entering_from_id() {
        return vms_visitor_entering_from_id;
    }

    public void setVms_visitor_entering_from_id(String vms_visitor_entering_from_id) {
        this.vms_visitor_entering_from_id = vms_visitor_entering_from_id;
    }

    public String getVms_visitor_purpose_id() {
        return vms_visitor_purpose_id;
    }

    public void setVms_visitor_purpose_id(String vms_visitor_purpose_id) {
        this.vms_visitor_purpose_id = vms_visitor_purpose_id;
    }

    public String getVms_visitor_other_purpose() {
        return vms_visitor_other_purpose;
    }

    public void setVms_visitor_other_purpose(String vms_visitor_other_purpose) {
        this.vms_visitor_other_purpose = vms_visitor_other_purpose;
    }

    public String getClients_id() {
        return clients_id;
    }

    public void setClients_id(String clients_id) {
        this.clients_id = clients_id;
    }

    public String getVisitor_title() {
        return visitor_title;
    }

    public void setVisitor_title(String visitor_title) {
        this.visitor_title = visitor_title;
    }

    public String getSexOffenderCheckSetting() {
        return sexOffenderCheckSetting;
    }

    public void setSexOffenderCheckSetting(String sexOffenderCheckSetting) {
        this.sexOffenderCheckSetting = sexOffenderCheckSetting;
    }

    public String getSexOffenderChecked() {
        return sexOffenderChecked;
    }

    public void setSexOffenderChecked(String sexOffenderChecked) {
        this.sexOffenderChecked = sexOffenderChecked;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getVipId() {
        return vipId;
    }

    public void setVipId(String vipId) {
        this.vipId = vipId;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }
}
