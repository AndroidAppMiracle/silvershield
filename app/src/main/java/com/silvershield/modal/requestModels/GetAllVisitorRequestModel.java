package com.silvershield.modal.requestModels;

import java.io.Serializable;

/**
 * Created by gagandeep.bhutani on 3/19/2018.
 */

public class GetAllVisitorRequestModel implements Serializable {
    private String user_id, parent_id, flag_all, clients_id, page_index, startTime, endTime, history_view_date;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getFlag_all() {
        return flag_all;
    }

    public void setFlag_all(String flag_all) {
        this.flag_all = flag_all;
    }

    public String getClients_id() {
        return clients_id;
    }

    public void setClients_id(String clients_id) {
        this.clients_id = clients_id;
    }

    public String getPage_index() {
        return page_index;
    }

    public void setPage_index(String page_index) {
        this.page_index = page_index;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHistory_view_date() {
        return history_view_date;
    }

    public void setHistory_view_date(String history_view_date) {
        this.history_view_date = history_view_date;
    }
}
