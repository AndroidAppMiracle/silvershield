package com.silvershield.modal.requestModels;

import java.io.Serializable;

/**
 * Created by gagandeep.bhutani on 3/19/2018.
 */

public class LogoutRequestModel implements Serializable {
    private String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
