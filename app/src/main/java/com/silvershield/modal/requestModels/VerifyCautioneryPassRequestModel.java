package com.silvershield.modal.requestModels;

import java.io.Serializable;

/**
 * Created by gagandeep.bhutani on 3/19/2018.
 */

public class VerifyCautioneryPassRequestModel implements Serializable {
    private String cautionary_code, code, dispatch_id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDispatch_id() {
        return dispatch_id;
    }

    public void setDispatch_id(String dispatch_id) {
        this.dispatch_id = dispatch_id;
    }

    public String getCautionary_code() {
        return cautionary_code;
    }

    public void setCautionary_code(String cautionary_code) {
        this.cautionary_code = cautionary_code;
    }
}
