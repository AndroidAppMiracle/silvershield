package com.silvershield.modal.responseModels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gagandeep.bhutani on 3/19/2018.
 */

public class GetAlertLogResponseModel implements Parcelable {
    private String sentByUserName, sent_by_firstname, pendingCount, sent_by_lastname, id, dispatch_id, message, alert_type, receiver_type, created_at, trigger_type, trigger_nature, message_type, location_lat, location_lng, policeCount, sent_by_name;
    private int count, acceptCount, rejectCount;

    protected GetAlertLogResponseModel(Parcel in) {
        sentByUserName = in.readString();
        sent_by_firstname = in.readString();
        pendingCount = in.readString();
        sent_by_lastname = in.readString();
        id = in.readString();
        dispatch_id = in.readString();
        message = in.readString();
        alert_type = in.readString();
        receiver_type = in.readString();
        created_at = in.readString();
        trigger_type = in.readString();
        trigger_nature = in.readString();
        message_type = in.readString();
        location_lat = in.readString();
        location_lng = in.readString();
        policeCount = in.readString();
        sent_by_name = in.readString();
        count = in.readInt();
        acceptCount = in.readInt();
        rejectCount = in.readInt();
    }

    public static final Creator<GetAlertLogResponseModel> CREATOR = new Creator<GetAlertLogResponseModel>() {
        @Override
        public GetAlertLogResponseModel createFromParcel(Parcel in) {
            return new GetAlertLogResponseModel(in);
        }

        @Override
        public GetAlertLogResponseModel[] newArray(int size) {
            return new GetAlertLogResponseModel[size];
        }
    };

    public String getSentByUserName() {
        return sentByUserName;
    }

    public void setSentByUserName(String sentByUserName) {
        this.sentByUserName = sentByUserName;
    }

    public String getSent_by_firstname() {
        return sent_by_firstname;
    }

    public void setSent_by_firstname(String sent_by_firstname) {
        this.sent_by_firstname = sent_by_firstname;
    }

    public String getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(String pendingCount) {
        this.pendingCount = pendingCount;
    }

    public String getSent_by_lastname() {
        return sent_by_lastname;
    }

    public void setSent_by_lastname(String sent_by_lastname) {
        this.sent_by_lastname = sent_by_lastname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDispatch_id() {
        return dispatch_id;
    }

    public void setDispatch_id(String dispatch_id) {
        this.dispatch_id = dispatch_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAlert_type() {
        return alert_type;
    }

    public void setAlert_type(String alert_type) {
        this.alert_type = alert_type;
    }

    public String getReceiver_type() {
        return receiver_type;
    }

    public void setReceiver_type(String receiver_type) {
        this.receiver_type = receiver_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTrigger_type() {
        return trigger_type;
    }

    public void setTrigger_type(String trigger_type) {
        this.trigger_type = trigger_type;
    }

    public String getTrigger_nature() {
        return trigger_nature;
    }

    public void setTrigger_nature(String trigger_nature) {
        this.trigger_nature = trigger_nature;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getLocation_lat() {
        return location_lat;
    }

    public void setLocation_lat(String location_lat) {
        this.location_lat = location_lat;
    }

    public String getLocation_lng() {
        return location_lng;
    }

    public void setLocation_lng(String location_lng) {
        this.location_lng = location_lng;
    }

    public String getPoliceCount() {
        return policeCount;
    }

    public void setPoliceCount(String policeCount) {
        this.policeCount = policeCount;
    }

    public String getSent_by_name() {
        return sent_by_name;
    }

    public void setSent_by_name(String sent_by_name) {
        this.sent_by_name = sent_by_name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getAcceptCount() {
        return acceptCount;
    }

    public void setAcceptCount(int acceptCount) {
        this.acceptCount = acceptCount;
    }

    public int getRejectCount() {
        return rejectCount;
    }

    public void setRejectCount(int rejectCount) {
        this.rejectCount = rejectCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sentByUserName);
        dest.writeString(sent_by_firstname);
        dest.writeString(pendingCount);
        dest.writeString(sent_by_lastname);
        dest.writeString(id);
        dest.writeString(dispatch_id);
        dest.writeString(message);
        dest.writeString(alert_type);
        dest.writeString(receiver_type);
        dest.writeString(created_at);
        dest.writeString(trigger_type);
        dest.writeString(trigger_nature);
        dest.writeString(message_type);
        dest.writeString(location_lat);
        dest.writeString(location_lng);
        dest.writeString(policeCount);
        dest.writeString(sent_by_name);
        dest.writeInt(count);
        dest.writeInt(acceptCount);
        dest.writeInt(rejectCount);
    }
}
