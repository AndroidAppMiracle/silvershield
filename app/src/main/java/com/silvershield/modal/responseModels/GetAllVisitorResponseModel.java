package com.silvershield.modal.responseModels;

import java.util.List;

public class GetAllVisitorResponseModel {

    private String status;
    private int endFlag;
    private List<VisitorResponseModel1> visitorsToday;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getEndFlag() {
        return endFlag;
    }

    public void setEndFlag(int endFlag) {
        this.endFlag = endFlag;
    }

    public List<VisitorResponseModel1> getVisitorsToday() {
        return visitorsToday;
    }

    public void setVisitorsToday(List<VisitorResponseModel1> visitorsToday) {
        this.visitorsToday = visitorsToday;
    }
}
