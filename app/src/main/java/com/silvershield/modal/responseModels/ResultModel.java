package com.silvershield.modal.responseModels;

/**
 * Created by gagandeep.bhutani on 3/19/2018.
 */

public class ResultModel<T> {

    public String status;
    public String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
