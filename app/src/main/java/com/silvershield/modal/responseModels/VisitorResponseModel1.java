package com.silvershield.modal.responseModels;

import java.io.Serializable;

public class VisitorResponseModel1 implements Serializable {
    private String watch_list_reason, entered_by, entered_by_firstname, bar_code_image, email, profile_pic, dob, visitor_suffix, check_out, check_in, ssn, first_name, middle_name, last_name, profile_pic_web, phone;
    private String entered_by_lastname, department_name, visit_purpose, visitor_id_document_type, visitor_id_number, vms_visitor_entering_from_id, entery_place, country_id, country_code, country_name;
    private int is_deleted, parent_id, log_id, is_qb, is_vip, is_watch, id, visitor_id_document_types_id;

    public String getWatch_list_reason() {
        return watch_list_reason;
    }

    public void setWatch_list_reason(String watch_list_reason) {
        this.watch_list_reason = watch_list_reason;
    }

    public String getEntered_by() {
        return entered_by;
    }

    public void setEntered_by(String entered_by) {
        this.entered_by = entered_by;
    }

    public String getEntered_by_firstname() {
        return entered_by_firstname;
    }

    public void setEntered_by_firstname(String entered_by_firstname) {
        this.entered_by_firstname = entered_by_firstname;
    }

    public String getBar_code_image() {
        return bar_code_image;
    }

    public void setBar_code_image(String bar_code_image) {
        this.bar_code_image = bar_code_image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getVisitor_suffix() {
        return visitor_suffix;
    }

    public void setVisitor_suffix(String visitor_suffix) {
        this.visitor_suffix = visitor_suffix;
    }

    public String getEntered_by_lastname() {
        return entered_by_lastname;
    }

    public void setEntered_by_lastname(String entered_by_lastname) {
        this.entered_by_lastname = entered_by_lastname;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public String getVisit_purpose() {
        return visit_purpose;
    }

    public void setVisit_purpose(String visit_purpose) {
        this.visit_purpose = visit_purpose;
    }

    public String getVisitor_id_document_type() {
        return visitor_id_document_type;
    }

    public void setVisitor_id_document_type(String visitor_id_document_type) {
        this.visitor_id_document_type = visitor_id_document_type;
    }

    public String getVisitor_id_number() {
        return visitor_id_number;
    }

    public void setVisitor_id_number(String visitor_id_number) {
        this.visitor_id_number = visitor_id_number;
    }

    public String getVms_visitor_entering_from_id() {
        return vms_visitor_entering_from_id;
    }

    public void setVms_visitor_entering_from_id(String vms_visitor_entering_from_id) {
        this.vms_visitor_entering_from_id = vms_visitor_entering_from_id;
    }

    public String getEntery_place() {
        return entery_place;
    }

    public void setEntery_place(String entery_place) {
        this.entery_place = entery_place;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getLog_id() {
        return log_id;
    }

    public void setLog_id(int log_id) {
        this.log_id = log_id;
    }

    public int getVisitor_id_document_types_id() {
        return visitor_id_document_types_id;
    }

    public void setVisitor_id_document_types_id(int visitor_id_document_types_id) {
        this.visitor_id_document_types_id = visitor_id_document_types_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCheck_out() {
        return check_out;
    }

    public void setCheck_out(String check_out) {
        this.check_out = check_out;
    }

    public String getCheck_in() {
        return check_in;
    }

    public void setCheck_in(String check_in) {
        this.check_in = check_in;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getProfile_pic_web() {
        return profile_pic_web;
    }

    public void setProfile_pic_web(String profile_pic_web) {
        this.profile_pic_web = profile_pic_web;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getIs_qb() {
        return is_qb;
    }

    public void setIs_qb(int is_qb) {
        this.is_qb = is_qb;
    }

    public int getIs_vip() {
        return is_vip;
    }

    public void setIs_vip(int is_vip) {
        this.is_vip = is_vip;
    }

    public int getIs_watch() {
        return is_watch;
    }

    public void setIs_watch(int is_watch) {
        this.is_watch = is_watch;
    }
}
