package com.silvershield.networkManager;

import com.google.gson.JsonObject;
import com.silvershield.modal.AlarmCheckModal;
import com.silvershield.modal.AssignToMeReponse;
import com.silvershield.modal.EventResponseObject;
import com.silvershield.modal.GetCategoryResponse;
import com.silvershield.modal.GetSuffixDoctypeResponse;
import com.silvershield.modal.PrinterSettingResponse;
import com.silvershield.modal.SOResponse;
import com.silvershield.modal.ScanResult;
import com.silvershield.modal.SettingMainResponse;
import com.silvershield.modal.StatusMessageModel;
import com.silvershield.modal.SuggesionResponse;
import com.silvershield.modal.UserLoginResponse;
import com.silvershield.modal.WatchListCheck;
import com.silvershield.modal.requestModels.CheckWatchListRequestModel;
import com.silvershield.modal.requestModels.ForgetPassRequestModel;
import com.silvershield.modal.requestModels.GetAllVisitorRequestModel;
import com.silvershield.modal.requestModels.LoginRequestModel;
import com.silvershield.modal.requestModels.LogoutRequestModel;
import com.silvershield.modal.requestModels.VerifyCautioneryPassRequestModel;
import com.silvershield.modal.responseModels.GetAllVisitorResponseModel;
import com.silvershield.modal.responseModels.ResultModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIReference {

    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);

    @POST("api/v1.0.1/login")
    Call<UserLoginResponse> login(@Body LoginRequestModel requestModel);

    @GET("api/v1.0.1/refreshToken")
    Call<JsonObject> refreshToken();

    @POST("api/v1.0.1/forgot_password")
    Call<ResultModel> forgetPassword(@Body ForgetPassRequestModel requestModel);

    @POST("api/v1.0.1/logout")
    Call<JsonObject> logoutUser(@Body LogoutRequestModel requestModel);

    @Multipart
    @POST("api/v1.0.1/vip_and_watch_list")
    Call<JsonObject> getVipWatcherList(/*@FieldMap Map<String, String> params*/@Header("x-access-token") String token, @Part("loggedin_user") RequestBody loggedin_user,
                                       @Part("parent_id") RequestBody parent_id, @Part("clients_id") RequestBody groupName
    );

    @Multipart
    @POST("api/v1.0.1/search_visitor")
    Call<JsonObject> getSearchedWatcherList(/*@FieldMap Map<String, String> params*/@Header("x-access-token") String token, @Part("loggedin_user") RequestBody loggedin_user,
                                            @Part("parent_id") RequestBody parent_id, @Part("clients_id") RequestBody groupName, @Part("search_string") RequestBody searchedSt
    );


    @FormUrlEncoded
    @POST("api/v1.0.1/police-check-permissions")
    Call<AlarmCheckModal> getNotificationFlag(@Header("x-access-token") String token, @FieldMap Map<String, String> params);
    /*@FormUrlEncoded
    @POST("api/page/invite-user" + "?")
    Call<GroupDeleteModal> inviteUserForPage(@Header("sessionkey") String header, @Query("id") String pageID, @FieldMap Map<String, String> params);
*/

    @GET("api/v1.0.1/categories/{clientId}")
    Call<GetCategoryResponse> getAllCateories(@Header("x-access-token") String token, @Path("clientId") String pageID);

    @Multipart
    @POST("api/v1.0.1/view_assigned_tests")
    Call<AssignToMeReponse> getCasesAssignedToMe(@Header("x-access-token") String token, @Part("user_id") RequestBody loggedin_user,
                                                 @Part("parent_id") RequestBody parent_id, @Part("clients_id") RequestBody groupName
    );

    /*@Multipart
    @POST("api/v1.0.1/get_staff_guard_settings")
    Call<SettingReponse> getAllSettings(@Header("x-access-token") String token, @Part("user_id") RequestBody loggedin_user);
*/

    @GET("api/v1.0.1/timezones")
    Call<JsonObject> getAllTimesZones(@Header("x-access-token") String token);

    @Multipart
    @POST("api/v1.0.1/all_visitor_create_options")
    Call<GetSuffixDoctypeResponse> getAllNewVisitorPageDetail(@Header("x-access-token") String token, @Part("parent_id") RequestBody loggedin_user, @Part("clients_id") RequestBody clientId, @Part("user_id ") RequestBody userId);

    //vms/api/view_visitor

    @Multipart
    @POST("api/v1.0.1/view_visitor")
    Call<ScanResult> getScanResult(@Header("x-access-token") String token, @Part("visitor_id") RequestBody visitor_id/*,@Part("clients_id") RequestBody clientId*/);

    @Multipart
    @POST("api/v1.0.1/visitor_suggestions")
    Call<SuggesionResponse> checkSuggession(@Part("first_name") RequestBody first_name, @Part("last_name") RequestBody last_name, @Part("dob") RequestBody dobSt, @Part("parent_id") RequestBody parentId, @Part("clients_id") RequestBody clientId);

    //api/sex_offender_check
    @Multipart
    @POST("api/v1.0.1/sex_offender_check")
    Call<SOResponse> checkSexOffender(@Header("x-access-token") String token, @Part("first_name") RequestBody first_name, @Part("last_name") RequestBody last_name, @Part("dob") RequestBody dobSt, @Part("clients_id") RequestBody clientId);

    @POST("api/v1.0.1/check_wl")
    Call<WatchListCheck> checkInWatchList(@Body CheckWatchListRequestModel requestModel);

    @Multipart
    @POST("api/v1.0.1/visitor_bg_check")
    Call<JsonObject> checkCB(@Header("x-access-token") String token, @Part("first_name") RequestBody first_name, @Part("last_name") RequestBody last_name,
                             @Part("dob") RequestBody dobSt, @Part("email") RequestBody email,
                             @Part("phone") RequestBody phone, @Part("loggedin_user") RequestBody loggedin_user,
                             @Part("created_by") RequestBody created_by, @Part MultipartBody.Part profile_pic,
                             @Part("clients_id") RequestBody clientId, @Part("parent_id") RequestBody parent);


    @Multipart
    @POST("api/v1.0.1/visitor_bg_check")
    Call<JsonObject> checkCBWithoutImage(@Header("x-access-token") String token, @Part("first_name") RequestBody first_name, @Part("last_name") RequestBody last_name,
                                         @Part("dob") RequestBody dobSt, @Part("email") RequestBody email,
                                         @Part("phone") RequestBody phone, @Part("loggedin_user") RequestBody loggedin_user,
                                         @Part("created_by") RequestBody created_by, @Part("profile_pic") RequestBody profile_pic,
                                         @Part("clients_id") RequestBody clientId, @Part("parent_id") RequestBody parent);

    @Multipart
    @POST("api/v1.0.1/create_visitor")
    Call<JsonObject> createVisitorWithoutImage(@Header("x-access-token") String token, @Part("loggedin_user") RequestBody loggedin_user, @Part("first_name") RequestBody first_name, @Part("middle_name") RequestBody middle_name,
                                               @Part("last_name") RequestBody last_name, @Part("suffix_name") RequestBody suffix_name,
                                               @Part("email") RequestBody email, @Part("phone") RequestBody phone,
                                               @Part("dob") RequestBody dobSt, @Part("created_by") RequestBody created_by,
                                               @Part("parent_id") RequestBody parent, @Part("visitor_id_document_types_id") RequestBody visitor_id_document_types_id,
                                               @Part("visitor_id_number") RequestBody visitor_id_number,
                                               @Part("check_in") RequestBody check_in, @Part("entered_by") RequestBody entered_by,
                                               @Part("visiting_individual_id") RequestBody visiting_individual_id, @Part("visiting_department_id") RequestBody visiting_department_id,
                                               @Part("vms_visitor_entering_from_id") RequestBody vms_visitor_entering_from_id, @Part("vms_visitor_purpose_id") RequestBody vms_visitor_purpose_id,
                                               @Part("vms_visitor_other_purpose") RequestBody vms_visitor_other_purpose,
                                               @Part("profile_pic") RequestBody profile_pic, @Part("clients_id") RequestBody clients_id,
                                               @Part("visitor_title") RequestBody visitor_title, @Part("sexOffenderCheckSetting") RequestBody sexOffenderCheckSetting,
                                               @Part("sexOffenderChecked") RequestBody sexOffenderChecked, @Part("country_id") RequestBody country_id,
                                               @Part("vipId") RequestBody vipId, @Part("match_id ") RequestBody match_id);

    @Multipart
    @POST("api/v1.0.1/create_visitor")
    Call<JsonObject> createVisitor(@Header("x-access-token") String token, @Part("loggedin_user") RequestBody loggedin_user, @Part("first_name") RequestBody first_name, @Part("middle_name") RequestBody middle_name,
                                   @Part("last_name") RequestBody last_name, @Part("suffix_name") RequestBody suffix_name,
                                   @Part("email") RequestBody email, @Part("phone") RequestBody phone,
                                   @Part("dob") RequestBody dobSt, @Part("created_by") RequestBody created_by,
                                   @Part("parent_id") RequestBody parent, @Part("visitor_id_document_types_id") RequestBody visitor_id_document_types_id,
                                   @Part("visitor_id_number") RequestBody visitor_id_number,
                                   @Part("check_in") RequestBody check_in, @Part("entered_by") RequestBody entered_by,
                                   @Part("visiting_individual_id") RequestBody visiting_individual_id, @Part("visiting_department_id") RequestBody visiting_department_id,
                                   @Part("vms_visitor_entering_from_id") RequestBody vms_visitor_entering_from_id, @Part("vms_visitor_purpose_id") RequestBody vms_visitor_purpose_id,
                                   @Part("vms_visitor_other_purpose") RequestBody vms_visitor_other_purpose,
                                   @Part MultipartBody.Part profile_pic, @Part("clients_id") RequestBody clients_id,
                                   @Part("visitor_title") RequestBody visitor_title, @Part("sexOffenderCheckSetting") RequestBody sexOffenderCheckSetting,
                                   @Part("sexOffenderChecked") RequestBody sexOffenderChecked, @Part("country_id") RequestBody country_id,
                                   @Part("vipId") RequestBody vipId, @Part("match_id") RequestBody match_id);


    @Headers({"Content-Type:image/jpeg"})
    @Multipart
    @POST("api/v1.0.1/create_visitor")
    Call<JsonObject> createVisitorWithUrl(@Header("x-access-token") String token, @Part("loggedin_user") RequestBody loggedin_user, @Part("first_name") RequestBody first_name, @Part("middle_name") RequestBody middle_name,
                                          @Part("last_name") RequestBody last_name, @Part("suffix_name") RequestBody suffix_name,
                                          @Part("email") RequestBody email, @Part("phone") RequestBody phone,
                                          @Part("dob") RequestBody dobSt, @Part("created_by") RequestBody created_by,
                                          @Part("parent_id") RequestBody parent, @Part("visitor_id_document_types_id") RequestBody visitor_id_document_types_id,
                                          @Part("visitor_id_number") RequestBody visitor_id_number,
                                          @Part("check_in") RequestBody check_in, @Part("entered_by") RequestBody entered_by,
                                          @Part("visiting_individual_id") RequestBody visiting_individual_id, @Part("visiting_department_id") RequestBody visiting_department_id,
                                          @Part("vms_visitor_entering_from_id") RequestBody vms_visitor_entering_from_id, @Part("vms_visitor_purpose_id") RequestBody vms_visitor_purpose_id,
                                          @Part("vms_visitor_other_purpose") RequestBody vms_visitor_other_purpose,
                                          @Part("profile_img") String profile_pic, @Part("clients_id") RequestBody clients_id,
                                          @Part("visitor_title") RequestBody visitor_title, @Part("sexOffenderCheckSetting") RequestBody sexOffenderCheckSetting,
                                          @Part("sexOffenderChecked") RequestBody sexOffenderChecked, @Part("country_id") RequestBody country_id,
                                          @Part("vipId") RequestBody vipId, @Part("match_id") RequestBody match_id);


    @Multipart
    @POST("api/v1.0.1/send_qb")
    Call<JsonObject> setQb(@Header("x-access-token") String token, @Part("user_id") RequestBody user_id, @Part("visitor_id") RequestBody visitor_id,
                           @Part("is_vip") RequestBody is_vip, @Part("is_qb") RequestBody is_qb,
                           @Part("is_se") RequestBody is_se, @Part("log_id") RequestBody log_id,
                           @Part("loggedin_user") RequestBody loggedin_user, @Part("email") RequestBody email,
                           @Part("phone") RequestBody phone, @Part MultipartBody.Part profile_pic);

    @Multipart
    @POST("api/v1.0.1/getEvents")
    Call<EventResponseObject> getAllEvents(@Header("x-access-token") String token, @Part("user_id") RequestBody user_id, @Part("clients_id") RequestBody clients_id
    );


    @GET
    Call<ResponseBody> downlload(@Url String fileUrl);
/*
    @Multipart
    @POST("api/stop_visitor")
    Call<PrinterSettingResponse> stopVisitor(@Part("event_id") RequestBody event_id);*/

    @Multipart
    @POST("api/v1.0.1/createEventGuest")
    Call<JsonObject> createEventGuest(@Header("x-access-token") String token, @Part("event_id") RequestBody event_id, @Part("first_name") RequestBody first_name,
                                      @Part("last_name") RequestBody last_name, @Part("title") RequestBody title,
                                      @Part("company_name") RequestBody company_name, @Part("phone") RequestBody phone,
                                      @Part("email") RequestBody emailID, @Part("rsvp") RequestBody rsvp,
                                      @Part("log_id") RequestBody log_id);


    @Multipart
    @POST("api/v1.0.1/printer-settings")
    Call<PrinterSettingResponse> getPrinterSetting(@Header("x-access-token") String token, @Part("clients_id") RequestBody event_id);

    @Multipart
    @POST("api/v1.0.1/stop_visitor")
    Call<StatusMessageModel> stopVisitorWithoutProfile(@Header("x-access-token") String token, @Part("user_id") RequestBody user_id,
                                                       @Part("parent_id") RequestBody parent_id,
                                                       @Part("clients_id") RequestBody clients_id,
                                                       @Part("location") RequestBody location,
                                                       @Part("matchName") RequestBody matchName,
                                                       @Part("matchCount") RequestBody matchCount,
                                                       @Part("offendType") RequestBody offendType,
                                                       @Part("userName") RequestBody userName,
                                                       @Part("visitorName") RequestBody visitorName,
                                                       @Part("matchImage") RequestBody matchImage,
                                                       @Part("first_name") RequestBody first_name,
                                                       @Part("last_name") RequestBody last_name,
                                                       @Part("email") RequestBody email,
                                                       @Part("phone") RequestBody phone,
                                                       @Part("dob") RequestBody dob,
                                                       @Part("match_id") RequestBody matchId,
                                                       @Part("profile_pic") String profile_pic);


    @Multipart
    @POST("api/v1.0.1/stop_visitor")
    Call<StatusMessageModel> stopVisitor(@Header("x-access-token") String token, @Part("user_id") RequestBody user_id,
                                         @Part("parent_id") RequestBody parent_id,
                                         @Part("clients_id") RequestBody clients_id,
                                         @Part("location") RequestBody location,
                                         @Part("matchName") RequestBody matchName,
                                         @Part("matchCount") RequestBody matchCount,
                                         @Part("offendType") RequestBody offendType,
                                         @Part("userName") RequestBody userName,
                                         @Part("visitorName") RequestBody visitorName,
                                         @Part("matchImage") String matchImage,
                                         @Part("first_name") RequestBody first_name,
                                         @Part("last_name") RequestBody last_name,
                                         @Part("email") RequestBody email,
                                         @Part("phone") RequestBody phone,
                                         @Part("dob") RequestBody dob,
                                         @Part("match_id") RequestBody matchId,
                                         @Part MultipartBody.Part profile_pic);

    @GET("api/v1.0.1/getEvent/{event_id}")
    Call<JsonObject> getEventBudgetDetail(@Header("x-access-token") String token, @Path("event_id") String eventId);

    //status
    @Multipart
    @POST("api/v1.0.1/update_event_attendence")
    Call<JsonObject> markAttendence(@Header("x-access-token") String token, @Part("user_id") RequestBody loggedin_user, @Part("eventGuestId") RequestBody eventGuestId, @Part("status") RequestBody status);

    //http://dev.miracleglobal.com:8010/vms/api/create_badge
    @Multipart
    @POST("api/v1.0.1/create_badge")
    Call<JsonObject> createBadgeWithoutImage(@Header("x-access-token") String token,
                                             @Part("user_id") RequestBody user_id,
                                             @Part("visitor_id") RequestBody visitor_id,
                                             @Part("is_vip") RequestBody is_vip,
                                             @Part("is_qb") RequestBody is_qb,
                                             @Part("is_se") RequestBody is_se,
                                             @Part("log_id") RequestBody log_id,
                                             @Part("profile_pic") RequestBody profile_pic,
                                             @Part("email") RequestBody email,
                                             @Part("phone") RequestBody phone);

    @Multipart
    @POST("api/v1.0.1/create_badge")
    Call<JsonObject> createBadgeWithImage(@Header("x-access-token") String token,
                                          @Part("user_id") RequestBody user_id,
                                          @Part("visitor_id") RequestBody parent_id,
                                          @Part("is_vip") RequestBody is_vip,
                                          @Part("is_qb") RequestBody is_qb,
                                          @Part("is_se") RequestBody is_se,
                                          @Part("log_id") RequestBody log_id,
                                          @Part MultipartBody.Part profile_pic,
                                          @Part("email") RequestBody email,
                                          @Part("phone") RequestBody phone);

    @GET("api/v1.0.1/automatic-alarm/settings/{clientId}")
    Call<JsonObject> getCautionaryDetail(@Header("x-access-token") String token, @Path("clientId") String user_id, @Query("type") String type);

    @GET("api/v1.0.1/automatic-alarm/settings/{clientId}")
    Call<JsonObject> setCautionaryClear(@Header("x-access-token") String token, @Path("clientId") String user_id, @Query("type") String clear);

    @POST("api/v1.0.1/verifyCautionaryPassword")
    Call<JsonObject> verifyCautionaryPassword(@Body VerifyCautioneryPassRequestModel requestModel);

    @POST("api/v1.0.1/clearAlert")
    Call<JsonObject> clearAlert(@Body VerifyCautioneryPassRequestModel requestModel);

    //api/alarm

    @Multipart
    @POST("api/v1.0.1/alarm")
    Call<JsonObject> callAlarmWithoutLocation(
            @Header("x-access-token") String token,
            @Part("loggedin_user") RequestBody loggedin_user,
            @Part("user_id") RequestBody user_id,
            @Part("parent_id") RequestBody parent_id,
            @Part("clients_id") RequestBody clients_id,
            @Part("alert_type") RequestBody alert_type,
            @Part("receiver_type") RequestBody receiver_type,
            @Part("message") RequestBody message,
            @Part("additional_info") RequestBody additional_info,
            @Part("username") RequestBody username,
            @Part("cautionary_code") RequestBody cautionary_code

    );


    @Multipart
    @POST("api/v1.0.1/get_staff_guard_settings")
    Call<SettingMainResponse> getSettingAPI(
            @Header("x-access-token") String token,
            @Part("loggedin_user") RequestBody loggedin_user,
            @Part("user_id") RequestBody user_id

    );

    @Multipart
    @POST("api/v1.0.1/alarm")
    Call<JsonObject> callAlarm(
            @Header("x-access-token") String token,
            @Part("loggedin_user") RequestBody loggedin_user,
            @Part("user_id") RequestBody user_id,
            @Part("parent_id") RequestBody parent_id,
            @Part("clients_id") RequestBody clients_id,
            @Part("alert_type") RequestBody alert_type,
            @Part("receiver_type") RequestBody receiver_type,
            @Part("location") RequestBody location,
            @Part("message") RequestBody message,
            @Part("additional_info") RequestBody additional_info,
            @Part("username") RequestBody username,
            @Part("cautionary_code") RequestBody cautionary_code,
            @Part("location_lat") RequestBody location_lat,
            @Part("location_lng") RequestBody location_lng
    );


    @GET("api/v1.0.1/locations/{clientId}")
    Call<JsonObject> getAllLocations(
            @Header("x-access-token") String token,
            @Path("clientId") String user_id);

    @Multipart
    @POST("api/v1.0.1/get-alarms")
    Call<JsonObject> getAllAlamrsLogs(@Header("x-access-token") String token, @Part("clients_id") RequestBody user_id);

    @POST("api/v1.0.1/view_all_visitors")
    Call<GetAllVisitorResponseModel> getAllVisitor(@Body GetAllVisitorRequestModel requestModel);

    @Multipart
    @POST("api/v1.0.1/get-notification-replies")
    Call<JsonObject> getAllARPList(
            @Header("x-access-token") String token,
            @Part("dispatch_id") RequestBody dispatch_id, @Part("replyStatus") RequestBody replyStatus);


    @Multipart
    @POST("api/v1.0.1/edit_visitor")
    Call<JsonObject> updateUserProfileWithImage(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody userID,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("visitor_id_document_types_id") RequestBody visitor_id_document_types_id,
            @Part("visitor_id_number") RequestBody visitor_id_number,
            @Part("check_in") RequestBody check_in,
            @Part("visiting_individual_id") RequestBody visiting_individual_id,
            @Part("visiting_department_id") RequestBody visiting_department_id,
            @Part("vms_visitor_entering_from_id") RequestBody vms_visitor_entering_from_id,
            @Part("vms_visitor_purpose_id") RequestBody vms_visitor_purpose_id,
            @Part("visitor_title") RequestBody visitor_title,
            @Part("country_id") RequestBody country_id,
            @Part("updated_by") RequestBody updated_by,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("middle_name") RequestBody middle_name,
            @Part("suffix_name") RequestBody suffix_name,
            @Part("dob") RequestBody dob,
            @Part("visitor_id") RequestBody visitor_id,
            @Part("log_id") RequestBody log_id,
            @Part MultipartBody.Part profile_pic
    );

    @Multipart
    @POST("api/v1.0.1/edit_visitor")
    Call<JsonObject> updateUserProfile(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody userID,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("visitor_id_document_types_id") RequestBody visitor_id_document_types_id,
            @Part("visitor_id_number") RequestBody visitor_id_number,
            @Part("check_in") RequestBody check_in,
            @Part("visiting_individual_id") RequestBody visiting_individual_id,
            @Part("visiting_department_id") RequestBody visiting_department_id,
            @Part("vms_visitor_entering_from_id") RequestBody vms_visitor_entering_from_id,
            @Part("vms_visitor_purpose_id") RequestBody vms_visitor_purpose_id,
            @Part("visitor_title") RequestBody visitor_title,
            @Part("country_id") RequestBody country_id,
            @Part("updated_by") RequestBody updated_by,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("middle_name") RequestBody middle_name,
            @Part("suffix_name") RequestBody suffix_name,
            @Part("dob") RequestBody dob,
            @Part("visitor_id") RequestBody visitor_id,
            @Part("log_id") RequestBody log_id,
            @Part("profile_pic") RequestBody profile_pic
    );

    @Multipart
    @POST("api/v1.0.1/lock_down_emergency")
    Call<JsonObject> callLockDownEmergencyAlarm(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody user_id,
            @Part("parent_id") RequestBody parent_id,
            @Part("clients_id") RequestBody clients_id,
            @Part("alert_type") RequestBody alert_type,
            @Part("message") RequestBody message,
            @Part("cautionary_code") RequestBody cautionary_code);

    @Multipart
    @POST("api/v1.0.1/call_police")
    Call<JsonObject> callNonEmergencyAlarm(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody user_id,
            @Part("parent_id") RequestBody parent_id,
            @Part("clients_id") RequestBody clients_id,
            @Part("alert_type") RequestBody alert_type,
            @Part("message") RequestBody message,
            @Part("cautionary_code") RequestBody cautionary_code
    );

    @Multipart
    @POST("api/v1.0.1/add_video")
    Call<JsonObject> addVideo(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody user_id,
            @Part("category_id") RequestBody category_id,
            @Part("title") RequestBody title,
            @Part("url") RequestBody url,
            @Part("tag_name") RequestBody tag_name,
            @Part("assign_for") RequestBody assign_for,
            @Part("assign_to") RequestBody assign_to,
            @Part("attempt") RequestBody attempt,
            @Part("is_publish") RequestBody is_publish,
            @Part("min_mark") RequestBody min_mark,
            @Part("clients_id") RequestBody clients_id,
            @Part("created_by") RequestBody created_by,
            @Part("parent_id") RequestBody parent_id,
            @Part("is_open_video") RequestBody is_open_video
    );


    @Multipart
    @POST("api/v1.0.1/assign_for_to_video")
    Call<JsonObject> get_asign_to_asign_to(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody user_id,
            @Part("parent_id") RequestBody parent_id,
            @Part("clients_id") RequestBody clients_id
    );


    @Multipart
    @POST("api/v1.0.1/update_watch_list")
    Call<JsonObject> updateWatchList(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody userId, @Part("visitor_id") RequestBody visitor_id,
            @Part("status") RequestBody status
    );

    @Multipart
    @POST("api/v1.0.1/is_vip")
    Call<JsonObject> updateVipList(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody userId, @Part("visitor_id") RequestBody visitor_id,
            @Part("status") RequestBody status
    );

    //api/check_out_visitor
    @Multipart
    @POST("api/v1.0.1/check_out_visitor")
    Call<JsonObject> checkOutVisitor(
            @Header("x-access-token") String token,
            @Part("user_id") RequestBody userId, @Part("visitor_id") RequestBody visitor_id
    );

    ///vms/api/notificationReply
    @Multipart
    @POST("api/v1.0.1/notificationReply")
    Call<JsonObject> acceptReject(
            @Header("x-access-token") String token,
            @Part("status_key") RequestBody alert_id, @Part("status") RequestBody status,
            @Part("userName") RequestBody userName, @Part("user_id") RequestBody user_id
    );

    @Multipart
    @POST("api/v1.0.1/cancel-notification")
    Call<JsonObject> cancleAlertLog(
            @Header("x-access-token") String token,
            @Part("clients_id") RequestBody clients_id,
            @Part("dispatch_id") RequestBody dispatch_id, @Part("userName") RequestBody userName
    );

    @Multipart
    @POST("api/v1.0.1/event/cb-check")
    Call<JsonObject> evenCbCheck(
            @Header("x-access-token") String token,
            @Part("first_name") RequestBody firstName,
            @Part("last_name") RequestBody lastName, @Part("dob") RequestBody dob);

    @Multipart
    @POST("api/v1.0.1/search_by_dl")
    Call<JsonObject> getDlDetailImage(
            @Header("x-access-token") String token,
            @Part("dl_no") RequestBody dl_no,
            @Part("parent_id") RequestBody parent_id, @Part("clients_id") RequestBody clients_id);


}
