package com.silvershield.networkManager;

import retrofit2.Response;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public interface APIServerResponse {
    int REGISTER = 1;
    int LOGIN = 2;
    int FORGET_PASSWORD = 3;
    int GET_VIP_WATCHER_LIST = 4;
    int GETNOTIFICATIONFLAGS = 5;
    int GETALLCATEGORIES = 6;
    int GET_CASES_ASIGNED_TO_ME = 7;
    int GET_SETTING = 8;
    int GET_NEW_VISITOR_DETAIL = 9;
    int GET_SCAN_VISITOR_DETAIL = 10;
    int GET_SUGESTION = 11;
    int CHECK_SEX_OFFENDER = 12;
    int CHECK_IN_WATCH_LIST = 13;
    int CHECK_CB = 14;
    int CREATE_VISITOR = 15;
    int SET_QB = 16;
    int GET_ALL_EVENT = 17;
    int CREATE_GUEST_EVENT = 18;
    int GET_ALL_PRINTER = 19;
    int STOP_VISITOR = 20;
    int EVENT_DETAIL = 21;
    int MARK_ATTENDENCE = 22;
    int CREATE_BADGE = 23;
    int SEARCHED_WATCH_LIST = 24;
    int GETCAUTIONARY = 25;
    int CALL_ALARM = 26;
    int GET_ALL_LOCATION = 27;
    int GET_ALL_LOGS = 28;
    int GETALLVISITOR = 29;
    int GET_ACC_REJ_PEND = 30;
    int UPDATE_INFORMATION = 31;
    int CALL_LOCK_EMERGENCY = 32;
    int CALL_LOCK_NON_EMERGENCY = 33;
    int GET_ASIGN_TO_BT_DETAIL = 34;
    int UPDATE_WATCHLIST_STATUS = 35;
    int UPDATE_VIP_STATUS = 36;
    int CHECK_OUT_VISITOR = 37;
    int SETTINGS = 38;
    int UPDATE_PROFILE = 39;
    int GET_ALL_TIMEZONES = 40;
    int DOWNLOAD_FILE = 41;
    int TIMEZONES = 42;
    int ACCEPT_REJECT_ALARAM = 43;
    int CLEAR_ALERT_LOG = 44;
    int REFRESH_TOKEN = 45;
    int LOGOUT = 46;
    int EVENT_CBC_CHECK = 47;
    int GETCAUTIONARYCLEAR = 48;
    int VERIFYCAUTIONERYPASSWORD = 49;
    int CLEARALERT = 50;

    void onSuccess(int tag, Response response);

    void onError(int tag, Throwable throwable);
}
