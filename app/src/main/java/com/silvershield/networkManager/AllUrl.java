package com.silvershield.networkManager;

/**
 * Created by gagandeep.bhutani on 3/8/2018.
 */

public class AllUrl {

    public static String LIVE_BASE_URL = "https://silvershield.com:8009/vms/";
    public static String STG_BASE_URL = "https://staging.silvershield.com:8009/vms/";

    public static String LIVE_BADGE_URL = "https://silvershield.com:8009/vms/visitorlogs/getbadge?id=";
    public static String STG_BADGE_BASE_URL = "http://staging.silvershield.com/visitorlogs/getbadge?id=";
}
