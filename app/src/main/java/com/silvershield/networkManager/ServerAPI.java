package com.silvershield.networkManager;

import android.app.Activity;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.google.gson.JsonObject;
import com.silvershield.modal.AlarmCheckModal;
import com.silvershield.modal.AssignToMeReponse;
import com.silvershield.modal.EventResponseObject;
import com.silvershield.modal.GetCategoryResponse;
import com.silvershield.modal.GetSuffixDoctypeResponse;
import com.silvershield.modal.PrinterSettingResponse;
import com.silvershield.modal.SOResponse;
import com.silvershield.modal.ScanResult;
import com.silvershield.modal.SettingMainResponse;
import com.silvershield.modal.StatusMessageModel;
import com.silvershield.modal.SuggesionResponse;
import com.silvershield.modal.UserLoginResponse;
import com.silvershield.modal.WatchListCheck;
import com.silvershield.modal.requestModels.CheckWatchListRequestModel;
import com.silvershield.modal.requestModels.ForgetPassRequestModel;
import com.silvershield.modal.requestModels.GetAllVisitorRequestModel;
import com.silvershield.modal.requestModels.LoginRequestModel;
import com.silvershield.modal.requestModels.LogoutRequestModel;
import com.silvershield.modal.requestModels.VerifyCautioneryPassRequestModel;
import com.silvershield.modal.responseModels.GetAllVisitorResponseModel;

import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class ServerAPI {
    private static ServerAPI serverAPI;
    RequestBody requestBody;
    MultipartBody.Part body;
    File futureStudioIconFile;
    SimpleDateFormat format;
    String fileName = "";
    String ipAddress = "";
    Activity activity;
    URL urlPath = null;
    URL finalUrl;
    FileInputStream fileInputStream;
    BufferedInputStream bufferedInputStream;
    OutputStream outputStream;
    private APIReference apiReference;

    private ServerAPI() {

    }

    public static ServerAPI getInstance() {
        if (serverAPI == null)
            serverAPI = new ServerAPI();
        return serverAPI;
    }

    private APIReference getApiReference(String token) {
        return apiReference = RestClient.build(token);
    }

    public void refreshToken(final int tag, String authToken, final APIServerResponse apiServerResponse) {
        Call call = getApiReference(authToken).refreshToken();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void login(final int tag, String email, String password, String deviceType, String device_id, String timeZone, final APIServerResponse apiServerResponse) {
        LoginRequestModel requestModel = new LoginRequestModel();
        requestModel.setDevice_token(device_id);
        requestModel.setUser(email);
        requestModel.setDevice_type(deviceType);
        requestModel.setPassword(password);
        requestModel.setTimezone(timeZone);

        Call<UserLoginResponse> call = getApiReference("").login(requestModel);
        call.enqueue(new Callback<UserLoginResponse>() {
            @Override
            public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void logoutUser(final int tag, String authToken, String userId, final APIServerResponse apiServerResponse) {
        LogoutRequestModel requestModel = new LogoutRequestModel();
        requestModel.setUser_id(userId);
        Call call = getApiReference(authToken).logoutUser(requestModel);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void forgetPassword(final int tag, String email, final APIServerResponse apiServerResponse) {
        ForgetPassRequestModel requestModel = new ForgetPassRequestModel();
        requestModel.setEmail(email);
        Call call = getApiReference("").forgetPassword(requestModel);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getAllVipWatcherList(final int tag, String authToken, String loggined_id, String client_id, String parent_id, final APIServerResponse apiServerResponse) {
        RequestBody loggedingReq = RequestBody.create(MediaType.parse("text/plain"), loggined_id);
        RequestBody parentReq = RequestBody.create(MediaType.parse("text/plain"), parent_id);
        RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);
        Map<String, String> params = new HashMap<>();
        params.put("loggedin_user", loggined_id);
        params.put("parent_id", parent_id);
        params.put("clients_id", client_id);

        Call<JsonObject> call = getApiReference(authToken).getVipWatcherList(authToken, loggedingReq, parentReq, clientReq/*params*/);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getNotificationFlag(final int tag, String authToken, String loggined_id, String parent_id, String client_id, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("loggedin_user", loggined_id);
        params.put("parent_id", parent_id);
        params.put("clients_id", client_id);

        Call<AlarmCheckModal> call = getApiReference(authToken).getNotificationFlag(authToken, params);
        call.enqueue(new Callback<AlarmCheckModal>() {
            @Override
            public void onResponse(Call<AlarmCheckModal> call, Response<AlarmCheckModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getAllCategories(final int tag, String authToken, String clientId, final APIServerResponse apiServerResponse) {
        Call<GetCategoryResponse> call = getApiReference(authToken).getAllCateories(authToken, clientId/*params*/);
        call.enqueue(new Callback<GetCategoryResponse>() {
            @Override
            public void onResponse(Call<GetCategoryResponse> call, Response<GetCategoryResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    //vms/api/view_assigned_tests
    public void getAllAssignedCasesToME(final int tag, String authToken, String userId, String parent_id, String client_id, final APIServerResponse apiServerResponse) {

        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody parentReq = RequestBody.create(MediaType.parse("text/plain"), parent_id);
        RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);

        Call<AssignToMeReponse> call = getApiReference(authToken).getCasesAssignedToMe(authToken, userIdReq, parentReq, clientReq/*params*/);
        call.enqueue(new Callback<AssignToMeReponse>() {
            @Override
            public void onResponse(Call<AssignToMeReponse> call, Response<AssignToMeReponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getSetting(final int tag, String authToken, String userId, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), userId);
        //clientID

        Call<SettingMainResponse> call = getApiReference(authToken).getSettingAPI(authToken, userIdReg, userIdReg);
        call.enqueue(new Callback<SettingMainResponse>() {
            @Override
            public void onResponse(Call<SettingMainResponse> call, Response<SettingMainResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void getAllTimesZones(final int tag, String authToken, final APIServerResponse apiServerResponse) {
        Call<JsonObject> call = getApiReference(authToken).getAllTimesZones(authToken);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void getAllNewVisitorPageDetail(final int tag, String authToken, String parentId, String clientId, String userid, final APIServerResponse apiServerResponse) {
        RequestBody parentIdReg = RequestBody.create(MediaType.parse("text/plain"), parentId);
        RequestBody clientIdReq = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userid);
        Call<GetSuffixDoctypeResponse> call = getApiReference(authToken).getAllNewVisitorPageDetail(authToken, parentIdReg, clientIdReq, userIdReq);
        call.enqueue(new Callback<GetSuffixDoctypeResponse>() {
            @Override
            public void onResponse(Call<GetSuffixDoctypeResponse> call, Response<GetSuffixDoctypeResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getScanBudgetResult(final int tag, String authToken, String userId, String clientId, final APIServerResponse apiServerResponse) {
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody clientIdReq = RequestBody.create(MediaType.parse("text/plain"), clientId);
        Call<ScanResult> call = getApiReference(authToken).getScanResult(authToken, userIdReq/*,clientIdReq*/);
        call.enqueue(new Callback<ScanResult>() {
            @Override
            public void onResponse(Call<ScanResult> call, Response<ScanResult> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    //get Suggestion Lidt
    public void checkSuggetion(final int tag, String authToken, String firstNameSt, String lastNameSt, String dobSt, String parentId, String clientId, String userid, final APIServerResponse apiServerResponse) {
        RequestBody parentIdReg = RequestBody.create(MediaType.parse("text/plain"), parentId);
        RequestBody clientIdReq = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userid);


        RequestBody firstNameReg = RequestBody.create(MediaType.parse("text/plain"), firstNameSt);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), lastNameSt);
        RequestBody dobReq = RequestBody.create(MediaType.parse("text/plain"), dobSt);


        Call<SuggesionResponse> call = getApiReference(authToken).checkSuggession(firstNameReg, lastNameReq, dobReq, parentIdReg, clientIdReq);
        call.enqueue(new Callback<SuggesionResponse>() {
            @Override
            public void onResponse(Call<SuggesionResponse> call, Response<SuggesionResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SuggesionResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void checkWatchListExistence(final int tag, String authToken, CheckWatchListRequestModel requestModel, final APIServerResponse apiServerResponse) {
        Call<WatchListCheck> call = getApiReference(authToken).checkInWatchList(requestModel);
        call.enqueue(new Callback<WatchListCheck>() {
            @Override
            public void onResponse(Call<WatchListCheck> call, Response<WatchListCheck> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<WatchListCheck> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    //check sex offender
    public void checkSexOffenderExistence(final int tag, String authToken, String firstNameSt, String lastNameSt, String dobSt, String clientId, final APIServerResponse apiServerResponse) {
        RequestBody firstNameReg = RequestBody.create(MediaType.parse("text/plain"), firstNameSt);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), lastNameSt);
        RequestBody dobStReg = RequestBody.create(MediaType.parse("text/plain"), dobSt);

        RequestBody clientIdReq = RequestBody.create(MediaType.parse("text/plain"), clientId);

        //checkInWatchList
        Call<SOResponse> call = getApiReference(authToken).checkSexOffender(authToken, firstNameReg, lastNameReq, dobStReg, clientIdReq);
        call.enqueue(new Callback<SOResponse>() {
            @Override
            public void onResponse(Call<SOResponse> call, Response<SOResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SOResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void checkCB(final int tag, String authtoken, String firstNameSt, String lastNameSt, String dobSt, String emailSt, String phoneSt, String created_by, String profile_pic, String clientId, String parentId, String loggenInSt, final APIServerResponse apiServerResponse) {
        RequestBody firstNameReg = RequestBody.create(MediaType.parse("text/plain"), firstNameSt);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), lastNameSt);
        RequestBody dobStReg = RequestBody.create(MediaType.parse("text/plain"), dobSt);
        RequestBody emailStReg = RequestBody.create(MediaType.parse("text/plain"), emailSt);
        RequestBody phoneReg = RequestBody.create(MediaType.parse("text/plain"), phoneSt);
        RequestBody created_byReq = RequestBody.create(MediaType.parse("text/plain"), created_by);
        if (profile_pic == null) {
            profile_pic = "";
        }
        RequestBody profilePicReq = RequestBody.create(MediaType.parse("text/plain"), profile_pic);

        if (!profile_pic.equalsIgnoreCase("")) {
            requestBody = RequestBody.create(MediaType.parse("image/png"), new File(profile_pic));
            body = MultipartBody.Part.createFormData("profile_pic", "profile.png", requestBody);

        }

        RequestBody clientIdReq = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody parentIdReq = RequestBody.create(MediaType.parse("text/plain"), parentId);
        RequestBody loggenInStReq = RequestBody.create(MediaType.parse("text/plain"), loggenInSt);

        if (profile_pic == null || profile_pic.equalsIgnoreCase("")) {
            Call</*CriminalResponse*/JsonObject> call = getApiReference(authtoken).checkCBWithoutImage(authtoken, firstNameReg, lastNameReq, dobStReg, emailStReg, phoneReg, loggenInStReq, created_byReq, profilePicReq, clientIdReq, parentIdReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {
            Call</*CriminalResponse*/JsonObject> call = getApiReference(authtoken).checkCB(authtoken, firstNameReg, lastNameReq, dobStReg, emailStReg, phoneReg, loggenInStReq, created_byReq, body, clientIdReq, parentIdReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }


    }

    public void createVisitorWithUrl(final int tag, String authtoken, String firstNameSt, String lastNameSt, String middleName, String suffix,
                                     String emailSt, String phoneSt, String dobSt, String created_by, String parentId,
                                     String visitor_id_document_type_id, String visitor_id_number, String checkIn,
                                     String enteredBy, String visiting_individualId, String visiting_dept_Id,
                                     String vms_visitor_entering_from_id, String vms_visitor_purpose_id,
                                     String vms_visitor_other_purpose, String profile_pic, String clientId, String visitor_title,
                                     String SOCheckFirst, String SOCheckSecond, String country_id, String vipID, String loggenInSt, String matchID, final APIServerResponse apiServerResponse) {

        RequestBody firstNameReg = RequestBody.create(MediaType.parse("text/plain"), firstNameSt);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), lastNameSt);
        RequestBody middleNameReg = RequestBody.create(MediaType.parse("text/plain"), middleName);
        RequestBody suffixReg = RequestBody.create(MediaType.parse("text/plain"), suffix);
        RequestBody emailStReg = RequestBody.create(MediaType.parse("text/plain"), emailSt);
        RequestBody phoneReg = RequestBody.create(MediaType.parse("text/plain"), phoneSt);
        if (dobSt == null) {
            dobSt = "";
        }
        RequestBody dobStReg = RequestBody.create(MediaType.parse("text/plain"), dobSt);
        RequestBody created_byReq = RequestBody.create(MediaType.parse("text/plain"), created_by);
        RequestBody parentIdReg = RequestBody.create(MediaType.parse("text/plain"), parentId);

        RequestBody visitor_id_document_type_idReg = RequestBody.create(MediaType.parse("text/plain"), visitor_id_document_type_id);
        RequestBody visitor_id_numberReg = RequestBody.create(MediaType.parse("text/plain"), visitor_id_number);
        RequestBody checkInReg = RequestBody.create(MediaType.parse("text/plain"), checkIn);
        RequestBody enteredByReg = RequestBody.create(MediaType.parse("text/plain"), enteredBy);
        RequestBody visiting_individualIdReg = RequestBody.create(MediaType.parse("text/plain"), visiting_individualId);
        RequestBody visiting_dept_IdReg = RequestBody.create(MediaType.parse("text/plain"), visiting_dept_Id);
        RequestBody vms_visitor_entering_from_idReg = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_entering_from_id);
        RequestBody vms_visitor_purpose_idReq = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_purpose_id);
        RequestBody vms_visitor_other_purposeReq = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_other_purpose);


        RequestBody clientIdReg = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody visitor_titleReq = RequestBody.create(MediaType.parse("text/plain"), visitor_title);
        RequestBody SOCheckFirstReq = RequestBody.create(MediaType.parse("text/plain"), SOCheckFirst);

        RequestBody SOCheckSecondReq = RequestBody.create(MediaType.parse("text/plain"), SOCheckSecond);
        RequestBody country_idReq = RequestBody.create(MediaType.parse("text/plain"), country_id);
        RequestBody vipIDReq = RequestBody.create(MediaType.parse("text/plain"), vipID);
        RequestBody loggenInStReq = RequestBody.create(MediaType.parse("text/plain"), loggenInSt);
        RequestBody matchIDReq = RequestBody.create(MediaType.parse("text/plain"), matchID);


        Call<JsonObject> call = getApiReference(authtoken).createVisitor(authtoken, loggenInStReq, firstNameReg, middleNameReg,
                lastNameReq, suffixReg
                , emailStReg, phoneReg, dobStReg, created_byReq,
                parentIdReg, visitor_id_document_type_idReg, visitor_id_numberReg, checkInReg,
                enteredByReg, visiting_individualIdReg, visiting_dept_IdReg,
                vms_visitor_entering_from_idReg, vms_visitor_purpose_idReq, vms_visitor_other_purposeReq, body,
                clientIdReg, visitor_titleReq, SOCheckFirstReq, SOCheckSecondReq, country_idReq, vipIDReq, matchIDReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void createVisitor(final int tag, String authtoken, String firstNameSt, String lastNameSt, String middleName, String suffix,
                              String emailSt, String phoneSt, String dobSt, String created_by, String parentId,
                              String visitor_id_document_type_id, String visitor_id_number, String checkIn,
                              String enteredBy, String visiting_individualId, String visiting_dept_Id,
                              String vms_visitor_entering_from_id, String vms_visitor_purpose_id,
                              String vms_visitor_other_purpose, String profile_pic, String clientId, String visitor_title,
                              String SOCheckFirst, String SOCheckSecond, String country_id, String vipID, String loggenInSt, String matchID, final APIServerResponse apiServerResponse) {

        RequestBody firstNameReg = RequestBody.create(MediaType.parse("text/plain"), firstNameSt);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), lastNameSt);
        RequestBody middleNameReg = RequestBody.create(MediaType.parse("text/plain"), middleName);
        RequestBody suffixReg = RequestBody.create(MediaType.parse("text/plain"), suffix);
        RequestBody emailStReg = RequestBody.create(MediaType.parse("text/plain"), emailSt);
        if (phoneSt.length() == 1) {
            phoneSt = "";
        }
        RequestBody phoneReg = RequestBody.create(MediaType.parse("text/plain"), phoneSt);
        if (dobSt == null) {
            dobSt = "";
        }
        RequestBody dobStReg = RequestBody.create(MediaType.parse("text/plain"), dobSt);
        RequestBody created_byReq = RequestBody.create(MediaType.parse("text/plain"), created_by);
        RequestBody parentIdReg = RequestBody.create(MediaType.parse("text/plain"), parentId);

        RequestBody visitor_id_document_type_idReg = RequestBody.create(MediaType.parse("text/plain"), visitor_id_document_type_id);
        RequestBody visitor_id_numberReg = RequestBody.create(MediaType.parse("text/plain"), visitor_id_number);
        RequestBody checkInReg = RequestBody.create(MediaType.parse("text/plain"), checkIn);
        RequestBody enteredByReg = RequestBody.create(MediaType.parse("text/plain"), enteredBy);
        RequestBody visiting_individualIdReg = RequestBody.create(MediaType.parse("text/plain"), visiting_individualId);
        RequestBody visiting_dept_IdReg = RequestBody.create(MediaType.parse("text/plain"), visiting_dept_Id);
        RequestBody vms_visitor_entering_from_idReg = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_entering_from_id);
        RequestBody vms_visitor_purpose_idReq = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_purpose_id);
        RequestBody vms_visitor_other_purposeReq = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_other_purpose);

        if (profile_pic == null || profile_pic.equalsIgnoreCase("")) {
            profile_pic = "";
        } else {
            requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profile_pic));
            body = MultipartBody.Part.createFormData("profile_pic", "profile.png", requestBody);
        }
        RequestBody profilePicReq = RequestBody.create(MediaType.parse("text/plain"), profile_pic);

        RequestBody clientIdReg = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody visitor_titleReq = RequestBody.create(MediaType.parse("text/plain"), visitor_title);
        RequestBody SOCheckFirstReq = RequestBody.create(MediaType.parse("text/plain"), SOCheckFirst);

        RequestBody SOCheckSecondReq = RequestBody.create(MediaType.parse("text/plain"), SOCheckSecond);
        RequestBody country_idReq = RequestBody.create(MediaType.parse("text/plain"), country_id);
        RequestBody vipIDReq = RequestBody.create(MediaType.parse("text/plain"), vipID);
        RequestBody loggenInStReq = RequestBody.create(MediaType.parse("text/plain"), loggenInSt);
        RequestBody matchIDReq = RequestBody.create(MediaType.parse("text/plain"), matchID);
//createVisitor
        /* /*{"id":9685,"visitor_suffix":"","first_name":"Myuser","middle_name":"","last_name":"Lastname","ssn":"0","dob":"","phone":"","email":"","profile_pic":"","profile_pic_web":"https://silverstaging.s3.amazonaws.com/uploads/visitors/i1517219664574.jpg","bar_code_image":"","is_deleted":0,"parent_id":670,"log_id":14511,"is_vip":0,"is_qb":0,"is_watch":0,"watch_list_reason":"MANUAL","check_in":"2018-01-29 15:24:15","check_out":"0000-00-00 00:00:00","entered_by":"zebu.staff1","entered_by_firstname":"Zebu","entered_by_lastname":"Staff","updated_by":"zebu.staff1","visiting_individual_id":"","visiting_individual":"","visiting_individual_firstname":"","visiting_individual_lastname":"","visiting_department_id":"","department_name":"","visit_purpose":"Other","purpose_other":"","visitor_id_document_types_id":0,"visitor_id_document_type":"No ID Available","visitor_id_number":"N/A","vms_visitor_entering_from_id":"5","entery_place":"DLF Gate 1","country_id":"","country_code":"","country_name":"","status":"OK","visitor_id":9685,"visitor_logs_id":14511,"message":"Visitor added","isExistingVisitor":0,"existingVisitorInfo":0}*/
        if (profile_pic != null || profile_pic.equalsIgnoreCase("")) {
            Call<JsonObject> call = getApiReference(authtoken).createVisitor(authtoken, loggenInStReq, firstNameReg, middleNameReg,
                    lastNameReq, suffixReg
                    , emailStReg, phoneReg, dobStReg, created_byReq,
                    parentIdReg, visitor_id_document_type_idReg, visitor_id_numberReg, checkInReg,
                    enteredByReg, visiting_individualIdReg, visiting_dept_IdReg,
                    vms_visitor_entering_from_idReg, vms_visitor_purpose_idReq, vms_visitor_other_purposeReq, body,
                    clientIdReg, visitor_titleReq, SOCheckFirstReq, SOCheckSecondReq, country_idReq, vipIDReq, matchIDReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {
            Call<JsonObject> call = getApiReference(authtoken).createVisitorWithoutImage(authtoken, loggenInStReq, firstNameReg, middleNameReg,
                    lastNameReq, suffixReg
                    , emailStReg, phoneReg, dobStReg, created_byReq,
                    parentIdReg, visitor_id_document_type_idReg, visitor_id_numberReg, checkInReg,
                    enteredByReg, visiting_individualIdReg, visiting_dept_IdReg,
                    vms_visitor_entering_from_idReg, vms_visitor_purpose_idReq, vms_visitor_other_purposeReq, profilePicReq,
                    clientIdReg, visitor_titleReq, SOCheckFirstReq, SOCheckSecondReq, country_idReq, vipIDReq, matchIDReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }

    }

    public void setQb(final int tag, String authtoken, String user_id, String visitor_id, String is_vip, String is_qb, String is_se, String log_id, String loggedin_user, String email, String phone, String profile_pic, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), user_id);
        RequestBody visitorIdReq = RequestBody.create(MediaType.parse("text/plain"), visitor_id);
        RequestBody vipIdReg = RequestBody.create(MediaType.parse("text/plain"), is_vip);
        RequestBody isQBReg = RequestBody.create(MediaType.parse("text/plain"), is_qb);
        RequestBody isSEReg = RequestBody.create(MediaType.parse("text/plain"), is_se);
        RequestBody logIDReg = RequestBody.create(MediaType.parse("text/plain"), log_id);
        RequestBody loggedInReg = RequestBody.create(MediaType.parse("text/plain"), loggedin_user);
        RequestBody emailReg = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), phone);

        if (profile_pic == null || profile_pic.equalsIgnoreCase("")) {
            profile_pic = "";
        } else {
            requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profile_pic));
            body = MultipartBody.Part.createFormData("profile_pic", "profile.png", requestBody);
        }


        Call<JsonObject> call = getApiReference(authtoken).setQb(authtoken, userIdReg, visitorIdReq, vipIdReg, isQBReg, isSEReg, logIDReg, loggedInReg, emailReg, phoneReq, body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public String download(final int tag, String url, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> call = getApiReference("").downloadFileWithDynamicUrlSync(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        // todo change the file location/name according to your needs
                        format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                        String myDate = format.format(new Date());
                        futureStudioIconFile = new File(Environment.getExternalStorageDirectory() + File.separator + "myfile" + myDate + ".png");
                        InputStream inputStream = null;
                        OutputStream outputStream = null;
                        try {
                            byte[] fileReader = new byte[4096];
                            long fileSize = response.body().contentLength();
                            long fileSizeDownloaded = 0;

                            inputStream = response.body().byteStream();
                            outputStream = new FileOutputStream(futureStudioIconFile);
                            while (true) {
                                int read = inputStream.read(fileReader);

                                if (read == -1) {
                                    break;
                                }

                                outputStream.write(fileReader, 0, read);

                                fileSizeDownloaded += read;
                            }

                            outputStream.flush();
                            fileName = futureStudioIconFile.getAbsolutePath();
                            apiServerResponse.onSuccess(tag, response);
                        } catch (IOException e) {

                        } finally {
                            if (inputStream != null) {
                                inputStream.close();
                            }

                            if (outputStream != null) {
                                outputStream.close();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("Server contact failed", "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
        //return fileName;
        return futureStudioIconFile + "";
    }

    public void downloadImage(final int tag, final Activity activity, String url, InetAddress address, final APIServerResponse apiServerResponse) {
        this.activity = activity;
        ipAddress = address.getHostAddress();
        try {
            urlPath = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String tDir = System.getProperty("java.io.tmpdir");
        String path = tDir + "tmp" + ".html";
        final File file = new File(path);
        file.deleteOnExit();


        finalUrl = urlPath;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FileUtils.copyURLToFile(finalUrl, file);
                    Socket client = new Socket(ipAddress, 9100);

                    byte[] mybytearray = new byte[(int) file.length()]; //create a byte array to file

                    fileInputStream = new FileInputStream(file);
                    bufferedInputStream = new BufferedInputStream(fileInputStream);

                    bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file

                    outputStream = client.getOutputStream();

                    outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
                    outputStream.flush();
                    bufferedInputStream.close();
                    outputStream.close();
                    client.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }

    public void printFile(String path) {

        final File file = new File(path);
        file.deleteOnExit();

        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Socket client = new Socket(ipAddress, 9100);
                    PrintWriter oStream = new PrintWriter(client.getOutputStream());
                    oStream.println("HI,test from Android Device");
                    oStream.println("\n\n\n");
                    oStream.close();
                    client.close();
                  /*  byte[] mybytearray = new byte[(int) file.length()]; //create a byte array to file

                    fileInputStream = new FileInputStream(file);
                    bufferedInputStream = new BufferedInputStream(fileInputStream);

                    bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file

                    outputStream = client.getOutputStream();

                    outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
                    outputStream.flush();
                    bufferedInputStream.close();
                    outputStream.close();
                    client.close();
                    // Get a PrintManager instance
                    PrintManager printManager = (PrintManager) activity.getSystemService(Context.PRINT_SERVICE);
                    printManager.print("jhdcsd", new MyPrintDocumentAdapter(activity),
                            null);
                    //  text.setText("File Sent");
*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    public void getAllEvents(final int tag, String authtoken, String user_id, String clientID, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), user_id);
        RequestBody clientIdReq = RequestBody.create(MediaType.parse("text/plain"), clientID);
        Call<EventResponseObject> call = getApiReference(authtoken).getAllEvents(authtoken, userIdReg, clientIdReq);
        call.enqueue(new Callback<EventResponseObject>() {
            @Override
            public void onResponse(Call<EventResponseObject> call, Response<EventResponseObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<EventResponseObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void createEventGuest(final int tag, String authtoken, String event_id, String firstName,
                                 String lastName, String title, String companyName,
                                 String phone, String email,
                                 String rsvp, String logId, final APIServerResponse apiServerResponse) {
        RequestBody eventIDReg = RequestBody.create(MediaType.parse("text/plain"), event_id);
        RequestBody firstNameReq = RequestBody.create(MediaType.parse("text/plain"), firstName);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), lastName);
        RequestBody titleReq = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody companyNameReq = RequestBody.create(MediaType.parse("text/plain"), companyName);
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody rsvpReq = RequestBody.create(MediaType.parse("text/plain"), rsvp);
        RequestBody logIdReq = RequestBody.create(MediaType.parse("text/plain"), logId);

        Call<JsonObject> call = getApiReference(authtoken).createEventGuest(authtoken, eventIDReg, firstNameReq, lastNameReq, titleReq, companyNameReq, phoneReq, emailReq,
                rsvpReq, logIdReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void getPrinterSetting(final int tag, String authtoken, String clientId, final APIServerResponse apiServerResponse) {
        RequestBody clientIDReg = RequestBody.create(MediaType.parse("text/plain"), clientId);
        Call<PrinterSettingResponse> call = getApiReference(authtoken).getPrinterSetting(authtoken, clientIDReg);
        call.enqueue(new Callback<PrinterSettingResponse>() {
            @Override
            public void onResponse(Call<PrinterSettingResponse> call, Response<PrinterSettingResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PrinterSettingResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void stopWatchVisitor(final int tag, String authtoken, String userID, String parent_id, String clients_id,
                                 String location, String matchName, String matchCount, String offendType,
                                 String userName, String visitorName, String matchImage, String first_name,
                                 String last_name, String email, String phone, String dob, String profile_pic, String matchId, final APIServerResponse apiServerResponse) {

        RequestBody userIDReg = RequestBody.create(MediaType.parse("text/plain"), userID);
        RequestBody parent_idReg = RequestBody.create(MediaType.parse("text/plain"), parent_id);
        RequestBody clients_idReg = RequestBody.create(MediaType.parse("text/plain"), clients_id);
        RequestBody locationReg = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody matchNameReg = RequestBody.create(MediaType.parse("text/plain"), matchName);
        RequestBody matchCountReg = RequestBody.create(MediaType.parse("text/plain"), matchCount);
        RequestBody offendTypeReg = RequestBody.create(MediaType.parse("text/plain"), offendType);
        RequestBody userNameReg = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody visitorNameReg = RequestBody.create(MediaType.parse("text/plain"), visitorName);

        RequestBody matchImageReg = RequestBody.create(MediaType.parse("text/plain"), matchImage);
        RequestBody first_nameReg = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody last_nameReg = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody emailReg = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody phoneReg = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody dobReg = RequestBody.create(MediaType.parse("text/plain"), dob);
        RequestBody profileReg = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody matchIdReg = RequestBody.create(MediaType.parse("text/plain"), matchId);
        Call<StatusMessageModel> call = getApiReference(authtoken).stopVisitorWithoutProfile(authtoken, userIDReg, parent_idReg, clients_idReg, locationReg, matchNameReg, matchCountReg
                , offendTypeReg, userNameReg, visitorNameReg, matchImageReg, first_nameReg, last_nameReg, emailReg, phoneReg,
                dobReg, matchIdReg, profile_pic);
        call.enqueue(new Callback<StatusMessageModel>() {
            @Override
            public void onResponse(Call<StatusMessageModel> call, Response<StatusMessageModel> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<StatusMessageModel> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void stopVisitor(final int tag, String authtoken, String userID, String parent_id, String clients_id,
                            String location, String matchName, String matchCount, String offendType,
                            String userName, String visitorName, String matchImage, String first_name,
                            String last_name, String email, String phone, String dob, String profile_pic, String matchId, final APIServerResponse apiServerResponse) {

        if (profile_pic != null) {
            if (!profile_pic.equalsIgnoreCase("")) {
                requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profile_pic));
                body = MultipartBody.Part.createFormData("profile_pic", "profile.png", requestBody);
            }
        }

        RequestBody userIDReg = RequestBody.create(MediaType.parse("text/plain"), userID);
        RequestBody parent_idReg = RequestBody.create(MediaType.parse("text/plain"), parent_id);
        RequestBody clients_idReg = RequestBody.create(MediaType.parse("text/plain"), clients_id);
        RequestBody locationReg = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody matchNameReg = RequestBody.create(MediaType.parse("text/plain"), matchName);
        RequestBody matchCountReg = RequestBody.create(MediaType.parse("text/plain"), matchCount);
        RequestBody offendTypeReg = RequestBody.create(MediaType.parse("text/plain"), offendType);
        RequestBody userNameReg = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody visitorNameReg = RequestBody.create(MediaType.parse("text/plain"), visitorName);
        if (matchImage == null) {
            matchImage = "";
        }


        RequestBody first_nameReg = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody last_nameReg = RequestBody.create(MediaType.parse("text/plain"), last_name);
        if (email == null) {
            email = "";
        }
        RequestBody emailReg = RequestBody.create(MediaType.parse("text/plain"), email);
        if (phone == null) {
            phone = "";
        }
        RequestBody phoneReg = RequestBody.create(MediaType.parse("text/plain"), phone);
        if (dob == null) {
            dob = "";
        }
        RequestBody dobReg = RequestBody.create(MediaType.parse("text/plain"), dob);

        if (matchId == null) {
            matchId = "";
        }
        RequestBody matchIdReg = RequestBody.create(MediaType.parse("text/plain"), matchId);
        RequestBody matchImageReg = RequestBody.create(MediaType.parse("text/plain"), matchImage);

        if (profile_pic == null || profile_pic.equalsIgnoreCase("")) {
            Call<StatusMessageModel> call = getApiReference(authtoken).stopVisitorWithoutProfile(authtoken, userIDReg, parent_idReg, clients_idReg, locationReg, matchNameReg, matchCountReg
                    , offendTypeReg, userNameReg, visitorNameReg, matchImageReg, first_nameReg, last_nameReg, emailReg, phoneReg,
                    dobReg, matchIdReg, "");
            call.enqueue(new Callback<StatusMessageModel>() {
                @Override
                public void onResponse(Call<StatusMessageModel> call, Response<StatusMessageModel> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<StatusMessageModel> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });

        } else {
            Call<StatusMessageModel> call = apiReference.stopVisitor(authtoken, userIDReg, parent_idReg, clients_idReg, locationReg, matchNameReg, matchCountReg
                    , offendTypeReg, userNameReg, visitorNameReg, matchImage, first_nameReg, last_nameReg, emailReg, phoneReg,
                    dobReg, matchIdReg, body);
            call.enqueue(new Callback<StatusMessageModel>() {
                @Override
                public void onResponse(Call<StatusMessageModel> call, Response<StatusMessageModel> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<StatusMessageModel> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });
        }

// @Part MultipartBody.Part profile_pic
    }

    public void getScanEventBudget(final int tag, String authtoken, String eventId, String userId, final APIServerResponse apiServerResponse) {
        RequestBody eventIdReq = RequestBody.create(MediaType.parse("text/plain"), eventId);
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), eventId);
        //
        Call<JsonObject> call = getApiReference(authtoken).getEventBudgetDetail(authtoken, eventId/*,userIdReq*/);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getEventBudgetDetail(final int tag, String authtoken, String loginedId, String eventGuestID, String statusSt, final APIServerResponse apiServerResponse) {
        RequestBody loginedIdReq = RequestBody.create(MediaType.parse("text/plain"), loginedId);
        RequestBody eventGuestIDReq = RequestBody.create(MediaType.parse("text/plain"), eventGuestID);
        RequestBody statusStReq = RequestBody.create(MediaType.parse("text/plain"), statusSt);
        Call<JsonObject> call = getApiReference(authtoken).markAttendence(authtoken, loginedIdReq, eventGuestIDReq, statusStReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void createBadge(final int tag, String authtoken, String userId, String visitorID,
                            String isVIP, String isQb, String isSE, String logedId,
                            String profilePic, String email, String phone,
                            final APIServerResponse apiServerResponse) {
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody visitorIdReq = RequestBody.create(MediaType.parse("text/plain"), visitorID);
        RequestBody isQbReq = RequestBody.create(MediaType.parse("text/plain"), isQb);
        RequestBody isSEReq = RequestBody.create(MediaType.parse("text/plain"), isSE);
        RequestBody isVIPReq = RequestBody.create(MediaType.parse("text/plain"), isVIP);
        RequestBody logedIdReq = RequestBody.create(MediaType.parse("text/plain"), logedId);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), phone);
        if (!profilePic.equalsIgnoreCase("")) {
            requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profilePic));
            body = MultipartBody.Part.createFormData("profile_pic", "profile_pic.png", requestBody);
            Call<JsonObject> call = getApiReference(authtoken).createBadgeWithImage(authtoken, userIdReq, visitorIdReq, isVIPReq, isQbReq, isSEReq, logedIdReq, body, emailReq, phoneReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });
        } else {
            RequestBody profileReq = RequestBody.create(MediaType.parse("text/plain"), profilePic);
            Call<JsonObject> call = apiReference.createBadgeWithoutImage(authtoken, userIdReq, visitorIdReq, isVIPReq, isQbReq, isSEReq, logedIdReq, profileReq, emailReq, phoneReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });
        }

    }

    public void getAllVisitorApi(final int tag, String authToken, String loggined_id, String client_id, String parent_id, String stringSearch, final APIServerResponse apiServerResponse) {
        RequestBody loggedingReq = RequestBody.create(MediaType.parse("text/plain"), loggined_id);
        RequestBody parentReq = RequestBody.create(MediaType.parse("text/plain"), parent_id);
        RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);
        RequestBody searchedStReq = RequestBody.create(MediaType.parse("text/plain"), stringSearch);
        Map<String, String> params = new HashMap<>();
        params.put("loggedin_user", loggined_id);
        params.put("parent_id", parent_id);
        params.put("clients_id", client_id);

        Call<JsonObject> call = getApiReference(authToken).getSearchedWatcherList(authToken, loggedingReq, parentReq, clientReq, searchedStReq/*params*/);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getSearchedWatchListVisitor(final int tag, String authToken, String loggined_id, String client_id, String parent_id, String stringSearch, final APIServerResponse apiServerResponse) {
        RequestBody loggedingReq = RequestBody.create(MediaType.parse("text/plain"), loggined_id);
        RequestBody parentReq = RequestBody.create(MediaType.parse("text/plain"), parent_id);
        RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);
        RequestBody searchedStReq = RequestBody.create(MediaType.parse("text/plain"), stringSearch);
        Map<String, String> params = new HashMap<>();
        params.put("loggedin_user", loggined_id);
        params.put("parent_id", parent_id);
        params.put("clients_id", client_id);

        Call<JsonObject> call = getApiReference(authToken).getSearchedWatcherList(authToken, loggedingReq, parentReq, clientReq, searchedStReq/*params*/);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    //getCautionaryDetail
    public void getCautionaryDetail(final int tag, String authToken, String client_id, final APIServerResponse apiServerResponse) {

        //   RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);
        Call<JsonObject> call = apiReference.getCautionaryDetail(authToken, client_id, "CREATE");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void setCautionaryClear(final int tag, String authToken, String client_id, final APIServerResponse apiServerResponse) {

        //   RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);
        Call<JsonObject> call = apiReference.setCautionaryClear(authToken, client_id, "CLEAR");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void verifyCautionaryPassword(final int tag, VerifyCautioneryPassRequestModel model, final APIServerResponse apiServerResponse) {
        Call<JsonObject> call = apiReference.verifyCautionaryPassword(model);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void clearAlert(final int tag, VerifyCautioneryPassRequestModel model, final APIServerResponse apiServerResponse) {
        Call<JsonObject> call = apiReference.clearAlert(model);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void callAlarm(final int tag, String authToken, String loggined_id, String client_id, String parent_id, String receiptType, String alertTYpe, String username, String message, String cautionaryCode, String latitude, String longitude, String location, String additionalInfo, final APIServerResponse apiServerResponse) {

        RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);
        RequestBody loggined_idReq = RequestBody.create(MediaType.parse("text/plain"), loggined_id);
        RequestBody parent_idtReq = RequestBody.create(MediaType.parse("text/plain"), parent_id);

        RequestBody receiptTypeReq = RequestBody.create(MediaType.parse("text/plain"), receiptType);
        RequestBody locationReq = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody alertTYpeReq = RequestBody.create(MediaType.parse("text/plain"), alertTYpe);
        RequestBody usernameReq = RequestBody.create(MediaType.parse("text/plain"), username);
        RequestBody additionalInfoReq = RequestBody.create(MediaType.parse("text/plain"), additionalInfo);
        RequestBody messageReq = RequestBody.create(MediaType.parse("text/plain"), message);
        RequestBody cautionaryCodeReq = RequestBody.create(MediaType.parse("text/plain"), cautionaryCode);

        RequestBody latReq = RequestBody.create(MediaType.parse("text/plain"), latitude);
        RequestBody longReq = RequestBody.create(MediaType.parse("text/plain"), longitude);

        if (location == null || location.equals("")) {
            Call<JsonObject> call = apiReference.callAlarmWithoutLocation(authToken, loggined_idReq, loggined_idReq, parent_idtReq, clientReq,
                    alertTYpeReq, receiptTypeReq, messageReq, additionalInfoReq, usernameReq, cautionaryCodeReq
            );
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    t.printStackTrace();
                    apiServerResponse.onError(tag, t);
                }
            });
        } else {

            Call<JsonObject> call = apiReference.callAlarm(authToken, loggined_idReq, loggined_idReq, parent_idtReq, clientReq,
                    alertTYpeReq, receiptTypeReq, locationReq, messageReq, additionalInfoReq, usernameReq, cautionaryCodeReq,
                    latReq, longReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    t.printStackTrace();
                    apiServerResponse.onError(tag, t);
                }
            });
        }
    }

    public void getAllLocations(final int tag, String authToken, String client_id, final APIServerResponse apiServerResponse) {
        Call<JsonObject> call = apiReference.getAllLocations(authToken, client_id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllLogsAlarm(final int tag, String authToken, String client_id, final APIServerResponse apiServerResponse) {
        RequestBody client_idReq = RequestBody.create(MediaType.parse("text/plain"), client_id);
        Call<JsonObject> call = apiReference.getAllAlamrsLogs(authToken, client_idReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllVisitor(final int tag, String authToken, GetAllVisitorRequestModel requestModel, final APIServerResponse apiServerResponse) {
        Call<GetAllVisitorResponseModel> call = getApiReference(authToken).getAllVisitor(requestModel);
        call.enqueue(new Callback<GetAllVisitorResponseModel>() {
            @Override
            public void onResponse(Call<GetAllVisitorResponseModel> call, Response<GetAllVisitorResponseModel> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GetAllVisitorResponseModel> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllListARP(final int tag, String authToken, String dispatchID, String status, final APIServerResponse apiServerResponse) {
        RequestBody dispatchIDReq = RequestBody.create(MediaType.parse("text/plain"), dispatchID);
        RequestBody statusReq = RequestBody.create(MediaType.parse("text/plain"), status);
        Call<JsonObject> call = getApiReference(authToken).getAllARPList(authToken, dispatchIDReq, statusReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void updateUserInformation(final int tag, String authToken, String firstNameSt, String lastNameSt, String middleName,
                                      String suffix,
                                      String emailSt, String phoneSt, String dobSt, String created_by,
                                      String visitor_id_document_type_id, String visitor_id_number, String checkIn,
                                      String visiting_individualId, String visiting_dept_Id,
                                      String vms_visitor_entering_from_id, String vms_visitor_purpose_id,
                                      String profile_pic, String visitor_title,
                                      String country_id, String visitorID, String matchId, final APIServerResponse apiServerResponse) {


        RequestBody emailStReg = RequestBody.create(MediaType.parse("text/plain"), emailSt);
        RequestBody phoneReg = RequestBody.create(MediaType.parse("text/plain"), phoneSt);
        RequestBody visitor_id_document_type_idReg = RequestBody.create(MediaType.parse("text/plain"), visitor_id_document_type_id);
        RequestBody visitor_id_numberReg = RequestBody.create(MediaType.parse("text/plain"), visitor_id_number);
        RequestBody checkInReg = RequestBody.create(MediaType.parse("text/plain"), checkIn);
        RequestBody visiting_individualIdReg = RequestBody.create(MediaType.parse("text/plain"), visiting_individualId);
        RequestBody visiting_dept_IdReg = RequestBody.create(MediaType.parse("text/plain"), visiting_dept_Id);
        RequestBody vms_visitor_entering_from_idReg = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_entering_from_id);
        RequestBody vms_visitor_purpose_idReq = RequestBody.create(MediaType.parse("text/plain"), vms_visitor_purpose_id);
        RequestBody visitor_titleReq = RequestBody.create(MediaType.parse("text/plain"), visitor_title);
        RequestBody country_idReq = RequestBody.create(MediaType.parse("text/plain"), country_id);
        RequestBody updatedByReq = RequestBody.create(MediaType.parse("text/plain"), created_by);
        RequestBody firstNameReg = RequestBody.create(MediaType.parse("text/plain"), firstNameSt);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), lastNameSt);
        RequestBody middleNameReg = RequestBody.create(MediaType.parse("text/plain"), middleName);
        RequestBody suffixReg = RequestBody.create(MediaType.parse("text/plain"), suffix);
        RequestBody dobStReg = RequestBody.create(MediaType.parse("text/plain"), dobSt);
        RequestBody visitorIDReg = RequestBody.create(MediaType.parse("text/plain"), visitorID);
        //visitorID
        RequestBody logReq = RequestBody.create(MediaType.parse("text/plain"), matchId);
        RequestBody profilePicReq = RequestBody.create(MediaType.parse("text/plain"), profile_pic);

        if (profile_pic.equalsIgnoreCase("") || profile_pic.equalsIgnoreCase(null)) {

            Call<JsonObject> call = getApiReference(authToken).updateUserProfile(authToken, updatedByReq, emailStReg, phoneReg, visitor_id_document_type_idReg, visitor_id_numberReg
                    , checkInReg, visiting_individualIdReg, visiting_dept_IdReg, vms_visitor_entering_from_idReg, vms_visitor_purpose_idReq,
                    visitor_titleReq, country_idReq, updatedByReq, firstNameReg, lastNameReq, middleNameReg, suffixReg
                    , dobStReg, visitorIDReg, logReq, profilePicReq);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });
        } else {
            requestBody = RequestBody.create(MediaType.parse("image/png"), new File(profile_pic));
            body = MultipartBody.Part.createFormData("profile_pic", "profile.png", requestBody);

            Call<JsonObject> call = getApiReference(authToken).updateUserProfileWithImage(authToken, updatedByReq, emailStReg, phoneReg, visitor_id_document_type_idReg, visitor_id_numberReg
                    , checkInReg, visiting_individualIdReg, visiting_dept_IdReg, vms_visitor_entering_from_idReg, vms_visitor_purpose_idReq,
                    visitor_titleReq, country_idReq, updatedByReq, firstNameReg, lastNameReq, middleNameReg, suffixReg
                    , dobStReg, visitorIDReg, logReq, body);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });
        }
    }

    public void callLockDownAlarm(final int tag, String authToken, String userId, String parentId, String clientId, String alarmType, String message, String codeSt, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody parentIdReg = RequestBody.create(MediaType.parse("text/plain"), parentId);
        RequestBody clientIdReg = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody alarmTypeReg = RequestBody.create(MediaType.parse("text/plain"), alarmType);
        RequestBody messageReg = RequestBody.create(MediaType.parse("text/plain"), message);
        RequestBody codeStReg = RequestBody.create(MediaType.parse("text/plain"), codeSt);
        Call<JsonObject> call = getApiReference(authToken).callLockDownEmergencyAlarm(authToken, userIdReg, parentIdReg, clientIdReg, alarmTypeReg, messageReg, codeStReg);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void callNonEmergenceyAlarm(final int tag, String authToken, String userId, String parentId, String clientId, String alarmType, String message, String code, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody parentIdReg = RequestBody.create(MediaType.parse("text/plain"), parentId);
        RequestBody clientIdReg = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody alarmTypeReg = RequestBody.create(MediaType.parse("text/plain"), alarmType);
        RequestBody messageReg = RequestBody.create(MediaType.parse("text/plain"), message);
        RequestBody codeReg = RequestBody.create(MediaType.parse("text/plain"), code);
        Call<JsonObject> call = getApiReference(authToken).callNonEmergencyAlarm(authToken, userIdReg, parentIdReg, clientIdReg, alarmTypeReg, messageReg, codeReg);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    //
    public void getAsignToByDetail(final int tag, String authToken, String userId, String parent_id, String client_id, final APIServerResponse apiServerResponse) {

        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody parentReq = RequestBody.create(MediaType.parse("text/plain"), parent_id);
        RequestBody clientReq = RequestBody.create(MediaType.parse("text/plain"), client_id);

        Call<JsonObject> call = getApiReference(authToken).get_asign_to_asign_to(authToken, userIdReq, parentReq, clientReq/*params*/);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void updateWatchList(final int tag, String authToken, String userID, String visitorId, String status, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), userID);
        RequestBody visitorIdReg = RequestBody.create(MediaType.parse("text/plain"), visitorId);
        RequestBody statusReq = RequestBody.create(MediaType.parse("text/plain"), status);


        Call<JsonObject> call = getApiReference(authToken).updateWatchList(authToken, userIdReg, visitorIdReg, statusReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
                t.printStackTrace();
            }
        });
    }

    public void updateVipList(final int tag, String authToken, String userID, String visitorId, String status, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), userID);
        RequestBody visitorIdReg = RequestBody.create(MediaType.parse("text/plain"), visitorId);
        RequestBody statusReq = RequestBody.create(MediaType.parse("text/plain"), status);


        Call<JsonObject> call = getApiReference(authToken).updateVipList(authToken, userIdReg, visitorIdReg, statusReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void checkoutVisitor(final int tag, String authToken, String userID, String visitorId, final APIServerResponse apiServerResponse) {
        RequestBody userIdReg = RequestBody.create(MediaType.parse("text/plain"), userID);
        RequestBody visitorIdReg = RequestBody.create(MediaType.parse("text/plain"), visitorId);

        Call<JsonObject> call = getApiReference(authToken).checkOutVisitor(authToken, userIdReg, visitorIdReg);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void acceptRequest(final int tag, String authToken, String alertID, String status, String userName, String userID, final APIServerResponse apiServerResponse) {
        RequestBody alertIDReg = RequestBody.create(MediaType.parse("text/plain"), alertID);
        RequestBody statusReg = RequestBody.create(MediaType.parse("text/plain"), status);

        RequestBody userNameReg = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody userIDReg = RequestBody.create(MediaType.parse("text/plain"), userID);

        Call<JsonObject> call = getApiReference(authToken).acceptReject(authToken, alertIDReg, statusReg, userNameReg, userIDReg);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void cancleAlertLog(final int tag, String authToken, String userName, String clientId, String dispatchId, final APIServerResponse apiServerResponse) {
        RequestBody userNameReg = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody clientIdReg = RequestBody.create(MediaType.parse("text/plain"), clientId);
        RequestBody dispatchIdReg = RequestBody.create(MediaType.parse("text/plain"), dispatchId);

        Call<JsonObject> call = getApiReference(authToken).cancleAlertLog(authToken, clientIdReg, dispatchIdReg, userNameReg);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void eventCBCheck(final int tag, String authToken, String firstName, String lastName, String donSt, final APIServerResponse apiServerResponse) {
        RequestBody firstNameReg = RequestBody.create(MediaType.parse("text/plain"), firstName);
        RequestBody lastNameReg = RequestBody.create(MediaType.parse("text/plain"), lastName);
        RequestBody donStReg = RequestBody.create(MediaType.parse("text/plain"), donSt);

        Call<JsonObject> call = getApiReference(authToken).evenCbCheck(authToken, firstNameReg, lastNameReg, donStReg);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getDlDetailImage(final int tag, String authToken, String customerid, String parentId, String clientId, final APIServerResponse apiServerResponse) {
        RequestBody customeridReq = RequestBody.create(MediaType.parse("text/plain"), customerid);
        RequestBody parentIdReq = RequestBody.create(MediaType.parse("text/plain"), parentId);
        RequestBody clientIdReq = RequestBody.create(MediaType.parse("text/plain"), clientId);

        Call<JsonObject> call = getApiReference(authToken).getDlDetailImage(authToken, customeridReq, parentIdReq, clientIdReq);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                apiServerResponse.onError(tag, t);
            }
        });
    }
}
