package com.silvershield.printers;

import android.bluetooth.BluetoothAdapter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.bumptech.glide.Glide;
import com.silvershield.R;
import com.silvershield.adapter.ServicesAdapter;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.services.OnServiceSelectedListener;
import com.silvershield.utils.BaseActivity;

import java.net.InetAddress;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.ennova.zerxconf.ZeRXconf;
import it.ennova.zerxconf.model.NetworkServiceDiscoveryInfo;
import rx.Subscription;
import rx.functions.Action1;

public class AllWifiPrinters extends BaseActivity implements OnServiceSelectedListener, APIServerResponse {
    @BindView(R.id.resultList)
    RecyclerView servicesList;
    String downloadedFilePath = "";
    InetAddress ipAddress;
    Printer mPrinter;
    PrinterStatus mPrintResult;
    @BindView(R.id.ivImg)
    ImageView ivImg;
    private Subscription subscription;
    private ServicesAdapter adapter;
    private Bundle bundle;
    private String eventId;
    private String url = "http://staging.silvershield.com/visitorlogs/getbadge?id=12476";
    private Action1<NetworkServiceDiscoveryInfo> onNext = new Action1<NetworkServiceDiscoveryInfo>() {
        @Override
        public void call(NetworkServiceDiscoveryInfo serviceInfo) {
            if (serviceInfo.isAdded()) {
                adapter.addService(serviceInfo);
            } else {
                adapter.removeService(serviceInfo);
            }
        }
    };
    private Action1<Throwable> onError = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            Toast.makeText(AllWifiPrinters.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };
    private NetworkServiceDiscoveryInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_wifi_printers);
        ButterKnife.bind(this);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            eventId = bundle.getString("eventId");
            if (eventId != null) {
                url = "http://staging.silvershield.com/visitorlogs/getbadge?id=" + eventId;
            }
        }

        Glide.with(AllWifiPrinters.this).load(url).into(ivImg);

        try {
            subscription = ZeRXconf.startDiscovery(this, "_ipp._tcp.", true).subscribe(onNext, onError);
        } catch (Exception ez) {
            ez.printStackTrace();
        }
        adapter = new ServicesAdapter(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        servicesList.setLayoutManager(layoutManager);
        servicesList.setAdapter(adapter);
    }

    @Override
    public void onServiceSelected(NetworkServiceDiscoveryInfo info) {
        Toast.makeText(getApplicationContext(), "printer added", Toast.LENGTH_SHORT).show();
        connectPrinter(info);
    }

    public void connectPrinter(NetworkServiceDiscoveryInfo info) {
        this.info = info;
        PrinterThread printerThread = new PrinterThread();
        printerThread.start();
    }

    public void setPrinterInfo() {
        mPrinter = new Printer();
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        mPrinter.setBluetooth(bluetoothAdapter);
        PrinterInfo printerInfo = new PrinterInfo();
        printerInfo = mPrinter.getPrinterInfo();

        printerInfo.printerModel = PrinterInfo.Model.QL_820NWB;
        printerInfo.paperSize = PrinterInfo.PaperSize.CUSTOM;
        printerInfo.printMode = PrinterInfo.PrintMode.FIT_TO_PAGE;
        printerInfo.halftone = PrinterInfo.Halftone.THRESHOLD;
//        printerInfo.ipAddress = "172.16.0.139";
        printerInfo.ipAddress = info.getAddress().getHostAddress();
//        printerInfo.labelNameIndex = LabelInfo.QL700.valueOf("W50").ordinal();
//        printerInfo.labelNameIndex = LabelInfo.PT.W24.ordinal();
        printerInfo.labelNameIndex = 15;
        printerInfo.paperPosition = PrinterInfo.Align.CENTER;
        printerInfo.orientation = PrinterInfo.Orientation.LANDSCAPE;
        printerInfo.isAutoCut = true;
        printerInfo.isCutAtEnd = true;
//        printerInfo.macAddress = "D8:0F:99:36:CE:14";
        printerInfo.port = PrinterInfo.Port.NET;
        mPrinter.setPrinterInfo(printerInfo);

    }

    private class PrinterThread extends Thread {
        @Override
        public void run() {
            setPrinterInfo();

            // start message
//            Message msg = mHandle.obtainMessage(Common.MSG_PRINT_START);
//            mHandle.sendMessage(msg);

            mPrintResult = new PrinterStatus();
            mPrinter.startCommunication();

            Bitmap bitmap = null;
            try {
                bitmap = ((BitmapDrawable) ivImg.getDrawable()).getBitmap();
            } catch (Exception e) {
                e.printStackTrace();
            }
//            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.alarm);
            PrinterStatus printerStatus = new PrinterStatus();
            printerStatus = mPrinter.printImage(bitmap);
            Log.e("printer_status", printerStatus.errorCode.toString());

            mPrinter.endCommunication();

            // end message
//            mHandle.setResult(showResult());
//            mHandle.setBattery(getBattery());
//            msg = mHandle.obtainMessage(Common.MSG_PRINT_END);
//            mHandle.sendMessage(msg);
        }
    }

}
