package com.silvershield.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.silvershield.R;
import com.silvershield.activities.AlertMapActivity;
import com.silvershield.fcm.NotifyAlertDialog;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;
import com.silvershield.utils.BaseActivity;
import com.silvershield.utils.Constants;

import retrofit2.Response;

public class ActionReceiver extends BroadcastReceiver {

    Context context;
    private AlertDialog loadingDialog;
    String message="", category="",alertId="";
    double location_lng = 0.0, location_lat = 0.0;
    Intent intentActivity;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        String action = intent.getAction();

        message = intent.getStringExtra("message");
        category = intent.getStringExtra("category");
        alertId=intent.getStringExtra("alertId");

        if (intent.hasExtra("location_lng")) {
            location_lng =intent.getDoubleExtra("location_lng",0.0);
        }

        if(intent.hasExtra("location_lat")) {
            location_lat =intent.getDoubleExtra("location_lat",0.0);
        }
        if (Constants.YES_ACTION.equals(action)) {

            showLoading();
            if (category.equalsIgnoreCase("LOCATION_MAP")) {
                intentActivity = new Intent(context, AlertMapActivity.class);
            } else {
                intentActivity = new Intent(context, NotifyAlertDialog.class);
            }
           // Intent intentActivity = new Intent(context, NotifyAlertDialog.class);
            intentActivity.putExtra("NOTIFICATION_MESSAGE", "" + message);
            intentActivity.putExtra("category", category);
            intentActivity.putExtra("alertId",alertId);
            intentActivity.putExtra("location_lat", location_lat);
            intentActivity.putExtra("location_lng", location_lng);
            intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(intentActivity);

        } else if (Constants.STOP_ACTION.equals(action)) {
            Log.v("shuffTest", "Pressed NO");
            showLoading();
        }
        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);
    }

    public void hideLoading() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showLoading() {
        try {
            if (loadingDialog == null) {
                LayoutInflater factory = LayoutInflater.from(context);
                final View deleteDialogView = factory.inflate(R.layout.layout_progress, null);
                loadingDialog = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle).create();
                loadingDialog.setView(deleteDialogView);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.setCancelable(false);
            }
            loadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
