package com.silvershield.services;

import it.ennova.zerxconf.model.NetworkServiceDiscoveryInfo;

public interface OnServiceSelectedListener {

    void onServiceSelected(NetworkServiceDiscoveryInfo info);
}