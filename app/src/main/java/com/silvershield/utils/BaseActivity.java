package com.silvershield.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.silvershield.R;
import com.silvershield.activities.LoginActivity;
import com.silvershield.modal.SettingsModel;
import com.silvershield.networkManager.APIServerResponse;
import com.silvershield.networkManager.ServerAPI;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

/**
 * Created by satoti.garg on 11/21/2017.
 */

public class BaseActivity extends AppCompatActivity implements APIServerResponse {
    public static String viewVipList = "viewVipList";
    static DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //HH for hour of the day (0 - 23)
    static DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd h:mm a");
    private static String userID = "user_id";
    private static String emailId = "email_id";
    private static String userType = "user_type";
    private static String firstName = "first_name";
    private static String lastName = "last_name";
    private static String userName = "user_name";
    private static String profile_image = "profile_image";
    private static String profileImage = "";
    private static String phoneNumber = "phone_number";
    private static String dob = "dob";
    private static String gender = "gender";
    private static String city = "city";
    private static String loggedIn = "logged_in";
    private static String authToken = "auth_token";
    private static String firebaseToken = "firebase_token";
    private static String userRole = "user_role";
    private static String clientId = "client_id";
    private static String parntId = "parent_id";
    private static String clientName = "clientName";
    private static String alertGuardsButton = "alertGuardsButton";
    private static String alertStaffButton = "alertStaffButton";
    private static String alertAdminButton = "alertAdminButton";
    private static String alertGuardAndAdminButton = "alertGuardAndAdmin";
    private static String alertSoftLockdownButton = "alertSoftLockdownButton";
    private static String alertHardLockdownButton = "alertHardLockdown";
    private static String alertLockdownButton = "alertLockdownButton";
    private static String alertPoliceEmergencyButton = "alertPoliceEmergencyButton";
    private static String alertPoliceNonEmergencyButton = "alertPoliceNonEmergencyButton";
    private static String manageSelfSignupRequests = "manageSelfSignupRequests";
    private static String clearAlerts = "clearAlerts";
    private static String manageBadges = "manageBadges";
    private static String manageEvent = "manageEvents";
    private static String manageGuards = "manageGuards";
    private static String checkInVisitors = "checkInVisitors";
    private static String checkOutVisitors = "checkOutVisitors";
    private static String createQuickBadge = "createQuickBadge";
    private static String createVipList = "createVipList";
    private static String createWatchList = "createWatchList";
    private static String sexOffenderCheck = "sexOffenderCheck";
    private static String alertHardLockdownButtonDrill = "alertHardLockdownButtonDrill";
    private static String alertScreen = "alertScreen";
    private static String viewWatchList = "viewWatchList";
    private static String manageTeachers = "manageTeachers";
    private static String manageStaff = "manageStaff";
    private static String manageVisitors = "manageVisitors";
    private static String manageVideos = "manageVideos";
    private static String eventCbcCheck = "eventCriminalBackgroundCheck";
    private static String manageStudents = "manageStudents";
    private static String manageParents = "manageParents";
    private static String eventSexOffenderCheck = "eventSexOffenderCheck";
    private static String criminalBackgroundCheck = "criminalBackgroundCheck";
    private static String allowCallPolice = "allowCallPolice";
    private static String editPoliceContactNumbers = "editPoliceContactNumbers";
    private static String timesZonesArray = "timezones";
    private static String setting = "";
    ObscuredSharedPreferences prefs;
    String timeSt;
    private Snackbar snackbar;
    private AlertDialog loadingDialog;
    private SharedPreferences mPrefs, tokenPrefs;
    private String countryCode = "countryCode";

    public static String getDeviceID(Context context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    public static Date gmttoLocalDate(Date date) {

        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        return local;
    }

    public static String getCountryDialCode(Context context) {
        String countryId = "";
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                countryId = simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    countryId = networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {

        }
//        return getResources().getConfiguration().locale.getCountry();
        countryId = Locale.getDefault().getCountry();
        String CountryZipCode = "";

        String[] rl = context.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(countryId.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    public static String convertTiming(String checkedIn) {
        String timeConverted = "";
        try {
            Date d = f1.parse(checkedIn);
            timeConverted = f2.format(d).toLowerCase();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return timeConverted;
    }

    public String getDialCode(Context context, String countryCode) {
        String CountryID = "";
        String CountryZipCode = "";

        String[] rl = context.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(countryCode.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    public SharedPreferences getmPrefs() {
        return mPrefs;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = ObscuredSharedPreferences.getPrefs(this, getString(R.string.app_name), Context.MODE_PRIVATE);
        mPrefs = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        tokenPrefs = getSharedPreferences(Constants.DEVICE_ID, MODE_PRIVATE);
    }

    public SharedPreferences gettokenPrefs() {
        return tokenPrefs;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    public void setFcm(String fcmToken) {
        Constants.FCM_TOKEN = fcmToken;
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    /**
     * For hiding the loading
     */
    public void hideLoading() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * For Hiding the keyboard
     */
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * For Displaying the loading when a service is hit
     */
    public void showLoading() {
        try {
            if (loadingDialog == null) {
                LayoutInflater factory = LayoutInflater.from(this);
                final View deleteDialogView = factory.inflate(R.layout.layout_progress, null);
                loadingDialog = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).create();
                loadingDialog.setView(deleteDialogView);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.setCancelable(false);
            }
            loadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissDialog() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
                loadingDialog = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFirebaseToken() {

        if (gettokenPrefs().getString(Constants.FCM_TOKEN, "").equalsIgnoreCase("")) {
            getmPrefs().edit().putString(Constants.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken()).apply();

            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        } else {
            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        }
    }

    public void setFirebaseToken(String firebaseTokenValue) {
        prefs.edit().putString(firebaseToken, firebaseTokenValue).apply();
    }

    /**
     * function for fetching user ID
     *
     * @return user ID
     */
    public String getUserFcmToken() {
        return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
    }

    /**
     * For Displaying the snackBar
     *
     * @param msg message to display in the snackBar.
     */
    public void showSnack(String msg) {
        try {
            snackbar = Snackbar.make(getCurrentFocus(), msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }

    public void showSnack(String msg, View view) {
        try {
            snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }

    public boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,3})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Function to display Toast
     *
     * @param message  Message of the Toast
     * @param duration Duration of the Toast 0 for Short and 1 for Long Toast
     */
    public void showToast(String message, int duration) {
        if (message != null && message.length() > 0) {
            if (duration == 0) {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * For Displaying Output in LogCat
     *
     * @param message Message to display
     */
    public void showOutput(String message) {
        System.out.println("------------------------");
        System.out.println(message);
        System.out.println("------------------------");
    }

    /**
     * For Displaying the Log Output in Logcat
     *
     * @param tag     Tag of the Log
     * @param message Message of the Log
     */
    public void showLog(String tag, String message) {
        System.out.println("--------------------");
        Log.d(tag, message);
        System.out.println("--------------------");
    }

    /**
     * function to check if internet connection is active or not.
     *
     * @return true if connected to internet else false
     */
    public boolean isConnectedToInternet() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null;
    }

    public String getUserID() {
        return prefs.getString(userID, "");
    }

    public void setUserID(String userIDValue) {
        prefs.edit().putString(userID, userIDValue).apply();
    }

    public String getEmailID() {
        return prefs.getString(emailId, "");
    }

    public void setEmailID(String emailIdValue) {
        prefs.edit().putString(emailId, emailIdValue).apply();
    }

    public String getUserType() {
        return prefs.getString(userType, "");
    }

    public void setUserType(String userTypeValue) {
        prefs.edit().putString(userType, userTypeValue).apply();
    }

    public String getFirstName() {
        return prefs.getString(firstName, "");
    }

    public void setFirstName(String firstNameValue) {
        prefs.edit().putString(firstName, firstNameValue).apply();
    }

    public String getAlertHardLockdownButtonDrill() {
        return prefs.getString(alertHardLockdownButtonDrill, "");
    }

    public void setAlertHardLockdownButtonDrill(String value) {
        prefs.edit().putString(alertHardLockdownButtonDrill, value).apply();
    }

    public String getAlertScreen() {
        return prefs.getString(alertScreen, "");
    }

    public void setAlertScreen(String value) {
        prefs.edit().putString(alertScreen, value).apply();
    }

    public String getViewWatchList() {
        return prefs.getString(viewWatchList, "");
    }

    public void setViewWatchList(String value) {
        prefs.edit().putString(viewWatchList, value).apply();
    }

    public String getViewVipList() {
        return prefs.getString(viewVipList, "");
    }

    public void setViewVipList(String value) {
        prefs.edit().putString(viewVipList, value).apply();
    }

    public String getLastName() {
        return prefs.getString(lastName, "");
    }

    public void setLastName(String lastNameValue) {
        prefs.edit().putString(lastName, lastNameValue).apply();
    }

    public String getUserName() {
        return prefs.getString(userName, "");
    }

    public void setUserName(String userNameValue) {
        prefs.edit().putString(userName, userNameValue).apply();
    }

    public String getProfileUrl() {
        return prefs.getString(profile_image, profileImage);
    }

    public void setProfileUrl(String profileImage) {
        prefs.edit().putString(profile_image, profileImage).apply();
    }

    public String getPhoneNumber() {
        return prefs.getString(phoneNumber, "");
    }

    public void setPhoneNumber(String phoneNumberValue) {
        prefs.edit().putString(phoneNumber, phoneNumberValue).apply();
    }

    public String getDOB() {
        return prefs.getString(dob, "");
    }

    public void setDOB(String dobValue) {
        prefs.edit().putString(dob, dobValue).apply();
    }

    public String getGender() {
        return prefs.getString(gender, "");
    }

    public void setGender(String genderValue) {
        prefs.edit().putString(gender, genderValue).apply();
    }

    public String getCity() {
        return prefs.getString(city, "");
    }

    public void setCity(String cityValue) {
        prefs.edit().putString(city, cityValue).apply();
    }

    public boolean getIsLoggedIn() {
        return prefs.getBoolean(loggedIn, false);
    }

    public void setIsLoggedIn(boolean loggedInValue) {
        prefs.edit().putBoolean(loggedIn, loggedInValue).apply();
    }

    public String getAuthToken() {
        Log.e("GET_AUTH_TOKEN:>", prefs.getString(authToken, ""));
        return prefs.getString(authToken, "");
    }

    public void setAuthToken(String auth) {
        Log.e("AUTH_TOKEN:>", auth);
        prefs.edit().putString(authToken, auth).apply();
    }

    public String getUserRole() {
        return prefs.getString(userRole, "");
    }

    public void setUserRole(String userRole) {
        prefs.edit().putString(this.userRole, userRole).apply();
    }

    public String getClientId() {
        return prefs.getString(clientId, "");
    }

    public void setClientId(String cId) {
        prefs.edit().putString(clientId, cId).apply();
    }

    public void setParentId(String parentId) {
        prefs.edit().putString(parntId, parentId).apply();
    }

    public String getParentIdValue() {
        return prefs.getString(parntId, "");
    }

    public void setClientName(String client_Name) {
        prefs.edit().putString(clientName, client_Name).apply();
    }

    public String getCountryCode() {
        return prefs.getString(countryCode, "");
    }

    public void setCountryCode(String countryCode) {
        prefs.edit().putString(countryCode, countryCode).apply();
    }

    public String getClientNameValue() {
        return prefs.getString(clientName, "");
    }

    public String getAlertGuardsButton() {
        return prefs.getString(alertGuardsButton, "");
    }

    public void setAlertGuardsButton(String alertGuarde) {
        prefs.edit().putString(alertGuardsButton, alertGuarde).apply();
    }

    public void setAlertStaffButton(String alertStaff) {
        prefs.edit().putString(alertStaffButton, alertStaff).apply();
    }

    public String getAlertStaffButtonValue() {
        return prefs.getString(alertStaffButton, "");
    }

    public String getSettings() {
        return prefs.getString(setting, "");
    }

    public void setSettings(String settings) {
        prefs.edit().putString(settings, settings).apply();
    }

    public void setAlertAdmin(String alertAdmin) {
        prefs.edit().putString(alertAdminButton, alertAdmin).apply();
    }

    public String getAlertAdminValue() {
        return prefs.getString(alertAdminButton, "");
    }

    public void setGuardAndAdmin(String alertGuard) {
        prefs.edit().putString(alertGuardAndAdminButton, alertGuard).apply();
    }

    public void setSoftLockDown(String softLockDown) {
        prefs.edit().putString(alertSoftLockdownButton, softLockDown).apply();
    }

    public String getAlertSoftLockdownButton() {
        return prefs.getString(alertSoftLockdownButton, "");
    }

    public String getHardLockDown() {
        return prefs.getString(alertHardLockdownButton, "");
    }

    public void setHardLockDown(String hardLockDown) {
        prefs.edit().putString(alertHardLockdownButton, hardLockDown).apply();
    }

    public String getGuardAndAdminValue() {
        return prefs.getString(alertGuardAndAdminButton, "");
    }

    public void setLockdown(String lockdown) {
        prefs.edit().putString(alertLockdownButton, lockdown).apply();
    }

    public String getLockdownValue() {
        return prefs.getString(alertLockdownButton, "");
    }

    public void setalertPoliceEmergency(String alertPoliceEmergency) {
        prefs.edit().putString(alertPoliceEmergencyButton, alertPoliceEmergency).apply();
    }

    public String getalertPoliceEmergencyValue() {
        return prefs.getString(alertPoliceEmergencyButton, "");
    }

    public void setAlertPoliceNonEmergencyButton(String policeNonEmergency) {
        prefs.edit().putString(alertPoliceNonEmergencyButton, policeNonEmergency).apply();
    }

    public String getAlertPoliceNonEmergencyButtonValue() {
        return prefs.getString(alertPoliceNonEmergencyButton, "");
    }

    public void setManageSelfSignupRequests(String param) {
        prefs.edit().putString(manageSelfSignupRequests, param).apply();
    }

    public String getManageSelfSignupRequestsValue() {
        return prefs.getString(manageSelfSignupRequests, "");
    }

    public void setClearAlerts(String param) {
        prefs.edit().putString(clearAlerts, param).apply();
    }

    public String getClearAlertsValue() {
        return prefs.getString(clearAlerts, "");
    }

    public void setManageBadges(String param) {
        prefs.edit().putString(manageBadges, param).apply();
    }

    public String getManageBadgesValue() {
        return prefs.getString(manageBadges, "");
    }

    public void setManageEvent(String manageEvt) {
        prefs.edit().putString(manageEvent, manageEvt).apply();
    }

    public String getManageEventValue() {
        return prefs.getString(manageEvent, "");
    }

    public void setManageGuards(String manageEvt) {
        prefs.edit().putString(manageGuards, manageEvt).apply();
    }

    public String getManageGuardsValue() {
        return prefs.getString(manageGuards, "");
    }

    public void setCheckInVisitors(String checkInVisitor) {
        prefs.edit().putString(checkInVisitors, checkInVisitor).apply();
    }

    public String getCheckInVisitorsValue() {
        return prefs.getString(checkInVisitors, "");
    }

    public void setCheckOutVisitors(String param) {
        prefs.edit().putString(checkOutVisitors, param).apply();
    }

    public String getCheckOutVisitorsValue() {
        return prefs.getString(checkOutVisitors, "");
    }

    public void setCreateQuickBadge(String param) {
        prefs.edit().putString(createQuickBadge, param).apply();
    }

    public String getCreateQuickBadgeValue() {
        return prefs.getString(createQuickBadge, "");
    }

    public String getCreateVipList() {
        return prefs.getString(createVipList, "");
    }

    public void setCreateVipList(String param) {
        prefs.edit().putString(createVipList, param).apply();
    }

    public String getCreateWatchList() {
        return prefs.getString(createWatchList, "");
    }

    public void setCreateWatchList(String param) {
        prefs.edit().putString(createWatchList, param).apply();
    }

    public String getSexOffenderCheck() {
        return prefs.getString(sexOffenderCheck, "");
    }

    public void setSexOffenderCheck(String param) {
        prefs.edit().putString(createWatchList, param).apply();
    }

    public String getManageTeachers() {
        return prefs.getString(manageTeachers, "");
    }

    public void setManageTeachers(String param) {
        prefs.edit().putString(manageTeachers, param).apply();
    }

    public void setManageStaff(String manageStaf) {
        prefs.edit().putString(manageStaff, manageStaf).apply();
    }

    public String getManageStaffValue() {
        return prefs.getString(manageStaff, "");
    }

    public void setManageVisitors(String param) {
        prefs.edit().putString(manageVisitors, param).apply();
    }

    public String getManageVisitorsValue() {
        return prefs.getString(manageVisitors, "");
    }

    public void setManageVideos(String param) {
        prefs.edit().putString(manageVideos, param).apply();
    }

    public String getManageVideosValue() {
        return prefs.getString(manageVideos, "");
    }

    public void setEventCbcCheck(String param) {
        prefs.edit().putString(eventCbcCheck, param).apply();
    }

    public String getEventCbcCheckValue() {
        return prefs.getString(eventCbcCheck, "");
    }

    public String getManageStudents() {
        return prefs.getString(manageStudents, "");
    }

    public void setManageStudents(String param) {
        prefs.edit().putString(manageStudents, param).apply();
    }

    public String getManageParents() {
        return prefs.getString(manageParents, "");
    }

    public void setManageParents(String param) {
        prefs.edit().putString(manageParents, param).apply();
    }

    public String getEventSexOffenderCheck() {
        return prefs.getString(eventSexOffenderCheck, "");
    }

    public void setEventSexOffenderCheck(String param) {
        prefs.edit().putString(eventSexOffenderCheck, param).apply();
    }

    public String getCriminalBackgroundCheck() {
        return prefs.getString(criminalBackgroundCheck, "");
    }

    public void setCriminalBackgroundCheck(String param) {
        prefs.edit().putString(criminalBackgroundCheck, param).apply();
    }

    public String getAllowCallPolice() {
        return prefs.getString(allowCallPolice, "");
    }

    public void setAllowCallPolice(String param) {
        prefs.edit().putString(allowCallPolice, param).apply();
    }

    public String getEditPoliceContactNumbers() {
        return prefs.getString(editPoliceContactNumbers, "");
    }

    public void setEditPoliceContactNumbers(String param) {
        prefs.edit().putString(editPoliceContactNumbers, param).apply();
    }

    public void setTimeZonesValue(String timeZones) {
        prefs.edit().putString(timesZonesArray, timeZones).apply();
    }

    public String getTimesZonesArrayValues() {
        return prefs.getString(timesZonesArray, "");
    }

    public Boolean getUserLoggedIn() {
        return getmPrefs().getBoolean(Constants.SHARED_PREF_NAME, false);
    }

    public void setUserLoggedIn(Boolean logged) {
        getmPrefs().edit().putBoolean(Constants.SHARED_PREF_NAME, true).apply();
    }

    public void clearPreferences() {
        mPrefs.edit().clear().apply();
        mPrefs.edit().clear().commit();
    }

    public String localToGMT(String dateSend) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

            Date date = format.parse(dateSend);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date gmt = new Date(sdf.format(date));
            String outputPattern = "yyyy-MM-dd kk:mm:ss";
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            timeSt = outputFormat.format(gmt.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateSend;
    }

    public void logoutCall() {
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().logoutUser(APIServerResponse.LOGOUT, getAuthToken(), getUserID(), BaseActivity.this);
            } else {
                showSnack(Constants.INTERNET_CONNECTION);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            JsonObject jsonObject1 = (JsonObject) response.body();
            if (jsonObject1.get("status").getAsString().equalsIgnoreCase("Ok")) {
                Toast.makeText(getApplicationContext(), jsonObject1.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                setUserLoggedIn(false);
                clearPreferences();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        alertWithSingleTitle("Message", "Error in getting response from Server.Please try again.", "error");
    }

    public void noInternetMessage() {
        showSnack(Constants.INTERNET_CONNECTION);
    }

    public void alertWithSingleTitle(String title, String message, String error) {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(BaseActivity.this);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    public void showAlertDialog(Context context, String title, String message, String positive, String negative, DialogInterface.OnClickListener listener, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onClick(dialog, which);
            }
        });

        builder.setNegativeButton(negative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onClickListener.onClick(dialog, which);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public List<SettingsModel> getSettingList() {
        String stng = getSettings();
        Gson gson = new Gson();
        return (List<SettingsModel>) gson.fromJson(stng, new TypeToken<List<SettingsModel>>() {
        }.getType());
    }

    public boolean showAlertIcon() {
        if (getAlertScreen().equalsIgnoreCase("1"))
            return true;
        return false;
    }
}
