package com.silvershield.utils;

import com.silvershield.networkManager.AllUrl;

/**
 * Created by gagandeep.bhutani on 3/15/2018.
 */

public class Config {
    private static MODE mode = MODE.STG;

    public static String getBaseUrl() {
        String url = AllUrl.STG_BASE_URL;
        switch (mode) {
            case STG: {
                url = AllUrl.STG_BASE_URL;
                break;
            }
            case LIVE: {
                url = AllUrl.LIVE_BASE_URL;
                break;
            }
        }
        return url;
    }

    public static String getBadgeUrl() {
        String url = AllUrl.STG_BASE_URL;
        switch (mode) {
            case STG: {
                url = AllUrl.STG_BASE_URL;
                break;
            }
            case LIVE: {
                url = AllUrl.LIVE_BASE_URL;
                break;
            }
        }
        return url;
    }

    enum MODE {
        STG, LIVE
    }
}
