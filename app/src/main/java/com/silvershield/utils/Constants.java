package com.silvershield.utils;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class Constants {

    public static final String ALERT_GUARD = "Alert Guards";
    public static final String ALERT_STAFF = "Alert Staff";
    public static final String ALERT_ADMIN = "Alert Admin";
    public static final String ALERT_GUARD_ADMIN = "Alert Guards and Admin";
    public static final String HARD_LOCKDOWN_DRILL = "Hard Lockdown DRILL";
    public static final String ALERT_SOFT_LOCKDOWN = "Soft Lockdown\n(outside threat)";
    public static final String ALERT_HARD_LOCKDOWN = "Hard Lockdown\n(inside threat)";
    public static final String CONTACT_POLICE_NON_EMERGENCY= "CONTACT POLICE NON EMERGENCY ALERT(Need Some Assistance)";
    public static final String CONTACT_POLICE_EMERGENCY= "CONTACT POLICE EMERGENCY LOCKDOWN ALERT(Active Killer)";

    public static final String MANAGE_VISITORS = "Visitors";
    public static final String MANAGE_STAFF = "Staff";
    public static final String WATCH_VIP = "Watch/VIP list";
    public static final String EVENTS = "Events";
    public static final String SETTINGS = "Settings";



    public static final String FIRST_NAMENOT_BLANK = "First name should not be blank.";
    public static final String FIRST_NAME_START = "Name should not starts with numeric character.";
    public static final String EMAIL_START = "Email should not starts with numeric character.";
    public static final String LAST_NAMENOT_BLANK = "Last name should not be blank.";
    public static final String COUNTRY_CODE_NOT_BLANK = "Country code should not be blank.";
    public static final String COUNTRY_CODE_LENGTH = "Country code should be Length greater than 3.";
    public static final String PHONE_NUMBER_NOT_BLANK = "Phone Number should not be blank.";
    public static final String FAT_PERCENTAGE = "Invalide Fat quantity";
    public static final String PHONE_NUMBER_LENGTH = "Phone number Length greater than 10.";
    public static final String EMAIL_NOT_BLANK = "Email Id should not be blank.";
    public static final String EMAIL_NOT_VALID = "Enter Valid Email Id.";
    public static final String PASSWORD_NOT_BLANK = "Password should not be blank.";
    public static final String PASSWORD_LENGTH = "Password Length should be equals to or greater than 6.";
    public static final String INTERNET_CONNECTION = "Please check your internet connection.";
    public static final String SHARED_PREF_USER_SESSION_ID = "MyFitnessUserSessionID";
    public static final String DEVICE_ID = "Device_ID";
    public static final String SHARED_PREF_NAME = "SilverShield";
    public static final String SHARED_TIMEZONES = "Timezones";
    public static final String YES_ACTION = "YES_ACTION";
    public static final String STOP_ACTION = "STOP_ACTION";
    public static final String CONTROLLER_WIFI_CONFIGURATION = "controller_wifi_configuration";
    public static final String CONTROLLER_PRINTER_CONFIGURATION = "controller_printer_configuration";
    public static final String CONTROLLER_PRINTER = "printer";
    public static final String CONTROLLER_WIFI = "wifi";
    public static final String CONTROLLER_MOBILE = "mobile";
    public static final int PRINTER_STATUS_COMPLETED = 1;
    public static final int PRINTER_STATUS_CANCELLED = 0;
    public static final int REQUEST_CODE_PRINTER = 1000;
    public static final int REQUEST_CODE_WIFI = 999;
    public static final int RESULT_CODE_PRINTER = 1001;
    public static final int RESULT_CODE_PRINTER_CONNECT_FAILED = 1002;
    public static final String CONTROLLER_PDF_FOLDER = "print_demo_folder";
    public static String SECURED_PREFERENCES = "my_fitness_app_preference";
    public static String FCM_TOKEN = "token";
    public static String ACCESS_TOKEN = "Access_token";
    public static String USER_ID = "user_id";
    public static String USER_NAME = "userName";
    public static String ALERT_ID = "alertId";

}
