package com.silvershield.utils;

import android.view.View;

/**
 * Created by satoti.garg on 12/8/2017.
 */

public interface RecyclerViewClick {
    void productClick(View v, int position);
}
